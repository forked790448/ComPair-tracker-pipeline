'''
2021-11-10  Anna Zajczyk
2022-02-04    - find hit algorithm modified
2022-10-06    - modified get_hit function in use.
              - significance calculated from /vdata/channel_cm_sub
              - map plotting oriented in accordance with XY dimensions
                of strips: Side 0 == X coord, and Side 1 == Y coord
              - added secondary XY axes to the maps with [cm] dimensions.
                Secondary axes required definition of new functions:
                  get_strip2x, get_strip2y, and their inverse get_x2strip
                  and get_y2strip. These functions are defined now in
                  ecalib_functions library

  Creates 2D map of hits in the tracker layer(s).
  Originally based on Carolyn's hit map plotting approach.
  Algorithm now modified and strip mean & std deviation is calculated
  per strip from its common mode subtracted events.

  Parameters
  ----------
  file_name: str
    File name of the hdf5 input file for which the hit maps are to be
    created. This needs to be provided with a full access path to the 
    directory in which the data file resides.
  sig_level: float
    Significance value that is used as a cut to distinguish between 
    true trk events and noise events. This is an optional parameter.
    When not provided, the significance of 3.0 is used.
  multi_flg: str
    This parameter tells the tool how to treat multi-hit events.
    This is an optional parameter. When not provided, the basic
    treatement is applied where all multi-hit events are ignored
    and only true-singles are included in the hit maps. When set 
    to 'yes', each multi-hit event is forced to be a single hit
    by choosing the mulit-event component that has the highest
    significance to represent this event.

  Returns
  -------
  Figure: 
    Figure with hit maps is created. Number of hit maps (subplots)
    in that figure corresponds to the number of layers in the 
    data file.
    The figure is saved to a PNG file. It is saved to the same
    directory where the input file lives.
  Terminal output:
    Tool generates terminal output that shows execution progress.
    Per each layer a number of all events is given and a number
    of events that went into the hit map creation. If the total
    number of events for a layer is larger than 300000, the layer
    data are sliced in 100000 evt chunks and significance calcs 
    & hit finding is performed per slice, and then all summed 
    into the hit map at the end.
    So user will see information about the size of the data slice
    and how long it took the tool to work with each slice.

'''

import os
import os.path
import sys
import h5py

import matplotlib.pyplot as plt
from numpy import where, zeros, array, transpose, argmax, \
  histogram, argwhere
from datetime import datetime

from ecalib_functions import get_hit, convert_mask, get_strip2x, \
  get_strip2y, get_x2strip, get_y2strip, get_significance


def create_hit_map(file_name, sig_level=None, multi_evt=None):
  """Creates 2D map of hits in the tracker layer(s)

  Parameters
  ----------
  file_name: str
    File name
  data_type: str
    String defining the type of the input data. The two allowed
    types are: 'multi' (.h5 input data file containing observations
    collected with Si tracker and converted to hdf5 format; multi-layer
    format) and 'single' (.h5 input data file containing observations
    collected with Si tracker and converted to hdf5 format; single-layer
    format).
  """
  dstart = datetime.utcnow()
  print('\n >>> Creating hit map for HDF5 file: ', file_name.split('/')[-1])

  h5 = h5py.File(file_name, 'r')

  fsig = file_name[:-3]+'p5.h5'
  isFile = os.path.isfile(fsig)
  if (isFile):
    h5sig = h5py.File(fsig, 'r')

  print('\n %%% Hit analysis performed on common mode subtracted data')
  if (sig_level is None):
    sig_level = 3.0
  print(' %%% Hit analysis using significance level of: {:.1f}'.format(sig_level))

  plt.close('all')

  h5_group_names = array([x for x in h5])
  q = where(h5_group_names != 'vdata')[0]
  h5_group_names = h5_group_names[q]
  print('\n >>> Number of tracker layers: ', len(h5_group_names))

  FONT_SIZE = 7
  plt.rc('font', size=FONT_SIZE)          # controls default text sizes
  plt.rc('axes', titlesize=FONT_SIZE)     # fontsize of the axes title
  plt.rc('axes', labelsize=FONT_SIZE)     # fontsize of the x and y labels
  plt.rc('xtick', labelsize=FONT_SIZE)    # fontsize of the tick labels
  plt.rc('ytick', labelsize=FONT_SIZE)    # fontsize of the tick labels
  plt.rc('legend', fontsize=FONT_SIZE)    # legend fontsize
  plt.rc('figure', titlesize=FONT_SIZE+2)

  plt.ion()

  if (len(h5_group_names) == 1):
    plt_num = 111
    fig = plt.figure(1, figsize=(6, 6))   # first is width, second is height dim.
  elif (len(h5_group_names) == 2):
    plt_num = 121
    fig = plt.figure(1, figsize=(10, 6))
  elif (len(h5_group_names) == 3):
    plt_num = 131
    fig = plt.figure(1, figsize=(10, 6))   # first is width, second is height dim.
  elif (len(h5_group_names) > 3) & (len(h5_group_names) <= 6):
    plt_num = 231
    fig = plt.figure(1, figsize=(10, 8))   # first is width, second is height dim.
  elif (len(h5_group_names) > 6) & (len(h5_group_names) <= 8):
    plt_num = 241
    fig = plt.figure(1, figsize=(12, 8))   # first is width, second is height dim.
  elif (len(h5_group_names) > 8):
    plt_num = 251
    fig = plt.figure(1, figsize=(14, 8))   # first is width, second is height dim.


  plt.clf()
  plt.suptitle(file_name.split('/')[-1])

  for detector_name in h5_group_names:
    detector_number = int(detector_name[-2:])

    trk_data = h5['/' + detector_name + '/vdata/channel_cm_sub']
    nevent = trk_data.shape[0]

    if (isFile):
      print(' >>> Reading significance values from: ' + fsig.split('/')[-1])
      sig_data = h5sig['/' + detector_name + '/vdata/channel_sig']

    mask = convert_mask(h5, detector_name)

    print('\n --- ' + detector_name)
    print(' >>>    Total number of events: ', nevent)
    print(' >>>    Ignoring sync_index info...')

    hit_map = zeros((192, 192), dtype=int)

    slice_size = 100000
    if (nevent > 3*slice_size):
      nslice = nevent // slice_size
      nrest = nevent % slice_size
      print('\n >>> Number of {:d} event slices = {:d} (with rest {:d})'.format(slice_size, nslice, nrest))

      if (isFile):
        print('\n >>> Reading in significance slices...')
      else:
        print('\n >>> Calculating significance slices...')
      print('     --- Slice.No   Evt_start   Evt_end   (exec.time) ---')
      for m in range(nslice):
        tmp_s = datetime.utcnow()
        if (m == 0):
          i_st = 0
          i_ed = (m+1)*slice_size
        elif (m == (nslice-1)):
          i_st = m*slice_size
          i_ed = (m+1)*slice_size+nrest
        else:
          i_st = m*slice_size
          i_ed = (m+1)*slice_size

        if (isFile):
          tmp_sig = array(sig_data[i_st:i_ed, :, :])
        else:
          data_arr = array(trk_data[i_st:i_ed, :, :])
          tmp_sig = get_significance(data_arr, mask)
        tmp_sig[(tmp_sig < sig_level)] = 0

        nevent_slice = tmp_sig.shape[0]
        for i in range(nevent_slice):
          trk_hit = get_hit(tmp_sig, i)
          xpos = where(trk_hit[0, :] == True)[0]
          ypos = where(trk_hit[1, :] == True)[0]
          if (len(xpos) == 1) & (len(ypos) == 1):
            hit_map[xpos, ypos] = hit_map[xpos, ypos] + 1
          if (multi_evt == 'yes'):
            # --- Force multi-hit events to be single hits
            if (len(xpos) > 1) & (len(ypos) == 1):
              stmp_sig = tmp_sig[ i, 0, xpos]
              ix = argmax(stmp_sig)
              hit_map[xpos[ix], ypos] = hit_map[xpos[ix], ypos] + 1
            elif (len(xpos) == 1) & (len(ypos) > 1):
              stmp_sig = tmp_sig[ i, 1, ypos]
              ix = argmax(stmp_sig)
              hit_map[xpos, ypos[ix]] = hit_map[xpos, ypos[ix]] + 1
            elif (len(xpos) > 1) & (len(ypos) > 1):
              stmp_sig1 = tmp_sig[ i, 0, xpos]
              ix1 = argmax(stmp_sig1)
              stmp_sig2 = tmp_sig[ i, 1, ypos]
              ix2 = argmax(stmp_sig2)
              hit_map[xpos[ix1], ypos[ix2]] = hit_map[xpos[ix1], ypos[ix2]] + 1

        tmp_e = datetime.utcnow()
        print('           {:d}   {:d}   {:d}   ({:.2f} s) '.format(m, i_st, i_ed, (tmp_e - tmp_s).total_seconds()))
    else:
      trk_data = array(trk_data)
      if (isFile):
        sig_array = array(h5sig['/' + detector_name + '/vdata/channel_sig'])
      else:
        sig_array = get_significance(trk_data, mask)
      sig_array[(sig_array < sig_level)] = 0
      print(' ... finding hits starts here')
      for i in range(nevent):
        trk_hit = get_hit(sig_array, i)
        xpos = where(trk_hit[0, :] == True)[0]
        ypos = where(trk_hit[1, :] == True)[0]
        if (len(xpos) == 1) & (len(ypos) == 1):
          hit_map[xpos, ypos] = hit_map[xpos, ypos] + 1
        elif (len(xpos) > 1) & (len(ypos) == 1):
          tmp_sig = sig_array[ i, 0, xpos]
          ix = argmax(tmp_sig)
          hit_map[xpos[ix], ypos] = hit_map[xpos[ix], ypos] + 1
        elif (len(xpos) == 1) & (len(ypos) > 1):
          tmp_sig = sig_array[ i, 1, ypos]
          ix = argmax(tmp_sig)
          hit_map[xpos, ypos[ix]] = hit_map[xpos, ypos[ix]] + 1
        elif (len(xpos) > 1) & (len(ypos) > 1):
          tmp_sig1 = sig_array[ i, 0, xpos]
          ix1 = argmax(tmp_sig1)

          tmp_sig2 = sig_array[ i, 1, ypos]
          ix2 = argmax(tmp_sig2)
          hit_map[xpos[ix1], ypos[ix2]] = hit_map[xpos[ix1], ypos[ix2]] + 1

    nevt_map = sum(sum(hit_map))
    nevt_map_percent = nevt_map / nevent * 100.0
    print('\n >>>    Number of events in the hit map: {:d} ({:.1f} %)'.format(\
      nevt_map, nevt_map_percent))



    ax = plt.subplot(plt_num)
    plt.title('Layer '+str(detector_number) + \
      '\n n_hit / n_event : ' + str(nevt_map) + ' / ' + \
      str(nevent) + ' ({:.1f}%)'.format(nevt_map_percent) )

    # --- now after checking XY orientation of the strips
    plt.xlabel('Side 0 strip')
    plt.imshow(transpose(hit_map), cmap='hot', interpolation='nearest', \
      aspect='equal', origin='upper')
    if ((plt_num == 231) | (plt_num == 234) | (plt_num == 131) | (plt_num == 111) | \
      (plt_num == 241) | (plt_num == 245) | \
      (plt_num == 251) | (plt_num == 256)):
      plt.ylabel('Side 1 strip')
    plt.colorbar(shrink=0.8, aspect=50, pad=0.14)
    hist, edges = histogram(hit_map, bins=100)
    xhist = 0.5*(edges[0:-1] + edges[1:])
    qh = argwhere(hist > 50)
    hicut = max(xhist[qh])
    plt.clim(0, hicut)
    secax = ax.secondary_xaxis('top', functions=(get_strip2x, get_x2strip))
    secay = ax.secondary_yaxis('right', functions=(get_strip2y, get_y2strip))
    plt.show()

    plt_num = plt_num + 1

  data_dir = file_name[:-len(file_name.split('/')[-1])]
  plt.savefig(data_dir + 'hit_map_' + file_name.split('/')[-1][:-3] + '.png')

  dend = datetime.utcnow()
  exec_time = (dend - dstart).total_seconds()
  if (exec_time < 120.0):
    print('\n --- Script execution time [sec] = {:.2f}'.format(exec_time))
  else:
    print('\n --- Script execution time [min] = {:.2f}'.format(exec_time/60.0))

if __name__ == "__main__":
  if (len(sys.argv) == 2):
    file_name = str(sys.argv[1]).rstrip()
    create_hit_map(file_name)
  elif (len(sys.argv) == 3):
    file_name = str(sys.argv[1]).rstrip()
    parm2 = str(sys.argv[2]).rstrip()
    if (parm2 == 'yes'):
      multi_evt = parm2
      create_hit_map(file_name, multi_evt=multi_evt)
    else:
      sig_level = float(parm2)
      create_hit_map(file_name, sig_level=sig_level)
  elif (len(sys.argv) == 4):
    file_name = str(sys.argv[1]).rstrip()
    sig_level = float(str(sys.argv[2]).rstrip())
    multi_evt = str(sys.argv[3]).rstrip()
    create_hit_map(file_name, sig_level=sig_level, multi_evt=multi_evt)
  else:
    print('\n *** Use error: incorrect number of arguments')
    print('\n     Use examples:')
    print('       plot_hit_map.py <file_name>')
    print('       plot_hit_map.py <file_name> <sig_level>')
    print('       plot_hit_map.py <file_name> <multi_flg>')
    print('       plot_hit_map.py <file_name> <sig_level> <multi_flg>')
