'''
2021-08-12  Anna Zajczyk

  Applies energy and position calibration to the Si tracker layers based
  on energy calibration tables and geometric position of strip & layers'
  centers.

    Energy calibration form: E = a1coef * PH + a2coef
    FitFlag values:
          -2 - no em lines / no data
          -1 - fit failed
           1 - > 2em lines
           2 - = 2 em lines
           3 - only 1 em line

  Updates
  -------
  2022-12-01   Reoved unsed functions

'''
import sys
import os
import os.path
import h5py

from datetime import datetime
from shutil import copy

from ecalib_functions import apply_calib2data

def apply_calib(file_name, ecalib_name, sig_level=None):
  """Applies energy and position calibration to the data and creates
  copy of a data file that includes calibrated data.

  Parameters
  ----------
  file_name: str
    Name of the data to be calibrated.
  ecalib_name: str
    Name of the file containing energy calibration solutions.
  """
  dstart = datetime.utcnow()

  if (sig_level is None):
    sig_level = 3.0

  file_out = file_name[:-5]+'L2.h5'

  isFile = os.path.isfile(file_out)
  if (isFile):
    print('\n >>> File containing calibrated data exists.')
    recalib = input(' >>> Redo the calibrations (yes / no)? ')
    if((recalib == 'yes') | (recalib == '1')):
      os.remove(file_out)

      copy(file_name, file_out)   # --- copy file content
      h5 = h5py.File(file_out, 'a')   # allow to append to file
      apply_calib2data(h5, ecalib_name, sig_level)

  else:
    print('\n >>> No file containing calibrated data found.')
    print(' >>> Will apply calibrations to the data.')

    copy(file_name, file_out)   # --- copy file content
    h5 = h5py.File(file_out, 'a')   # allow to append to file

    apply_calib2data(h5, ecalib_name, sig_level)

  dend = datetime.utcnow()
  print('\n --- Script execution time [sec] = {:.5f}'.format((\
    dend - dstart).total_seconds()))

if __name__ == "__main__":
  if (len(sys.argv) == 3):
    file_name = str(sys.argv[1]).rstrip()
    ecalib_name = str(sys.argv[2]).rstrip()
    apply_calib(file_name, ecalib_name)
  elif (len(sys.argv) == 4):
    file_name = str(sys.argv[1]).rstrip()
    ecalib_name = str(sys.argv[2]).rstrip()
    sig_level = float(sys.argv[3]).rstrip()
    apply_calib(file_name, ecalib_name, sig_level=sig_level)
  else:
    print('\n *** Use error: incorrect number of arguments')
    print('\n     Use examples:')
    print('       apply_calib.py <file_name> <ecalib_name>')
    print('       apply_calib.py <file_name> <ecalib_name> <sig_level>')
