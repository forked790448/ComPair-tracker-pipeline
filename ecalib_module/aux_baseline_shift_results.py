'''
2023-03-14  Anna Zajczyk

  This tool plots calculated baseline shifts from aux_baseline_shift_calc.py tool.


  Parameters
  ----------
  data_dir: str
    Full access path to the directory where the input HDF5 baseline shift data file resides.
    Remember to include the forward slash at the end of the access path.
  file_name: str
    Name of the input baseline shift data.

  Returns
  -------
  plot
    Plot of 3 x 3 subplots, each displaying baseline shift value as a function of strip number.
    Tracker side is color-coded: blue for side 0, and orange for side 1.

  Updates
  -------
  YYYY-MM-DD
'''

import h5py

import matplotlib.pyplot as plt
from numpy import *

from ecalib_functions import fgauss

data_dir = '/Users/azajczyk/work/AMEGO/Si_tracker/data/hdf5/LaboratoryDataFiles/multi_layer/20230303/'
file_name = 'baseline_shift_numbers_2023-03-03_1003_9Layers_Co57_L2.h5'


h5 = h5py.File(data_dir+file_name, 'r')
group_names = array([x for x in h5])
q = where(group_names != 'vdata')[0]
group_names = group_names[q]
nlayer = len(group_names)

print('\n >>> Number of tracker layers in the file: {:d}'.format(nlayer))
for i in group_names:
  print('       ' + i)

xr = arange(0, 192, 1)

FONT_SIZE = 8
plt.rc('font', size=FONT_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=FONT_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=FONT_SIZE)     # fontsize of the x and y labels
plt.rc('xtick', labelsize=FONT_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=FONT_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=FONT_SIZE)    # legend fontsize
plt.rc('figure', titlesize=FONT_SIZE+2)

plt.ion()
plt.figure(1, figsize=(14, 10))
plt.clf()
for detector_name in group_names:
  detector_number = int(detector_name[-2:])
  layer_data = array(h5['/' + detector_name + '/baseline_shift'][:, :])
  plt.subplot(3, 3, detector_number + 1)
  plt.title(detector_name)
  if ((detector_number + 1) == 7) | ((detector_number + 1) == 8) | ((detector_number + 1) == 9):
    plt.xlabel('Strip number')
  if ((detector_number + 1) == 1) | ((detector_number + 1) == 4) | ((detector_number + 1) == 7):
    plt.ylabel('Baseline shift [ADU]')
  plt.plot(xr, layer_data[0, :], '.', color='blue', label='side 0')
  plt.plot(xr, layer_data[1, :], '.', color='orange', label='side 1')
  plt.legend()
  plt.show()