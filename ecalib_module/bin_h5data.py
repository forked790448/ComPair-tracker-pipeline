'''
2022-08-06  Anna Zajczyk

  Reads in data for a set of hdf5 listed in the input file <list_name>.
  First line of this file should be a full access path to where the hdf5
  reside. The consecutive lines should contain names of the hdf5 files
  to be analyzed.

  <list_name> - the input list name with a full access path
  <output_file> - the name of the output file that will contain the binned data.
                This should be just the name of the file without full access
                path. As a default the directory to which this output file is
                saved is the directory where the data provided in the input
                list reside. This is an optional argument. If not provided
                the output name is set to:
                  binned_data_YYYYMMDD.h5
                where YYYYMMDD is the date the file was created
  <dataset_flg> - this tool was originally made to work with L1 level data.
                But by setting this argument to 'L1p5' the tool is instructed
                that the input data are the L1p5 auxilary file type. When
                using that option, the <output_file> argument needs to be
                given.

  Tool reads data in each of the input hdf5 files and then creates binned
  spectra for each detector / layer side / strip, and then for each
  detector / layer side / strip sums all the created spectra.

  The results are saved to an output hdf5 file <file_out>. The structure
  of the output hdf5 is the following:

      GROUP "/"
        DATASET "edges"
          DATATYPE  H5T_IEEE_F64LE
          DATASHAPE (1024, 3)

      GROUP "layerXX"
        DATASET "binned_data"
           DATATYPE  H5T_STD_I64LE
           DATASHAPE (2, 192, 1024)

        DATASET "chan_disable"
           DATATYPE  H5T_STD_I64LE
           DATASHAPE (2, 192)

  where XX in "layerXX" designates detector number.

  - edges dataset contains information on bins in the array of shape
    (n_bin, 3), where centers are in 0'th column, lower edges of bins
    are in 1'st column, and upper edges of bins are in 2'nd column
  - binned_data dataset contains binned spectra in the array of shape
    (n_side, n_strip, n_bin)
  - channel_disable contains information on ASIC channel masking in
    the side / strip domain rather than in the asic / asic_channel
    domain as in the original hdf5 files

  Hdf5 created by this tool can be analysed using:
    fit_emline_user.py
    fit_emline_auto.py
    inspectres_user.py
  When using these tools set <data_type> or <input_data_type> to 'binned'.

'''

import os
import sys
import h5py

from datetime import datetime
import matplotlib.pyplot as plt
from numpy import sqrt, log, diag, exp, argmax, append, array, median, where,\
  linspace, random, zeros, arange, random, histogram

from ecalib_functions import read_data, get_channel_mask

def bin_h5data(list_name, file_out=None, dataset_flg=None):
  dstart = datetime.utcnow()

  if (len(list_name.split('/')) == 1):
    cwd = os.getcwd()
    list_name = cwd + '/' + list_name

  # --- OUTPUT: data will be saved to hdf5, prepare output info here
  if (file_out is None):
    file_out = 'binned_data_' + dstart.strftime('%Y%m%d') + '.h5'
  else:
    # --- make sure that the output file has the right extension
    if (file_out.split('.')[-1] != 'h5'):
      file_out = file_out.split('.')[0] + '.h5'

  # --- INPUT: reading in the input data
  input_list, lrow, lcol = read_data(list_name, data_type='list')
  data_dir = input_list[0]
  if (data_dir[-1] != '/'):
    data_dir = data_dir+'/'
  input_list =input_list[1:]
  nfiles = len(input_list)

  # --- Prepare COMMON BIS to be used for all input data
  bin_min = 0
  bin_max = 1024
  binw = 1

  bins = arange(bin_min, bin_max, binw)
  nbins = len(bins)

  edge_low = bins - 0.5*binw
  edge_high = bins + 0.5*binw
  edges = append(edge_low, [edge_high[-1]])

  # --- Start working with the actual data
  data_hist = zeros((10, 2, 192, nbins))

  print(' --- working in the directory: ', data_dir)
  if (dataset_flg is None):
    dataset_name = 'channel_cm_sub'
  elif (dataset_flg == 'L1p5'):
    dataset_name = 'channel_energy'

  for i in range(nfiles):
    print(' --- working on data in file: ', input_list[i])
    h5 = h5py.File(data_dir + input_list[i], 'r')
    if (dataset_flg is None):
      channel_mask = get_channel_mask(h5)
    elif (dataset_flg == 'L1p5'):
      h5mask = h5py.File(data_dir + input_list[i][:-5] + '.h5', 'r')
      channel_mask = get_channel_mask(h5mask)

    group_names = array([x for x in h5])
    q = where(group_names != 'vdata')[0]
    group_names = group_names[q]
    nlayer = len(group_names)

    flg = []
    for i in group_names:
      isCM = ('/' + i + '/vdata/' + dataset_name in h5)
      flg = append(flg, isCM)
    q = where(flg == 0)[0]

    if (len(q) == 0):
      print(' --- binning data in vdata/' + dataset_name)
      for detname in group_names:
        key = '/' + detname + '/vdata/' + dataset_name
        detnum = int(detname[-2:])

        for layer_side in range(2):
          for strip_number in range(192):
            print(' ...    detnum / side / strip = {:d} / {:d} / {:d}'.format( \
              detnum, layer_side, strip_number))
            single_channel_mask = channel_mask[detname][layer_side, strip_number]

            if (single_channel_mask == 0):
              strip_events = array(h5[key][:, layer_side, strip_number])
              strip_hist, edges_tmp = histogram(strip_events, bins=edges)

              data_hist[detnum, layer_side, strip_number, :] = \
                data_hist[detnum, layer_side, strip_number, :] + strip_hist[:]

  # --- OUTPUT: save results including bin parameters and masked channel info
  isFile = os.path.isfile(data_dir + file_out)
  if (isFile):
    print('\n --- Output file containing binned data exists and will be overwritten.')
    os.remove(data_dir + file_out)
  h5out = h5py.File(data_dir + file_out, 'a')

  for detname in group_names:
    detnum = int(detname[-2:])

    binned_data = \
      h5out.create_dataset('/' + detname + '/binned_data', data_hist[detnum, ].shape, dtype='int')
    binned_data[:] = data_hist[detnum, :]

    mask_data = \
      h5out.create_dataset('/' + detname + '/chan_disable', channel_mask[detname].shape, dtype='int')
    mask_data[:] = channel_mask[detname]

  bin_values = h5out.create_dataset('/edges', (nbins, 3), dtype='float')
  bin_values[:, 0] = bins
  bin_values[:, 1] = edge_low
  bin_values[:, 2] = edge_high
  h5out.close()

  dend = datetime.utcnow()
  total_time = (dend - dstart).total_seconds()
  if (total_time > 180):
    print('\n --- Execution time [min] = {:.5f}'.format(total_time/60.))
  else:
    print('\n --- Execution time [sec] = {:.5f}'.format(total_time))


if __name__ == "__main__":
  if (len(sys.argv) == 2):
    list_name = str(sys.argv[1]).rstrip()
    bin_h5data(list_name)
  elif (len(sys.argv) == 3):
    list_name = str(sys.argv[1]).rstrip()
    parm2 = str(sys.argv[2]).rstrip()
    if (parm2 == 'L1p5'):
      bin_h5data(list_name, dataset_flg=parm2)
    else:
      bin_h5data(list_name, file_out=parm2)
  elif (len(sys.argv) == 4):
    list_name = str(sys.argv[1]).rstrip()
    file_out = str(sys.argv[2]).rstrip()
    dataset_flg = str(sys.argv[3]).rstrip()
    bin_h5data(list_name, file_out=file_out, dataset_flg=dataset_flg)
  else:
    print('\n *** Use error: incorrect number of arguments')
    print('\n     Use examples:')
    print('       bin_h5data.py <list_name>')
    print('       bin_h5data.py <list_name> <output_file>')
    print('       bin_h5data.py <list_name> <dataset_flg>')
    print('       bin_h5data.py <list_name> <output_file> <dataset_flg>')
