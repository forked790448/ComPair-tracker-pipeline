'''
2023-03-14  Anna Zajczyk

  This tool compares raw and common mode subtracted spectra for two input files.
  The tool was created to visualize the emergence of the secondary noise peak
  (double peak noise behavior) in 9-layer Co57 data collected on 2023-01-25.
  Subsequent Co57 run of the tracker in 9-layer setup performed on 2023-03-03 does
  not show this secondary noise peak.

  This tool plots raw and common mode subtracted data for the two Co57 input files.
  The 2x2 plot is created with left column displaying Januray dataset, and right
  column showing March data set. Top row shows raw data, while bottom shows common
  mode subtracted data.

  Noise peaks are fitted with a gaussian for both types of spectra, and baseline
  shift is calculated. The values of the peak centroid and the calculated baseline
  shift are also displayed.

  Though written to inspect two datasets of Co57 data, this tool can be used to
  compare any two data files. Just make sure that both files contain the layer data
  you want to visualize.

  User needs to choose for which layer / side / strip to create the comparison plot.

  Parameters
  ----------
  data_dir1: str
    Full access path to the directory where the input HDF5 source data file resides.
    Remember to include the forward slash at the end of the access path.
  file_name_tbl1: str
    Name of the input source data.
  data_dir2: str
    Full access path to the directory where the input HDF5 source data file resides.
    Remember to include the forward slash at the end of the access path.
  file_name_tbl2: str
    Name of the input source data.
  detector_number: int
    Number of the tracker layer for which to inspect the data. Tracker layers span
    0-9 range (assembly of the 9-th layer is in the workings at the moment).  
  layer_side: int
    Tracker side number spans 0-1 range, where 0 is top/A, and 1 is bottom/B.
  strip_number: int
    Strip number on the tracker layer / side. The strip number spans 0-191 range.


  Returns
  -------
  plot
    The 2x2 plot is created with left column displaying Januray dataset, and right
    column showing March data set. Top row shows raw data, while bottom shows common
    mode subtracted data.
    Plot is saved to the following location and file:
      data_dir2 + 'NoisePeaks_' + file_name2[:-3] + '_layerX_sideY_stripZ' + \
      '_Co57data_comparison.png'
 
   Updates
  -------
  YYYY-MM-DD
'''

import h5py

import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from numpy import *

from ecalib_functions import fgauss, plot_hist, find_element, convert_mask

def prep_data_file(file_name, detector_number, layer_side, strip_number):
  print(' --- Working with file: ' + file_name.split('/')[-1])
  h5 = h5py.File(file_name, 'r')
  group_names = array([x for x in h5])
  q = where(group_names != 'vdata')[0]
  group_names = group_names[q]
  nlayer = len(group_names)

  print('\n >>> Number of tracker layers in the file: {:d}'.format(nlayer))
  for i in group_names:
    print('       ' + i)

  detector_name = 'layer{:02d}'.format(detector_number)
  print(' --- Dealing with: ' + detector_name + ' / ' + str(layer_side) + ' / ' + str(strip_number))

  # --- Read in data for the strip
  strip_data_raw = array(h5['/' + detector_name + '/vdata/channel_data'][:, layer_side, strip_number])
  strip_data_cm = array(h5['/' + detector_name + '/vdata/channel_cm_sub'][:, layer_side, strip_number])
  mask = convert_mask(h5, detector_name)
  return strip_data_raw, strip_data_cm, mask


def deal_with_data(bins, edges, strip_data_raw, strip_data_cm, mask):
  if (mask[layer_side, strip_number] == 1):
    print(' *** Dealing with MASKED channel ***')
    mask_flg = True
  else:
    mask_flg = False

  nevent1 = strip_data_raw.shape[0]
  nevent2 = strip_data_cm.shape[0]
  print('\n --- Total number of events   RAW: {:d}   CM: {:d}'.format(nevent1, nevent2))

  pinit1 = [1e4, 200, 10]
  strip_hist_raw, edges_tmp = histogram(strip_data_raw, bins=edges)
  q = (strip_hist_raw > 0)
  p1,t1 = curve_fit(fgauss, bins[q], strip_hist_raw[q], p0=pinit1)
  xnoise = p1[1] + abs(5*p1[2])
  ixn_hi = find_element(bins, xnoise)
  xnoise = p1[1] - abs(5*p1[2])
  ixn_lo = find_element(bins, xnoise)
  noise_cnt1 = sum(strip_hist_raw[ixn_lo:ixn_hi]) * 100 / nevent1

  pinit2 = [1e4, 0, 10]
  strip_hist_cm, edges_tmp = histogram(strip_data_cm, bins=edges)
  p2,t2 = curve_fit(fgauss, bins, strip_hist_cm, p0=pinit2)
  xnoise = p2[1] + abs(5*p2[2])
  ixn_hi = find_element(bins, xnoise)
  xnoise = p2[1] - abs(5*p2[2])
  ixn_lo = find_element(bins, xnoise)
  noise_cnt2 = sum(strip_hist_cm[ixn_lo:ixn_hi]) * 100 / nevent2
  print(' --- Total number of events in noise peak   RAW: {:.1f} %    CM: {:.1f} %'.format(noise_cnt1, noise_cnt2))

  bs_shift = p1[1] - p2[1]
  print(' --- Calculated baseline shift [ADU] = {:.1f}'.format(bs_shift))
  return strip_hist_raw, strip_hist_cm, p1, p2, bs_shift, mask_flg


data_dir1 = '/Users/azajczyk/work/AMEGO/Si_tracker/data/hdf5/LaboratoryDataFiles/multi_layer/20230125/'
file_name_tbl1 = ['2023-01-25_1027_Co57_9Layers_1hr_L1.h5']

data_dir2 = '/Users/azajczyk/work/AMEGO/Si_tracker/data/hdf5/LaboratoryDataFiles/multi_layer/20230303/'
file_name_tbl2 = ['2023-03-03_1003_9Layers_Co57_L2.h5']


detector_number = 4
layer_side = 0
strip_number = 115

# --- FILE 1
file_name1 = file_name_tbl1[0]
strip_data_raw1, strip_data_cm1, mask1 = prep_data_file(data_dir1 + file_name1, detector_number, layer_side, strip_number)

# --- FILE 2
file_name2 = file_name_tbl2[0]
strip_data_raw2, strip_data_cm2, mask2 = prep_data_file(data_dir2 + file_name2, detector_number, layer_side, strip_number)

# --- Prepare COMMON BIS to be used for all input data
bin_min = -250
bin_max = 1024
binw = 1

bins = arange(bin_min, bin_max, binw)
nbins = len(bins)

edge_low = bins - 0.5*binw
edge_high = bins + 0.5*binw
edges = append(edge_low, [edge_high[-1]])



plt.close('all')
plt.ion()
plt.figure(1, figsize=(12,8))
plt.clf()
plt.suptitle('Layer: '+str(detector_number)+'   Side: '+str(layer_side)+\
  '   Strip: '+str(strip_number))

strip_hist_raw, strip_hist_cm, p1, p2, bs_shift, mask_flg = deal_with_data(bins, edges, strip_data_raw1, strip_data_cm1, mask1)
ymax = 2*max([max(strip_hist_raw), max(strip_hist_cm)])

plt.subplot(221)
plt.title(file_name1)
plt.yscale('log')
plt.ylim(bottom=1e-2, top=ymax)
plt.ylabel('Counts')
plot_hist(strip_hist_raw, bins, edge_low, edge_high, color='violet')
plt.plot(bins, fgauss(bins, p1[0], p1[1], p1[2]), ':', color='grey', alpha=0.8, label='gaus.fit')
plt.text(600, 1e3, 'raw data', color='blue')
plt.text(600, 1e2, 'peak: {:.1f} ADU'.format(p1[1]), color='purple')
if (mask_flg):
  plt.text(-200, 1e3, '*** masked ***', color='black')
plt.legend()

plt.subplot(223)
plt.yscale('log')
plt.ylim(bottom=1e-2, top=ymax)
plt.xlabel('Peak height [ADU]')
plt.ylabel('Counts')
plot_hist(strip_hist_cm, bins, edge_low, edge_high, color='violet')
plt.plot(bins, fgauss(bins, p2[0], p2[1], p2[2]), ':', color='grey', alpha=0.8, label='gaus.fit')
plt.text(600, 1e3, 'common mode', color='blue')
plt.text(600, 0.3e3, 'subtracted data', color='blue')
plt.text(600, 1e1, 'peak: {:.1f} ADU'.format(p2[1]), color='purple')
plt.text(600, 1e-0, 'shift: {:.1f} ADU'.format(bs_shift), color='green')
plt.legend()
plt.show()


strip_hist_raw, strip_hist_cm, p1, p2, bs_shift, mask_flg = deal_with_data(bins, edges, strip_data_raw2, strip_data_cm2, mask2)
ymax = 2*max([max(strip_hist_raw), max(strip_hist_cm)])

plt.subplot(222)
plt.title(file_name2)
plt.yscale('log')
plt.ylim(bottom=1e-2, top=ymax)
plt.ylabel('Counts')
plot_hist(strip_hist_raw, bins, edge_low, edge_high, color='violet')
plt.plot(bins, fgauss(bins, p1[0], p1[1], p1[2]), ':', color='grey', alpha=0.8, label='gaus.fit')
plt.text(600, 1e3, 'raw data', color='blue')
plt.text(600, 1e2, 'peak: {:.1f} ADU'.format(p1[1]), color='purple')
if (mask_flg):
  plt.text(-200, 1e3, '*** masked ***', color='black')
plt.legend()

plt.subplot(224)
plt.yscale('log')
plt.ylim(bottom=1e-2, top=ymax)
plt.xlabel('Peak height [ADU]')
plt.ylabel('Counts')
plot_hist(strip_hist_cm, bins, edge_low, edge_high, color='violet')
plt.plot(bins, fgauss(bins, p2[0], p2[1], p2[2]), ':', color='grey', alpha=0.8, label='gaus.fit')
plt.text(600, 1e3, 'common mode', color='blue')
plt.text(600, 0.3e3, 'subtracted data', color='blue')
plt.text(600, 1e1, 'peak: {:.1f} ADU'.format(p2[1]), color='purple')
plt.text(600, 1e-0, 'shift: {:.1f} ADU'.format(bs_shift), color='green')
plt.legend()
plt.show()

plt.savefig(data_dir2 + 'NoisePeaks_' + file_name2[:-3] + \
  '_layer{:d}_side{:d}_strip{:d}'.format(detector_number, layer_side, strip_number) + \
  '_Co57data_comparison.png')