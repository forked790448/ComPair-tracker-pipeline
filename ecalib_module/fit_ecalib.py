'''
2021-06-14    Anna Zajczyk

  Perform automated energy calibration fitting. Fit is performed for
  strips (layer / side / strip) for which good emission line data are
  found. Results of the fit are saved to a text file. Log file
  tracking the progress of the fit is also created. User can
  choose level of plotting. Plots are created in an interactive mode
  and are not saved to any output file.

  A quality flag is assigned to each calibration fit. The flag
  values are:
      -2 - no calibration lines / no data
      -1 - fit failed
       1 - >2 different calibration lines
       2 - =2 different calibration lines
       3 - only 1 calibration line

  Notes
  -----

  Updates
  -------
  2022-12-01   Cleaned old comments and removed unused functions

'''

import sys

import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from datetime import datetime
from numpy import append, array, where, sqrt, diag, arange, \
  zeros, unique

from ecalib_functions import read_data, flinear


def get_ecalib_coef(const1, const2, const1_err, const2_err):
  """Calculates energy calibration coeffcients.

  (const1, const2) are transformation coefficients from the formula
    PH = const1 * E + const2
  where E is the energy [in keV] and PH is the peak height [in ADC].
  (const1_err, const2_err) are the errors on the coefficients.
  (aecal, becal) and (aerr, berr) are the coefficients and their
  errors, respectively, calculated for an inverse transformation:
    E = aecal * PH + becal

  Parameters
  ----------
  const1: float
  const2: float
  const1_err: float
  const2_err: float

  Returns
  -------
  aecal: float
  becal: float
  aerr: float
  berr: float

  """
  aecal = 1.0/const1
  becal = -const2/const1

  aerr = abs(-1.0/const1**2.0)*const1_err
  berr = abs(-const2/const1**2.0)*const1_err + abs(-1.0/const1)*const2_err
  return aecal, becal, aerr, berr


def analyze_strip_ecalib(
    emline_data, emlines, layer_side, strip_number, detector_number,
    ecal_table, flog, plot_flg):
  """Finds transformation between peak height (in ADC) and energy
  (in keV) scales.

  Parameters
  ----------
  emline_data: array_like
    Table containing results of the emission line fitting.
  emlines:

  layer_side: int
    Intiger (0, 1) defining q top/bottom side of a Si layer.
  strip_number: int
    Integer (0, 191) defining a strip number in a selected Si layer.
  detector_number: int
    Integer (0, 9) defining a layer number in Si tracker.
  ecal_table: array_like
    Table where the results of the energy calibration fitting will be
    appended.
  flog: ?
    Access to log file, so that information on individual fits can
    be saved to the log file.
  plot_flg: int
    Flag that sets the level of plots that are produced during
    script execution.
  fgood1: bool
    Flag that when set to 'False' allows for creation of an example
    plot for a '>2' fit. Plot is created only when the plot_flg is
    set to '2'.
  fgood2: bool
    Flag that when set to 'False' allows for creation of an example
    plot for a '=2' fit. Plot is created only when the plot_flg is
    set to '2'.
  fgood3: bool
    Flag that when set to 'False' allows for creation of an example
    plot for a '=1' fit. Plot is created only when the plot_flg is
    set to '2'.

  Returns
  -------
  ecal_table: array_like
    Table whith the results of the energy calibration fitting.
  fgood1: bool
    The flag is set to 'True' once the plot is created. This way only
    one plot per case per layer per side is shown.
  fgood2: bool
    The flag is set to 'True' once the plot is created. This way only
    one plot per case per layer per side is shown.
  fgood3: bool
    The flag is set to 'True' once the plot is created. This way only
    one plot per case per layer per side is shown.
  """

  # select all events in layer / side /strip
  q = ((emline_data[:, :, 0] == detector_number) &
    (emline_data[:, :, 1] == layer_side) &
    (emline_data[:, :, 2] == strip_number))

  rflg = emline_data[q, :][:, 3]        # quality flag for em.line fit
  rpeak = emline_data[q, :][:, 4]       # peak centroid
  rpeak_err = emline_data[q, :][:, 5]   # error on peak centroid

  qflg = where(rflg == 1)   # choose only GOOD quality fits
  uene = unique(emlines[qflg[0]])   # unique calibration sources

  if (len(uene) > 2):
    try:
      pinit = [10., 1.]
      p1, t1 = curve_fit(
        flinear, emlines[qflg[0]], rpeak[qflg[0]], p0=pinit,
        sigma=rpeak_err[qflg[0]], absolute_sigma=True)
      perr1 = sqrt(diag(t1))

      tmp_ecal = get_ecalib_coef(p1[0], p1[1], perr1[0], perr1[1])
      tmp_rc = append([detector_number, layer_side, strip_number, 1],
        tmp_ecal)
      ecal_table.append(tmp_rc)
    except:
      print(' *** Warning: Fit failed for strip (layer / side / ' \
        'strip): ' + str(detector_number) + ' / '+ str(layer_side) + \
        ' / ' + str(strip_number))
      flog.write(' *** Warning: Fit failed for strip (layer / side / ' \
        'strip): ' + str(detector_number) + ' / '+ str(layer_side) + \
        ' / ' + str(strip_number) + '\n')
      tmp_rc = [detector_number, layer_side, strip_number,
        -1, -1, -1, -1, -1]
      ecal_table.append(tmp_rc)
  elif(len(uene) == 2):
    print(' *** Caution: TWO emission lines for strip (layer / side / ' \
      'strip): ' + str(detector_number) + ' / '+ str(layer_side) + \
      ' / ' + str(strip_number))
    flog.write(' *** Caution: TWO emission lines for strip (layer / side / '\
      'strip): ' + str(detector_number) + ' / '+ str(layer_side) + \
      ' / ' + str(strip_number) + '\n')
    try:
      pinit = [10., 1.]
      p1, t1 = curve_fit(
        flinear, emlines[qflg[0]], rpeak[qflg[0]], p0=pinit,
        sigma=rpeak_err[qflg[0]], absolute_sigma=True)
      perr1 = sqrt(diag(t1))
      tmp_ecal = get_ecalib_coef(p1[0], p1[1], perr1[0], perr1[1])
      tmp_rc = append(
        [detector_number, layer_side, strip_number, 2], tmp_ecal)
      ecal_table.append(tmp_rc)
    except:
      print(' *** Warning: Fit failed for strip (layer / side / ' \
        'strip): ' + str(detector_number) + ' / '+ str(layer_side) + \
        ' / ' + str(strip_number))
      flog.write(' *** Warning: Fit failed for strip (layer / side / ' \
        'strip): ' + str(detector_number) + ' / '+ str(layer_side) + \
        ' / ' + str(strip_number) + '\n')
      tmp_rc = [detector_number, layer_side, strip_number,
        -1, -1, -1, -1, -1]
      ecal_table.append(tmp_rc)
  elif (len(uene) == 1):
    print(' *** Caution: SINGLE emission line for strip (layer / side / ' \
      'strip): ' + str(detector_number) + ' / '+ str(layer_side) + \
      ' / ' + str(strip_number))
    flog.write(' *** Caution: SINGLE emission line for strip (layer / ' \
      'side / strip): ' + str(detector_number) + ' / '+ str(layer_side) + \
      ' / ' + str(strip_number) + '\n')
    weights = rpeak_err**(-2.0)   # calculate weights
    rpeak_weightavg = sum(rpeak[qflg[0]]*weights[qflg[0]]) / \
      sum(weights[qflg[0]])   # calculate weighted average
    rpeak_weightavg_err = sqrt( sum(
      weights[qflg[0]]**2.0 * rpeak_err[qflg[0]]**2.0) / \
      sum(weights[qflg[0]])**2.0)   # calculate error on weighted average

    p1 = [rpeak_weightavg/uene, 0.0]
    perr1 = [rpeak_weightavg_err/uene, 0.0]
    tmp_ecal = get_ecalib_coef(p1[0], p1[1], perr1[0], perr1[1])

    tmp_rc = append([detector_number, layer_side, strip_number, 3], tmp_ecal)
    ecal_table.append(tmp_rc)
  else:
    print(' *** Warning: no good data points for strip (layer / side / ' \
      'strip): ' + str(detector_number) + ' / '+ str(layer_side) + \
      ' / ' + str(strip_number))
    flog.write(' *** Warning: no good data points for strip (layer / ' \
      'side / strip): ' + str(detector_number) + ' / '+ str(layer_side) + \
      ' / ' + str(strip_number) + '\n')
    tmp_rc = [detector_number, layer_side, strip_number, -2, -1, -1, -1, -1]
    ecal_table.append(tmp_rc)
  return ecal_table


def fit_energy_calibration(list_name, data_dir=None, plot_flg=None):
  """Function that allows for automated energy calibration fitting.

  The script runs automated energy calibration fitting. For a given
  set of input files that contain results of the emission line
  fitting step, the scripts selects for each strip only those
  entries that are marked with '1' as a quality flag (i.e., good
  fit). Then it is determined how many unique calibration points
  (different calibration source) are there for each strip.
  Depending on the number of points the following happens:
    > 2: transformation is found through least square fitting
         and upon successful fit quality flag '1' is assigned.
    = 2: transformation is found through least square fitting
         and upon successful fit quality flag '2' is assigned.
    = 1: transformation is found by forcing it to go through
         zero-zero point and the single measurement point.
         If there are multiple measuremnts for the same calibration
         source a weighted average is calculated to represent
         the measurement point.
  In each of the above cases it is assumed that the energy
  transformation has the form of a linear function:
    E = A * PH + B,
  where E is energy [in keV], PH is measured peak height [in ADC],
  and (A, B) are the coefficients.
  The actual fitting (non-linear least squares) is performed on
  a reverse relation:
    PH = a * E + b
  where (a, b) coefficients are determined first, and then (A, B)
  coefficients are calculated and their errors propagated.
  The calculated values of (A, B) coefficients and their errors
  for each strip are saved to an output file (.dat). Also a log
  (.log) file of the stript execution is saved.

  Parameters
  ----------
  list_name: str
    Name of the file containg a list of files with the results from
    the emission line fitting step. The file structure must be such
    that the first column contains names of the files, while the
    second column contains calibration source designation (co57,
    am241 or cd109).
  data_dir: str
    Directory access path where the input file sits. The access
    path must end with a forward slash, e.g., 'dir/subdir/' .
    When no path is given, it is assumed that the input file is
    in the current directory './' .
  plot_flg: int
    Flag that sets the level of plots that are produced during
    script execution. If not provided, the plot_flg is set to 2.
    Level 0 - no plots are produced.
    Level 1 - a plot showing the energy calibration coefficients
    for all the tracker layers.
    Level 2 - plots produced in Level 1 and examples of the energy
    transformation fits. A one fit example is plotted for each side
    of each layer.

  """
  dstart = datetime.utcnow()

  if (data_dir is None):
    data_dir = './'

  if (plot_flg is None):
    plot_flg = 1

  # dictionary of calibration sources [src_name:energy_in_keV]
  calsource = {'am241':59.5409, 'cd109':88.0336, 'co57':122.06065}

  file_out = 'EnergyCalibrationCoefficients_' + \
    dstart.strftime('%Y%m%d') + '.dat'

  list_table, nrow, ncol = read_data(data_dir+list_name, data_type='list')

  if (len(list_table.shape) != 1):
    file_table = list_table[:, 0]
    emsource = list_table[:, 1]
    nfiles = len(file_table)
  else:
    file_table = [list_table[0]]
    emsource = [list_table[1]]
    nfiles = 1

  flog = open(data_dir+file_out[:-4]+'.log', 'w')
  flog.write(' --- Analysis start [UTC]: ' + \
    dstart.strftime('%Y-%m-%d %H:%M:%S\n'))

  emlines = zeros(nfiles)

  for l in range(nfiles):
    # assign energy to emission line used in each file
    emlines[l] = calsource[emsource[l]]

  emline_data = zeros((nfiles, 10*2*192, 10))
  emline_data[:] = -1
  nlayer_in = zeros(nfiles, dtype=int)
  print('')
  for i in range(nfiles):
    print(' --- Reading data file: ', data_dir+file_table[i])
    flog.write(' --- Reading data file: '+data_dir+file_table[i]+'\n')
    tmp_data, tmp_nrow, tmp_ncol = read_data(
      data_dir+file_table[i], data_type='float')

    qdet = where(tmp_data[:, 3] != 0)
    tmp_nlayer = len(unique(tmp_data[qdet, 0]))
    nlayer_in[i] = tmp_nlayer
    print(' --- Number of the tracker layers in the data file = ' + \
      str(tmp_nlayer) + ' ' + str(unique(tmp_data[qdet, 0])))
    flog.write(' --- Number of the tracker layers in the data file = ' + \
      str(tmp_nlayer) + ' ' + str(unique(tmp_data[qdet, 0])) +'\n')

    if (tmp_nrow > 10*2*192):
      print(' *** Warning: Too many strip entries in the data file')
      print(' ***          Exiting the program.')
      flog.close()
      sys.exit()

    emline_data[i, 0:tmp_nrow, :] = tmp_data
  flog.write('\n')

  qdet = where(nlayer_in != min(nlayer_in))
  if (len(qdet[0]) != 0):
    print('')
    print(' *** Warning: Different number of the tracker layers found ' + \
      'across the input data files')
    flog.write(' *** Warning: Different number of the tracker layers ' + \
      'found across the input data files\n')
    print(' ***          File#    TotalLayer#')
    flog.write(' ***          File#    TotalLayer#\n')
    for i in range(nfiles):
      print(' ***             ' + str(i) + '         ' + str(nlayer_in[i]))
      flog.write(' ***             ' + str(i) + '         ' + \
        str(nlayer_in[i]) +'\n')
    go_inpt = input(' >>> Continue the energy calibration fitting ' \
      '(yes / no)? ')
    flog.write(' >>> Continue the energy calibration fitting (yes / no)? ' + \
      go_inpt)
    if (go_inpt == 'no'):
      print(' ***          Exiting the program.')
      flog.close()
      sys.exit()

  ecal_table = []   # will store ecalib coefficients here [L#, SD#, ST#, FLG,
                    # a1, a2, a1err, a2err]

  plt.close('all')   # close any existing figures
  print('')

  for j in range(10):
    for k in range(2):
      fgood1, fgood2, fgood3 = False, False, False
      for i in range(192):
        ecal_table = analyze_strip_ecalib(
            emline_data, emlines, k, i, j,
            ecal_table, flog, plot_flg)

  ecal_table = array(ecal_table)

  # ---------------------------------------------------------------------------
  # --- Write fit results to text file
  # ---------------------------------------------------------------------------
  fout = open(data_dir+file_out, 'w')
  fout.write('#  Date created [UTC]: ' + \
    dstart.strftime('%Y-%m-%d %H:%M:%S') + '\n')
  fout.write('#  Files used for obtaining energy calibration & assigned source type:\n')
  for i in range(nfiles):
    fout.write('#     ' + file_table[i] + '   ' + emsource[i] + '\n')
  fout.write('#  Energy calibration form: E = a1coef * PH + a2coef\n')
  fout.write('#  FitFlag values:\n')
  fout.write('#        -2 - no em lines / no data\n')
  fout.write('#        -1 - fit failed\n')
  fout.write('#         1 - > 2em lines\n')
  fout.write('#         2 - = 2 em lines\n')
  fout.write('#         3 - only 1 em line\n')
  fout.write('#  Detector   LayerSide   Strip   FitFlag   a1coef   ' \
    'a1coeff_err   a2coef   a2coef_err\n')
  for i in range(ecal_table.shape[0]):
    fout.write(' {:3.0f}   {:3.0f}   {:3.0f}   {:3.0f}   {:8.4f}   ' \
      '{:8.4f}   {:8.4f}   {:8.4f}\n'.format(\
      ecal_table[i, 0], ecal_table[i, 1], ecal_table[i, 2], ecal_table[i, 3], \
      ecal_table[i, 4], ecal_table[i, 6], ecal_table[i, 5], ecal_table[i, 7]))
  fout.close()

  # ---------------------------------------------------------------------------
  # --- Sanity check for now: Plot a1, a2 coefficients
  # ---------------------------------------------------------------------------
  if (plot_flg == 1):
    for j in range(tmp_nlayer):
      plt.ion()
      plt.figure(figsize=(12,8))
      plt.clf()
      plt.suptitle('Layer '+str(j) +'\n Linear transformation E = A1 * PH + A2')
      plt.subplot(221)
      plt.title('Side: 0')
      plt.xlim(-10, 200)
      plt.xlabel('Strip number')
      plt.ylabel('A1 coeff')
      qp = ((ecal_table[:, 1] == 0) & (ecal_table[:, 0] == j))  # select strips on side: 0
      rfit_table = ecal_table[qp, :]
      # with good fit
      qclr = (rfit_table[:, 3] == 1)
      plt.errorbar(rfit_table[qclr, 2], rfit_table[qclr, 4], \
        rfit_table[qclr, 6], fmt='.', color='blue', label='>2eml')
      # with 2 em.lines fit
      qclr = (rfit_table[:, 3] == 2)
      plt.errorbar(rfit_table[qclr, 2], rfit_table[qclr, 4], \
        rfit_table[qclr, 6], fmt='.', color='green', label='2eml')
      # with 1 em.lines fit & forced ZERO
      qclr = (rfit_table[:, 3] == 3)
      plt.errorbar(rfit_table[qclr, 2], rfit_table[qclr, 4], \
        rfit_table[qclr, 6], fmt='.', color='violet', label='1eml')
      # with failed fit or no good data
      qclr = ((rfit_table[:, 3] == -1) | (rfit_table[:, 3] == -2))
      plt.plot(rfit_table[qclr, 2], rfit_table[qclr, 4], '.', color='red', \
        label='bad')
      plt.legend()
      plt.show()

      plt.subplot(223)
      plt.xlim(-10, 200)
      plt.xlabel('Strip number')
      plt.ylabel('A2 coeff')
      # with good fit
      qclr = (rfit_table[:, 3] == 1)
      plt.errorbar(rfit_table[qclr, 2], rfit_table[qclr, 5], \
        rfit_table[qclr, 7], fmt='.', color='blue', label='>2eml')
      # with 2 em.lines fit
      qclr = (rfit_table[:, 3] == 2)
      plt.errorbar(rfit_table[qclr, 2], rfit_table[qclr, 5], \
        rfit_table[qclr, 7], fmt='.', color='green', label='2eml')
      # with 1 em.lines fit & forced ZERO
      qclr = (rfit_table[:, 3] == 3)
      plt.errorbar(rfit_table[qclr, 2], rfit_table[qclr, 5], \
        rfit_table[qclr, 7], fmt='.', color='violet', label='1eml')
      # with failed fit or no good data
      qclr = ((rfit_table[:, 3] == -1) | (rfit_table[:, 3] == -2))
      plt.plot(rfit_table[qclr, 2], rfit_table[qclr, 5], '.', color='red', \
        label='bad')
      plt.legend()
      plt.show()

      plt.subplot(222)
      plt.title('Side: 1')
      plt.xlim(-10, 200)
      plt.xlabel('Strip number')
      plt.ylabel('A1 coeff')
      qp = ((ecal_table[:, 1] == 1) & (ecal_table[:, 0] == j)) # select strips on side: 1
      rfit_table = ecal_table[qp, :]
      # with good fit
      qclr = (rfit_table[:, 3] == 1)
      plt.errorbar(rfit_table[qclr, 2], rfit_table[qclr, 4], \
        rfit_table[qclr, 6], fmt='.', color='blue', label='>2eml')
      # with 2 em.lines fit
      qclr = (rfit_table[:, 3] == 2)
      plt.errorbar(rfit_table[qclr, 2], rfit_table[qclr, 4], \
        rfit_table[qclr, 6], fmt='.', color='green', label='2eml')
      # with 1 em.lines fit & forced ZERO
      qclr = (rfit_table[:, 3] == 3)
      plt.errorbar(rfit_table[qclr, 2], rfit_table[qclr, 4], \
        rfit_table[qclr, 6], fmt='.', color='violet', label='1eml')
      # with failed fit or no good data
      qclr = ((rfit_table[:, 3] == -1) | (rfit_table[:, 3] == -2))
      plt.plot(rfit_table[qclr, 2], rfit_table[qclr, 4], '.', color='red', \
        label='bad')
      plt.legend()
      plt.show()

      plt.subplot(224)
      plt.xlim(-10, 200)
      plt.xlabel('Strip number')
      plt.ylabel('A2 coeff')
      # with good fit
      qclr = (rfit_table[:, 3] == 1)
      plt.errorbar(rfit_table[qclr, 2], rfit_table[qclr, 5], \
        rfit_table[qclr, 7], fmt='.', color='blue', label='>2eml')
      # with 2 em.lines fit
      qclr = (rfit_table[:, 3] == 2)
      plt.errorbar(rfit_table[qclr, 2], rfit_table[qclr, 5], \
        rfit_table[qclr, 7], fmt='.', color='green', label='2eml')
      # with 1 em.lines fit & forced ZERO
      qclr = (rfit_table[:, 3] == 3)
      plt.errorbar(rfit_table[qclr, 2], rfit_table[qclr, 5], \
        rfit_table[qclr, 7], fmt='.', color='violet', label='1eml')
      # with failed fit or no good data
      qclr = ((rfit_table[:, 3] == -1) | (rfit_table[:, 3] == -2))
      plt.plot(rfit_table[qclr, 2], rfit_table[qclr, 5], '.', color='red', \
        label='bad')
      plt.legend()
      plt.show()
      plt.savefig(data_dir+'EnergyCalibCoefficientsCheck_Layer'+ str(j) + \
        '_' + dstart.strftime('%Y%m%d') + '.png')

  # ---------------------------------------------------------------------------
  # --- Print some stats for the automated fit part
  # ---------------------------------------------------------------------------
  ntotx = len(where(ecal_table[:, 1] == 0)[0])
  ntoty = len(where(ecal_table[:, 1] == 1)[0])
  print('\n\n --- Total number of X strips = ', ntotx)
  print(' --- Total number of Y strips = ', ntoty)

  # --- More than 2 em.lines Cases
  qclr = where((ecal_table[:, 1] == 0) & (ecal_table[:, 3] == 1))
  print('\n --- Number of side 0 strips w/ > 2 emission lines: {:.0f} ' \
    '({:.1f} %)'.format(len(qclr[0]), len(qclr[0])*1.0/ntotx*100.))
  flog.write('\n --- Number of side 0 strips w/ > 2 emission lines: {:.0f} ' \
    '({:.1f} %)\n'.format(len(qclr[0]), len(qclr[0])*1.0/ntotx*100.))

  qclr = where((ecal_table[:, 1] == 1) & (ecal_table[:, 3] == 1))
  print(' --- Number of side 1 strips w/ > 2 emission lines: {:.0f} ' \
    '({:.1f} %)'.format(len(qclr[0]), len(qclr[0])*1.0/ntoty*100.))
  flog.write(' --- Number of side 1 strips w/ > 2 emission lines: {:.0f} ' \
    '({:.1f} %)\n'.format(len(qclr[0]), len(qclr[0])*1.0/ntoty*100.))

  # --- 2 em.lines Cases
  qclr = where((ecal_table[:, 1] == 0) & (ecal_table[:, 3] == 2))
  print(' --- Number of side 0 strips w/ = 2 emission lines: {:.0f} ' \
    '({:.1f} %)'.format(len(qclr[0]), len(qclr[0])*1.0/ntotx*100.))
  flog.write(' --- Number of side 0 strips w/ = 2 emission lines: {:.0f} ' \
    '({:.1f} %)\n'.format(len(qclr[0]), len(qclr[0])*1.0/ntotx*100.))

  qclr = where((ecal_table[:, 1] == 1) & (ecal_table[:, 3] == 2))
  print(' --- Number of side 1 strips w/ = 2 emission lines: {:.0f} ' \
    '({:.1f} %)'.format(len(qclr[0]), len(qclr[0])*1.0/ntoty*100.))
  flog.write(' --- Number of side 1 strips w/ = 2 emission lines: {:.0f} ' \
    '({:.1f} %)\n'.format(len(qclr[0]), len(qclr[0])*1.0/ntoty*100.))

  # --- 1 em.lines Cases
  qclr = where((ecal_table[:, 1] == 0) & (ecal_table[:, 3] == 3))
  print(' --- Number of side 0 strips w/ 1 emission lines: {:.0f} ' \
    '({:.1f} %)'.format(len(qclr[0]), len(qclr[0])*1.0/ntotx*100.))
  flog.write(' --- Number of side 0 strips w/ 1 emission lines: {:.0f} ' \
    '({:.1f} %)\n'.format(len(qclr[0]), len(qclr[0])*1.0/ntotx*100.))

  qclr = where((ecal_table[:, 1] == 1) & (ecal_table[:, 3] == 3))
  print(' --- Number of side 1 strips w/ 1 emission lines: {:.0f} ' \
    '({:.1f} %)'.format(len(qclr[0]), len(qclr[0])*1.0/ntoty*100.))
  flog.write(' --- Number of side 1 strips w/ 1 emission lines: {:.0f} ' \
    '({:.1f} %)\n'.format(len(qclr[0]), len(qclr[0])*1.0/ntoty*100.))

  # --- No Data Cases
  qclr = where((ecal_table[:, 1] == 0) & (ecal_table[:, 3] == -2))
  print(' --- Number of side 0 strips w/o events: {:.0f} ' \
    '({:.1f} %)'.format(len(qclr[0]), len(qclr[0])*1.0/ntotx*100.))
  flog.write(' --- Number of side 0 strips w/o events: {:.0f} ' \
    '({:.1f} %)\n'.format(len(qclr[0]), len(qclr[0])*1.0/ntotx*100.))

  qclr = where((ecal_table[:, 1] == 1) & (ecal_table[:, 3] == -2))
  print(' --- Number of side 1 strips w/o events: {:.0f} ' \
    '({:.1f} %)'.format(len(qclr[0]), len(qclr[0])*1.0/ntoty*100.))
  flog.write(' --- Number of side 1 strips w/o events: {:.0f} ' \
    '({:.1f} %)\n'.format(len(qclr[0]), len(qclr[0])*1.0/ntoty*100.))

  # --- Failed fit cases
  qclr = where((ecal_table[:, 2] == 0) & (ecal_table[:, 3] == -1))
  print(' --- Number of side 0 strips w/ failed fit: {:.0f} ' \
    '({:.1f} %)'.format(len(qclr[0]), len(qclr[0])*1.0/ntotx*100.))
  flog.write(' --- Number of side 0 strips w/ failed fit: {:.0f} ' \
    '({:.1f} %)\n'.format(len(qclr[0]), len(qclr[0])*1.0/ntotx*100.))

  qclr = where((ecal_table[:, 1] == 1) & (ecal_table[:, 3] == -1))
  print(' --- Number of side 1 strips w/ failed fit: {:.0f} ' \
    '({:.1f} %)'.format(len(qclr[0]), len(qclr[0])*1.0/ntoty*100.))
  flog.write(' --- Number of side 1 strips w/ failed fit: {:.0f} ' \
    '({:.1f} %)\n'.format(len(qclr[0]), len(qclr[0])*1.0/ntoty*100.))

  flog.write('\n --- Fit results saved to file: '+data_dir+file_out+'\n')

  # ---------------------------------------------------------------------------
  # --- End of program execution
  # ---------------------------------------------------------------------------
  dend = datetime.utcnow()
  print('\n --- Execution time [sec] = {:.5f}'.format(\
    (dend - dstart).total_seconds()))
  flog.write(' --- Total execution time [sec] = {:.5f}\n'.format(\
    (dend - dstart).total_seconds()))
  flog.write(' --- Analysis end [UTC]: '+dend.strftime('%Y-%m-%d %H:%M:%S\n'))
  flog.close()


if __name__ == "__main__":
  if (len(sys.argv) == 4):
    data_dir = str(sys.argv[1]).rstrip()
    if (data_dir[-1] != '/'):
      data_dir = data_dir + '/'
    list_name = str(sys.argv[2]).rstrip()
    plot_flg = int(sys.argv[3])
    fit_energy_calibration(list_name, data_dir, plot_flg)
  elif (len(sys.argv) == 3):
    data_dir = str(sys.argv[1]).rstrip()
    if (data_dir[-1] != '/'):
      data_dir = data_dir + '/'
    list_name = str(sys.argv[2]).rstrip()
    fit_energy_calibration(list_name, data_dir)
  elif (len(sys.argv) == 2):
    list_name_tmp = str(sys.argv[1]).rstrip()
    list_name = list_name_tmp.split('/')[-1]
    len_list_str = len(list_name_tmp.split('/')[-1])
    if (len_list_str == len(list_name_tmp)):
      fit_energy_calibration(list_name)
    else:
      data_dir = list_name_tmp[:-len_list_str]
      if (data_dir[-1] != '/'):
        data_dir = data_dir + '/'
      fit_energy_calibration(list_name, data_dir)
  else:
    print('\n *** Use error: incorrect number of arguments')
    print('\n     Use examples:')
    print('       fit_ecalib.py <list_name>')
    print('       fit_ecalib.py <data_dir> <list_name>')
    print('       fit_ecalib.py <data_dir> <list_name> <plot_flag>')
