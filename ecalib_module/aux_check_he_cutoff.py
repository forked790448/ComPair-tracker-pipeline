'''
2023-03-14  Anna Zajczyk

  This tool estimates the position of the high end of the tracker dynamic range.
  For this estimate one needs to provide the tool with the energy calibration 
  file (fit_ecalib.py) and the baseline shift file (aux_baseline_shift_calc.py).
  The position of the high energy end of the dynamic range is estimated assuming
  the maximum of the ADU range of each strip is 1023. This max value is then 
  adjusted by the baseline shift calculated for each strip. This "adjusted
  maximu ADU" value is then converted to energy using energy calibration solution.
  The maximum energy value calculated in this way is the proxy for high energy cutoff
  of tracker dynamic range at a given iramp. Calculations are done per layer, per side
  and per strip.

  Parameters
  ----------
  ecalib_name: str
    File containing the energy calibration solution obtained with fit_ecalib.py 
    tool. Provide the file name with the full access path to the directory where 
    the file resides.
  shift_file: str
    File containing the baseline shifts obtained with aux_baseline_shift_calc.py 
    tool. Provide the file name with the full access path to the directory where 
    the file resides. If shift_file is not provided, then it is assumed that 
    there is no baseline shift and the maximum 1023 ADU value is converted to energy
    and represents the high energy cutoff.
  mask_file: str
    File containing the information on masked channels. This can be any L1- or 
    L2-level data file that contains data for all layers that one wants to inspect.
    Provide the file name with the full access path to the directory where 
    the file resides.
    This parameter can be left blank, then tool assumes all channels are unmasked.

  Returns
  -------
  file_out: str
    Name of the output file where the results of the cutoff calculations will be saved.
    The output file will be saved in HDF5 format, in the same data directory as 
    the ecalib_name file.
    The data are saved to the following datasets: 
      /layerXX/adu_max_val - calculated maximum in ADU units
      /layerYY/energy_max_val - calculated maximum converted to energy in keV units
    Each data set has size (2, 192).
    The output file name is: 
      HE_Cutoff_Values_for_HiRamp_YYYYMMDD_wShiftApplied.h5 (w/ baseline shift applied)
        or
      HE_Cutoff_Values_for_HiRamp_YYYYMMDD_noShiftApplied.h5 (w/o baseline shift applied)
  plot: png
    Plot of calculated high energy cutoffs. The plot consists of a number of subplots 
    which correspond to individual tracker layers found in the input data. 
    Tracker side is color-coded: blue for side 0, and pink for side 1. Shaded region
    shows region of interest for Cs137 features (emission line and compton edge).
    Exact energy of emission line and compton edge are marked with dot-dashed and
    dotted lines, respectively.
    The output file will be saved in HDF5 format, in the same data directory as 
    the ecalib_name file. The plot file will be saved in PNG format, in the same data 
    directory as the ecalib_name file.

  Updates
  -------
  YYYY-MM-DD

'''
import sys
import h5py
import matplotlib.pyplot as plt
from datetime import datetime
from numpy import zeros, where, nan, arange, append, isnan, argwhere, array, floor

from ecalib_functions import read_data, convert_mask

def check_he_cutoff(ecalib_name, shift_name = None, mask_name = None):
  ecalib_table, enrow, encol = read_data(
    ecalib_name, data_type='float')   # get energy calibration params
  if (shift_name is not None):
    print(' >>> Reading baseline shift data from: ')
    print(' >>>    ' + shift_name.split('/')[-1])
    h5shift = h5py.File(shift_name, 'r')
  if (mask_name is not None):
    print(' >>> Reading masked strip information from: ')
    print(' >>>    ' + mask_name.split('/')[-1])
    h5mask = h5py.File(mask_name, 'r')

  he_thr1 = 661.0
  he_thr2 = 477.0
  ymax_plot = 1100
  ymin_plot = 250
  
  ene_max_data = zeros((10, 2, 192), dtype='float')
  adu_max_data = zeros((10, 2, 192), dtype='float')

  date_now = datetime.utcnow().strftime('%Y%m%d') 
  if (shift_name is not None):
    file_out = ecalib_name.rsplit('/', 1)[0] + '/' + 'HE_Cutoff_Values_for_HiRamp_' + date_now + '_wShiftApplied.h5'
  else:
    file_out = ecalib_name.rsplit('/', 1)[0] + '/' + 'HE_Cutoff_Values_for_HiRamp_' + date_now + '_noShiftApplied.h5'
  h5out = h5py.File(file_out, 'w')

  print('\n --- !!! Negative values for the high energy cutoff')
  print('   detector / side / strip   =   mod_cutoff[ADU]   /   shift[ADU]   /   mod_cutoff[keV]')
  for detector_number in range(10):
    detector_name = 'layer{:02d}'.format(detector_number)
    if (mask_name is not None):
      try:
        mask = convert_mask(h5mask, detector_name)
      except:
        print(' *** No mask strip information for detector: {:02d}'.format(detector_number))
        print(' *** Assuming all strips are unmasked !!!')
        mask = zeros((2, 192))
    else:
      mask = zeros((2, 192))
    if (shift_name is not None):
      try:
        bs_shift = array(h5shift['/' + detector_name + '/baseline_shift'][:, :])
      except:
        print(' *** No baseline shift data for detector: {:02d}'.format(detector_number))
        print(' *** Assuming baseline shift value of: 0 !!!')
        bs_shift = zeros((2, 192))

    for layer_side in range(2):
      for strip_number in range(192):
        if (mask[layer_side, strip_number] == 0):
          adu_max = 1023
          if (shift_name is not None):
            adu_max = floor(adu_max - bs_shift[layer_side, strip_number])
          adu_max_data[detector_number, layer_side, strip_number] = adu_max
          qf = where((ecalib_table[:, 0] == detector_number) &
              (ecalib_table[:, 1] == layer_side) &
              (ecalib_table[:, 2] == strip_number))
          indx = qf[0][0]
          if ((ecalib_table[indx, 3] == -2) | (ecalib_table[indx, 3] == -1)):
            ene_max_data[detector_number, layer_side, strip_number] = nan
          else:
            ene_max_data[detector_number, layer_side, strip_number] = \
              ecalib_table[indx, 4] * adu_max + ecalib_table[indx, 6]
            if (ene_max_data[detector_number, layer_side, strip_number] < 0):
              if (shift_name is not None):       
                print('   {:02d} / {:01d} / {:03d}   =   {:.1f}   /   {:.1f}   /   {:.1f}'.format(\
                  detector_number, layer_side, strip_number, adu_max, bs_shift[layer_side, strip_number], \
                  ene_max_data[detector_number, layer_side, strip_number]))
              else:
                print('   {:02d} / {:01d} / {:03d}   =   {:.1f}   /   {:.1f}   /   {:.1f}'.format(\
                  detector_number, layer_side, strip_number, adu_max, 0, \
                  ene_max_data[detector_number, layer_side, strip_number]))
        else:
          adu_max_data[detector_number, layer_side, strip_number] = nan
          ene_max_data[detector_number, layer_side, strip_number] = nan
          
    dset_adu_max = \
      h5out.create_dataset('/' + detector_name + '/adu_max_val', adu_max_data[detector_number, :, :].shape, dtype='float64')
    dset_adu_max[:, :] = adu_max_data[detector_number, :, :]

    dset_ene_max = \
      h5out.create_dataset('/' + detector_name + '/energy_max_val', ene_max_data[detector_number, :, :].shape, dtype='float64')
    dset_ene_max[:, :] = ene_max_data[detector_number, :, :]

  h5out.close()
  print('\n --- HE Cutoff data written to: ' + file_out.split('/')[-1])

  xtbl = arange(0, 192, 1)
  xbounds = xtbl - 0.5 * (xtbl[1]-xtbl[0])
  xbounds = append(xbounds, [xtbl[-1] + 0.5 * (xtbl[1]-xtbl[0])])

  FONT_SIZE = 8
  plt.rc('font', size=FONT_SIZE)          # controls default text sizes
  plt.rc('axes', titlesize=FONT_SIZE)     # fontsize of the axes title
  plt.rc('axes', labelsize=FONT_SIZE)     # fontsize of the x and y labels
  plt.rc('xtick', labelsize=FONT_SIZE)    # fontsize of the tick labels
  plt.rc('ytick', labelsize=FONT_SIZE)    # fontsize of the tick labels
  plt.rc('legend', fontsize=FONT_SIZE)    # legend fontsize
  plt.rc('figure', titlesize=FONT_SIZE+2)

  plt.close('all')
  plt.ion()
  plt.figure(1, figsize=(12, 8))
  plt.suptitle('High energy (HE) cutoff values across the tracker strips')
  for i in range(10):
    pltnum = i+1
    plt.subplot(3,4,pltnum)
    plt.title('Layer {:d}'.format(i))
    plt.ylim(ymin_plot, ymax_plot)
    if (pltnum == 9) | (pltnum == 10) | (pltnum == 11) | (pltnum == 12):
      plt.xlabel('Strip number')
    if (pltnum == 1) | (pltnum == 5) | (pltnum == 9):
      plt.ylabel('Energy [keV]')
    plt.fill_between([0, 192], [400, 400], [700, 700], color='grey', alpha=0.2, \
      edgecolor='none')
    plt.plot([0, 192], [662, 662], '-.', color='steelblue', alpha=0.5, \
      label='Cs137 line')
    plt.plot([0, 192], [477, 477], ':', color='steelblue', alpha=0.5, \
      label='Cmpt edge')

    qnan = ~isnan(ene_max_data[i, 0, :])
    plt.plot(xtbl[qnan], ene_max_data[i, 0, qnan], '.', color='blue', \
      label='side 0')
    qnan = ~isnan(ene_max_data[i, 1, :])
    plt.plot(xtbl[qnan], ene_max_data[i, 1, qnan], '.', color='violet', \
      label='side 1')
    if (pltnum == 1):
      plt.legend()
    plt.show()
  plt.savefig(file_out[:-3] + '.png')
  print('\n --- HE Cutoff plot written to: ' + file_out.split('/')[-1][:-3] + '.png')

  for i in range(10):
    print('\n -----------------------------------------------------------------')
    print(' --- Some stats for Layer {:d}'.format(i))
    print(' -----------------------------------------------------------------')
    print(' --- Side 0')
    qnan = argwhere(isnan(ene_max_data[i, 0, :]))
    ene_max_data[i, 0, qnan] = -1e5
    qthr1 = where(ene_max_data[i, 0, :] < he_thr1)[0]
    print(' ---    Number of strips with HE threshold lower than 661 keV: ' + \
      '{:d} ({:.0f}%)'.format(len(qthr1)-len(qnan), (len(qthr1)-len(qnan))/192*100.0))
    qthr2 = where(ene_max_data[i, 0, qthr1] < he_thr2)[0]
    print(' ---    Number of strips with HE threshold lower than 477 keV: ' + \
      '{:d} ({:.0f}%)'.format(len(qthr2)-len(qnan), (len(qthr2)-len(qnan))/192*100.0))
    print(' ---    Number of strips with invalid calibration: ' + \
      '{:d} ({:.0f}%)'.format(len(qnan), len(qnan)/192*100.0))

    print(' --- Side 1')
    qnan = argwhere(isnan(ene_max_data[i, 1, :]))
    ene_max_data[i, 1, qnan] = -1e5
    qthr1 = where(ene_max_data[i, 1, :] < he_thr1)[0]
    print(' ---    Number of strips with HE threshold lower than 661 keV: ' + \
      '{:d} ({:.0f}%)'.format(len(qthr1)-len(qnan), (len(qthr1)-len(qnan))/192*100.0))
    qthr2 = where(ene_max_data[i, 1, qthr1] < he_thr2)[0]
    print(' ---    Number of strips with HE cutoff lower than 477 keV: ' + \
      '{:d} ({:.0f}%)'.format(len(qthr2)-len(qnan), (len(qthr2)-len(qnan))/192*100.0))
    print(' ---    Number of strips with invalid calibration: ' + \
      '{:d} ({:.0f}%)'.format(len(qnan), len(qnan)/192*100.0))


if __name__ == "__main__":
  if (len(sys.argv) == 2):
    ecalib_name = str(sys.argv[1]).rstrip()
    check_he_cutoff(ecalib_name)
  elif (len(sys.argv) == 3):
    ecalib_name = str(sys.argv[1]).rstrip()
    shift_name = str(sys.argv[2]).rstrip()
    check_he_cutoff(ecalib_name, shift_name)
  elif (len(sys.argv) == 4):
    ecalib_name = str(sys.argv[1]).rstrip()
    shift_name = str(sys.argv[2]).rstrip()
    mask_name = str(sys.argv[3]).rstrip()
    check_he_cutoff(ecalib_name, shift_name, mask_name)
  else:
    print('\n *** Use error: incorrect number of arguments')
    print('\n     Use examples:')
    print('       aux_check_he_cutoff.py <ecalib_name>')
    print('       aux_check_he_cutoff.py <ecalib_name> <shift_file>')
    print('       aux_check_he_cutoff.py <ecalib_name> <shift_file> <mask_file>')
    print('\n     <shift_file> - HDF5 file containg calculated spectral shift [in ADUs] due to coommon mode subtraction')
    print('     <mask_file> - HDF5 data file from which information on masked strips will be extracted')
