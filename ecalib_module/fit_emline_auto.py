'''
2021-05-14    Anna Zajczyk

  Perform emission line fitting in an automated manner. Fit is
  performed for the selected number of layers, for all strips on both
  sides of each layer. Results of the fit are saved to a text file.
  Log file tracking progress of the fit is also created. Script
  creates set of plots that summarize the fit results. User can
  choose level of plotting. Plots are created in an interactive mode
  and are not saved to any output file.

  Updates
  -------
  2022-11-22 Removed options for single & sims data types, set cm_mode=yes
              to be the standard operations, so no cm_mode designation
              needs to be given at the command line; also data_type=multi
              is set as a default, so no needs to provide this info at
              the command line
  2022-08-07 Added option to rebin the data to user-selected bin width.
              The bin size is an integer, so if float is given as an input
              its type will be casted to integer. There is no intelligent
              handling of the conversion float -> integer like ceiling or
              floor or rounding.
  2022-08-06 Added treatment of HDF5 that contain binned spectra
  2021-07-27 Finished update to docstrings to reflect most recent changes
  2021-07-16 Added treatment of HDF5 that contain multi-layer data
  2021-07-16 Added treatment of common mode subtraction


'''

import os
import sys
import h5py

import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from scipy.stats import chisquare
from datetime import datetime
from numpy import sqrt, diag, append, array, where

from ecalib_functions import bin_data2, plot_hist, fgauss, \
  get_strip_counts, find_empeak, get_channel_mask, rebin_data, select_bin_size

from tracker_pipeline import layer

def analyze_strip_spectrum(
    trk_table, layer_side, strip_number, detector_number, data_type,
    binavoid, numpeak, fit_table, flog, cm_mode=None, bin_size=None,
    edges=None, plot_flg=None):
  """Performs automated line fitting for strips in Si tracker detector.

  Parameters
  ----------
  trk_table: array_like
    Table containing events. Table structure depends on `data_type`.
  layer_side: int
    Intiger (0, 1) defining q top/bottom side of a Si layer.
  strip_number: int
    Integer (0, 191) defining a strip number in a selected Si layer.
  detector_number: int
    Integer (0, 9) defining a layer number in Si tracker.
  data_type: str
    String defining the type of the input data. The allowed
    types are: 'multi' (.h5 input data file containing observations
    collected with Si tracker and converted to hdf5 format; multi-layer
    format), and 'binned' (.h5 input data file containing binned
    data created with bin_h5data.py).
  binavoid: int
    Number of bins at the pulse height (or energy) end of the spectrum
    that will be removed from the analysis. This is to remove data in
    the overflow bin. However, this assumes binning spans the whole
    peak height range. Current implementation does the binning based
    on peak height (energies) values found in the data.
  numpeak: int
    Number of peaks that is selected from the number found with
    a second derivative method. Out of this subset the emission
    line peak is selected.
  fit_table: array_like
    Table with results of the emission line fit for the selected strip.
  flog: ?
    Access to log file, so that information on individual fits can
    be saved to the log file.
  cm_mode: str
    Flag (yes / no) that selects if the common mode subtracted data
    should be used in the analysis. If common mode subtracted data are
    not present in the file, the common mode subtracted data will be
    calculated. The default option is to use common mode subtracted data.
  plot_flg: int
    Flag that sets the level of plots that are produced during
    script execution.

  Returns
  -------
  fit_row: array_like
    Table with results of the emission line fit for the selected strip.

  """
  if (cm_mode is None):
    cm_mode = 'yes'

  if (bin_size is None):
    bin_size = 2

  if (plot_flg is None):
    plot_flg = 0

  if (data_type == 'binned'):
    rbins = edges[:, 0]
    redglo = edges[:, 1]
    redghi = edges[:, 2]
    rhist = trk_table['layer{:02d}'.format(detector_number)][layer_side, strip_number, :]
    if (bin_size != 1):
      rhist, rbins, redglo, redghi = rebin_data(\
        rhist, rbins, bin_size)
    rerror = 1.0 + sqrt(rhist + 0.75)
    no_data_flag = False
  else:
    rbins, rhist, rerror, no_data_flag = get_strip_counts(
      trk_table, layer_side, strip_number, detector_number,
      cm_mode=cm_mode, bin_size=bin_size)
    redglo = rbins - 0.5*(rbins[1]-rbins[0])
    redghi = rbins + 0.5*(rbins[1]-rbins[0])

  if (no_data_flag is False):
    try:
      pinit, fit_ll, fit_ul = find_empeak(rbins, rhist)

      qf = ((fit_ll < rbins) & (rbins < fit_ul))
      p1,t1 = curve_fit(
        fgauss, rbins[qf], rhist[qf], p0=pinit, sigma=rerror[qf],
        absolute_sigma=True)
      perr1 = sqrt(diag(t1))

      # --- Conditions on a relative error are a guess-timate.
      # --- Condition based on the value of the reduced chisquare
      # --- fails because the values are >> 2.0 even for good fits.
      # --- This is because the fit region is large and there is
      # --- more structure in the spectrum than just one emission
      # --- line.
      if ((p1[1] < 0.0) or (p1[2] < 0.0) or (p1[0] < 0.0) \
        or (perr1[1]/p1[1]*100.0 > 10.0)):
        print(' *** Caution: problematic fit for strip (layer / side / ' \
          'strip): ' + str(detector_number) + ' / '+ str(layer_side) + \
          ' / ' + str(strip_number))
        flog.write(' *** Caution: problematic fit for strip (layer / side / ' \
          'strip): ' + str(detector_number) + ' / '+ str(layer_side) + \
          ' / ' + str(strip_number) + '\n')
        if (layer_side == 0):
          fit_table.append([detector_number, 0, strip_number, 2,
            p1[1], perr1[1], p1[2], perr1[2], p1[0], perr1[0]])
        elif (layer_side == 1):
          fit_table.append([detector_number, 1, strip_number, 2,
            p1[1], perr1[1], p1[2], perr1[2], p1[0], perr1[0]])
      else:
        if (layer_side == 0):
          fit_table.append([detector_number, 0, strip_number, 1,
            p1[1], perr1[1], p1[2], perr1[2], p1[0], perr1[0]])
        elif (layer_side == 1):
          fit_table.append([detector_number, 1, strip_number, 1,
            p1[1], perr1[1], p1[2], perr1[2], p1[0], perr1[0]])

      if (plot_flg == 2):
        if (strip_number == 87):
          plt.ion()
          plt.figure(figsize=(8,8))
          plt.clf()
          if ((p1[1] < 0.0) or (p1[2] < 0.0) or (p1[0] < 0.0) \
            or (perr1[1]/p1[1]*100.0 > 10.0)):
            plt.title('Layer: '+str(detector_number)+'   Side: ' + \
              str(layer_side) + '   Strip: '+str(strip_number) + \
              '\n problematic fit')
          else:
            plt.title('Layer: '+str(detector_number)+'   Side: ' + \
              str(layer_side) + '   Strip: '+str(strip_number))
          # plt.xlim(min(rbins), max(rbins))
          plt.xlim(min(rbins)-2.0, 2*fit_ul)
          plt.ylim(-0.2*p1[0], 1.2*p1[0])
          plt.xlabel('Peak centroid [ADC]')
          plt.ylabel('Counts/bin')
          plot_hist(rhist, rbins, redglo, redghi, color='violet')
          plt.plot(rbins, fgauss(rbins, p1[0], p1[1], p1[2]), '-.', \
            color='blue', alpha=0.5)
          plt.plot([fit_ll, fit_ul], [-1, -1], 'k')
          plt.show()
          if (data_type == 'sims'):
            plt.text(5, 36e2, 'FWHM and Noise effect added')
            plt.show()
    except:
      print(' *** Warning: Fit failed for strip (layer / side / ' \
        'strip): ' + str(detector_number) + ' / '+ str(layer_side) + \
        ' / ' + str(strip_number))
      flog.write(' *** Warning: Fit failed for strip (layer / side / ' \
        'strip): ' + str(detector_number) + ' / '+ str(layer_side) + \
        ' / ' + str(strip_number) + '\n')
      if (layer_side == 0):
        fit_table.append([detector_number, 0, strip_number, 3,
          -1, -1, -1, -1, -1, -1])
      elif (layer_side == 1):
        fit_table.append([detector_number, 1, strip_number, 3,
          -1, -1, -1, -1, -1, -1])
  else:
    print(' *** No events registered by strip (layer / side / '
      'strip): ' + str(detector_number) + ' / '+ str(layer_side) +\
      ' / ' + str(strip_number))
    flog.write(' *** No events registered by strip (layer / side / '
      'strip): ' + str(detector_number) + ' / '+ str(layer_side) +\
      ' / ' + str(strip_number) + '\n')
    if (layer_side == 0):
      fit_table.append([detector_number, 0, strip_number, 4,
        -1, -1, -1, -1, -1, -1])
    elif (layer_side == 1):
      fit_table.append([detector_number, 1, strip_number, 4,
        -1, -1, -1, -1, -1, -1])

  return fit_table

def fit_emission_line(file_name, data_dir=None, data_type=None,
  cm_mode=None, plot_flg=None):
  """Function that allows for automated emission line fitting.

  The script runs automated emission line fitting. User can select with
  'nlayer' number of tracker layers that will be analyzed. User will be
  asked if common mode subtraction should be applied to the data prior
  to line fitting. The script then takes each layer and goes through all
  strips in the layer to find emission line and fit a Gaussian to it.
  The peaks in the spectrum are found using a second derivative, and
  then the peak with the heighest peak height [ADC] value is selected
  as the emission peak. In the analysis top 4 spectral bins are ignored
  to avoid an overflow bin, if such exists. The script work with simulations
  and observations. The script produces a set of plots (see plot_flg
  description), a log file with the execution progress info, which is
  also printed on the screen during the execution, and a text file
  with results of the fit. The text file serves as an input to
  the energy calibration fitting script.

  Parameters
  ----------
  file_name: str
    File name
  data_dir: str
    Directory access path where the input file sits. The access
    path must end with a forward slash, e.g., 'dir/subdir/' .
    When no path is given, it is assumed that the input file is
    in the current directory './' .
  data_type: str
    String defining the type of the input data. The three allowed
    types are: 'sims' (.sim input data file created in Megalib
    simulations), 'multi' (.h5 input data file containing observations
    collected with Si tracker and converted to hdf5 format; multi-layer
    format), 'single' (.h5 input data file containing observations
    collected with Si tracker and converted to hdf5 format; single-layer
    format), and 'binned' (.h5 input data file containing binned
    data created with bin_h5data.py).
  nlayer: int
    Number of tracker layers in the data file that is being analysed.
    If not provided, the nlayer is set to 1.
  cm_mode: str
    Flag (yes / no) that selects if the common mode subtracted data
    should be used in the analysis. If common mode subtracted data are
    not present in the file, the common mode subtracted data will be
    calculated. The default option is to use common mode subtracted data.
  plot_flg: int
    Flag that sets the level of plots that are produced during
    script execution. If not provided, the plot_flg is set to 2.
    Level 0 - histograms of fit parameters for good and problematic
    fits (if identified) are created.
    Level 1 - plots produced in Level 0 and fitted values of peak
    centroids and sigmas as a function of strip number and layer
    side. If dealing with simulated data, histograms of fake energy
    calibration coefficients are also plotted.
    Level 2 - plots produced in Level 1 and spectra for strip(s) #87.

  """
  dstart = datetime.utcnow()

  if (data_dir is None):
    data_dir = './'

  if (data_type is None):
    data_type = 'multi'   # what data is fed to the program

  if (cm_mode is None):
    cm_mode = 'yes'

  if (plot_flg is None):
    plot_flg = 2

  binavoid = 4  # number of top PHA bins to avoid (overflow bins)
  numpeak = 3   # number of peaks from which emission peak will be selected
  fext_num = {'multi':-3, 'binned':-3}

  if (os.path.isfile(data_dir+file_name)):
    bin_size = select_bin_size()

    if ((data_type == 'multi') & (file_name[fext_num[data_type]:] == '.h5')):
      h5 = h5py.File(data_dir+file_name, 'r')

      group_names = array([x for x in h5])
      q = where(group_names != 'vdata')[0]
      group_names = group_names[q]
      nlayer = len(group_names)

      print(' >>> Number of tracker layers in the file: {:d}'.format(nlayer))
      for i in group_names:
        print('       ' + i)

      trk_table = {}
      if (cm_mode == 'no'):
        print(' >>> working on channel_data')
        for i in group_names:
          print(' ... reading vdata:channel_data for '+i)
          trk_table[i] = array(h5['/' + i + '/vdata/channel_data'])

      elif(cm_mode == 'yes'):
        print(' >>> working on channel_cm_sub')
        for i in group_names:
          print(' ... reading vdata:channel_cm_sub for '+i)
          trk_table[i] = array(h5['/' + i + '/vdata/channel_cm_sub'])

      channel_mask = get_channel_mask(h5)

      flog = open(data_dir+file_name[:fext_num[data_type]]+'.log', 'a+')
    elif ((data_type == 'binned') & \
      (file_name[fext_num[data_type]:] == '.h5')):
      h5 = h5py.File(data_dir+file_name, 'r')

      group_names = array([x for x in h5])
      q = where(group_names != 'edges')[0]
      group_names = group_names[q]
      nlayer = len(group_names)

      print(' >>> Number of tracker layers in the file: {:d}'.format(nlayer))
      for i in group_names:
        print('       ' + i)

      print(' >>> working on binned data')
      trk_table = {}
      channel_mask = {}
      for i in group_names:
        print(' ... reading binned_data for '+i)
        trk_table[i] = array(h5['/' + i + '/binned_data'])
        channel_mask[i] = array(h5['/' + i + '/chan_disable'])
      edges = array(h5['/edges'])
      flog = open(data_dir+file_name[:fext_num[data_type]]+'.log', 'a+')
    else:
      print('\n *** Error: Mismatch between the selected data file and '\
        'the chosen data type found.')
      print(' ***    Selected data file: ' + file_name)
      print(' ***    Selected data type: ' + data_type)
      sys.exit()

    fit_table = []   # will store gauss coefficients here [L#, SD#,
                     # ST#, FLG, peak_centroid, peak_centroid_error,
                     # gauss_sigma, gauss_sigma_error,
                     # norm, norm_error]

    flog.write('\n ------------------------------------------------------- \n')
    flog.write(' --- Analysis start [UTC]: ' + \
      dstart.strftime('%Y-%m-%d %H:%M:%S\n'))
    flog.write(' --- Data file: ' + data_dir + file_name + '\n')
    flog.write(' --- Analysis settings \n')
    flog.write('            fit method = automated \n')
    flog.write('             data_type = '+str(data_type)+'\n')
    flog.write('              plot_flg = '+str(plot_flg)+' \n')
    flog.write('  common mode subtract = '+str(cm_mode)+' \n')
    flog.write('       bin width [ADU] = '+str(bin_size)+' \n\n')

    plt.close('all')   # close any existing figures

    print('')

    for m in group_names:   # - layer number
      detnum = int(m[-2:])
      for k in range(2):      # - layer side
        for i in range(192):  # - layer strip
          single_channel_mask = channel_mask[m][k, i]

          if (single_channel_mask == 0):
            if (data_type == 'multi'):
              fit_table = analyze_strip_spectrum(
                trk_table, k, i, detnum, data_type,
                binavoid, numpeak, fit_table, flog,
                cm_mode=cm_mode, bin_size=bin_size,
                plot_flg=plot_flg)
            elif (data_type == 'binned'):
              fit_table = analyze_strip_spectrum(
                trk_table, k, i, detnum, data_type,
                binavoid, numpeak, fit_table, flog,
                cm_mode=cm_mode, bin_size=bin_size,
                edges=edges, plot_flg=plot_flg)
          else:
            print(' *** Masked channel for strip (layer / side / ' \
              'strip): ' + str(detnum) + ' / '+ str(k) + \
              ' / ' + str(i))
            flog.write(' *** Masked channel for strip (layer / side / ' \
              'strip): ' + str(detnum) + ' / '+ str(k) + \
              ' / ' + str(i) + '\n')
            fit_table.append([detnum, k, i, 5,
              -1, -1, -1, -1, -1, -1])

    fit_table = array(fit_table)

    dend = datetime.utcnow()
    print('\n --- Automated fit execution time [sec] = {:.5f}'.format((\
      dend - dstart).total_seconds()))

    flog.write('\n --- Automated fit execution time [sec] = {:.5f}\n'.format((\
      dend - dstart).total_seconds()))

    if (cm_mode == 'no'):
      if (data_type == 'multi'):
        h5.close()

    # -------------------------------------------------------------------------
    # --- Sanity checks
    # -------------------------------------------------------------------------
    plt.ion()
    plt.figure(figsize=(8,8))
    plt.clf()
    plt.suptitle('Histograms for Good fits')
    plt.subplot(221)
    plt.title('All side 0 strips')
    plt.xlabel('Peak centroid [ADC]')
    plt.ylabel('Number of strips')
    qp = (fit_table[:, 1] == 0)   # select Strips: side 0
    rfit_table = fit_table[qp, :]
    qclr = (rfit_table[:, 3] == 1)   # select GOOD fits
    hist, bins, edglo, edghi = bin_data2(rfit_table[qclr, 4], bin_size)
    plot_hist(hist, bins, edglo, edghi, color='b')

    plt.subplot(223)
    plt.xlabel('Peak sigma [ADC]')
    plt.ylabel('Number of strips')
    hist, bins, edglo, edghi = bin_data2(rfit_table[qclr, 6], bin_size)
    plot_hist(hist, bins, edglo, edghi, color='b')

    plt.subplot(222)
    plt.title('All side 1 strips')
    plt.xlabel('Peak centroid [ADC]')
    qp = (fit_table[:, 1] == 1)   # select Strips: side 1
    rfit_table = fit_table[qp, :]
    qclr = (rfit_table[:, 3] == 1)   # select GOOD fits
    hist, bins, edglo, edghi = bin_data2(rfit_table[qclr, 4], bin_size)
    plot_hist(hist, bins, edglo, edghi, color='r')

    plt.subplot(224)
    plt.xlabel('Peak sigma [ADC]')
    hist, bins, edglo, edghi = bin_data2(rfit_table[qclr, 6], bin_size)
    plot_hist(hist, bins, edglo, edghi, color='r')
    plt.show()


    # --- This plot should be created only if there are problematic fits
    if (len(where(fit_table[:, 3] == 2)[0]) != 0):
      plt.ion()
      plt.figure(figsize=(8,8))
      plt.clf()
      plt.suptitle('Histograms for Problematic fits')
      plt.subplot(221)
      plt.title('All side 0 strips')
      plt.xlabel('Peak centroid [ADC]')
      plt.ylabel('Number of strips')
      qp = (fit_table[:, 1] == 0)   # select Strips: side 0
      rfit_table = fit_table[qp, :]
      qclr = (rfit_table[:, 3] == 2)   # select PROBLEMATIC fits
      hist, bins, edglo, edghi = bin_data2(rfit_table[qclr, 4], bin_size)
      plot_hist(hist, bins, edglo, edghi, color='b')

      plt.subplot(223)
      plt.xlabel('Peak sigma [ADC]')
      plt.ylabel('Number of strips')
      hist, bins, edglo, edghi = bin_data2(rfit_table[qclr, 6], bin_size)
      plot_hist(hist, bins, edglo, edghi, color='b')

      plt.subplot(222)
      plt.title('All side 1 strips')
      plt.xlabel('Peak centroid [ADC]')
      qp = (fit_table[:, 1] == 1)   # select Strips: side 1
      rfit_table = fit_table[qp, :]
      qclr = (rfit_table[:, 3] == 2)   # select PROBLEMATIC fits
      hist, bins, edglo, edghi = bin_data2(rfit_table[qclr, 4], bin_size)
      plot_hist(hist, bins, edglo, edghi, color='r')

      plt.subplot(224)
      plt.xlabel('Peak sigma [ADC]')
      hist, bins, edglo, edghi = bin_data2(rfit_table[qclr, 6], bin_size)
      plot_hist(hist, bins, edglo, edghi, color='r')
      plt.show()

    if ((plot_flg == 1) or (plot_flg == 2)):
      # -----------------------------------------------------------------------
      # --- Plot fitted peak centroids vs strip number w/o recognizing layer
      # --- number
      # -----------------------------------------------------------------------
      for kk in range(nlayer):
        if (data_type == 'multi') | (data_type == 'binned'):
          tmp = group_names[kk]
          kk = int(tmp[-2:])

        plt.ion()
        plt.figure(figsize=(12,8))
        plt.clf()
        plt.suptitle('Fit parameters \n Layer {:02d}'.format(kk))
        plt.subplot(221)
        plt.xlim(-1, 192)
        plt.xlabel('Strip number on side 0')
        plt.ylabel('Peak centroid [ADC]')

        qp = ((fit_table[:, 1] == 0) & (fit_table[:, 0] == kk))  # select Strips: side 0
        rfit_table = fit_table[qp, :]

        qclr = (rfit_table[:, 3] == 1)   # select GOOD fits
        plt.errorbar(rfit_table[qclr, 2], rfit_table[qclr, 4], \
          rfit_table[qclr, 5], fmt='.', color='b', label='good fit')

        qclr = (rfit_table[:, 3] == 2)   # select PROBLEMATIC fits
        plt.plot(rfit_table[qclr, 2], rfit_table[qclr, 4], \
          '.', color='g', label='problem fit')

        qclr = (rfit_table[:, 3] == 3)   # select FAILED fits
        plt.plot(rfit_table[qclr, 2], rfit_table[qclr, 4], \
          '.', color='violet', label='fail fit')

        qclr = (rfit_table[:, 3] == 4)   # select NO DATA cases
        plt.plot(rfit_table[qclr, 2], rfit_table[qclr, 4], \
          '.', color='r', label='no data')

        qclr = (rfit_table[:, 3] == 5)   # masked strips
        plt.plot(rfit_table[qclr, 2], rfit_table[qclr, 4], \
          'x', color='black', label='masked')
        plt.legend(fontsize='small')
        plt.show()

        plt.subplot(223)
        plt.xlim(-1, 192)
        plt.xlabel('Strip number on side 1')
        plt.ylabel('Peak centroid [ADC]')

        qp = ((fit_table[:, 1] == 1) & (fit_table[:, 0] == kk))   # select Strips: side 1
        rfit_table = fit_table[qp, :]

        qclr = (rfit_table[:, 3] == 1)   # select GOOD fits
        plt.errorbar(rfit_table[qclr, 2], rfit_table[qclr, 4], \
          rfit_table[qclr, 5], fmt='.', color='b', label='good fit')

        qclr = (rfit_table[:, 3] == 2)   # select PROBLEMATIC fits
        plt.plot(rfit_table[qclr, 2], rfit_table[qclr, 4], \
          '.', color='g', label='problem fit')

        qclr = (rfit_table[:, 3] == 3)   # select FAILED fits
        plt.plot(rfit_table[qclr, 2], rfit_table[qclr, 4], \
          '.', color='violet', label='fail fit')

        qclr = (rfit_table[:, 3] == 4)   # select NO DATA cases
        plt.plot(rfit_table[qclr, 2], rfit_table[qclr, 4], \
          '.', color='r', label='no data')

        qclr = (rfit_table[:, 3] == 5)   # masked strips
        plt.plot(rfit_table[qclr, 2], rfit_table[qclr, 4], \
          'x', color='black', label='masked')
        plt.legend(fontsize='small')
        plt.show()


        plt.subplot(222)
        plt.xlim(-1, 192)
        plt.xlabel('Strip number on side 0')
        plt.ylabel('Peak sigma [ADC]')

        qp = ((fit_table[:, 1] == 0) & (fit_table[:, 0] == kk)) # select Strips: side 0
        rfit_table = fit_table[qp, :]

        qclr = (rfit_table[:, 3] == 1)   # select GOOD fits
        plt.errorbar(rfit_table[qclr, 2], rfit_table[qclr, 6], \
          rfit_table[qclr, 7], fmt='.', color='b', label='good fit')

        qclr = (rfit_table[:, 3] == 2)   # select PROBLEMATIC fits
        plt.plot(rfit_table[qclr, 2], rfit_table[qclr, 6], \
          '.', color='g', label='problem fit')

        qclr = (rfit_table[:, 3] == 3)   # select FAILED fits
        plt.plot(rfit_table[qclr, 2], rfit_table[qclr, 6], \
          '.', color='violet', label='fail fit')

        qclr = (rfit_table[:, 3] == 4)   # select NO DATA cases
        plt.plot(rfit_table[qclr, 2], rfit_table[qclr, 6], \
          '.', color='r', label='no data')

        qclr = (rfit_table[:, 3] == 5)   # masked strips
        plt.plot(rfit_table[qclr, 2], rfit_table[qclr, 6], \
          'x', color='black', label='masked')
        plt.legend(fontsize='small')
        plt.show()

        plt.subplot(224)
        plt.xlim(-1, 192)
        plt.xlabel('Strip number on side 1')
        plt.ylabel('Peak sigma [ADC]')

        qp = ((fit_table[:, 1] == 1) & (fit_table[:, 0] == kk)) # select Strips: side 1
        rfit_table = fit_table[qp, :]

        qclr = (rfit_table[:, 3] == 1)   # select GOOD fits
        plt.errorbar(rfit_table[qclr, 2], rfit_table[qclr, 6], \
          rfit_table[qclr, 7], fmt='.', color='b', label='good fit')

        qclr = (rfit_table[:, 3] == 2)   # select PROBLEMATIC fits
        plt.plot(rfit_table[qclr, 2], rfit_table[qclr, 6], \
          '.', color='g', label='problem fit')

        qclr = (rfit_table[:, 3] == 3)   # select FAILED fits
        plt.plot(rfit_table[qclr, 2], rfit_table[qclr, 6], \
          '.', color='violet', label='fail fit')

        qclr = (rfit_table[:, 3] == 4)   # select NO DATA cases
        plt.plot(rfit_table[qclr, 2], rfit_table[qclr, 6], \
          '.', color='r', label='no data')

        qclr = (rfit_table[:, 3] == 5)   # masked strips
        plt.plot(rfit_table[qclr, 2], rfit_table[qclr, 6], \
          'x', color='black', label='masked')
        plt.legend(fontsize='small')
        plt.show()

    # -------------------------------------------------------------------------
    # --- print(some stats for the automated fit part
    # -------------------------------------------------------------------------
    ntotx = len(where(fit_table[:, 1] == 0)[0])
    ntoty = len(where(fit_table[:, 1] == 1)[0])
    print('\n\n --- Total number of side 0 strips = '+str(ntotx))
    print(' --- Total number of side 1 strips = '+str(ntoty))
    flog.write('\n --- Total number of side 0 strips = '+str(ntotx)+'\n')
    flog.write(' --- Total number of side 1 strips = '+str(ntoty)+'\n')

    # --- No Data Cases
    qclr = where((fit_table[:, 1] == 0) & (fit_table[:, 3] == 4))
    print('\n --- Number of side 0 strips w/o events: {:.0f} ' \
      '({:.1f} %)'.format(len(qclr[0]), len(qclr[0])*1.0/ntotx*100.))
    flog.write('\n --- Number of side 0 strips w/o events: {:.0f} ' \
      '({:.1f} %)\n'.format(len(qclr[0]), len(qclr[0])*1.0/ntotx*100.))

    qclr = where((fit_table[:, 1] == 1) & (fit_table[:, 3] == 4))
    print(' --- Number of side 1 strips w/o events: {:.0f} ' \
      '({:.1f} %)'.format(len(qclr[0]), len(qclr[0])*1.0/ntoty*100.))
    flog.write(' --- Number of side 1 strips w/o events: {:.0f} ' \
      '({:.1f} %)\n'.format(len(qclr[0]), len(qclr[0])*1.0/ntoty*100.))

    # --- Failed fit cases
    qclr = where((fit_table[:, 1] == 0) & (fit_table[:, 3] == 3))
    print(' --- Number of side 0 strips w/ failed fit: {:.0f} ' \
      '({:.1f} %)'.format(len(qclr[0]), len(qclr[0])*1.0/ntotx*100.))
    flog.write(' --- Number of side 0 strips w/ failed fit: {:.0f} ' \
      '({:.1f} %)\n'.format(len(qclr[0]), len(qclr[0])*1.0/ntotx*100.))

    qclr = where((fit_table[:, 1] == 1) & (fit_table[:, 3] == 3))
    print(' --- Number of side 1 strips w/ failed fit: {:.0f} ' \
      '({:.1f} %)'.format(len(qclr[0]), len(qclr[0])*1.0/ntoty*100.))
    flog.write(' --- Number of side 1 strips w/ failed fit: {:.0f} ' \
      '({:.1f} %)\n'.format(len(qclr[0]), len(qclr[0])*1.0/ntoty*100.))

    # --- Problematic fit cases
    qclr = where((fit_table[:, 1] == 0) & (fit_table[:, 3] == 2))
    print(' --- Number of side 0 strips w/ problematic fit: {:.0f} ' \
      '({:.1f} %)'.format(len(qclr[0]), len(qclr[0])*1.0/ntotx*100.))
    flog.write(' --- Number of side 0 strips w/ problematic fit: {:.0f} ' \
      '({:.1f} %)\n'.format(len(qclr[0]), len(qclr[0])*1.0/ntotx*100.))

    qclr = where((fit_table[:, 1] == 1) & (fit_table[:, 3] == 2))
    print(' --- Number of side 1 strips w/ problematic fit: {:.0f} ' \
      '({:.1f} %)'.format(len(qclr[0]), len(qclr[0])*1.0/ntoty*100.))
    flog.write(' --- Number of side 1 strips w/ problematic fit: {:.0f} ' \
      '({:.1f} %)\n'.format(len(qclr[0]), len(qclr[0])*1.0/ntoty*100.))

    # --- Good fit cases
    qclr = where((fit_table[:, 1] == 0) & (fit_table[:, 3] == 1))
    print(' --- Number of side 0 strips w/ good fit: {:.0f} ' \
      '({:.1f} %)'.format(len(qclr[0]), len(qclr[0])*1.0/ntotx*100.))
    flog.write(' --- Number of side 0 strips w/ good fit: {:.0f} ' \
      '({:.1f} %)\n'.format(len(qclr[0]), len(qclr[0])*1.0/ntotx*100.))

    qclr = where((fit_table[:, 1] == 1) & (fit_table[:, 3] == 1))
    print(' --- Number of side 1 strips w/ good fit: {:.0f} ' \
      '({:.1f} %)\n'.format(len(qclr[0]), len(qclr[0])*1.0/ntoty*100.))
    flog.write(' --- Number of side 1 strips w/ good fit: {:.0f} ' \
      '({:.1f} %)\n\n'.format(len(qclr[0]), len(qclr[0])*1.0/ntoty*100.))

    # -------------------------------------------------------------------------
    # --- Write fit results to text file
    # -------------------------------------------------------------------------
    fout = open(data_dir+file_name[:fext_num[data_type]]+'_autoFit.dat', 'w+')
    fout.write('#  Common mode subtracted data: ' + cm_mode + '\n')
    fout.write('#  \n')
    fout.write('#  Detector   LayerSide   Strip   FitFlag   Peak   Peak_error'\
      '   Sigma   Sigma_error   Norm   Norm_error\n')
    for i in range(fit_table.shape[0]):
      fout.write(' {:3.0f}   {:3.0f}   {:3.0f}   {:3.0f}   {:8.4f}   ' \
        '{:8.4f}   {:8.4f}   {:8.4f}   {:8.4f}   {:8.4f}\n'.format(\
        fit_table[i, 0], fit_table[i, 1], fit_table[i, 2], fit_table[i, 3], \
        fit_table[i, 4], fit_table[i, 5], fit_table[i, 6], fit_table[i, 7], \
        fit_table[i, 8], fit_table[i, 9]))
    fout.close()

    flog.write(' --- Fit results saved to file: ' + data_dir + \
      file_name[:fext_num[data_type]]+'.dat \n')

    dend = datetime.utcnow()
    print(' --- Execution time [sec] = {:.5f}'.format(\
      (dend - dstart).total_seconds()))
    flog.write(' --- Total execution time [sec] = {:.5f}\n'.format(\
      (dend - dstart).total_seconds()))
    flog.write(' --- Analysis end [UTC]: ' + \
      dend.strftime('%Y-%m-%d %H:%M:%S\n'))
    flog.close()
  else:
    print(' *** No data file found: ' + data_dir + file_name)

if __name__ == "__main__":
  if (len(sys.argv) == 5):
    data_dir = str(sys.argv[1]).rstrip()
    if (data_dir[-1] != '/'):
      data_dir = data_dir + '/'
    file_name = str(sys.argv[2]).rstrip()
    data_type = str(sys.argv[3]).rstrip()
    cm_mode = str(sys.argv[4]).rstrip()
    fit_emission_line(file_name, data_dir, data_type=data_type, cm_mode=cm_mode)
  elif (len(sys.argv) == 4):
    data_dir = str(sys.argv[1]).rstrip()
    if (data_dir[-1] != '/'):
      data_dir = data_dir + '/'
    file_name = str(sys.argv[2]).rstrip()
    data_type = str(sys.argv[3]).rstrip()
    fit_emission_line(file_name, data_dir, data_type=data_type)
  elif (len(sys.argv) == 3):
    data_dir = str(sys.argv[1]).rstrip()
    if (data_dir[-1] != '/'):
      data_dir = data_dir + '/'
    file_name = str(sys.argv[2]).rstrip()
    fit_emission_line(file_name, data_dir)
  elif (len(sys.argv) == 2):
    file_name_tmp = str(sys.argv[1]).rstrip()
    file_name = file_name_tmp.split('/')[-1]
    data_dir = file_name_tmp[:-len(file_name_tmp.split('/')[-1])]
    if (data_dir[-1] != '/'):
      data_dir = data_dir + '/'
    fit_emission_line(file_name, data_dir)
  else:
    print('\n *** Use error: incorrect number of arguments')
    print('\n     Use examples:')
    print('       fit_emline_auto.py <file_name>')
    print('       fit_emline_auto.py <data_dir> <file_name>')
    print('       fit_emline_auto.py <data_dir> <file_name> <data_type> ')
    print('       fit_emline_auto.py <data_dir> <file_name> <data_type> ')
    print('       fit_emline_auto.py <data_dir> <file_name> <data_type> '\
      '<cm_mode>')
