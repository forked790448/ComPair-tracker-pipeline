'''
2023-01-24  Anna Zajczyk

  Tool to plot energy-calibrated spectra for a selected tracker layer. User
  is prompted to select the tracker layer for which to plot the spectra.
  Spectra for side 0/A and side 1/B of the selected tracker layer are plotted.
  For each spectrum the number of counts in the first bin is forced to ZERO.

  To run the tool an auxilary HDF5 file that contains the energy-calibrated events
  needs to be created first. This can be easily achieved with the following tool:

    prep_big_data.py

  To obtain the energy-calibrated events with prep_big_data.py, it should be run

    prep_big_data.py <file_name> <action_flag> <ecalib_name>

  where:

    file_name is the name of the input h5 file from L1 stage (with full access path)
    action_flag should be set to ene
    ecalib_name is the name of the energy calibration file (with full access path)


  Updates
  -------
  2023-01-26   Added:
                - identification of masked channels. Masked channel spectra
                  are then set to ZEROs
                - plotting of layer/side spectra on logarithmic and linear
                  scales
                - saving of the spectral plot
                - saving of the spectral tables into HDF5 for later use

'''

import sys
import h5py

from datetime import datetime
import matplotlib.pyplot as plt
from numpy import array, where, isnan, arange, histogram, zeros

from ecalib_functions import convert_mask


def prep_layer_data(layer_side, data_arr, mask):
  layer_data = array(data_arr[:, layer_side, :])

  for i in range(0, 192):
    if (mask[layer_side, i] != 1):
      strip_data = array(data_arr[:, layer_side, i])

      ix1 = where(isnan(strip_data))[0]
      layer_data[ix1, i] = 0.0
      strip_data[ix1] = 0.0

      ix2 = where(strip_data < 0.0)[0]
      strip_data[ix2] = 0.0
      layer_data[ix2, i] = 0.0
    else:
      layer_data[:, i] = 0.0
  return layer_data


def layer_spectra(file_name, h5, detector_number):
  fsig = file_name[:-3]+'p5.h5'
  h5sig = h5py.File(fsig, 'r')

  detector_name = 'layer{:02d}'.format(detector_number)
  mask = convert_mask(h5, detector_name)

  print(' >>> Reading energy values from: ' + fsig.split('/')[-1])
  isENE = ('/' + detector_name + '/vdata/channel_energy' in h5sig)
  if (isENE):
    en_arr = h5sig['/' + detector_name + '/vdata/channel_energy']
  else:
    print('\n *** Warning: no /vdata/channel_energy dataset present in HDF5 file!')
    print(' ***    Use: prep_big_data.py to create such dataset.')
    print(' ***    Exiting detector_spectra_layer.py ...')
    sys.exit()

  print(' >>> Creating energy bins')
  bin_edges = arange(0.0, 1000.0, 1)
  counts_layer = zeros((len(bin_edges)-1, 2))
  nevent = en_arr.shape[0]

  slice_size = 100000
  if (nevent > 3*slice_size):
    nslice = nevent // slice_size
    nrest = nevent % slice_size
    print('\n >>> Number of {:d} event slices = {:d} (with rest {:d})'.format(slice_size, nslice, nrest))
    print('     --- Slice.No   Evt_start   Evt_end   (exec.time) ---')

    for m in range(nslice):
      tmp_s = datetime.utcnow()
      if (m == 0):
        i_st = 0
        i_ed = (m+1)*slice_size
      elif (m == (nslice-1)):
        i_st = m*slice_size
        i_ed = (m+1)*slice_size+nrest
      else:
        i_st = m*slice_size
        i_ed = (m+1)*slice_size

      layer_side = 0
      layer_ene = prep_layer_data(layer_side, en_arr[i_st:i_ed, :, :], mask)
      counts, bins = histogram(layer_ene, bin_edges)
      counts_layer[:, layer_side] = counts_layer[:, layer_side] + counts

      layer_side = 1
      layer_ene = prep_layer_data(layer_side, en_arr[i_st:i_ed, :, :], mask)
      counts, bins = histogram(layer_ene, bin_edges)
      counts_layer[:, layer_side] = counts_layer[:, layer_side] + counts

      tmp_e = datetime.utcnow()
      print('           {:d}   {:d}   {:d}   ({:.2f} s) '.format(m, i_st, i_ed, (tmp_e - tmp_s).total_seconds()))

  else:
    layer_side = 0
    layer_ene = prep_layer_data(layer_side, en_arr, mask)
    counts, bins = histogram(layer_ene, bin_edges)
    counts_layer[:, layer_side] = counts_layer[:, layer_side] + counts

    layer_side = 1
    layer_ene = prep_layer_data(layer_side, en_arr, mask)
    counts, bins = histogram(layer_ene, bin_edges)
    counts_layer[:, layer_side] = counts_layer[:, layer_side] + counts

  print(' >>> Plotting spectra... ')
  plt.ion()
  plt.figure(1, figsize=(10, 8))
  plt.clf()
  fname = file_name.split('/')[-1]
  plt.suptitle('File: ' + fname)

  plt.subplot(221)
  plt.title('Yscale: LOG')
  plt.ylabel('Counts')
  plt.yscale('log')
  layer_side = 0
  print(' *** Caution: Forcing the first bin to contain ZERO events!!!')
  counts_layer[0, layer_side] = 0
  plt.stairs(counts_layer[:, layer_side], bin_edges, label='side {:d}'.format(layer_side))
  plt.legend()
  plt.show()

  plt.subplot(223)
  plt.xlabel('Energy [keV]')
  plt.ylabel('Counts')
  plt.yscale('log')
  layer_side = 1
  print(' *** Caution: Forcing the first bin to contain ZERO events!!!')
  counts_layer[0, layer_side] = 0
  plt.stairs(counts_layer[:, layer_side], bin_edges, label='side {:d}'.format(layer_side))
  plt.legend()
  plt.show()

  plt.subplot(222)
  plt.title('Yscale: LINEAR')
  layer_side = 0
  print(' *** Caution: Forcing the first bin to contain ZERO events!!!')
  counts_layer[0, layer_side] = 0
  plt.stairs(counts_layer[:, layer_side], bin_edges, label='side {:d}'.format(layer_side))
  plt.legend()
  plt.show()

  plt.subplot(224)
  plt.xlabel('Energy [keV]')
  layer_side = 1
  print(' *** Caution: Forcing the first bin to contain ZERO events!!!')
  counts_layer[0, layer_side] = 0
  plt.stairs(counts_layer[:, layer_side], bin_edges, label='side {:d}'.format(layer_side))
  plt.legend()
  plt.show()


  user_input = input(
    '\n >>> Do you want to save plots to a PNG file (yes / no)? ')
  user_input = user_input.strip()
  if (user_input == 'yes') | (user_input == 'y') | (user_input == '1'):
    figure_name = file_name[:-3]+'_SingleLayerSpectra_' + detector_name + '.png'
    plt.savefig(figure_name)
    print(' >>> Plots saved to: ')
    print(' >>>        ' + figure_name)

  user_input = input(
    '\n >>> Do you want to save spectra to a HDF5 file (yes / no)? ')
  user_input = user_input.strip()
  if (user_input == 'yes') | (user_input == 'y') | (user_input == '1'):
    print(' >>> Saving spectra... ')
    fspec = file_name[:-3]+'_SingleLayerSpectra_' + detector_name + '.h5'
    h5out = h5py.File(fspec, 'a')

    dset_name = '/'+detector_name+'/bin_edges'
    isSPEC = (dset_name in h5out)
    if (isSPEC):
      print(' *** Found in hdf5 file dataset: ' + dset_name + ' and will overwrite it')
      del h5out[dset_name]
    dset_bin_edges = \
      h5out.create_dataset(dset_name, bin_edges.shape)
    dset_bin_edges[:] = bin_edges[:]

    dset_name = '/'+detector_name+'/spectra'
    isSPEC = (dset_name in h5out)
    if (isSPEC):
      print(' *** Found in hdf5 file dataset: ' + dset_name + ' and will overwrite it')
      del h5out[dset_name]
    dset_spectra = \
      h5out.create_dataset(dset_name, counts_layer.shape)
    dset_spectra[:, :] = counts_layer[:, :]

    h5out.close()
    print(' >>> Spectra saved to: ')
    print(' >>>        ' + fspec)


def detector_spectra_L1p5(file_name):
  h5 = h5py.File(file_name, 'r')
  h5_group_names = array([x for x in h5])
  q = where(h5_group_names != 'vdata')[0]
  h5_group_names = h5_group_names[q]

  print('\n >>> Number of tracker layers: ', len(h5_group_names))
  for p in h5_group_names:
    print(' >>>           ', p)

  user_input = input(
    '\n >>> Define detector number (0-9): ')
  if user_input:
    detector_number = user_input.strip()
  else:
    print(' *** Ooops! something went wrong, let\'s try again...')
    detector_number = input(
      '\n >>> Define detector number (0-9): ').strip()

  detector_number = int(detector_number)

  layer_spectra(file_name, h5, detector_number)


if __name__ == "__main__":
  if (len(sys.argv) == 2):
    file_name = str(sys.argv[1]).rstrip()
    detector_spectra_L1p5(file_name)
  else:
    print('\n *** Use error: incorrect number of arguments')
    print('\n     Use examples:')
    print('       detector_spectra_L1p5.py <file_name>')
