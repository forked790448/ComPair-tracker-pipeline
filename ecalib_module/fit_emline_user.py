'''
2021-05-06  Anna Zajczyk

  Perform emission line fitting for the selected strip with user input.
  Emission line shape is assumed to be well approximated by a Gaussian.
  User can 'accept' the fit or 'reject and refit'. After the fit is
  complete user can perform emission line fitting for additional strips.
  Results can be saved to a text file.


  Updates
  -------
  2022-08-07 Added option to rebin the data to user-selected bin width.
              The bin size is an integer, so if float is given as an input
              its type will be casted to integer. There is no intelligent
              handling of the conversion float -> integer like ceiling or
              floor or rounding.
  2022-08-06 Added treatment of HDF5 that contain binned spectra
  2022-03-28 Modified how the data from hdf5 file is read in.
              was: read whole file up fron and then do spectral shenanigans
              now: read only the data for user-defined strip,
                   also
                   removed option for manipulation on sims data,
                   and calculation of common mode subtraction on-the-fly
  2021-07-27 Finished update to docstrings to reflect most recent changes
  2021-07-16 Added treatment of HDF5 that contain multi-layer data
  2021-07-16 Added treatment of common mode subtraction

'''

import os
import sys
import h5py

import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from scipy.stats import chisquare
from numpy import sqrt, diag, argmax, append, array, where

from ecalib_functions import plot_hist, fgauss, bin_data2, get_channel_mask, \
  rebin_data, select_bin_size


def analyze_strip_spectrum(
    h5, key, layer_side, strip_number, detector_number, data_type, bin_size,
    cm_mode=None):
  """Performs line fitting for the selected strip.

  Performs line fitting for the selected strip. Strip selection is
  based on provided `strip number`, `layer side` and `detector number`
  inputs.

  Parameters
  ----------
  h5: hdf5
    HDF5 file
  key: str
    Path name to the HDF5 dataset
  layer_side: int
    Intiger (0, 1) defining q top/bottom side of a Si layer.
  strip_number: int
    Integer (0, 191) defining a strip number in a selected Si layer.
  detector_number: int
    Integer (0, 9) defining a layer number in Si tracker.
  data_type: str
    String defining the type of the input data. The three allowed
    types are: 'sims' (.sim input data file created in Megalib
    simulations), 'multi' (.h5 input data file containing observations
    collected with Si tracker and converted to hdf5 format; multi-layer
    format), 'single' (.h5 input data file containing observations
    collected with Si tracker and converted to hdf5 format; single-layer
    format), and 'binned' (.h5 input data file containing binned
    data created with bin_h5data.py).
  cm_mode: str
    Flag (yes / no) that selects if the common mode subtracted data
    should be used in the analysis. If common mode subtracted data are
    not present in the file, the common mode subtracted data will be
    calculated. The default option is to use common mode subtracted data.

  Returns
  -------
  fit_row: array_like
    Table with results of the emission line fit for the selected strip.

  """
  if (cm_mode is None):
    cm_mode = 'yes'

  if (data_type == 'binned'):
    rbins = array(h5['/edges'][:, 0])
    redglo = array(h5['/edges'][:, 1])
    redghi = array(h5['/edges'][:, 2])
    rhist = array(h5[key][layer_side, strip_number, :])
    if (bin_size != 1):
      rhist, rbins, redglo, redghi = rebin_data(\
        rhist, rbins, bin_size)
    rerror = 1.0 + sqrt(rhist + 0.75)
    no_data_flag = False
  else:
    strip_spec = array(h5[key][:, layer_side, strip_number])
    if (len(strip_spec) != 0):
      rhist, rbins, redglo, redghi = bin_data2(strip_spec, bin_size)
      rerror = 1.0 + sqrt(rhist+0.75)
      no_data_flag = False
    else:
      no_data_flag = True
      rhist, rbins, rerror = 0, 0, 0

  if (no_data_flag is False):
    try:
      fit_flag = True
      while(fit_flag):
        plt.ion()
        plt.figure(1)
        plt.clf()
        plt.title('Layer: '+str(detector_number)+'   Side: '+str(layer_side)+\
          '   Strip: '+str(strip_number))
        plt.xlabel('PH [ADC]')
        plt.ylabel('Counts/bin')
        redglo = rbins - 0.5*(rbins[1]-rbins[0])
        redghi = rbins + 0.5*(rbins[1]-rbins[0])
        plot_hist(rhist, rbins, redglo, redghi, color='violet')
        plt.show()
        plt.pause(0.0001)

        fit_ll, fit_ul = input(\
          ' >>> Define LOWER and UPPER limit for the fit region: ').split()
        fit_ll, fit_ul = [int(fit_ll), int(fit_ul)]

        qf = ((fit_ll < rbins) & (rbins < fit_ul))
        pinit = [max(rhist[qf]), rbins[qf][argmax(rhist[qf])], 1.]
        print('\n Total number of counts in the fit region: ')
        print('   {:.0f} \n'.format(sum(rhist[qf])))

        print('\n Initial parameters for the fit (norm, centroid, sigma): ')
        print('   {:.4f}   {:.4f}   {:.4f}'.format(\
          pinit[0], pinit[1], pinit[2]))
        fit_params = input(' >>> Execute the fit with these initial'
          ' parameters (yes / no): ')
        if (fit_params.strip() == 'no'):
          pi1, pi2, pi3 = input(' >>> Define initial parameters for'
            ' the fit (norm, centroid, sigma): ').split()
          pinit[0] = float(pi1)
          pinit[1] = float(pi2)
          pinit[2] = float(pi3)

        p1,t1 = curve_fit(
          fgauss, rbins[qf], rhist[qf], p0=pinit, sigma=rerror[qf],
          absolute_sigma=True)
        perr1 = sqrt(diag(t1))

        plt.figure(1)
        plt.clf()
        plt.suptitle('Layer: '+str(detector_number)+'   Side: '+str(layer_side)+\
          '   Strip: '+str(strip_number))
        plt.subplot(211)
        # plt.xlabel('PH [ADC]')
        plt.ylabel('Counts/bin')
        plt.xlim(fit_ll, fit_ul)
        plt.ylim(0, 1.1*max(rhist[qf]))
        plot_hist(rhist, rbins, redglo, redghi, color='violet')
        plt.plot(rbins[qf], fgauss(rbins[qf], p1[0], p1[1], p1[2]), '-.', \
          color='red', alpha=0.5)
        plt.show()

        plt.subplot(212)
        plt.xlabel('PH [ADC]')
        plt.ylabel('Data - Fit')
        plt.xlim(fit_ll, fit_ul)
        ltmp_min = min(rhist[qf]-fgauss(rbins[qf], p1[0], p1[1], p1[2]))
        ltmp_max = max(rhist[qf]-fgauss(rbins[qf], p1[0], p1[1], p1[2]))
        tmp_dev = abs(ltmp_max - ltmp_min)/10.
        plt.ylim(ltmp_min-2*tmp_dev, ltmp_max+2*tmp_dev)
        plt.plot(rbins[qf], rhist[qf]-fgauss(rbins[qf], p1[0], p1[1], p1[2]), 'o', \
          color='red', alpha=0.5)
        plt.plot([fit_ll, fit_ul], [0, 0], ':', color='gray')
        plt.show()

        plt.pause(0.0001)

        print('\n -----------------------------------------------------------')
        print(' Best fit parameters for a Gaussian model')
        print('                Norm = {:11.5f} +/- {:9.5f}'.format(p1[0], \
          perr1[0]))
        print('       Peak centroid = {:11.5f} +/- {:9.5f}'.format(p1[1], \
          perr1[1]))
        print('               Sigma = {:11.5f} +/- {:9.5f}'.format(p1[2], \
          perr1[2]))
        print(' -----------------------------------------------------------\n')

        fit_row = [detector_number, layer_side, strip_number, 1, p1[1], perr1[1],
          p1[2], perr1[2], p1[0], perr1[0]]

        fit_inpt = input(' >>> Refit the spectrum (yes / no / [f]ail): ')
        if (fit_inpt.strip() == 'yes') | (fit_inpt.strip() == '1') \
          | (fit_inpt.strip() == 'y'):
          fit_flag = True
        elif(fit_inpt.strip() == 'f'):
          fit_flag = False
          fit_row = [detector_number, layer_side, strip_number, 3,
            -1, -1, -1, -1, -1, -1]
        elif(fit_inpt.strip() == 'no') | (fit_inpt.strip() == '0'):
          fit_flag = False
        else:
          fit_flag = False

    except:
      print(' *** Warning: Fit failed for strip (layer / side / '
        'strip): ' + str(detector_number) + ' / '+ str(layer_side) +\
        ' / ' + str(strip_number))
      fit_row = [detector_number, layer_side, strip_number, 3,
        -1, -1, -1, -1, -1, -1]
  else:
    print(' *** No events registered by strip (layer / side / '
      'strip): ' + str(detector_number) + ' / '+ str(layer_side) +\
      ' / ' + str(strip_number))
    fit_row = [detector_number, layer_side, strip_number, 4,
      -1, -1, -1, -1, -1, -1]

  return fit_row

def fit_emission_line(file_name, data_dir=None, data_type=None, cm_mode=None):
  """Function that allows for interactive emission line fitting.

  The script runs interactive smission line fitting. User is asked
  to provide layer number, layer side and strip number for which
  the line fitting will be performed. User will be shown a spectrum
  for the selected strip and asked to provide an upper and a lower
  limit (in ADC) for the fit. A Gaussian function will be fitted.
  Fit results will be printed on the screen. User will be asked
  whether: to refit the line (yes / no), to perform fit for another
  strip (yes / no), and to save results of the fit to a text file
  (yes / no). The text file can be merged with the one from
  the automated fitting, and fed as an input to the energy calibration
  fitting script.

  Parameters
  ----------
  file_name: str
    File name
  data_dir: str
    Directory access path where the input file sits. The access
    path must end with a forward slash, e.g., 'dir/subdir/' .
    When no path is given, it is assumed that the input file is
    in the current directory './' .
  data_type: str
    String defining the type of the input data. The three allowed
    types are: 'sims' (.sim input data file created in Megalib
    simulations), 'multi' (.h5 input data file containing observations
    collected with Si tracker and converted to hdf5 format; multi-layer
    format), 'single' (.h5 input data file containing observations
    collected with Si tracker and converted to hdf5 format; single-layer
    format), and 'binned' (.h5 input data file containing binned
    data created with bin_h5data.py). When no type is given, it is assumed
    that the input data are of 'multi' type.
  cm_mode: str
    Flag (yes / no) that selects if the common mode subtracted data
    should be used in the analysis. If common mode subtracted data are
    not present in the file, the common mode subtracted data will be
    calculated. The default option is to use common mode subtracted data.
  """
  plt.close('all')
  plt.ion()

  if (data_dir is None):
    data_dir = './'

  if (data_type is None):
    data_type = 'multi'   # what data is fed to the program

  if (cm_mode is None):
    cm_mode = 'yes'

  fext_num = {'multi':-3, 'binned':-3}

  if (os.path.isfile(data_dir+file_name)):
    fit_table = []   # will store gauss coefficients here [L#, SD#,
                     # ST#, FLG, peak_centroid, peak_centroid_error,
                     # gauss_sigma, gauss_sigma_error,
                     # norm, norm_error]

    h5 = h5py.File(data_dir+file_name, 'r')
    group_names = array([x for x in h5])
    q = where(group_names != 'vdata')[0]
    group_names = group_names[q]
    nlayer = len(group_names)

    print('\n >>> Number of tracker layers in the file: {:d}'.format(nlayer))
    for i in group_names:
      print('       ' + i)

    next_strip_flag = True
    while (next_strip_flag):
      plt.close('all')   # close any existing plots

      detector_number, layer_side, strip_number = input(
        '\n >>> Define detector'\
        ' number (0-9), layer side (0 / 1), and strip number (0-191) for '\
        'spectral fit: '\
        ).split()

      detector_number = int(detector_number)
      strip_number = int(strip_number)
      layer_side = int(layer_side)

      bin_size = select_bin_size()

      if ((data_type == 'multi') & (file_name[fext_num[data_type]:] == '.h5')):
        detname = 'layer{:02d}'.format(detector_number)
        channel_mask = get_channel_mask(h5)
        single_channel_mask = channel_mask[detname][layer_side, strip_number]
        if (cm_mode == 'no'):
          print(' --- MULTI: working on channel_data')
          key = '/' + detname + '/vdata/channel_data'
        elif(cm_mode == 'yes'):
          print(' --- MULTI: working on channel_cm_sub')
          key = '/' + detname + '/vdata/channel_cm_sub'

      elif ((data_type == 'binned') & \
        (file_name[fext_num[data_type]:] == '.h5')):
        detname = 'layer{:02d}'.format(detector_number)
        single_channel_mask = array(h5['/' + detname + '/chan_disable'][layer_side, strip_number])
        key = '/' + detname + '/binned_data'
        print(' --- BINNED: working on binned_data')
      else:
        print('\n *** Error: Mismatch between the selected data file and '\
          'the chosen data type found.')
        print(' ***    Selected data file: '+file_name)
        print(' ***    Selected data type: '+data_type)
        sys.exit()

      if (single_channel_mask == 0):
        fit_row = analyze_strip_spectrum(
          h5, key, layer_side, strip_number, detector_number,
          data_type, bin_size, cm_mode)
        plt.pause(0.0001)
      else:
        print(' --- !!! This channel is masked: skipping fitting')
        fit_row = [detector_number, layer_side, strip_number, 5,
          -1, -1, -1, -1, -1, -1]

      fit_table.append(fit_row)

      next_strip_flag = input(' >>> Fit another strip (yes / no): ')
      if (next_strip_flag.strip() == 'yes') | (next_strip_flag.strip() == '1') \
        | (next_strip_flag.strip() == 'y'):
        next_strip_flag = True
      elif(next_strip_flag.strip() == 'no'):
        next_strip_flag = False
      else:
        next_strip_flag = False

    if (cm_mode == 'no'):
      h5.close()


    fit_table = array(fit_table)

    write_inpt = input(' >>> Save results to an output file? (yes / no): ')
    # --- Write fit results to a text file
    if (write_inpt.strip() == 'yes') | (write_inpt.strip() == '1') \
      | (write_inpt.strip() == 'y'):
      fout = open(
        data_dir+file_name[:fext_num[data_type]]+'_userFit.dat', 'a')
      fout.write('#  Detector   LayerSide   Strip   FitFlag   Peak   '\
        'Peak_error   Sigma   Sigma_error   Norm   Norm_error\n')
      for i in range(fit_table.shape[0]):
        fout.write(' {:3.0f}   {:3.0f}   {:3.0f}   {:3.0f}   {:8.4f}   ' \
          '{:8.4f}   {:8.4f}   {:8.4f}   {:8.4f}   {:8.4f}\n'.format(\
          fit_table[i, 0], fit_table[i, 1], fit_table[i, 2], fit_table[i, 3], \
          fit_table[i, 4], fit_table[i, 5], fit_table[i, 6], fit_table[i, 7], \
          fit_table[i, 8], fit_table[i, 9]))
      fout.close()
      print(' --- Results saved to: '+file_name[:fext_num[data_type]]+'_userFit.dat')
  else:
    print(' *** No data file found: '+data_dir+file_name)

if __name__ == "__main__":
  if (len(sys.argv) == 4):
    data_dir = str(sys.argv[1]).rstrip()
    if (data_dir[-1] != '/'):
      data_dir = data_dir + '/'
    file_name = str(sys.argv[2]).rstrip()
    data_type = str(sys.argv[3]).rstrip()
    fit_emission_line(file_name, data_dir, data_type)
  elif (len(sys.argv) == 5):
    data_dir = str(sys.argv[1]).rstrip()
    if (data_dir[-1] != '/'):
      data_dir = data_dir + '/'
    file_name = str(sys.argv[2]).rstrip()
    data_type = str(sys.argv[3]).rstrip()
    cm_mode = str(sys.argv[4]).rstrip()
    fit_emission_line(file_name, data_dir, data_type, cm_mode)
  elif (len(sys.argv) == 3):
    data_dir = str(sys.argv[1]).rstrip()
    if (data_dir[-1] != '/'):
      data_dir = data_dir + '/'
    file_name = str(sys.argv[2]).rstrip()
    fit_emission_line(file_name, data_dir)
  elif (len(sys.argv) == 2):
    file_name_tmp = str(sys.argv[1]).rstrip()
    file_name = file_name_tmp.split('/')[-1]
    data_dir = file_name_tmp[:-len(file_name_tmp.split('/')[-1])]
    if (data_dir[-1] != '/'):
      data_dir = data_dir + '/'
    fit_emission_line(file_name, data_dir)
  else:
    print('\n *** Use error: incorrect number of arguments')
    print('\n     Use examples:')
    print('       fit_emline_user.py <file_name>')
    print('       fit_emline_user.py <data_dir> <file_name>')
    print('       fit_emline_user.py <data_dir> <file_name> <data_type>')
    print('       fit_emline_user.py <data_dir> <file_name> <data_type> <cm_mode>')
