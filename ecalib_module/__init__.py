"""energy_calibration_module

Package dedicated to finding energy calibration for the tracker.
It includes the emission line fitting scripts.

"""
