'''
2023-03-14  Anna Zajczyk

  This tool compares raw and common mode subtracted spectra for given input files.
  The tool was created to visualize the emergence of the secondary noise peak
  (double peak noise behavior) in 9-layer Co57 data collected on 2023-01-25.

  This tool plots for each input file:
   1) The raw and common mode subtracted spectra for the two Co57 input files.
      The 2x1 plot is created with top panel showing raw data, while bottom 
      panel showing common mode subtracted data.

      Noise peaks are fitted with a gaussian for both types of spectra, and baseline
      shift is calculated. The values of the peak centroid and the calculated baseline
      shift are also displayed.

      This version of the plot is created when plot_flag = True

      When: plot_flag = False , the plot structure changes to 2x2 with the left
      column displaying spectra (top = raw, bottom = common mode subtracted), and 
      the right column showing the peak height versus event sequence number 
      (top = for full data set, bottom = for first 1000 events).

    2) The peak height (in ADUs) as a function of event sequence number (very loosely 
      a proxy for time).

  Though written to inspect two datasets of Co57 data, this tool can be used to
  compare any two data files. Just make sure that both files contain the layer data
  you want to visualize.

  User needs to choose for which layer / side / strip to create the comparison plot.

  Parameters
  ----------
  data_dir: str
    Full access path to the directory where the input HDF5 source data file resides.
    Remember to include the forward slash at the end of the access path.
  file_name_tbl: list
    List of the input file names containing data that you want to inspect.
  detector_number: int
    Number of the tracker layer for which to inspect the data. Tracker layers span
    0-9 range (assembly of the 9-th layer is in the workings at the moment).  
  layer_side: int
    Tracker side number spans 0-1 range, where 0 is top/A, and 1 is bottom/B.
  strip_number: int
    Strip number on the tracker layer / side. The strip number spans 0-191 range.
  plot_flag: bool
    Flag that changes how the input data are displayed. The flag values are:
      True or False

  Returns
  -------
  plots
    Two plots are created (see description above). Only plot 1) is saved.
    The 1) plot is saved to the following location and file:
      if (plot_flag == True): 
        data_dir + 'NoisePeaks_' + file_name[:-3] + '_layer{:d}_side{:d}_strip{:d}_basic.png'
      if (plot_flag == False):
        data_dir + 'NoisePeaks_' + file_name[:-3] + '_layer{:d}_side{:d}_strip{:d}.png'

  Updates
  -------
  YYYY-MM-DD
'''

import h5py

import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from numpy import *

from ecalib_functions import fgauss, plot_hist, find_element, convert_mask

data_dir = '/Users/azajczyk/work/AMEGO/Si_tracker/data/hdf5/LaboratoryDataFiles/multi_layer/20230303/'
file_name_tbl = ['2023-03-03_1003_9Layers_Co57_L2.h5']

plot_flag = True

detector_number = 3
layer_side = 0
strip_number = 93

for file_name in file_name_tbl:
  print(' --- Working with file: ' + file_name)
  h5 = h5py.File(data_dir+file_name, 'r')
  group_names = array([x for x in h5])
  q = where(group_names != 'vdata')[0]
  group_names = group_names[q]
  nlayer = len(group_names)

  print('\n >>> Number of tracker layers in the file: {:d}'.format(nlayer))
  for i in group_names:
    print('       ' + i)

  # --- Prepare COMMON BIS to be used for all input data
  bin_min = -250
  bin_max = 1024
  binw = 1

  bins = arange(bin_min, bin_max, binw)
  nbins = len(bins)

  edge_low = bins - 0.5*binw
  edge_high = bins + 0.5*binw
  edges = append(edge_low, [edge_high[-1]])

  detector_name = 'layer{:02d}'.format(detector_number)
  print(' --- Dealing with: ' + detector_name + ' / ' + str(layer_side) + ' / ' + str(strip_number))

  # --- Read in data for the strip
  strip_data_raw = array(h5['/' + detector_name + '/vdata/channel_data'][:, layer_side, strip_number])
  strip_data_cm = array(h5['/' + detector_name + '/vdata/channel_cm_sub'][:, layer_side, strip_number])
  event_id = array(h5['/' + detector_name + '/data/event_id'])
  mask = convert_mask(h5, detector_name)
  if (mask[layer_side, strip_number] == 1):
    print(' *** Dealing with MASKED channel ***')

  nevent1 = strip_data_raw.shape[0]
  nevent2 = strip_data_cm.shape[0]
  print(' --- Total number of events   RAW: {:d}   CM: {:d}'.format(nevent1, nevent2))

  pinit1 = [1e4, 200, 10]
  strip_hist_raw, edges_tmp = histogram(strip_data_raw, bins=edges)
  q = (strip_hist_raw > 0)
  p1,t1 = curve_fit(fgauss, bins[q], strip_hist_raw[q], p0=pinit1)
  # perr1 = sqrt(diag(t1))   # --- if you want to know errors
  xnoise = p1[1] + abs(5*p1[2])
  ixn_hi = find_element(bins, xnoise)
  xnoise = p1[1] - abs(5*p1[2])
  ixn_lo = find_element(bins, xnoise)
  noise_cnt1 = sum(strip_hist_raw[ixn_lo:ixn_hi]) * 100 / nevent1

  pinit2 = [1e4, 0, 10]
  strip_hist_cm, edges_tmp = histogram(strip_data_cm, bins=edges)
  p2,t2 = curve_fit(fgauss, bins, strip_hist_cm, p0=pinit2)
  # perr2 = sqrt(diag(t2))   # --- if you want to know errors
  xnoise = p2[1] + abs(5*p2[2])
  ixn_hi = find_element(bins, xnoise)
  xnoise = p2[1] - abs(5*p2[2])
  ixn_lo = find_element(bins, xnoise)
  noise_cnt2 = sum(strip_hist_cm[ixn_lo:ixn_hi]) * 100 / nevent2
  print(' --- Total number of events in noise peak   RAW: {:.1f} %    CM: {:.1f} %'.format(noise_cnt1, noise_cnt2))

  bs_shift = p1[1] - p2[1]

  print(' --- Calculated baseline shift [ADU] = {:.1f}'.format(bs_shift))

  ymax = 2*max([max(strip_hist_raw), max(strip_hist_cm)])

  if (plot_flag):
    plt.close('all')
    plt.ion()
    plt.figure(1, figsize=(12,8))
    plt.clf()
    plt.suptitle('Layer: '+str(detector_number)+'   Side: '+str(layer_side)+\
      '   Strip: '+str(strip_number))
    plt.subplot(211)
    plt.yscale('log')
    plt.ylim(bottom=1e-2, top=ymax)
    plt.ylabel('Counts')
    plot_hist(strip_hist_raw, bins, edge_low, edge_high, color='violet')
    plt.plot(bins, fgauss(bins, p1[0], p1[1], p1[2]), ':', color='grey', alpha=0.8, label='gaus.fit')
    plt.text(600, 1e3, 'raw data', color='blue')
    plt.text(600, 1e2, 'peak: {:.1f} ADU'.format(p1[1]), color='purple')
    if (mask[layer_side, strip_number] == 1):
      plt.text(-200, 1e3, '*** masked ***', color='black')
    plt.legend()

    plt.subplot(212)
    plt.yscale('log')
    plt.ylim(bottom=1e-2, top=ymax)
    plt.xlabel('Peak height [ADU]')
    plt.ylabel('Counts')
    plot_hist(strip_hist_cm, bins, edge_low, edge_high, color='violet')
    plt.plot(bins, fgauss(bins, p2[0], p2[1], p2[2]), ':', color='grey', alpha=0.8, label='gaus.fit')
    plt.text(600, 1e3, 'common mode', color='blue')
    plt.text(600, 0.3e3, 'subtracted data', color='blue')
    plt.text(600, 1e1, 'peak: {:.1f} ADU'.format(p2[1]), color='purple')
    plt.text(600, 1e-0, 'shift: {:.1f} ADU'.format(bs_shift), color='green')
    plt.legend()
    plt.show()
    plt.savefig(data_dir + 'NoisePeaks_' + file_name[:-3] + \
      '_layer{:d}_side{:d}_strip{:d}'.format(detector_number, layer_side, strip_number) + '_basic.png')
  else:
    plt.close('all')
    plt.ion()
    plt.figure(1, figsize=(12,8))
    plt.clf()
    plt.suptitle('Layer: '+str(detector_number)+'   Side: '+str(layer_side)+\
      '   Strip: '+str(strip_number))
    plt.subplot(221)
    plt.yscale('log')
    plt.ylim(bottom=1e-2, top=ymax)
    plt.ylabel('Counts')
    plot_hist(strip_hist_raw, bins, edge_low, edge_high, color='violet')
    plt.plot(bins, fgauss(bins, p1[0], p1[1], p1[2]), ':', color='grey', alpha=0.8, label='gaus.fit')
    plt.text(600, 1e3, 'raw data', color='blue')
    plt.text(600, 1e2, 'peak: {:.1f} ADU'.format(p1[1]), color='purple')
    if (mask[layer_side, strip_number] == 1):
      plt.text(-200, 1e3, '*** masked ***', color='black')
    plt.legend()

    plt.subplot(223)
    plt.yscale('log')
    plt.ylim(bottom=1e-2, top=ymax)
    plt.xlabel('Peak height [ADU]')
    plt.ylabel('Counts')
    plot_hist(strip_hist_cm, bins, edge_low, edge_high, color='violet')
    plt.plot(bins, fgauss(bins, p2[0], p2[1], p2[2]), ':', color='grey', alpha=0.8, label='gaus.fit')
    plt.text(600, 1e3, 'common mode', color='blue')
    plt.text(600, 0.3e3, 'subtracted data', color='blue')
    plt.text(600, 1e1, 'peak: {:.1f} ADU'.format(p2[1]), color='purple')
    plt.text(600, 1e-0, 'shift: {:.1f} ADU'.format(bs_shift), color='green')
    plt.legend()
    plt.show()

    xtbl = arange(0, strip_data_raw.shape[0], 1)
    plt.subplot(222)
    plt.ylim(bin_min, bin_max)
    plt.ylabel('Peak height [ADU]')
    plt.xlabel('Evt sequence number')
    plt.scatter(xtbl, strip_data_raw, marker='.', label='raw data')
    plt.text(0.5*xtbl[-1], -100, 'full evt.seq. range', color='blue')
    plt.legend()
    plt.show()

    plt.subplot(224)
    plt.ylim(bin_min, bin_max)
    plt.xlim(0, 1000)
    plt.ylabel('Peak height [ADU]')
    plt.xlabel('Evt sequence number')
    plt.scatter(xtbl, strip_data_raw, marker='.', label='raw data')
    plt.text(500, -100, 'first 1000 events', color='blue')
    plt.legend()
    plt.show()
    plt.savefig(data_dir + 'NoisePeaks_' + file_name[:-3] + \
      '_layer{:d}_side{:d}_strip{:d}'.format(detector_number, layer_side, strip_number) + '.png')


  uevtid = unique(event_id)
  if (uevtid.shape[0] > 1):
    plt.figure(3, figsize=(10,8))
    plt.clf()
    plt.suptitle('Layer: '+str(detector_number)+'   Side: '+str(layer_side)+\
      '   Strip: '+str(strip_number))
    xtbl = arange(0, strip_data_raw.shape[0], 1)
    plt.subplot(211)
    plt.ylim(bin_min, bin_max)
    plt.ylabel('Peak height [ADU]')
    plt.xlabel('Evt id number')
    plt.scatter(event_id, strip_data_raw, marker='.')
    plt.text(0.5*xtbl[-1], -100, 'full evt.seq. range', color='blue')
    plt.show()

    plt.subplot(212)
    plt.ylim(bin_min, bin_max)
    plt.ylabel('Peak height [ADU]')
    plt.xlabel('Evt id number')
    plt.scatter(event_id[0:1000], strip_data_raw[0:1000], marker='.')
    plt.text(500, -100, 'first 1000 events', color='blue')
    plt.show()
  else:
    print(' --- No TRIGGER MODULE: Event id numbers are all set to 1')

  xtbl = arange(0, strip_data_raw.shape[0], 1)
  q = where(strip_data_raw == 0)[0]

  if (len(q) > 0):
    plt.figure(2, figsize=(10,5))
    plt.clf()
    plt.title('Layer: '+str(detector_number)+'   Side: '+str(layer_side)+\
      '   Strip: '+str(strip_number)+'\n Zero-value events only')
    plt.ylim(bin_min, bin_max)
    plt.ylabel('Peak height [ADU]')
    plt.xlabel('Evt sequence number')
    plt.scatter(xtbl[q], strip_data_raw[q], marker='.', label='raw data')
    plt.text(0.5*xtbl[-1], -100, 'full evt.seq. range', color='blue')
    plt.legend()
    plt.show()
