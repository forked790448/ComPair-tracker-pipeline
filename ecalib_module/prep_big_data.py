'''
2021-12-01  Anna Zajczyk

  When action_flag set to the following values this tool calculates selected
  parameters and saves them to a secondary HDF5 called <file_name_L1p5.h5>.
  This is useful when inspecting large files.

  Action flags:
    sig - event significances are calculated. Results are stored in
          /layerXX/vdata/channel_sig dataset
    ene - event ADUs are converted to energy in keV using provided energy calibration
          tables. Results are stored in /layerXX/vdata/channel_energy dataset
    multi - event multiplicities "per side" are calculated. Results are stored in
         /layerXX/vdata/event_multiplicity dataset. Calculation of event multiplicity
         requires the existence/pre-calculation of the significance dataset.

  Updates
  -------


'''

import sys
import os
import os.path
import h5py
import gc

from datetime import datetime
from shutil import copy
from numpy import array, where, zeros, mean, std, concatenate
from math import nan, isnan

from ecalib_functions import read_data, convert_mask, get_significance

def apply_ecalib2data(detector_number, data_arr, ecalib_table):
  ene_arr = zeros(data_arr.shape, dtype='float')
  ene_flg = zeros(data_arr.shape, dtype='float')
  for layer_side in range(2):
    for strip_number in range(192):
      qf = where((ecalib_table[:, 0] == detector_number) &
           (ecalib_table[:, 1] == layer_side) &
           (ecalib_table[:, 2] == strip_number))
      indx = qf[0][0]
      if ((ecalib_table[indx, 3] == -2) | (ecalib_table[indx, 3] == -1)):
        ene_arr[ :, layer_side, strip_number] = nan
        ene_flg[ :, layer_side, strip_number] = ecalib_table[indx, 3]
      else:
        ene_arr[ :, layer_side, strip_number] = \
          ecalib_table[indx, 4] * \
          data_arr[ :, layer_side, strip_number] + \
          ecalib_table[indx, 6]
        ene_flg[ :, layer_side, strip_number] = ecalib_table[indx, 3]
  return ene_arr, ene_flg


def get_sig_array(file_name, file_out, slice_size=None):
  if (slice_size is None):
    slice_size = 100000

  h5 = h5py.File(file_name, 'r')   # allow to append to file
  h5out = h5py.File(file_out, 'a')   # allow to append to file

  print('\n *** Warning 1: No sync-index cleaning is applied to the data   ***')
  print(' *** Warning 2: No significance cleaning is applied to the data ***\n')

  h5_group_names = array([x for x in h5])
  q = where(h5_group_names != 'vdata')[0]
  h5_group_names = h5_group_names[q]

  print('\n >>> Number of tracker layers: ', len(h5_group_names))
  for p in h5_group_names:
    print(' >>>           ', p)

  isSIG = ('/' + h5_group_names[0] + '/vdata/channel_sig' in h5out)
  if (isSIG):
    print(' *** Found in hdf5 file dataset: /XXX/vdata/channel_sig')
    usr_input = input(' --- Overwrite the existing dataset (yes / no)? ').strip()
    if (usr_input == '1') | (usr_input == 'yes') | (usr_input == 'y'):
      for p in h5_group_names:
        del h5out['/' + p + '/vdata/channel_sig']
    else:
      print(' --- Exiting the program... ')
      sys.exit()

  for detector_name in h5_group_names:
    detector_number = int(detector_name[-2:])
    print('\n >>> working on channel_cm_sub: '+ detector_name)

    trk_data = h5['/' + detector_name + '/vdata/channel_cm_sub']
    nevent = trk_data.shape[0]
    print(' >>> Number of events = {:d}'.format(nevent))

    mask = convert_mask(h5, detector_name)

    if (nevent > 3*slice_size):
      nslice = nevent // slice_size
      nrest = nevent % slice_size
      print('\n >>> Number of {:d} event slices = {:d} (with rest {:d})'.format(slice_size, nslice, nrest))

      tmp_sig = zeros((0, 2, 192))
      dset_name = '/'+detector_name+'/vdata/channel_sig'

      h5out.create_dataset(dset_name, \
        data=tmp_sig, compression="gzip", chunks=True, maxshape=(None,2,192))

      print('\n >>> Calculating significances...')
      print('     --- Slice.No   Evt_start   Evt_end   (exec.time) ---')
      for m in range(nslice):
        tmp_s = datetime.utcnow()
        if (m == 0):
          i_st = 0
          i_ed = (m+1)*slice_size
        elif (m == (nslice-1)):
          i_st = m*slice_size
          i_ed = (m+1)*slice_size+nrest
        else:
          i_st = m*slice_size
          i_ed = (m+1)*slice_size
        data_arr = array(trk_data[i_st:i_ed, :, :])
        tmp_sig = get_significance(data_arr, mask)

        h5out[dset_name].resize((h5out[dset_name].shape[0] + tmp_sig.shape[0]), axis=0)
        h5out[dset_name][-tmp_sig.shape[0]:] = tmp_sig

        tmp_e = datetime.utcnow()
        print('           {:d}   {:d}   {:d}   ({:.2f} s) '.format(m, i_st, i_ed, (tmp_e - tmp_s).total_seconds()))

      del tmp_sig
      gc.collect()
    else:
      data_arr = array(trk_data)
      sig_arr = zeros(trk_data.shape)
      sig_arr = get_significance(data_arr, mask)

      dset_name = '/'+detector_name+'/vdata/channel_sig'
      dset_calib_sig = \
        h5out.create_dataset(dset_name, sig_arr.shape)
      dset_calib_sig[:] = sig_arr[:]
  h5out.close()


def get_ene_array(file_name, file_out, ecalib_name, slice_size=None):
  if (slice_size is None):
    slice_size = 100000

  h5 = h5py.File(file_name, 'r')   # allow to append to file
  h5out = h5py.File(file_out, 'a')   # allow to append to file
  ecalib_table, enrow, encol = read_data(ecalib_name, data_type='float')   # get energy calibration params

  print('\n *** Warning 1: No sync-index cleaning is applied to the data   ***')
  print(' *** Warning 2: No significance cleaning is applied to the data ***\n')

  h5_group_names = array([x for x in h5])
  q = where(h5_group_names != 'vdata')[0]
  h5_group_names = h5_group_names[q]

  print('\n >>> Number of tracker layers: ', len(h5_group_names))
  for p in h5_group_names:
    print(' >>>           ', p)

  isSIG = ('/' + h5_group_names[0] + '/vdata/channel_energy' in h5out)
  if (isSIG):
    print(' *** Found in hdf5 file dataset: /XXX/vdata/channel_energy')
    usr_input = input(' --- Overwrite the existing dataset (yes / no)? ').strip()
    if (usr_input == '1') | (usr_input == 'yes') | (usr_input == 'y'):
      for p in h5_group_names:
        del h5out['/' + p + '/vdata/channel_energy']
    else:
      print(' --- Exiting the program... ')
      sys.exit()

  for detector_name in h5_group_names:
    detector_number = int(detector_name[-2:])
    print('\n >>> working on channel_cm_sub: '+ detector_name)

    trk_data = h5['/' + detector_name + '/vdata/channel_cm_sub']
    nevent = trk_data.shape[0]
    print(' >>> Number of events = {:d}'.format(nevent))

    mask = convert_mask(h5, detector_name)
    if (nevent > 3*slice_size):
      nslice = nevent // slice_size
      nrest = nevent % slice_size
      print('\n >>> Number of {:d} event slices = {:d} (with rest {:d})'.format(slice_size, nslice, nrest))

      tmp_ene = zeros((0, 2, 192))
      dset_name = '/'+detector_name+'/vdata/channel_energy'
      h5out.create_dataset(dset_name, \
        data=tmp_ene, compression="gzip", chunks=True, maxshape=(None,2,192))

      print('\n >>> Calibrating energies...')
      print('     --- Slice.No   Evt_start   Evt_end   (exec.time) ---')
      for m in range(nslice):
        tmp_s = datetime.utcnow()
        if (m == 0):
          i_st = 0
          i_ed = (m+1)*slice_size
        elif (m == (nslice-1)):
          i_st = m*slice_size
          i_ed = (m+1)*slice_size+nrest
        else:
          i_st = m*slice_size
          i_ed = (m+1)*slice_size
        data_arr = array(trk_data[i_st:i_ed, :, :])
        tmp_ene, tmp_eflg = apply_ecalib2data(detector_number, data_arr, ecalib_table)

        h5out[dset_name].resize((h5out[dset_name].shape[0] + tmp_ene.shape[0]), axis=0)
        h5out[dset_name][-tmp_ene.shape[0]:] = tmp_ene

        tmp_e = datetime.utcnow()
        print('           {:d}   {:d}   {:d}   ({:.2f} s) '.format(m, i_st, i_ed, (tmp_e - tmp_s).total_seconds()))

      del tmp_ene
      gc.collect()
    else:
      data_arr = array(trk_data)
      ene_arr = zeros(trk_data.shape)
      ene_arr, ene_flg = apply_ecalib2data(detector_number, data_arr, ecalib_table)

      dset_name = '/'+detector_name+'/vdata/channel_energy'
      dset_calib_ene = \
        h5out.create_dataset(dset_name, ene_arr.shape)
      dset_calib_ene[:] = ene_arr[:]
  h5out.close()


def get_evt_status_array(file_name, file_out, sig_level, slice_size=None):
  if (slice_size is None):
    slice_size = 100000

  h5 = h5py.File(file_name, 'r')   # allow to append to file
  h5out = h5py.File(file_out, 'a')   # allow to append to file

  h5_group_names = array([x for x in h5])
  q = where(h5_group_names != 'vdata')[0]
  h5_group_names = h5_group_names[q]

  print('\n >>> Number of tracker layers: ', len(h5_group_names))
  for p in h5_group_names:
    print(' >>>           ', p)

  isSIG = ('/' + h5_group_names[0] + '/vdata/event_multiplicity' in h5out)
  if (isSIG):
    print(' *** Found in hdf5 file dataset: /XXX/vdata/event_multiplicity')
    usr_input = input(' --- Overwrite the existing dataset (yes / no)? ').strip()
    if (usr_input == '1') | (usr_input == 'yes') | (usr_input == 'y'):
      for p in h5_group_names:
        del h5out['/' + p + '/vdata/event_multiplicity']
    else:
      print(' --- Exiting the program... ')
      sys.exit()

  for detector_name in h5_group_names:
    detector_number = int(detector_name[-2:])
    print('\n >>> working on channel_cm_sub: '+ detector_name)

    sig_arr = h5out['/' + detector_name + '/vdata/channel_sig']
    nevent = sig_arr.shape[0]
    print(' >>> Number of events = {:d}'.format(nevent))

    multi_stat = zeros(nevent)

    if (nevent > 3*slice_size):
      nslice = nevent // slice_size
      nrest = nevent % slice_size
      print('\n >>> Number of {:d} event slices = {:d} (with rest {:d})'.format(slice_size, nslice, nrest))

      tmp_multi = zeros((0, 2))
      dset_name = '/'+detector_name+'/vdata/event_multiplicity'
      h5out.create_dataset(dset_name, \
        data=tmp_multi, compression="gzip", chunks=True, maxshape=(None,2))

      print('\n >>> Calculating event multiplicity...')
      print('     --- Slice.No   Evt_start   Evt_end   (exec.time) ---')
      for m in range(nslice):
        tmp_s = datetime.utcnow()
        if (m == 0):
          i_st = 0
          i_ed = (m+1)*slice_size
        elif (m == (nslice-1)):
          i_st = m*slice_size
          i_ed = (m+1)*slice_size+nrest
        else:
          i_st = m*slice_size
          i_ed = (m+1)*slice_size
        slice_arr = array(sig_arr[i_st:i_ed, :, :])
        nevent_slice = slice_arr.shape[0]
        tmp_multi = zeros((nevent_slice, 2))

        for layer_side in range(2):
          for i in range(nevent_slice):
            ix1 = where(slice_arr[i, layer_side, :] >= sig_level)[0]
            tmp_multi[i, layer_side] = len(ix1)

        h5out[dset_name].resize((h5out[dset_name].shape[0] + tmp_multi.shape[0]), axis=0)
        h5out[dset_name][-tmp_multi.shape[0]:] = tmp_multi

        tmp_e = datetime.utcnow()
        print('           {:d}   {:d}   {:d}   ({:.2f} s) '.format(m, i_st, i_ed, (tmp_e - tmp_s).total_seconds()))
    else:
      sig_arr = array(sig_arr)
      nevent = sig_arr.shape[0]
      multi_stat = zeros((nevent, 2))
      for layer_side in range(2):
        for i in range(nevent):
          ix1 = where(sig_arr[i, layer_side, :] >= sig_level)[0]
          multi_stat[i, layer_side] = len(ix1)

      dset_name = '/'+detector_name+'/vdata/event_multiplicity'
      dset_evt_multi = \
        h5out.create_dataset(dset_name, multi_stat.shape)
      dset_evt_multi[:] = multi_stat[:]
  h5out.close()

def prep_data(file_name, action_flg, ecalib_name=None):
  dstart = datetime.utcnow()

  if ((action_flg == 'ene') & (ecalib_name is None)):
    print(' --- No energy calibration table provided. Exiting the program...')
    sys.exit()

  file_out = file_name[:-5]+'L1p5.h5'

  slice_size = 100000

  if (action_flg == 'sig'):
    get_sig_array(file_name, file_out, slice_size)
  elif (action_flg == 'ene'):
    get_ene_array(file_name, file_out, ecalib_name, slice_size)
  elif (action_flg == 'multi'):
    sig_level = 3.0
    print(' %%% Event multiplicity analysis using significance level of: {:.1f}'.format(sig_level))
    get_evt_status_array(file_name, file_out, sig_level, slice_size)

  dend = datetime.utcnow()
  total_time = (dend - dstart).total_seconds()
  if (total_time > 180):
    print('\n --- Execution time [min] = {:.5f}'.format(total_time/60.))
  else:
    print('\n --- Execution time [sec] = {:.5f}'.format(total_time))

if __name__ == "__main__":
  if (len(sys.argv) == 4):
    file_name = str(sys.argv[1]).rstrip()
    action_flag = str(sys.argv[2]).rstrip()
    ecalib_name = str(sys.argv[3]).rstrip()
    prep_data(file_name, action_flag, ecalib_name)
  elif (len(sys.argv) == 3):
    file_name = str(sys.argv[1]).rstrip()
    action_flag = str(sys.argv[2]).rstrip()
    prep_data(file_name, action_flag)
  else:
    print('\n *** Use error: incorrect number of arguments')
    print('\n     Use examples:')
    print('       prep_big_data.py <file_name> <action_flag=sig or multi>')
    print('       prep_big_data.py <file_name> <action_flag=ene> <ecalib_name>')
