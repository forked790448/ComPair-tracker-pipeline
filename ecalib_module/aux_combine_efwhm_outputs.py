'''
2023-03-15  Anna Zajczyk

  Tool combines into a single hdf5 file data from a list of input hdf5 files.
  The hdf5 files in the input list were created with the fit_efwhm.py tool
  and contain data for only a subset of tracker layers. To create a single
  hdf5 file with information for the full tracker stack on how energy resolution 
  changes with energy these "subfiles" need to be combined into one master file.
  This is a simple tool to do just that.

  Parameters
  ----------
  list_name: str
    A simple text file with a list of file names that you want to combine. There
    should be one file name per line, and the last input line should be broken
    (with enter) as sometimes tool may crash if the break is not there.
    The list_name must be given with a full access path. Tool assumes that 
    the files that are listed in the list_name text file reside in the same
    directory as the list_name file.

  Returns
  -------
  file_out: str / hdf5
    Tool creates a master hdf5 that contains combined data from all the input 
    files. This master files is saved in the same directory as the one where
    the list_name file lives. This master hdf5 file should contain the following
    datasets:
      /layerXX/values_peak_ecalib : these datasets store the measured peak centroids 
        values and their errors, peak gaussian sigmas and their errors. Structure 
        of the tables is the following: (nrows, 7), where the columns are:
        detector_number, layer_side, strip_number, centroid_kev, err_centroid_kev, 
        sigma_kev, err_sigma_kev
      /layerXX/values_fwhm_coef : these datasets store fit results for FWHM(E) 
        dependence. Structure of the tables is the following: (384, 7), where 
        the columns are:
        detector_number, layer_side, strip_number, fit_flag, noise2, err_noise2, 
        number_of_peaks_used_in_fit
  
  Updates
  -------
  YYYY-MM-DD
'''


import sys
import h5py

from datetime import datetime
from numpy import array, ones, unique, argwhere, delete

from ecalib_functions import read_data

def identify_valid_entries(list_table):
  nrow = list_table.shape[0]
  track_layers = ones((nrow, 10)).astype(int)
  track_layers = -1 * track_layers
  i = 0
  for file_name in list_table:
    file_name = list_name[:-len(list_name.split('/')[-1])] + \
      file_name
    
    h5tmp = h5py.File(file_name, 'r')
    group_names = array([x for x in h5tmp])

    for detector_name in group_names:
      detector_number = int(detector_name[-2:])
      track_layers[i, detector_number] = detector_number
    i += 1

  ulayers, ulayers_cnt = unique(track_layers, return_counts=True) # --- unique layers
  q = argwhere(ulayers == -1) # --- do some cleaning and indexing
  ulayers = delete(ulayers, q)
  ulayers_cnt = delete(ulayers_cnt, q)
  qcnt = argwhere(ulayers_cnt != 1)

  if (len(qcnt) != 0):
    mltlayers = ulayers[qcnt]
    print(' *** Duplicate inputs found for the following layers: ', mltlayers)
    
    for mlt in mltlayers:
      print(' --- Layer {:d}'.format(mlt[0]))
      qall = argwhere(track_layers == mlt)
      for k in range(qall.shape[0]):
        print('       {:d}'.format(k) + '   ' + list_table[qall[k][0]])
      uix = input(' >>> Which file entry for this layer is valid (choose file number)? ').strip()
      uix = int(uix)

      good_entry = False
      for k in range(qall.shape[0]):
        if (k == uix):
          good_entry = True
      if (not good_entry):
        print(' *** Ehh! Wrong file number... let\'s try this again...')
        uix = input(' >>> Which file entry for this layer is valid (choose file number)? ').strip()
        uix = int(uix)

      for k in range(qall.shape[0]):
        if (k != uix):
          track_layers[qall[k][0], qall[k][1]] = -1
  else:
    print(' --- No duplicates found in the input data')


  print('\n --- Combining the following File / Layer data:')
  for k in range(nrow):
    print('   File {:d}  |'.format(k) + '   ' + list_table[k])

  print('\n    Layer       0   1   2   3   4   5   6   7   8   9')
  print('  -------------------------------------------------------')
  for k in range(nrow):
    tmp = ' '
    for j in range(10):
      if (track_layers[k, j] == -1):
        tmp = tmp + '   X'
      else:
        tmp = tmp + '   {:d}'.format(track_layers[k, j])
    print('   File {:d}  |'.format(k) + tmp)
  return track_layers

def combine_fwhm_files(list_name):
  dstart = datetime.utcnow()

  file_out = list_name[:-len(list_name.split('/')[-1])] + \
    'Master_FwhmDependenceCoefficients_' + \
    dstart.strftime('%Y%m%d') + '.h5'

  list_table, nrow, ncol = read_data(list_name, data_type='list')
  track_layers = identify_valid_entries(list_table)

  h5out = h5py.File(file_out, 'w')

  i = 0
  print('')
  for file_name in list_table:
    file_name = list_name[:-len(list_name.split('/')[-1])] + \
      file_name
    
    h5tmp = h5py.File(file_name, 'r')
    group_names = array([x for x in h5tmp])
    for detector_name in group_names:
      detector_number = int(detector_name[-2:])
      if (track_layers[i, detector_number] != -1):
        print(' ... combining File {:d} / Layer {:d}'.format(i, detector_number))
        efwhm_table = array(h5tmp['/'+detector_name+'/values_peak_ecalib'])

        dset_efwhm_table = \
          h5out.create_dataset('/'+detector_name+'/values_peak_ecalib', efwhm_table.shape, dtype='float64')
        dset_efwhm_table[:] = efwhm_table[:]

        efwhm_fit_table = array(h5tmp['/'+detector_name+'/values_fwhm_coef'])

        dset_efwhm_fit_table = \
          h5out.create_dataset('/'+detector_name+'/values_fwhm_coef', efwhm_fit_table.shape, dtype='float64')
        dset_efwhm_fit_table[:] = efwhm_fit_table[:]
      else:
        print(' --- skipping  File {:d} / Layer {:d} as invalid'.format(i, detector_number))
    i += 1
  h5out.close()

if __name__ == "__main__":
  if (len(sys.argv) == 2):
    list_name = str(sys.argv[1]).rstrip()
    combine_fwhm_files(list_name)
  else:
    print('\n *** Use error: incorrect number of arguments')
    print('\n     Use examples:')
    print('       aux_combine_efwhm_outputs.py <list_name>')