'''
2023-03-14  Anna Zajczyk

  This tool tries to fit a gaussian to the noise peak in the raw data and in the common 
  mode subtracted data. Then from the fitted centroids the tool calculates the baseline
  shift, i.e., the difference between the position of the noise peak in the raw and commom 
  mode subtracted data. This baseline shift is used to estimate where the high end cutoff
  of TRK dynamic range is. The calculation is performed per layer, per side and per strip.
  Hardcoded are the region and initial values of the gaussian function that will be fitted
  to the data. If the gaussian fit cannot converge for any reason the baseline shift value 
  is set to -1. Tool does not check for masked channels. Average execution time for 9 layer
  file was ~10min. The results are saved to an hdf5 file.

  Parameters
  ----------
  data_dir: str
    Full access path to the directory where the input HDF5 L1- or L2-type data file resides.
    Remember to include the forward slash at the end of the access path.
  file_name: str
    Name of the input HDF5 L1- or L2-type data.

  Returns
  -------
  file_out: str
    Name of the output file where the results of the baseline shift calculations will be saved.
    The output file will be saved in HDF5 format, in the same data directory as the input file.
    The data are saved to: /layerXX/baseline_shift datasets. Each data set has size (2, 192).
    The output file name is: 'baseline_shift_numbers_' + file_name

  Updates
  -------
  YYYY-MM-DD
'''

import h5py

from scipy.optimize import curve_fit
from numpy import *
from datetime import datetime

from ecalib_functions import fgauss

data_dir = '/Users/azajczyk/work/AMEGO/Si_tracker/data/hdf5/LaboratoryDataFiles/multi_layer/20230303/'
file_name = '2023-03-03_1003_9Layers_Co57_L2.h5'
file_out = 'baseline_shift_numbers_' + file_name

dstart = datetime.utcnow()

h5 = h5py.File(data_dir+file_name, 'r')
group_names = array([x for x in h5])
q = where(group_names != 'vdata')[0]
group_names = group_names[q]
nlayer = len(group_names)

print('\n >>> Number of tracker layers in the file: {:d}'.format(nlayer))
for i in group_names:
  print('       ' + i)

# --- Prepare COMMON BINS to be used for all input data
bin_min = -250
bin_max = 1024
binw = 1

bins = arange(bin_min, bin_max, binw)
nbins = len(bins)

edge_low = bins - 0.5*binw
edge_high = bins + 0.5*binw
edges = append(edge_low, [edge_high[-1]])


h5out = h5py.File(data_dir + file_out, 'w')

print('')
for detector_name in group_names:
  print(' --- Dealing with: ' + detector_name)
  detector_number = int(detector_name[-2:])
  shift_tbl = zeros((2, 192))
  for layer_side in range(2):
    for strip_number in range(192):
      try:
        # --- Read in data for the strip
        strip_data_raw = array(h5['/' + detector_name + '/vdata/channel_data'][:, layer_side, strip_number])
        strip_data_cm = array(h5['/' + detector_name + '/vdata/channel_cm_sub'][:, layer_side, strip_number])

        # --- Gaussian fit: initial parameters for the raw data
        pinit1 = [1e4, 250, 10]
        strip_hist_raw, edges_tmp = histogram(strip_data_raw, bins=edges)
        q = (strip_hist_raw > 0)
        p1,t1 = curve_fit(fgauss, bins[q], strip_hist_raw[q], p0=pinit1)
        # perr1 = sqrt(diag(t1))   # --- comment out if interested in fit errors

        # --- Gaussian fit: initial parameters for the common mode subtracted data
        pinit2 = [1e4, 0, 10]
        strip_hist_cm, edges_tmp = histogram(strip_data_cm, bins=edges)
        p2,t2 = curve_fit(fgauss, bins, strip_hist_cm, p0=pinit2)
        # perr2 = sqrt(diag(t2))

        bs_shift = p1[1] - p2[1]
        shift_tbl[layer_side, strip_number] = bs_shift
      except:
        bs_shift = -1
        shift_tbl[layer_side, strip_number] = bs_shift

  dset_bs_shift = \
    h5out.create_dataset('/' + detector_name + '/baseline_shift', shift_tbl.shape, dtype='float64')
  dset_bs_shift[:, :] = shift_tbl[:, :]

h5out.close()
print('\n --- Baseline shift data written to: ' + file_out)

dend = datetime.utcnow()
total_time = (dend - dstart).total_seconds()
if (total_time > 180):
  print('\n --- Execution time [min] = {:.1f}'.format(total_time/60.))
else:
  print('\n --- Execution time [sec] = {:.1f}'.format(total_time))