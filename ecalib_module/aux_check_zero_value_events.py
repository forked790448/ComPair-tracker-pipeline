'''
2023-03-15  Anna Zajczyk

  ************************************************************************************
  *** To run this tool <moviepy> python library is needed. It can be installed by: ***
  ***       pip install moviepy                                                    ***
  ************************************************************************************

  Tool to inspect ZERO-VALUE type of events that are present in Co57 data taken on 2023-03-03.

  One can choose three wasy to look at the zero value event data. The output_type flag is used
  to select how to look at the data. The flag values are:
    'single layer' : in this mode user needs to provide details (detector_number, layer_side
                     and strip_number) of the strip that will be used as a reference for
                     identifying the zero-value events. From the zero-type events found
                     a selected number will be plotted. The variable nevts2plot sets how many
                     events will be plotted. For each selected event a plot showing peak height
                     value as a function of strip number (or state of layer for that event, 
                     if you will) will be created. Entries corresponding to side 0 / side 1
                     will be color-coded (blue = side 0, violet = side 1). Each plot is saved 
                     to PNG file to a temporary directory. These files are then converted into 
                     GIF to show a movie of the changing state of the layer for those zero-val 
                     events. Once GIF is created then the temporary directory containing all 
                     the png plots is deleted.
    'all layers'   : in this mode user needs to provide layer_side and strip_number information.
                     The detector_number variable is ignored as the tool loops over all layers
                     in this mode. For each layer and the selected side/strip number (same for
                     all layers) the tool calculates the % of all events that the zero-val events 
                     make up. No plots are created. All is printed in the terminal.
    'select event' : in this mode user needs to provide detector_number, layer_side and 
                     strip_number information. Additional parameter to provide is select_num.
                     For the zero-val event as identified with a select_num parameter from 
                     the chosen layer / side / strip a state of the full tracker stack will 
                     be plotted. Two plots each consisting of 3x3 subpanels will be created.
                     One plot per tracker side. Each subpanel will show the state of individual
                     tracker layer (i.e., plot of peak height versus strip number).
                     These two plots will be saved as PNG files.

  Parameters
  ----------
  data_dir: str
    Full access path to the directory where the input HDF5 baseline shift data file resides.
    Remember to include the forward slash at the end of the access path.
  file_name: str
    Name of the input baseline shift data.
  output_type: str
    Flag that selects what the output of the tool is going to be. The flag value can be set
    to: 'single layer', 'all layers' or 'select event'.
  detector_number: int
    The tracker layer for which to inspect the data. Tracker layers span
    0-9 range (assembly of the 9-th layer is in the workings at the moment).  
  layer_side: int
    Tracker side number spans 0-1 range, where 0 is top/A, and 1 is bottom/B.
  strip_number: int
    Strip number on the tracker layer / side. The strip number spans 0-191 range.
  select_num: int
    Zero value event number for which the full tracker stack status will be inspected.

  Returns
  -------
  terminal output
    Give information on what % of all events the zero-val events make up.
  plots
    See description of outputs under in main description in the part discussing output_type flag.

  Updates
  -------
  YYYY-MM-DD
'''

import h5py
import os
import shutil
import moviepy.video.io.ImageSequenceClip

import matplotlib.pyplot as plt
from numpy import *

from ecalib_functions import convert_mask

def get_layer_info(h5, detector_number, layer_side, strip_number):
  detector_name = 'layer{:02d}'.format(detector_number)
  print(' --- Dealing with: ' + detector_name + ' / ' + str(layer_side) + ' / ' + str(strip_number))

  # --- Read in data for the strip
  strip_data_raw = array(h5['/' + detector_name + '/vdata/channel_data'][:, layer_side, strip_number])
  event_id = array(h5['/' + detector_name + '/data/event_id'])
  mask = convert_mask(h5, detector_name)
  if (mask[layer_side, strip_number] == 1):
    print(' *** Dealing with MASKED channel ***')

  nevent = event_id.shape[0]
  print(' ---    Total number of ALL events: {:d}'.format(nevent))
  idzero = argwhere(strip_data_raw == 0)
  print(' ---    Total number of ZERO-VALUE events: {:d} ({:.1f} %)'.format(len(idzero), len(idzero)/nevent*100.0))
  return idzero


def plot_zeros_single_layer(h5, detector_number, layer_side, strip_number, idzero, frame_num, data_dir):
  detector_name = 'layer{:02d}'.format(detector_number)

  # --- prepare directories
  image_path_parent = data_dir + 'images/'
  if not os.path.exists(image_path_parent):
    os.mkdir(image_path_parent)

  image_folder = image_path_parent + detector_name + '/'
  if not os.path.exists(image_folder):
    os.mkdir(image_folder)

  image_folder_tmp = image_folder + 'tmp/'
  if not os.path.exists(image_folder_tmp):
    os.mkdir(image_folder_tmp)

  # --- prepare figures and save to tmp directory
  for evt_num in range(frame_num):
    side0 = array(h5['/' + detector_name + '/vdata/channel_data'][idzero[evt_num], 0, :][0])
    side1 = array(h5['/' + detector_name + '/vdata/channel_data'][idzero[evt_num], 1, :][0])

    print('\n --- Event sequence number: {:d}   ({:d}/{:d})'.format(idzero[evt_num][0], \
      evt_num, frame_num))
    q0 = where(side0 == 0)[0]
    q1 = where(side0 != 0)[0]
    print(' ---    Side 0: ') 
    print(' ---             zero val events = {:d}'.format(len(q0)))
    print(' ---         non-zero val events = {:d}'.format(len(q1)))

    q0 = where(side1 == 0)[0]
    q1 = where(side1 != 0)[0]
    print(' ---    Side 1: ') 
    print(' ---             zero val events = {:d}'.format(len(q0)))
    print(' ---         non-zero val events = {:d}'.format(len(q1)))

    xtbl = arange(0, 192, 1)

    plt.ion()
    plt.figure(1)
    plt.clf()
    plt.suptitle('Layer {:d} / event seq number {:d}'.format(detector_number, idzero[evt_num][0]))
    plt.subplot(211)
    plt.ylabel('Peak height [ADU]')
    plt.plot(xtbl, side0, '.', color='blue', label='side 0')
    if (layer_side == 0):
      if (max(side0) !=0 ):
        plt.plot([strip_number, strip_number], [0, max(side0)], '-.', color='orange', alpha=0.5)
      else:
        plt.plot([strip_number, strip_number], [-5, 5], '-.', color='orange', alpha=0.5)
    for i in range(7):
      if (max(side0) !=0 ):
        plt.plot([i*32, i*32], [0, max(side0)], ':', color='grey', alpha=0.5)
      else:
        plt.plot([i*32, i*32], [-5, 5], ':', color='grey', alpha=0.5)
    plt.legend()

    plt.subplot(212)
    plt.xlabel('Strip number')
    plt.ylabel('Peak height [ADU]')
    plt.plot(xtbl, side1, '.', color='violet', label='side 1')
    if (layer_side == 1):
      if (max(side1) !=0 ):
        plt.plot([strip_number, strip_number], [0, max(side1)], '-.', color='orange', alpha=0.5)
      else:
        plt.plot([strip_number, strip_number], [-5, 5], '-.', color='orange', alpha=0.5)
    for i in range(7):
      if (max(side1) !=0 ):
        plt.plot([i*32, i*32], [0, max(side1)], ':', color='grey', alpha=0.5)
      else:
        plt.plot([i*32, i*32], [-5, 5], ':', color='grey', alpha=0.5)
    plt.legend()
    plt.show()
    plt.savefig(image_folder_tmp + 'ZERO_VAL_EVTS_layer{:d}_eventseqnum_{:d}.png'.format(detector_number, idzero[evt_num][0]))
    plt.pause(0.1)

  # --- create movie
  fps=5

  image_files = [os.path.join(image_folder_tmp, img)
                for img in os.listdir(image_folder_tmp)
                if img.endswith(".png")]
  clip = moviepy.video.io.ImageSequenceClip.ImageSequenceClip(image_files, fps=fps)
  clip.write_gif(image_folder + detector_name + '_video.gif')

  # --- clean tmp director and remove it, leave only GIF file
  shutil.rmtree(image_folder_tmp)


def plot_zeros_all_layers(h5, detector_number, layer_side, strip_number, idzero, select_num, data_dir):
  detector_name = 'layer{:02d}'.format(detector_number)
  # --- prepare directories
  image_path_parent = data_dir + 'images/'
  if not os.path.exists(image_path_parent):
    os.mkdir(image_path_parent)

  image_folder = image_path_parent + detector_name + '/'
  if not os.path.exists(image_folder):
    os.mkdir(image_folder)

  id_select = idzero[select_num]
  mark_color = {0 : 'blue', 1 : 'violet'}

  plt.close('all')
  for k in range(2):
    plt.ion()
    plt.figure(1+k, figsize=(10, 8))
    plt.clf()
    plt.suptitle('Evtent sequence number: {:d} \n\n Side {:d} '.format(id_select[0], k))

    for i in range(nlayer):
      detector_name = 'layer{:02d}'.format(i)
      sidexx = array(h5['/' + detector_name + '/vdata/channel_data'][id_select, k, :][0])
      xtbl = arange(0, 192, 1)

      plt_num = 331 + i
      plt.subplot(plt_num)
      plt.plot(xtbl, sidexx, '.', color = mark_color[k], label='layer {:d}'.format(i))
      if (plt_num == 331) | (plt_num == 334) | (plt_num == 337):
        plt.ylabel('Peak height [ADU]')
      if (plt_num == 337) | (plt_num == 338) | (plt_num == 339):
        plt.xlabel('Strip number')

      for j in range(7):
        if (max(sidexx) !=0 ):
          plt.plot([j*32, j*32], [0, max(sidexx)], ':', color='grey', alpha=0.5)
        else:
          plt.plot([j*32, j*32], [-5, 5], ':', color='grey', alpha=0.5)

      if (layer_side == k):
        if (detector_number == i):
          if (max(sidexx) !=0 ):
            plt.plot([strip_number, strip_number], [0, max(sidexx)], '-.', color='orange', alpha=0.8, label='ref.strip')
          else:
            plt.plot([strip_number, strip_number], [-5, 5], '-.', color='orange', alpha=0.8, label='ref.strip')
      plt.legend()
      plt.show()
    plt.savefig(image_folder + 'ZERO_VAL_EVTS_AllLayers_SingleEventNum_{:d}_Side_{:d}.png'.format(id_select[0], k))


data_dir = '/Users/azajczyk/work/AMEGO/Si_tracker/data/hdf5/LaboratoryDataFiles/multi_layer/20230303/'
file_name = '2023-03-03_1003_9Layers_Co57_L2.h5'

detector_number = 5
layer_side = 0
strip_number = 83

output_type = 'all layers' #'select event' # 'single layer' # 'all layers' # 

nevts2plot = 100
select_num = 4371


print(' --- Working with file: ' + file_name)
h5 = h5py.File(data_dir+file_name, 'r')
group_names = array([x for x in h5])
q = where(group_names != 'vdata')[0]
group_names = group_names[q]
nlayer = len(group_names)

plt.close('all')

if (output_type == 'single layer'):
  idzero = get_layer_info(h5, detector_number, layer_side, strip_number)
  plot_zeros_single_layer(h5, detector_number, layer_side, strip_number, idzero, nevts2plot, data_dir)
elif (output_type == 'all layers'):
  for detector_number in range(9):
    idzero = get_layer_info(h5, detector_number, layer_side, strip_number)
elif (output_type == 'select event'):
  idzero = get_layer_info(h5, detector_number, layer_side, strip_number)
  plot_zeros_all_layers(h5, detector_number, layer_side, strip_number, idzero, select_num, data_dir)
