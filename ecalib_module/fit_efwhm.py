'''
2022-11-16    Anna Zajczyk

  Tool reads in the file outputs from emission line fitting tools (fit_emline_user/auto)
  and outputs table of peak centroids and line sigmas both converted from ADUs
  to keVs. The tool also fits FWHM(E) dependence to the input data and outputs
  table with results of the fits.

  Inputs
  ------
  There are two input parameters needed to run the tool:

  <list_name> - first input parameter. It is a file that contains a list of
    file names that contain results of the line fitting, usually named
    <hdf5_file_name_autoFit.dat> or <hdf5_file_name_userFit.dat> depending
    on the fitting tool used. Each row of this file should hold file name
    as the first entry in the row. Same input files as used in <fit_ecalib.py>
    tool can be used here. Provide full access path to the input file.

  <ecalib_name> - second input parameter. It is the "caldb" file, i.e., file
    containing energy calibration solutions that is relevant for the input
    files to be processed. This calibration file is created with fit_ecalib.py
    tool.

  Outputs
  -------
  There is single output file in HDF5 format. The file contains two types of
  tables that are created for each detector layer.

  values_peak_ecalib - these tables store the measured peak centroids values
    and their errors, peak gaussian sigmas and their errors. All these values
    are converted from ADUs to keV using the data (acoef, bcoef) from
    <ecalib_name> file. Structure of the tables is the following:
      (nrows, 7), where the columns are:
        detector_number, layer_side, strip_number, centroid_kev, err_centroid_kev, sigma_kev, err_sigma_kev
    The conversion to keV goes as follows:
      centroid_kev = acoeff * centroid_adu + bcoeff
      err_centroid_kev = err_centroid_adu / centroid_adu * centroid_kev
      sigma_kev = sigma_adu / centroid_adu * centroid_kev
      err_sigma_kev = err_sigma_adu / sigma_adu * sigma_kev
    So there is no true error propagation/addition of accuracy of energy
    calibration solutions. This is quick and dirty approach.

  values_fwhm_coef - these tables store fit results for FWHM(E) dependence.
    The fit takes <centroid_kev> and <sigma_kev> values for each strip and
    tries to fit function that descripes energy dependence of fwhm in Si
    detectors. The function is the following (see e.g., Fraser "X-ray detectors
    in astronomy"):
      FWHM(E) = sqrt(noise2 + 8.0*log(2)*3.66*0.114*E)

        noise2 - is the square of the electronic noise in eV^2 units

        8.0*log(2)*3.66*0.114*E - is the term describing variance of the number
        of primary electron-hole pairs created per interaction. The values:
           3.66 - the electron-hole energy in eV units
          0.114 - the Fano factor for Si
        E - is the energy in eV units!

        FWHM(E)calculated with this equation is given in eV units!


    Structure of the tables is the following:
      (nrows, 7), where the columns are:
        detector_number, layer_side, strip_number, fit_flag, noise2, err_noise2, number_of_peaks_used_in_fit
      The fit_flag is assigned to each fit. The flag values are:
          -3 - no energy calibration solution for this strip
          -2 - no lines / no data
          -1 - fit failed
           1 - fit converged (but no quality checks are performed at this moment)

  Notes
  -----
  Probably energy calibration of values in ADUs can be done better and also
  error propagation can be done better.


  Updates
  -------

'''

import os
import sys
import h5py

import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from datetime import datetime
from numpy import append, array, where, sqrt, diag, zeros, unique, log

from ecalib_functions import read_data


def func_fwhm_si(x, noise2):
  # --- FWHM as a function of energy and electronic noise (Schole & Procop 2008)
  # --- x - photon energy in [eV]
  # --- noise2 - square of the electronic noise
  # eps = 3.66   # [eV] minimum impact ionization for SDD at room temperature
  # fano = 0.114   # Fano factor for silicon detectors
  return sqrt(noise2 + 8.0*log(2)*3.66*0.114*x)
fwhm_si = lambda p, x: sqrt(p[0] + 8.0*log(2)*3.66*0.114*x)

def analyze_strip_ecalib(
    emline_data, ecalib_table, layer_side, strip_number, detector_number,
    efwhm_table, efwhm_fit_table):
  """Fits function that describes dependence of energy resolution on energy
     to the data. Returns two tables:

     efwhm_table - stores energy-calibrated values [L#, SD#, ST#,
                   centroid, centroid_error, sigma, sigma_error]
     efwhm_fit_table - stores fwhm(E) solution [L#, SD#, ST#, FLG,
                   noise2[eV], noise2_error[eV], number_of_peaks_used_in_fit]

     FWHM(E) dependence is approximated with <func_fwhm_si>

  """

  # select all events in layer / side /strip
  q = ((emline_data[:, :, 0] == detector_number) &
    (emline_data[:, :, 1] == layer_side) &
    (emline_data[:, :, 2] == strip_number))

  rflg = emline_data[q, :][:, 3]         # quality flag for em.line fit
  rpeak = emline_data[q, :][:, 4]        # peak centroid
  rpeak_err = emline_data[q, :][:, 5]    # error on peak centroid
  rsigma = emline_data[q, :][:, 6]       # peak sigma
  rsigma_err = emline_data[q, :][:, 7]   # error on peak sigma

  qflg = where(rflg == 1)[0]   # choose only GOOD quality fits

  qcal = where((ecalib_table[:, 0] == detector_number) &
       (ecalib_table[:, 1] == layer_side) &
       (ecalib_table[:, 2] == strip_number))
  indx = qcal[0][0]

  if ((ecalib_table[indx, 3] == -2) | (ecalib_table[indx, 3] == -1)):
    print(' *** Warning: No valid energy calibration solution for strip (layer / side / ' \
      'strip): ' + str(detector_number) + ' / '+ str(layer_side) + \
      ' / ' + str(strip_number))

    tmp_fit = [detector_number, layer_side, strip_number, -3, \
      -1, -1, -1]
    efwhm_fit_table.append(tmp_fit)
  else:
    rpeak_ene = ecalib_table[indx, 4] * rpeak + ecalib_table[indx, 6]
    rpeak_err_ene = rpeak_err/rpeak * rpeak_ene
    rsigma_ene = rsigma/rpeak * rpeak_ene
    rsigma_err_ene = rsigma_err/rsigma * rsigma_ene

    if (len(rpeak_ene[qflg]) >= 1):
      cpeak_ene = rpeak_ene[qflg]
      cpeak_err_ene = rpeak_err_ene[qflg]
      csigma_ene = rsigma_ene[qflg]
      csigma_err_ene = rsigma_err_ene[qflg]

      for m in range(len(cpeak_ene)):
        tmp_rc = [detector_number, layer_side, strip_number, \
          cpeak_ene[m], cpeak_err_ene[m], csigma_ene[m], csigma_err_ene[m]]
        efwhm_table.append(tmp_rc)

      try:
        pinit = [1.0e5]   # [eV^2] electronic noise
        fwhm_factor = 2*sqrt(2*log(2))
        p1,t1 = curve_fit(func_fwhm_si, cpeak_ene*1e3, fwhm_factor*csigma_ene*1e3,\
          p0=pinit, sigma=fwhm_factor*csigma_err_ene*1e3, absolute_sigma=True)
        perr1 = sqrt(diag(t1))

        tmp_fit = [detector_number, layer_side, strip_number, 1, \
          p1[0], perr1[0], len(cpeak_ene)]
        efwhm_fit_table.append(tmp_fit)
      except:
        print(' *** Warning: Fit failed for strip (layer / side / ' \
          'strip): ' + str(detector_number) + ' / '+ str(layer_side) + \
          ' / ' + str(strip_number))
        tmp_fit = [detector_number, layer_side, strip_number, -1, \
          -1, -1, -1]
        efwhm_fit_table.append(tmp_fit)
    else:
      print(' *** Warning: No valid emission line entries (layer / side / ' \
        'strip): ' + str(detector_number) + ' / '+ str(layer_side) + \
        ' / ' + str(strip_number))
      tmp_fit = [detector_number, layer_side, strip_number, -2, \
        -1, -1, -1]
      efwhm_fit_table.append(tmp_fit)
  return efwhm_table, efwhm_fit_table


def fit_fwhm_dependence(list_name, ecalib_name):
  """Finds FWHM(E) dependence of energy resolution on energy
  """
  dstart = datetime.utcnow()

  data_dir = list_name[:-len(list_name.split('/')[-1])]
  if (data_dir[-1] != '/'):
    data_dir = data_dir + '/'

  list_table, nrow, ncol = read_data(list_name, data_type='list')

  ecalib_table, enrow, encol = read_data(
    ecalib_name, data_type='float')   # get energy calibration params

  if (len(list_table.shape) != 1):
    file_table = list_table[:, 0]
    nfiles = len(file_table)
  else:
    file_table = [list_table[0]]
    nfiles = 1

  file_out = 'FwhmDependenceCoefficients_' + \
    dstart.strftime('%Y%m%d') + '.h5'

  emline_data = zeros((nfiles, 10*2*192, 10))
  emline_data[:] = -1
  nlayer_in = zeros(nfiles, dtype=int)
  print('')
  for i in range(nfiles):
    print('\n --- Reading data file: ', data_dir+file_table[i])
    tmp_data, tmp_nrow, tmp_ncol = read_data(
      data_dir + file_table[i], data_type='float')

    qdet = where(tmp_data[:, 3] != 0)
    tmp_nlayer = len(unique(tmp_data[qdet, 0]))
    nlayer_in[i] = tmp_nlayer
    print(' --- Number of the tracker layers in the data file = ' + \
      str(tmp_nlayer) + ' ' + str(unique(tmp_data[qdet, 0])))

    if (tmp_nrow > 10*2*192):
      print(' *** Warning: Too many strip entries in the data file')
      print(' ***          Exiting the program.')
      sys.exit()

    emline_data[i, 0:tmp_nrow, :] = tmp_data

  qdet = where(nlayer_in != min(nlayer_in))
  if (len(qdet[0]) != 0):
    print('')
    print(' *** Warning: Different number of the tracker layers found ' + \
      'across the input data files')
    print(' ***          File#    TotalLayer#')
    for i in range(nfiles):
      print(' ***             ' + str(i) + '         ' + str(nlayer_in[i]))
    go_inpt = input(' >>> Continue the energy calibration fitting ' \
      '(yes / no)? ')
    if (go_inpt == 'no'):
      print(' ***          Exiting the program.')
      sys.exit()

  efwhm_table = []   # will store energy-calibrated values [L#, SD#, ST#,
                    # centroid, centroid_error, sigma, sigma_error]
  efwhm_fit_table = []   # will store fwhm(E) solution [L#, SD#, ST#, FLG,
                    # noise2[eV], noise2_error[eV], number_of_peaks_used_in_fit]

  plt.close('all')   # close any existing figures
  print('')

  for j in range(10):    # layer
    for k in range(2):     # side
      for i in range(192):   # strip
        efwhm_table, efwhm_fit_table = analyze_strip_ecalib(
            emline_data, ecalib_table, k, i, j, \
            efwhm_table, efwhm_fit_table)

  efwhm_table = array(efwhm_table)
  efwhm_fit_table = array(efwhm_fit_table)

  # ---------------------------------------------------------------------------
  # --- Write fit results to text file
  # ---------------------------------------------------------------------------
  isFile = os.path.isfile(data_dir + file_out)
  if (isFile):
    print('\n >>> File containing energy-calibrated centroids and FWHM(E) solution exists')
    print(' >>> and will be overwritten.')
    os.remove(data_dir + file_out)

  h5 = h5py.File(data_dir + file_out, 'w')

  ulayer = unique(efwhm_table[:, 0])

  for detector_number in ulayer:
    detector_name = 'layer{:02d}'.format(int(detector_number))

    q1 = where(efwhm_table[:, 0] == int(detector_number))[0]
    dset_efwhm_table = \
      h5.create_dataset('/'+detector_name+'/values_peak_ecalib', efwhm_table[q1, :].shape, dtype='float64')
    dset_efwhm_table[:] = efwhm_table[q1, :]

    q2 = where(efwhm_fit_table[:, 0] == int(detector_number))[0]
    dset_efwhm_fit_table = \
      h5.create_dataset('/'+detector_name+'/values_fwhm_coef', efwhm_fit_table[q2, :].shape, dtype='float64')
    dset_efwhm_fit_table[:] = efwhm_fit_table[q2, :]

  h5.close()

  # ---------------------------------------------------------------------------
  # --- End of program execution
  # ---------------------------------------------------------------------------
  dend = datetime.utcnow()
  total_time = (dend - dstart).total_seconds()
  if (total_time > 180):
    print('\n --- Execution time [min] = {:.5f}'.format(total_time/60.))
  else:
    print('\n --- Execution time [sec] = {:.5f}'.format(total_time))


if __name__ == "__main__":
  if (len(sys.argv) == 3):
    list_name = str(sys.argv[1]).rstrip()
    ecalib_name = str(sys.argv[2]).rstrip()
    fit_fwhm_dependence(list_name, ecalib_name)
  elif (len(sys.argv) == 4):
    data_dir = str(sys.argv[1]).rstrip()
    if (data_dir[-1] != '/'):
      data_dir = data_dir + '/'
    list_name = str(sys.argv[2]).rstrip()
    ecalib_name = str(sys.argv[3]).rstrip()
    fit_fwhm_dependence(data_dir + list_name, ecalib_name)
  else:
    print('\n *** Use error: incorrect number of arguments')
    print('\n     Use examples:')
    print('       fit_efwhm.py <list_name> <ecalib_name>')
    print('       fit_efwhm.py <data_dir> <list_name> <ecalib_name>')
