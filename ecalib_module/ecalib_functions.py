'''
2020-05-18    Anna Zajczyk

  Collection of functions used to handle ComPair Si-tracker emission
  line and energy calibration fitting related tasks.

  Updates
  -------
  2021-07-16 Added treatment of common mode subtraction
  2021-07-27 Finished update to docstrings to reflect most recent changes
  2022-12-08 Removed unused and/or obsolete functions. Change names for some.
'''

import os
import sys
import h5py

import matplotlib.pyplot as plt
from math import nan, isnan
from numpy import sqrt, exp, argmax, append, array, where, zeros, arange, \
  genfromtxt, histogram, mean, std

from tracker_pipeline import l1_qlook


def get_calsources_lib():
  """List of emission sources used for deetector calibration. Line energies
     are given in keV units.
     Expand this if need to.
  """
  print(' --- Reading in the library of calibration sources...')
  calsource = { 'am241':59.5409, \
                'cd109':88.0336, \
                'co57':122.06065 }
  return calsource


def rebin_data(hist, bins, bin_size):
  """Rebins histogram to new binsize given as bin_size'

  Parameters
  ----------
  bins: array_like
    Table with the histogram bins
  hist: array_like
    Table with the histogrammed data
  bin_size: int
    New bin width to which the input histogram will be rebinned

  Returns
  -------
  new_hist: array_like
    Table with rebinned data
  new_bins: array_like
    Table with new bin values
  new_edge_low: array_like
    Table with new bin low edge values
  new_edge_high: array_like
    Table with new bin high edge values
  """
  bin_min = bins[0]
  bin_max = bins[-1]

  new_bins = arange(bin_min, bin_max, bin_size)
  new_nbins = len(new_bins)

  new_edge_low = new_bins - 0.5 * bin_size
  new_edge_high = new_bins + 0.5 * bin_size

  new_hist = zeros(new_nbins)

  for i in range(new_nbins):
    q = ((new_edge_low[i] <= bins) & (bins < new_edge_high[i]))
    new_hist[i] = sum(hist[q])
  return new_hist, new_bins, new_edge_low, new_edge_high


def bin_data(data_table, nbins):
  """Creates histogram with 'nbins' from the input data 'data_table'

  Parameters
  ----------
  data_table: array_like
    Table with the data to be histogrammed
  nbins: int
    Number of bins in the histogram

  Returns
  -------
  hist: array_like
    Table with histogrammed data
  bins: array_like
    Table with bin values
  edge_low: array_like
    Table with bin low edge values
  edge_high: array_like
    Table with bin high edge values
  """
  hist, edges = histogram(data_table, bins=nbins)

  n = len(hist)
  bins = 0.5*(edges[0:n]+edges[1:n+1])
  edge_low = edges[0:n]
  edge_high = edges[1:n+1]
  return hist, bins, edge_low, edge_high


def bin_data2(data_table, binw):
  """Creates histogram with bin width 'binw' from the input data 'data_table'

  Parameters
  ----------
  data_table: array_like
    Table with the data to be histogrammed
  binw: int
    Width of bins in the histogram

  Returns
  -------
  hist: array_like
    Table with histogrammed data
  bins: array_like
    Table with bin values
  edge_low: array_like
    Table with bin low edge values
  edge_high: array_like
    Table with bin high edge values
  """
  if (min(data_table) == max(data_table)):
    bins_tmp = arange(min(data_table), 5*binw, binw)
  else:
    bins_tmp = arange(min(data_table), max(data_table), binw)

  if (max(data_table) >= max(bins_tmp)):
    bins_tmp = append(bins_tmp, [max(bins_tmp)+binw])
  hist, edges = histogram(data_table, bins=bins_tmp)

  n = len(hist)
  bins = 0.5*(edges[0:n]+edges[1:n+1])
  edge_low = edges[0:n]
  edge_high = edges[1:n+1]
  return hist, bins, edge_low, edge_high


def plot_hist(hist, bins, edge_low, edge_high, color=None, vertical=None, alpha_factor=None):
  """Plots histogram.

  Parameters
  ----------
  hist: array_like
    Table with histogrammed data
  bins: array_like
    Table with bin values
  edge_low: array_like
    Table with bin low edge values
  edge_high: array_like
    Table with bin high edge values
  color: str
    Color string to use in plotting
  """
  if (vertical is None):
    vertical = False

  nbins = len(bins)
  if (alpha_factor is None):
    alpha_factor = 0.5
  if (vertical == False):
    if (color is None):
      for i in range(nbins):
        plt.fill_between([edge_low[i], edge_high[i]], [0, 0], \
          [hist[i], hist[i]], facecolor='r', alpha=alpha_factor)
    else:
      for i in range(nbins):
        plt.fill_between([edge_low[i], edge_high[i]], [0, 0], \
          [hist[i], hist[i]], facecolor=color, alpha=alpha_factor)

  if (vertical == True):
    if (color is None):
      for i in range(nbins):
        plt.fill_between([0, hist[i]], [edge_low[i], edge_low[i]], \
          [edge_high[i], edge_high[i]], facecolor='r', alpha=alpha_factor)
    else:
      for i in range(nbins):
        plt.fill_between([0, hist[i]], [edge_low[i], edge_low[i]], \
          [edge_high[i], edge_high[i]], facecolor=color, alpha=alpha_factor)


def read_data_inspectresu(list_name, list_type=None):
  """Reads data from a text file structured to match inspectres_user.py
     input requirements.

  Reads data from a text file. All rows starting with # are treated
  as comment lines. Then,

  For list_type=None:
    First row after comments is a full access path to the directory
    with hdf5 data files, following rows contain names of hdf5 data files
    to be analyzed (one per row!) and the name of the emission source in
    the given hdf5 file. Emission source name should start with small letter.
    Last row specifies type of hdf5 files being analyzed:
      binned - is for binned data sets created with bin_h5data.py
      None - is for regular hdf5 structure

  For list_type='var':
    Each row after comments contains data directory (one per row), name of hdf5
    data file (one per row), the name of the emission source in the given hdf5
    file,and the type of file.
    Emission source name should start with a small letter (am241, co57, cd109).
    Type of file is either:
       binned - is for binned data sets created with bin_h5data.py
       None or multi - is for regular hdf5 structure


  Parameters
  ----------
  list_name: str
    File name
  list_type: str
    Type of the list contained in the input file. Can be None or var. See above
    for exact structure definition.

  Returns
  -------
  data_dir: str
    Path to the directory where the hdf5 files are located.
  file_name: array-like
    List of hdf5 files to be analyzed.
  source_name: array-like
    List of emission source names, which order corresponds to the hdf5 files
    to be analyzed.
  data_type: str
    Data type for the input hdf5 files.
  """
  f = open(list_name)
  flines = f.readlines()

  first_symbol = '#'
  i = 0
  while (first_symbol == '#'):
    first_symbol = flines[i][0]
    i = i + 1
  ix = i-1

  if (list_type is None):
    data_dir = flines[ix].strip()
    if (data_dir[-1] != '/'):
      data_dir = data_dir + '/'

    cflines = flines[ix+1:]
    tmp_length = []
    for j in range(len(cflines)):
      tmp_length = append(tmp_length, [len(cflines[j].strip().split())])
    tmp_length = array(tmp_length)
    q = where(tmp_length == 2)[0]

    file_name = []
    source_name = []

    for k in range(len(q)):
      file_name = append(file_name, [cflines[q[k]].strip().split()[0]])
      source_name = append(source_name, [cflines[q[k]].strip().split()[1]])

    data_type = cflines[-1].strip()
    if (data_type == 'None'):
      data_type = None

  elif (list_type == 'var'):
    cflines = flines[ix:]
    tmp_length = []
    for j in range(len(cflines)):
      tmp_length = append(tmp_length, [len(cflines[j].strip().split())])
    tmp_length = array(tmp_length)
    q = where(tmp_length == 4)[0]

    data_dir = []
    file_name = []
    source_name = []
    data_type = []

    for k in range(len(q)):
      tmp_data_dir = cflines[q[k]].strip().split()[0]
      if (tmp_data_dir[-1] != '/'):
        tmp_data_dir = tmp_data_dir + '/'

      data_dir = append(data_dir, [tmp_data_dir])
      file_name = append(file_name, [cflines[q[k]].strip().split()[1]])
      source_name = append(source_name, [cflines[q[k]].strip().split()[2]])
      data_type = append(data_type, [cflines[q[k]].strip().split()[3]])

  return data_dir, file_name, source_name, data_type


def read_data(file_name, data_type=None):
  """Reads data from a text file.

  Reads data from a text file. All rows starting with # are treated
  as comment lines. Assumes that there is no footer.

  Parameters
  ----------
  file_name: str
    File name
  data_type: str
    Data type for the input file. Choice is: 'float' for an input file
    with numerical data, 'list' for an input file that contains a list
    (e.g., a list of file names), and 'str' for an input file that is
    to be read as containing strings.

  Returns
  -------
  data_table: array_like
  nrow: int
    Number of rows in 'data_table'
  ncol: int
    Number of columns in 'data_table'
  """
  if (data_type):
    if (data_type == 'float'):
      data_table = genfromtxt(file_name, dtype='float', comments='#')
      tmp = data_table.shape
      if (len(tmp) == 1):
        ncol = tmp[0]
        nrow = 1
        data_table = array([data_table])
      else:
        ncol = tmp[1]
        nrow = tmp[0]
    elif (data_type == 'list'):
      data_table = genfromtxt(file_name, dtype='str', comments='#')
      tmp = data_table.size
      nrow = tmp
      ncol = 0
      if (nrow == 1):
        data_table = array([data_table])
    elif (data_type == 'str'):
      data_table = genfromtxt(file_name, dtype='str', comments='#')
      tmp = data_table.shape
      if (len(tmp) == 1):
        ncol = tmp[0]
        nrow = 1
        data_table = array([data_table])
      else:
        ncol = tmp[1]
        nrow = tmp[0]
  else:
    data_table = genfromtxt(file_name, comments='#')
    nrow=-1
    ncol=-1
  return data_table, nrow, ncol


def find_tperiods(data_table, qrst):
  """Finds start-end periods where condition 'qrst' is fulfilled
  in the input 'data_table'

  Parameters
  ----------
  data_table: array_like
    Table with input data
  qrst: bool
    Condition to be checked

  Returns
  -------
  reset_table: array_like
    Table with start and end times where condition 'qrst' is
    fulfilled. Table has two columns (start, end), and number
    of rows that equals to the number of times the 'qrst'
    condition is fulfilled.

  Notes
  -----
  Transition from False-to-True == start point
  Transition from True-to-False == end point
  """
  nhsk = len(qrst)
  rst_tbl_start = []
  rst_tbl_end = []
  tmp = where(qrst == False)
  if (len(tmp[0]) == 0):
    rst_tbl_start.append(data_table[0])
    rst_tbl_end.append(data_table[-1])
  else:
    for i in range(nhsk-1):
      if ((qrst[i] == False) & (qrst[i+1] == True)):
        rst_tbl_start.append(data_table[i+1])
      if ((qrst[i] == True) & (qrst[i+1] == False)):
        rst_tbl_end.append(data_table[i])

  if ((len(rst_tbl_start) == 0) & (len(rst_tbl_end) == 0)):
    reset_flag=-1
    reset_table = zeros((1,2))
    reset_table[0,0] = -1
    reset_table[0,1] = -1
  elif (len(rst_tbl_start) > len(rst_tbl_end)):
    reset_flag=1
    nreset = max(len(rst_tbl_start), len(rst_tbl_end))
    reset_table = zeros((nreset,2))
    for i in range(nreset):
      if (i == (nreset-1)):
        reset_table[i,0] = rst_tbl_start[i]
        reset_table[i,1] = data_table[-1]
      else:
        reset_table[i,0] = rst_tbl_start[i]
        reset_table[i,1] = rst_tbl_end[i]
  elif (len(rst_tbl_start) < len(rst_tbl_end)):
    reset_flag=1
    nreset = max(len(rst_tbl_start), len(rst_tbl_end))
    reset_table = zeros((nreset,2))
    for i in range(nreset):
      if (i == 0):
        reset_table[i,0] = data_table[0]
        reset_table[i,1] = rst_tbl_end[i]
      else:
        reset_table[i,0] = rst_tbl_start[i-1]
        reset_table[i,1] = rst_tbl_end[i]
  elif (len(rst_tbl_start) == len(rst_tbl_end)):
    reset_flag=1
    if (rst_tbl_start[0] <= rst_tbl_end[0]):
      nreset = len(rst_tbl_start)
      reset_table = zeros((nreset,2))
      for i in range(nreset):
        reset_table[i,0] = rst_tbl_start[i]
        reset_table[i,1] = rst_tbl_end[i]
    else:
      nreset = len(rst_tbl_start)+1
      reset_table = zeros((nreset,2))
      for i in range(nreset-1):
        if (i == 0):
          reset_table[i,0] = data_table[0]
          reset_table[i,1] = rst_tbl_end[i]
        else:
          reset_table[i,0] = rst_tbl_start[i-1]
          reset_table[i,1] = rst_tbl_end[i]
        reset_table[nreset-1,0] = rst_tbl_start[nreset-2]
        reset_table[nreset-1,1] = data_table[-1]
  return reset_table


def derivative(y, x):
  """Computes a derivative of the input data.

  Parameters
  ----------
  x: array_like
    Table with X-values
  y: array_like
    Table with Y-values

  Returns
  -------
  dydx: array_like
    Table with derivative values
  x0: array_like
    Table with X-values of where dydx derivative was calculated
  """
  dx = x[1:] - x[0:-1]
  dydx = (y[1:] - y[0:-1])/dx
  x0 = 0.5*(x[0:-1] + x[1:])
  return dydx, x0


def find_empeak(rbins, rhist, binavoid=None):
  """Finds emission peaks in the input spectrum using second
  derivative method.

  Parameters
  ----------
  rbins: array_like
    Table with bin values
  rhist: array_like
    Table of histogrammed data
  binavoid: int
    Number of bins at the top end of the spectrum to avoid (remove
    from analysis)

  Returns
  -------
  pinit: array_like
    Table with initial parameters to be used as an input to a line
    fitting. The parameters are for a Gaussian function: normalization,
    peak centroid and Gaussian sigma.
  fit_ll: float
    Lower limit value to be used as an input to a line fitting.
  fit_ul: float
    Upper limit value to be used as an input to a line fitting.
  """
  if (binavoid is None):
    # to avoid last bin where overflow events live
    binavoid = 1

  ins = argmax(rhist)  # guess Noise Peak parameters
  # sigfit = 0.2*rbins[ins]/2.35
  sigfit = 2.0
  noise_limit = rbins[ins]+3.5*sigfit
  icut = find_element(rbins, noise_limit)

  modhist = rhist[icut:-binavoid]
  modbins = rbins[icut:-binavoid]
  delbin = modbins[1]-modbins[0]

  dydx1, x1 = derivative(modhist, modbins)   # first derivative
  dydx2, x2 = derivative(dydx1, x1)          # second derivative
  # find local maxima from sign of second derivative
  qneg = (dydx2 < 0.0)
  tb1 = find_tperiods(x2, qneg)
  npeaks = tb1.shape[0]

  pk_select = []
  # choose numpeak number of maksimums with highest ADC vals
  for kk in range(npeaks):
    qpk = ((tb1[kk, 0] <= rbins) & (rbins <= tb1[kk, 1]))
    if (max(rhist[qpk]) > 5.0): # --- at least 5cnts in bin to be considered
      pk_select.append([kk, 0.5*(tb1[kk, 0] + tb1[kk, 1]), max(rhist[qpk])])
    else:
      pk_select.append([-1, -1, -1])
  pk_select = array(pk_select)
  pk_select = pk_select[(pk_select[:, 0] != -1)]

  # only interested in the strongest peak of the numpeak number selected
  imx = argmax(pk_select[:, 2])

  sigfit = 0.2*pk_select[imx, 1]/2.35   # guess for the peak sigma
  fit_ll = pk_select[imx, 1]-4.0*sigfit # guess lower peak edge
  fit_ul = pk_select[imx, 1]+4.0*sigfit # guess upper peak edge

  pinit = [pk_select[imx, 2], pk_select[imx, 1], sigfit]
  return pinit, fit_ll, fit_ul


def fgauss(x,norm, cent, width):
  """Returns a Gaussian function."""
  return norm*exp(-(x-cent)**2/(2*width**2))


def flinear(x, const1, const2):
  """Returns a linear function."""
  return const1*x + const2


def get_strip_counts(
    trk_table, layer_side, strip_number, detector_number,
    cm_mode=None, bin_size=None):
  """Creates a binned spectrum for the selected strip.

  Creates a binned spectrum for the selected strip. Strip selection is
  based on provided `strip number`, `layer side` and `detector number`
  inputs.

  Parameters
  ----------
  trk_table: array_like
    Table containing events. Table structure depends on `data_type`.
  layer_side: int
    Intiger (0, 1) defining a top/bottom side of a Si layer.
  strip_number: int
    Integer (0, 191) defining a strip number in a selected Si layer.
  detector_number: int
    Integer (0, 9) defining a layer number in Si tracker.
  cm_mode: str
    Flag (yes / no) that selects if the common mode subtracted data
    should be used in the analysis. If common mode subtracted data are
    not present in the file, the common mode subtracted data will be
    calculated. The default option is to use common mode subtracted data.

  Returns
  -------
  rbins: array_like
    Table containg peak height bins.
  rhist: array_like
    Table containg binned events.
  rerror: array_like
    Table conating errors calculated for each event bin following
    Gehrels (1986, ApJ 303, 336): error = 1 + sqrt(cnts + 0.75)
  no_data_flag: bool
    Flag that indicates lack of data for the selected strip.
    True value is given when there are no events found for the selected
    strip, while False is given when the opposite is true.
  """
  if (cm_mode is None):
    cm_mode = 'yes'

  if (bin_size is None):
    bin_size = 2

  if (cm_mode == 'no'):
    try:
      strip_spec = trk_table['layer{:02d}'.format(detector_number)][:, layer_side, strip_number]
    except:
      strip_spec = trk_table['layer-{:02d}'.format(detector_number)][:, layer_side, strip_number]
  elif(cm_mode == 'yes'):
    try:
      strip_spec = trk_table['layer{:02d}'.format(detector_number)][:, layer_side, strip_number]
    except:
      strip_spec = trk_table['layer-{:02d}'.format(detector_number)][:, layer_side, strip_number]
  if (len(strip_spec) != 0):
    rhist, rbins, redglo, redghi = bin_data2(strip_spec, 2)
    rerror = 1.0 + sqrt(rhist+0.75)
    no_data_flag = False
  else:
    no_data_flag = True
    rhist, rbins, rerror = 0, 0, 0

  return rbins, rhist, rerror, no_data_flag


def find_element(data_table, search_value):
  """Searches in the array for a given value and returns its indexin the array.

  Parameters
  ----------
  data_table: array_like
    An array where search_value is looked for.
  search_value: float
    A value that is to be looked for in the input array data_table.

  Returns
  -------
  search_index: int
    Index in the array where the closest-matching search value was found
  """
  tmp = abs(data_table - search_value)
  tmp_index = where(tmp == min(tmp))
  search_index = tmp_index[0][0]
  return search_index


def get_xyz_pos():
  """Function that calculates XYZ coordinates for the layers and strips of the
     ComPair Tracker system.

  Returns XYZ coordinates in [cm] for the strips in the ComPair tracker.
  It is assumed there are 10 tracker layers in the stack and the topmost layer
  (layer00) is at 8.55cm in Z-coordinate. The XY (0, 0) point is in the center
  of the Si-layer wafer. The spacing between the layers is 1.9 cm, and
  the strips pitch is 510 microns.

  Returns
  -------
  xpos_tbl: array_like
    X-position in [cm] of the tracker strips
  ypos_tbl: array_like
    Y-positions in [cm] of the tracker strips
  zpos_tbl: array_like
    Z-positions in [cm] of the tracker layers
  """
  TKR_strip_pitch = 0.051 # [cm] 510 microns
  TKR_width = 192 * TKR_strip_pitch # [cm] 192 strips of constant pitch
  TKR_edge = TKR_width/2 # DSSD is centered at 0, thus the edges are equivalent to half of the width
  TKR_strip_centroid = TKR_edge - (TKR_strip_pitch)/2 # Gives the center of strip 191
  TKR_layer_spacing = 1.9 # [cm] z spacing between layers
  TKR_top_layer = 8.55 # [cm] Global z position of layer 0 of the TKR (centroid)

  xpos_tbl = zeros((192))
  ypos_tbl = zeros((192))
  zpos_tbl = zeros((10))

  for detector_number in range(10):
    zpos_tbl[detector_number] = float(-detector_number*TKR_layer_spacing + \
      TKR_top_layer)

  for strip_number in range(192):
    xpos_tbl[strip_number] = float(strip_number*TKR_strip_pitch - \
      TKR_strip_centroid)
    ypos_tbl[strip_number] = float(-strip_number*TKR_strip_pitch + \
      TKR_strip_centroid)

  return xpos_tbl, ypos_tbl, zpos_tbl


def get_hit(sig_array, event_number):
  """Finds valid hits in the tracker layer for a given event number.

  For a given event_number tool finds valid hits. Valid hit is identified as
  a hit with positive significance. It is assumed that prior to utilizing
  get_hit function the significance array sig_array is cleaned, i.e.,
  significance threshold is applied to the significance array and all entries
  for which the significance is below that threshold are set to zero.

  Parameters
  ----------
  sig_array: array_like
    Array with significance values
  event_number: int
    Event number

  Returns
  -------
  trk_hit: array_like
    Array with valid hits identified. Array has shape (2, 192), and valid hits
    are marked with 1, while non-valid ones are marked with 0.
  """
  trk_hit = zeros((2, 192), dtype=int)

  for layer_side in range(2):
    q = where(sig_array[event_number, layer_side, :] > 0.0)
    nhits = len(q[0])
    if (nhits > 0):
      trk_hit[layer_side, q[0]] = 1
  return trk_hit


def convert_mask(h5, detector_name):
  """Converts array of masked channels for a given tracker layer to side/strip
     domain.

  Parameters
  ----------
  h5: hdf5 file

  detector_name: string
    Detector layer name for which masked channels are to be identified.

  Returns
  -------
  mask_sd: array_like
    Array of size (2,192) representing layer_side / strip_number position of
    the masked channel. When masked the value in the array is set to 1 and
    0 if unmasked.
  """
  mask_ac = zeros((12, 32), dtype=int)

  for a in range(0,12):
    mask_dir = '/' + detector_name + '/config/asic{:02d}/chan_disable'.format(a)
    mask_ac[a, :] = h5[mask_dir]

  mask_sd = zeros((2, 192), dtype=int)
  for a in range(0, 12):
    for c in range(0, 32):
      tmp1 = l1_qlook.conv_ac2sd(a, c)
      mask_sd[tmp1[0], tmp1[1]] = mask_ac[a, c]
  return mask_sd


def apply_calib2data(h5, ecalib_name, sig_level=None):
  """Applies energy and position calibration to the data

  Parameters
  ----------
  h5: hdf5 object
    HDF5 object
  ecalib_name: str
    Name of the file containing energy calibration solutions.
  sig_level: float
    Significance level above which hits in tracker layers will be considered
    as real events and not noise.


  Returns
  -------
  calib_data: array_like
    Table containg energy- and position-calibrated data. Dimensions
    of the array are (nevent_good, 5) for each layer.
    The five dimensions of the array are as follows:
    (evtid#, xpos, ypos, zpos, energy)
    The number of events nevent_good is equal to the total number of events
    for which position (xyz) and energy (keV) was found. Some events that
    have good positions, but for which no good energy calibration exists
    (based on and/or top/bottom strip information) will not be included
    in this table.
  """

  print('\n *** Warning: No sync-index cleaning is applied to the data ***\n')
  if (sig_level is None):
    sig_level = 3.0
  print(' %%% Hit analysis using significance level of: {:.1f}'.format(sig_level))

  ecalib_table, enrow, encol = read_data(
    ecalib_name, data_type='float')   # get energy calibration params
  x_tbl, y_tbl, z_tbl = get_xyz_pos()   # get XYZ coordinates for TRK

  h5_group_names = array([x for x in h5])
  q = where(h5_group_names != 'vdata')[0]
  h5_group_names = h5_group_names[q]
  print('\n >>> Number of tracker layers in the file: {:d}'.format(len(h5_group_names)))
  for i in h5_group_names:
    print('       ' + i)

  for detector_name in h5_group_names:
    detector_number = int(detector_name[-2:])

    print('\n >>> working on channel_cm_sub: '+ detector_name)
    if ('/' + detector_name + '/vdata/channel_cm_sub' in h5):
      trk_data = h5['/' + detector_name + '/vdata/channel_cm_sub']
      nevent = trk_data.shape[0]
      data_arr = zeros(trk_data.shape, dtype='int')
      ene_arr = zeros(trk_data.shape, dtype='float')
      ene_flg = zeros(trk_data.shape, dtype='float')
      data_arr = array(trk_data)

      evtid = array(h5['/' + detector_name + '/data/event_id'])

      # --- Apply energy calibration to all events if valid calib exists
      for layer_side in range(2):
        for strip_number in range(192):
          qf = where((ecalib_table[:, 0] == detector_number) &
               (ecalib_table[:, 1] == layer_side) &
               (ecalib_table[:, 2] == strip_number))
          indx = qf[0][0]
          if ((ecalib_table[indx, 3] == -2) | (ecalib_table[indx, 3] == -1)):
            ene_arr[ :, layer_side, strip_number] = nan
            ene_flg[ :, layer_side, strip_number] = ecalib_table[indx, 3]
          else:
            ene_arr[ :, layer_side, strip_number] = \
              ecalib_table[indx, 4] * \
              data_arr[ :, layer_side, strip_number] + \
              ecalib_table[indx, 6]
            ene_flg[ :, layer_side, strip_number] = ecalib_table[indx, 3]

      mask = convert_mask(h5, detector_name)

      calib_data = zeros((nevent, 5), dtype='float')
      calib_data[:, 0] = -1

      sig_array = get_significance(data_arr, mask)
      sig_array[(sig_array < sig_level)] = 0

      # --- set up some counters to check where events go
      s_cnt = 0
      m1_cnt = 0
      m2_cnt = 0
      m3_cnt = 0
      z_cnt = 0

      # --- Apply position calibration with this crude algorithm
      for i in range(nevent):
        trk_hit = get_hit(sig_array, i)
        xpos = where(trk_hit[0, :] == True)[0]
        ypos = where(trk_hit[1, :] == True)[0]

        if (len(xpos) == 1) & (len(ypos) == 1):
          s_cnt = s_cnt + 1

          ene = select_energy(ene_arr[ i, 0, xpos[0]], ene_flg[ i, 0, xpos[0]], \
            ene_arr[ i, 1, ypos[0]], ene_flg[ i, 1, ypos[0]])
          if (~isnan(ene)):
            calib_data[i, :] = [evtid[i], \
              x_tbl[xpos[0]], y_tbl[ypos[0]], z_tbl[detector_number], ene]

        # --- FORCE MULTI-HIT events to be SINGLE-HIT type starts here
        elif (len(xpos) > 1) & (len(ypos) == 1):
          m1_cnt = m1_cnt + 1

          tmp_sig = sig_array[ i, 0, xpos]
          ix = argmax(tmp_sig)

          ene = select_energy(ene_arr[ i, 0, xpos[ix]], ene_flg[ i, 0, xpos[ix]], \
            ene_arr[ i, 1, ypos[0]], ene_flg[ i, 1, ypos[0]])
          if (~isnan(ene)):
            calib_data[i, :] = [evtid[i], \
              x_tbl[xpos[ix]], y_tbl[ypos[0]], z_tbl[detector_number], ene]

        elif (len(xpos) == 1) & (len(ypos) > 1):
          m2_cnt = m2_cnt + 1

          tmp_sig = sig_array[ i, 1, ypos]
          ix = argmax(tmp_sig)

          ene = select_energy(ene_arr[ i, 0, xpos[0]], ene_flg[ i, 0, xpos[0]], \
            ene_arr[ i, 1, ypos[ix]], ene_flg[ i, 1, ypos[ix]])
          if (~isnan(ene)):
            calib_data[i, :] = [evtid[i], \
              x_tbl[xpos[0]], y_tbl[ypos[ix]], z_tbl[detector_number], ene]

        elif (len(xpos) > 1) & (len(ypos) > 1):
          m3_cnt = m3_cnt + 1

          tmp_sig1 = sig_array[ i, 0, xpos]
          ix1 = argmax(tmp_sig1)

          tmp_sig2 = sig_array[ i, 1, ypos]
          ix2 = argmax(tmp_sig2)

          ene = select_energy(ene_arr[ i, 0, xpos[ix1]], ene_flg[ i, 0, xpos[ix1]], \
            ene_arr[ i, 1, ypos[ix2]], ene_flg[ i, 1, ypos[ix2]])
          if (~isnan(ene)):
            calib_data[i, :] = [evtid[i], \
              x_tbl[xpos[ix1]], y_tbl[ypos[ix2]], z_tbl[detector_number], ene]
        # --- FORCE MULTI-HIT events to be SINGLE-HIT type ends here
        else:
          z_cnt = z_cnt + 1

      # --- Print some event stats
      print('\n     Total number of analyzed events: ', nevent)
      print('         Single hit events: {:d} ({:.1f} %)'.format(s_cnt, s_cnt/nevent*100.0))
      m_cnt = m1_cnt + m2_cnt + m3_cnt
      print('         Multi hit events: {:d} ({:.1f} %)'.format(m_cnt, m_cnt/nevent*100.0))
      print('               type 1 (multi+single): {:d} ({:.1f} %)'.format(m1_cnt, m1_cnt/nevent*100.0))
      print('               type 2 (single+multi): {:d} ({:.1f} %)'.format(m2_cnt, m2_cnt/nevent*100.0))
      print('               type 3   (multi+multi): {:d} ({:.1f} %)'.format(m3_cnt, m3_cnt/nevent*100.0))
      print('         Zero hit events: {:d} ({:.1f} %)'.format(z_cnt, z_cnt/nevent*100.0))

      qecal = where(calib_data[:, 0] != -1)[0]
      print('\n         Energy and position-calibrated events: {:d} ({:.1f} %)'.format(len(qecal), len(qecal)/nevent*100.0))

      # --- Create dataset in the copy of h5 file that will host calibrated events
      dset_calib = \
        h5.create_dataset('/'+detector_name+'/vdata/calib_data', calib_data.shape, dtype='float64')
      dset_calib[:] = calib_data[:]
    else:
      print(' *** Error: channel_cm_sub dataset missing in HDF structure.')
      print(' ***        Check that /layerXX/vdata/channel_cm_sub datasets')
      print(' ***        are in the HDF5 file.')
      sys.exit()
  h5.close()


def select_energy(ene_x, x, ene_y, y):
  """Energy selection algorithm

  Parameters
  ----------
  ene_x: float
    Energy of the event in X-going strip or top layer.
  x: float
    Energy calibration quality flag for the X-going strip or top layer.
  ene_y: float
    Energy of the event in Y-going strip or bottom layer.
  y: float
    Energy calibration quality flag for the Y-going strip or bottom layer.

  Returns
  -------
  ene: float
    Energy selected for the given event based on meeting one of the conditions
    in the energy selection algorithm.
  """
  # --- Energy selection algorithm ---
  if ((x < 0) & (y < 0)):
    ene = nan
  elif ((x < 0) & (y > 0)):
    ene = ene_y
  elif ((x > 0) & (y < 0)):
    ene = ene_x
  elif (x == y):
    ene = 0.5*(ene_x + ene_y)
  elif ((x > 0) & (y > 0) & (x < y)):
    ene = ene_x
  elif ((x > 0) & (y > 0) & (x > y)):
    ene = ene_y
  return ene


def get_channel_mask(h5):
  """Compiles array of masked channels for all layers present in the input hdf5
     file.

  Parameters
  ----------
  h5: hdf5 file

  Returns
  -------
  channel_mask: dictionary_like
    Dictionary of arrays containing information on masked channels. Each array
    is of size (2,192) representing layer_side / strip_number position of
    the masked channel. When masked the value in the array is set to 1 and
    0 if unmasked. To retrieve information from the dictionary name of the
    layer (string 'layerXX', where XX is layer number) needs to be provided
    as an argument of channel_mask.
  """
  print(' >>> Compiling array of masked channels')
  channel_mask = {}

  group_names = array([x for x in h5])
  q = where(group_names != 'vdata')[0]
  group_names = group_names[q]
  nlayer = len(group_names)

  for detname in group_names:
    detnum = int(detname[-2:])

    mask = zeros((12, 32), dtype=int)
    lr = zeros((2, 192), dtype=int)
    tt = 0
    for a in range(12):
      mask_dir = '/'+detname+'/config/asic{:02d}/chan_disable'.format(a)
      mask[a, :] = h5[mask_dir]

      q = where(mask[a, :] == 1)[0]   # 1 = masked channel, 0 = unmasked channel
      tt = tt + len(q)
    print('        Number of disabled channels for Layer{:02d}: {:02d}'.format(detnum, tt))
    for a in range(12):
      for ch in range(32):
        tmp1 = l1_qlook.conv_ac2sd(a, ch)
        lr[tmp1[0], tmp1[1]] = mask[a, ch]
    channel_mask[detname] = lr
  return channel_mask


def select_bin_size():
  """Prompts user to select bin size for analysis of event spectra.

  Returns
  -------
  bin_size: int
    Selected bin width for spectral analysis
  """
  bin_size = input('\n >>> Choose spectral bin width in ADUs (min value is ' + \
    '1ADU) : ').strip()
  if (bin_size == ''):
    bin_size = 1
  else:
    bin_size = float(bin_size)

  if (bin_size < 1):
    print(' *** Chosen bin size smaller than 1ADU. Setting bin size to 1ADU.')
    bin_size = 1

  bin_size = int(bin_size)
  print(' --- Bin size set to: {:2d} ADUs'.format(bin_size))
  return bin_size


def get_strip2x(s):
  """Converst strip number located on side 0 to X-coordinate in cm.

  Parameters
  ----------
  s: int
    Strip number on side 0 for which to calculate X-coordinate

  Returns
  -------
  x: float
    X-coordinate in cm of the input strip number on side 0
  """
  TKR_strip_pitch = 0.051 # [cm] 510 microns
  TKR_width = 192 * TKR_strip_pitch # [cm] 192 strips of constant pitch
  TKR_edge = TKR_width/2 # DSSD is centered at 0, thus the edges are equivalent to half of the width
  TKR_strip_centroid = TKR_edge - (TKR_strip_pitch)/2 # Gives the center of strip 191

  x = s * TKR_strip_pitch - TKR_strip_centroid
  return x


def get_x2strip(x):
  """Converst X-coordinate in cm to strip number located on side 0.

  Parameters
  ----------
  x: float
    X-coordinate in cm of the position on the tracker layer

  Returns
  -------
  s: int
    Strip number on side 0 to which input X-coordinate corresponds to
  """
  TKR_strip_pitch = 0.051 # [cm] 510 microns
  TKR_width = 192 * TKR_strip_pitch # [cm] 192 strips of constant pitch
  TKR_edge = TKR_width/2 # DSSD is centered at 0, thus the edges are equivalent to half of the width
  TKR_strip_centroid = TKR_edge - (TKR_strip_pitch)/2 # Gives the center of strip 191

  s = (x + TKR_strip_centroid) / TKR_strip_pitch
  return s


def get_strip2y(s):
  """Converst strip number located on side 1 to Y-coordinate in cm.

  Parameters
  ----------
  s: int
    Strip number on side 0 for which to calculate Y-coordinate

  Returns
  -------
  y: float
    Y-coordinate in cm of the input strip number on side 0
  """
  TKR_strip_pitch = 0.051 # [cm] 510 microns
  TKR_width = 192 * TKR_strip_pitch # [cm] 192 strips of constant pitch
  TKR_edge = TKR_width/2 # DSSD is centered at 0, thus the edges are equivalent to half of the width
  TKR_strip_centroid = TKR_edge - (TKR_strip_pitch)/2 # Gives the center of strip 191

  y = - s * TKR_strip_pitch + TKR_strip_centroid
  return y


def get_y2strip(y):
  """Converst Y-coordinate in cm to strip number located on side 1.

  Parameters
  ----------
  y: float
    Y-coordinate in cm of the position on the tracker layer

  Returns
  -------
  s: int
    Strip number on side 1 to which input Y-coordinate corresponds to
  """
  TKR_strip_pitch = 0.051 # [cm] 510 microns
  TKR_width = 192 * TKR_strip_pitch # [cm] 192 strips of constant pitch
  TKR_edge = TKR_width/2 # DSSD is centered at 0, thus the edges are equivalent to half of the width
  TKR_strip_centroid = TKR_edge - (TKR_strip_pitch)/2 # Gives the center of strip 191

  s = (TKR_strip_centroid - y) / TKR_strip_pitch
  return s


def get_significance(data_arr, mask):
  """Calculates significance of entries in the input data array for the tracker
  layer. Applies masked channel information.

  Parameters
  ----------
  data_arr: array_like
    Data array with events in ADU units for which significance is to be
    calculated.
  mask: array_like
    Array with information on masked channels for the tracker layer

  Returns
  -------
  sig_array: array_like
    Array with calculated significance values for the tracker layer events.
  """
  sig_array = zeros(data_arr.shape, dtype='float')
  for layer_side in range(0, 2):
    for strip_number in range(0, 192):
      if (mask[layer_side, strip_number] == 0):
        stdev_strip = std(data_arr[:, layer_side, strip_number])
        if (stdev_strip != 0):
          sig_array[:, layer_side, strip_number] = \
            (data_arr[:, layer_side, strip_number] - \
            mean(data_arr[:, layer_side, strip_number])) / \
            std(data_arr[:, layer_side, strip_number])
        else:
          sig_array[:, layer_side, strip_number] = 0
  return sig_array
