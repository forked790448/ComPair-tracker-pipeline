'''
2022-04-01  Anna Zajczyk

  Inspect results for runs with calibration sources. Displays and fits
  emission lines for 3 input files for selected strip. Also performs
  fitting of energy calibration coefficients based on the fitted emission
  line results. User can select which strip to work on or can provide
  text input file with a list of strips to work on (e.g., created with
  extract_info.py tool)

  Updates
  -------
  2022-11-22 Added option of providing as input a type of list with files
              located in different directories and of binned/multi type.
              Main change done to read_data_inspectresu function and then
              propagated and cleaned in the main code.
  2022-08-07 Added option to rebin the data to user-selected bin width.
              The bin size is an integer, so if float is given as an input
              its type will be casted to integer. There is no intelligent
              handling of the conversion float -> integer like ceiling or
              floor or rounding.
  2022-08-06 Added handling of binned hdf5 data
'''

import h5py
import sys
import os

from datetime import datetime
import matplotlib.pyplot as plt

from scipy.optimize import curve_fit
from numpy import sqrt, diag, argmax, append, array, where, zeros, arange, \
  unique

from ecalib_functions import plot_hist, fgauss, bin_data2, find_empeak, \
  flinear, read_data, get_channel_mask, rebin_data, read_data_inspectresu, \
  get_calsources_lib, select_bin_size


def print_ecal_coef(tmp_ecal):
  print('\n -----------------------------------------------------------')
  print(' --- Energy calibration coefficients E = A*PH + B')
  print(' -----------------------------------------------------------')
  print('        A = {:10.4f} +/- {:10.4f}'.format(tmp_ecal[0], tmp_ecal[2]))
  print('        B = {:10.4f} +/- {:10.4f}'.format(tmp_ecal[1], tmp_ecal[3]))
  print(' -----------------------------------------------------------\n')


def get_ecalib_coef(const1, const2, const1_err, const2_err):
  """Calculates energy calibration coeffcients.

  (const1, const2) are transformation coefficients from the formula
    PH = const1 * E + const2
  where E is the energy [in keV] and PH is the peak height [in ADU].
  (const1_err, const2_err) are the errors on the coefficients.
  (aecal, becal) and (aerr, berr) are the coefficients and their
  errors, respectively, calculated for an inverse transformation:
    E = aecal * PH + becal

  Parameters
  ----------
  const1: float
  const2: float
  const1_err: float
  const2_err: float

  Returns
  -------
  aecal: float
  becal: float
  aerr: float
  berr: float

  """
  aecal = 1.0/const1
  becal = -const2/const1

  aerr = abs(-1.0/const1**2.0)*const1_err
  berr = abs(-const2/const1**2.0)*const1_err + abs(-1.0/const1)*const2_err
  return aecal, becal, aerr, berr

def save2file(fit_table_all, ecal_table, data_dir):
  print(' --- Writing results to output files... ')

  fout = open(
    data_dir+'FitReview_EmissionLineFits.dat', 'a')
  fout.write('#  Date created [UTC]: ' + \
    datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S') + '\n')
  fout.write('#  FitFlag values:\n')
  fout.write('#         1 - good fit\n')
  fout.write('#         2 - problematic fit\n')
  fout.write('#         3 - fit failed\n')
  fout.write('#         4 - no data\n')
  fout.write('#         5 - masked channel\n')
  fout.write('#\n')
  fout.write('#  Source   Detector   LayerSide   Strip   FitFlag   Peak   '\
    'Peak_error   Sigma   Sigma_error   Norm   Norm_error\n')

  fit_table_all =array(fit_table_all)
  for i in range(fit_table_all.shape[0]):
    for j in range(fit_table_all.shape[1]):
      fout.write(' {:7.2f}   {:3.0f}   {:3.0f}   {:3.0f}   {:3.0f}   {:8.4f}   ' \
        '{:8.4f}   {:8.4f}   {:8.4f}   {:8.4f}   {:8.4f}\n'.format(\
        fit_table_all[i, j, 0], fit_table_all[i, j, 1], fit_table_all[i, j, 2], fit_table_all[i, j, 3], \
        fit_table_all[i, j, 4], fit_table_all[i, j, 5], fit_table_all[i, j, 6], fit_table_all[i, j, 7], \
        fit_table_all[i, j, 8], fit_table_all[i, j, 9], fit_table_all[i, j, 10]))
  fout.close()

  fout2 = open(
    data_dir+'FitReview_EnergyCalibrationCoefficients.dat', 'a')
  fout2.write('#  Date created [UTC]: ' + \
    datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S') + '\n')
  fout2.write('#  Energy calibration form: E = a1coef * PH + a2coef\n')
  fout2.write('#  FitFlag values:\n')
  fout2.write('#        -3 - masked channel\n')
  fout2.write('#        -2 - no em lines / no data\n')
  fout2.write('#        -1 - fit failed\n')
  fout2.write('#         1 - > 2em lines\n')
  fout2.write('#         2 - = 2 em lines\n')
  fout2.write('#         3 - only 1 em line\n')
  fout2.write('#\n')
  fout2.write('#  Detector   LayerSide   Strip   FitFlag   a1coef   ' \
    'a1coeff_err   a2coef   a2coef_err\n')

  ecal_table = array(ecal_table)
  for i in range(ecal_table.shape[0]):
    fout2.write(' {:6.0f}   {:3.0f}   {:3.0f}   {:3.0f}   {:8.4f}   ' \
      '{:8.4f}   {:8.4f}   {:8.4f}\n'.format(\
      ecal_table[i, 0], ecal_table[i, 1], ecal_table[i, 2], ecal_table[i, 3], \
      ecal_table[i, 4], ecal_table[i, 6], ecal_table[i, 5], ecal_table[i, 7]))
  fout2.close()


def analyze_multi_file_strip_data(detector_number, layer_side, strip_number,
  data_dir, file_name, emsource, emlines, bin_size, fit_table_all, ecal_table,
  input_data_type=None, list_type=None):

  nfiles = len(file_name)
  detname = 'layer{:02d}'.format(detector_number)

  fit_table = []   # will store gauss coefficients here [SRS_ENE, L#, SD#,
                   # ST#, FLG, peak_centroid, peak_centroid_error,
                   # gauss_sigma, gauss_sigma_error,
                   # norm, norm_error]
  # -----------------------------------------------------------------------------
  # --- Inspect emission line fitting for selected strip and various source files
  # -----------------------------------------------------------------------------
  if (nfiles <= 3):
    scrn_hi = 131
    fsize = [12, 4]
  elif (3 < nfiles) & (nfiles <= 6):
    scrn_hi = 231
    fsize = [12, 8]
  elif (6 < nfiles) & (nfiles <= 8):
    scrn_hi = 241
    fsize = [14, 8]

  plt.close('all')
  plt.ion()
  plt.figure(1, figsize=fsize)
  plt.clf()
  plt.suptitle('Layer: '+str(detector_number)+'   Side: '+str(layer_side)+\
    '   Strip: '+str(strip_number))

  good_files_flg = []
  for k in range(nfiles):
    print('\n -----------------------------------------------------------')
    print(' --- File: '+file_name[k])
    print(' -----------------------------------------------------------')
    if (list_type == 'var'):
      h5 = h5py.File(data_dir[k] + file_name[k], 'r')
      input_data_type_file = input_data_type[k]
    else:
      h5 = h5py.File(data_dir + file_name[k], 'r')
      input_data_type_file = input_data_type

    if (input_data_type_file == 'binned'):
      try:
        key = '/' + detname + '/binned_data'
        single_channel_mask = array(h5['/' + detname + '/chan_disable'][layer_side, strip_number])
        no_layer_flag = False
      except:
        no_layer_flag = True
    else:
      try:
        key = '/' + detname + '/vdata/channel_cm_sub'
        channel_mask = get_channel_mask(h5)
        single_channel_mask = channel_mask[detname][layer_side, strip_number]
        no_layer_flag = False
      except:
        no_layer_flag = True

    good_files_flg = append(good_files_flg, no_layer_flag)

    if (no_layer_flag is False):
      if (single_channel_mask == 0):
        if (input_data_type_file == 'binned'):
          rbins = array(h5['/edges'][:, 0])
          redglo = array(h5['/edges'][:, 1])
          redghi = array(h5['/edges'][:, 2])
          rhist = array(h5[key][layer_side, strip_number, :])
          if (bin_size != 1):
            rhist, rbins, redglo, redghi = rebin_data(\
              rhist, rbins, bin_size)
          rerror = 1.0 + sqrt(rhist + 0.75)
          no_data_flag = False
        else:
          strip_spec = array(h5[key][:, layer_side, strip_number])

          if (len(strip_spec) != 0):
            rhist, rbins, redglo, redghi = bin_data2(strip_spec, bin_size)
            rerror = 1.0 + sqrt(rhist+0.75)
            no_data_flag = False
          else:
            no_data_flag = True
            rhist, rbins, rerror = 0, 0, 0

        if (no_data_flag is False):
          plt.subplot(scrn_hi)
          plt.cla()
          plt.xlabel('PH [ADU]')
          if(k == 0):
            plt.ylabel('Counts/bin')
          plt.xlim(-20, 220)
          plot_hist(rhist, rbins, redglo, redghi, color='violet')
          plt.text(20, 0.8*max(rhist), emsource[k]+': auto fit failed')
          plt.show()
          plt.pause(0.0001)

          try:
            pinit, fit_ll, fit_ul = find_empeak(rbins, rhist)
            qf0 = ((fit_ll < rbins) & (rbins < fit_ul))
            p0,t0 = curve_fit(
              fgauss, rbins[qf0], rhist[qf0], p0=pinit, sigma=rerror[qf0],
              absolute_sigma=True)
            perr0 = sqrt(diag(t0))

            plt.subplot(scrn_hi)
            plt.cla()
            plt.xlabel('PH [ADU]')
            if(k == 0):
              plt.ylabel('Counts/bin')
            plt.xlim(-20, 220)
            plt.ylim(0, 1.1*max(rhist[qf0]))
            plot_hist(rhist, rbins, redglo, redghi, color='violet')
            plt.plot(rbins[qf0], fgauss(rbins[qf0], p0[0], p0[1], p0[2]), '-.', \
              color='blue', alpha=0.5, label=emsource[k])
            plt.legend()
            plt.show()
            plt.pause(0.0001)

            print(' AUTO: Best fit parameters for a Gaussian model')
            print('                Norm = {:11.5f} +/- {:9.5f}'.format(p0[0], \
              perr0[0]))
            print('       Peak centroid = {:11.5f} +/- {:9.5f}'.format(p0[1], \
              perr0[1]))
            print('               Sigma = {:11.5f} +/- {:9.5f}'.format(p0[2], \
              perr0[2]))

            refit_flag = input(\
              ' >>> Refit the emission peak (yes/no/[f]ail): ')
            if ((refit_flag.strip() == 'yes') | (refit_flag.strip() == '1') \
              | (refit_flag.strip() == 'y')):
              fit_ll, fit_ul = input(\
                ' >>> Define LOWER and UPPER limit for the fit region: ').split()

              fit_ll, fit_ul = [int(fit_ll), int(fit_ul)]

              qf = ((fit_ll < rbins) & (rbins < fit_ul))
              pinit = [max(rhist[qf]), rbins[qf][argmax(rhist[qf])], 1.]
              print('\n Initial parameters for the fit (norm, centroid, sigma): ')
              print('   {:.4f}   {:.4f}   {:.4f}'.format(\
                pinit[0], pinit[1], pinit[2]))
              print('\n Total number of counts in the fit region: ')
              print('   {:.0f} \n'.format(sum(rhist[qf])))

              fit_params = input(' >>> Execute the fit with these initial'
                ' parameters (yes / no): ')
              if (fit_params.strip() == 'no') | (fit_params.strip() == '0'):
                pi1, pi2, pi3 = input(' >>> Define initial parameters for'
                  ' the fit (norm, centroid, sigma): ').split()
                pinit[0] = float(pi1)
                pinit[1] = float(pi2)
                pinit[2] = float(pi3)

              p1,t1 = curve_fit(
                fgauss, rbins[qf], rhist[qf], p0=pinit, sigma=rerror[qf],
                absolute_sigma=True)
              perr1 = sqrt(diag(t1))

              plt.subplot(scrn_hi)
              plt.cla()
              plt.xlabel('PH [ADU]')
              if(k == 0):
                plt.ylabel('Counts/bin')
              plt.xlim(-20, 220)
              plt.ylim(0, 1.1*max(rhist[qf]))
              plot_hist(rhist, rbins, redglo, redghi, color='violet')
              plt.plot(rbins[qf], fgauss(rbins[qf], p1[0], p1[1], p1[2]), '-.', \
                color='red', alpha=0.5)
              plt.plot(rbins[qf0], fgauss(rbins[qf0], p0[0], p0[1], p0[2]), '-.', \
                color='blue', alpha=0.5)
              plt.show()
              plt.pause(0.0001)

              print(' ----')
              print(' USER: Best fit parameters for a Gaussian model')
              print('                Norm = {:11.5f} +/- {:9.5f}'.format(p1[0], \
                perr1[0]))
              print('       Peak centroid = {:11.5f} +/- {:9.5f}'.format(p1[1], \
                perr1[1]))
              print('               Sigma = {:11.5f} +/- {:9.5f}'.format(p1[2], \
                perr1[2]))
              print(' -----------------------------------------------------------\n')

              good_flag = input(' >>> Use USER fit or AUTO fit results?'
                ' (user = 1 / auto = 0): ')
              if (good_flag.strip() == '0'):
                p1 = p0
                perr1 = perr0
              else:
                print(' ---- Using USER fit results...')

              fit_row = [emlines[k], detector_number, layer_side, strip_number, 1, p1[1], perr1[1],
                p1[2], perr1[2], p1[0], perr1[0]]

            elif(refit_flag.strip() == 'f'):
              fit_row = [emlines[k], detector_number, layer_side, strip_number, 3,
                -1, -1, -1, -1, -1, -1]
            else:
              p1 = p0
              perr1 = perr0
              fit_row = [emlines[k], detector_number, layer_side, strip_number, 1, p1[1], perr1[1],
                p1[2], perr1[2], p1[0], perr1[0]]

          except:
            print(' *** Warning: Fit failed for strip (layer / side / '
              'strip): ' + str(detector_number) + ' / '+ str(layer_side) +\
              ' / ' + str(strip_number))
            fit_row = [emlines[k], detector_number, layer_side, strip_number, 3,
              -1, -1, -1, -1, -1, -1]

    else:
      if (no_layer_flag is True):
        print(' --- !!! No layer data in this file: ', file_name[k])
        print(' --- !!!    skipping fitting (layer / side / '
          'strip): ' + str(detector_number) + ' / '+ str(layer_side) +\
          ' / ' + str(strip_number))
      else:
        print(' --- !!! This channel is masked: skipping fitting (layer / side / '
          'strip): ' + str(detector_number) + ' / '+ str(layer_side) +\
          ' / ' + str(strip_number))

      fit_row = [emlines[k], detector_number, layer_side, strip_number, 5,
        -1, -1, -1, -1, -1, -1]

    fit_table.append(fit_row)
    scrn_hi = scrn_hi+1

  print('\n -----------------------------------------------------------')
  print(' --- Finding energy calibration solution... ')
  print(' -----------------------------------------------------------')

  qfile = where(good_files_flg == 0)[0]
  lqfile = len(qfile)

  if (lqfile == 0):
    work_this_strip = False
    fit_table_all.append(fit_table)
    tmp_rc = [detector_number, layer_side, strip_number, -3, -1, -1, -1, -1]
    ecal_table.append(tmp_rc)
  elif ((lqfile != 0) & (single_channel_mask == 1)):
    work_this_strip = False
    fit_table_all.append(fit_table)
    tmp_rc = [detector_number, layer_side, strip_number, -3, -1, -1, -1, -1]
    ecal_table.append(tmp_rc)
  else:
    # -----------------------------------------------------------------------------
    # --- Get energy calibration for selected strip based on fits
    # -----------------------------------------------------------------------------
    emline_data = array(fit_table)

    rflg = emline_data[:, 4]        # quality flag for em.line fit
    rpeak = emline_data[:, 5]       # peak centroid
    rpeak_err = emline_data[:, 6]   # error on peak centroid
    remlines = emline_data[:, 0]    # assigned energies of emission lines

    qflg = where(rflg == 1)   # choose only GOOD quality fits
    uene = unique(remlines[qflg[0]])   # unique calibration sources

    if (len(uene) > 2):
      try:
        pinit = [10., 1.]

        p1, t1 = curve_fit(
          flinear, remlines[qflg[0]], rpeak[qflg[0]], p0=pinit,
          sigma=rpeak_err[qflg[0]], absolute_sigma=True)
        perr1 = sqrt(diag(t1))

        tmp_ecal = get_ecalib_coef(p1[0], p1[1], perr1[0], perr1[1])
        tmp_rc = append([detector_number, layer_side, strip_number, 1],
          tmp_ecal)

        print_ecal_coef(tmp_ecal)

        xtmp = arange(-10, 250, 1)
        plt.ion()
        plt.figure(2)
        plt.clf()
        plt.title('Layer: '+str(detector_number)+'   Side: ' + \
          str(layer_side) + '   Strip: '+str(strip_number))
        plt.xlabel('Energy [keV]')
        plt.ylabel('PH [ADU]')
        plt.errorbar(remlines[qflg[0]], rpeak[qflg[0]], rpeak_err[qflg[0]],\
          fmt='.', label='data')
        plt.plot(xtmp, flinear(xtmp, p1[0], p1[1]), '-.', label='fit')
        plt.legend(loc='upper left')
        plt.show()

      except:
        print(' *** Warning: EnergyCalib fit failed for strip (layer / side / ' \
          'strip): ' + str(detector_number) + ' / '+ str(layer_side) + \
          ' / ' + str(strip_number))
        tmp_rc = [detector_number, layer_side, strip_number,
          -1, -1, -1, -1, -1]
    elif(len(uene) == 2):
      print(' *** Caution: TWO emission lines for strip (layer / side / ' \
        'strip): ' + str(detector_number) + ' / '+ str(layer_side) + \
        ' / ' + str(strip_number))
      try:
        pinit = [10., 1.]
        p1, t1 = curve_fit(
          flinear, remlines[qflg[0]], rpeak[qflg[0]], p0=pinit,
          sigma=rpeak_err[qflg[0]], absolute_sigma=True)
        perr1 = sqrt(diag(t1))
        tmp_ecal = get_ecalib_coef(p1[0], p1[1], perr1[0], perr1[1])
        tmp_rc = append(
          [detector_number, layer_side, strip_number, 2], tmp_ecal)

        print_ecal_coef(tmp_ecal)

        xtmp = arange(-10, 250, 1)
        plt.ion()
        plt.figure(2)
        plt.clf()
        plt.title('Layer: '+str(detector_number)+'   Side: ' + \
          str(layer_side) + '   Strip: '+str(strip_number))
        plt.xlabel('Energy [keV]')
        plt.ylabel('PH [ADU]')
        plt.errorbar(remlines[qflg[0]], rpeak[qflg[0]], rpeak_err[qflg[0]],\
          fmt='.', label='data')
        plt.plot(xtmp, flinear(xtmp, p1[0], p1[1]), '-.', label='fit')
        plt.legend(loc='upper left')
        plt.show()
      except:
        print(' *** Warning: EnergyCalib fit failed for strip (layer / side / ' \
          'strip): ' + str(detector_number) + ' / '+ str(layer_side) + \
          ' / ' + str(strip_number))
        tmp_rc = [detector_number, layer_side, strip_number,
          -1, -1, -1, -1, -1]
    elif (len(uene) == 1):
      print(' *** Caution: SINGLE emission line for strip (layer / side / ' \
        'strip): ' + str(detector_number) + ' / '+ str(layer_side) + \
        ' / ' + str(strip_number))
      weights = rpeak_err**(-2.0)   # calculate weights
      rpeak_weightavg = sum(rpeak[qflg[0]]*weights[qflg[0]]) / \
        sum(weights[qflg[0]])   # calculate weighted average
      rpeak_weightavg_err = sqrt( sum(
        weights[qflg[0]]**2.0 * rpeak_err[qflg[0]]**2.0) / \
        sum(weights[qflg[0]])**2.0)   # calculate error on weighted average

      p1 = [rpeak_weightavg/uene[0], 0.0]
      perr1 = [rpeak_weightavg_err/uene[0], 0.0]
      tmp_ecal = get_ecalib_coef(p1[0], p1[1], perr1[0], perr1[1])

      tmp_rc = append([detector_number, layer_side, strip_number, 3], tmp_ecal)

      print_ecal_coef(tmp_ecal)

      xtmp = arange(-10, 250, 1)
      plt.ion()
      plt.figure(2)
      plt.clf()
      plt.title('Layer: '+str(detector_number)+'   Side: ' + \
        str(layer_side) + '   Strip: '+str(strip_number))
      plt.xlabel('Energy [keV]')
      plt.ylabel('PH [ADU]')
      plt.errorbar(uene[0], rpeak_weightavg, rpeak_weightavg_err, fmt='.', \
        label='data')
      plt.plot(xtmp, flinear(xtmp, p1[0], p1[1]), '-.', label='fit')
      plt.legend(loc='upper left')
      plt.show()
    else:
      print(' *** Warning: EnergyCalib no good data points for strip (layer / side / ' \
        'strip): ' + str(detector_number) + ' / '+ str(layer_side) + \
        ' / ' + str(strip_number))
      tmp_rc = [detector_number, layer_side, strip_number, -2, -1, -1, -1, -1]

    try_again = input(' >>> Redo full work for strip (yes/no): ')
    if ((try_again.strip() == 'yes') | (try_again.strip() == '1') \
      | (try_again.strip() == 'y')):
      work_this_strip = True
    else:
      work_this_strip = False
      fit_table_all.append(fit_table)
      ecal_table.append(tmp_rc)

  return fit_table_all, ecal_table, work_this_strip


def inspectres_user(list_name, list_type=None):
  if (len(list_name.split('/')) == 1):
    cwd = os.getcwd()
    list_name = cwd + '/' + list_name

  data_dir, file_name, emsource, input_data_type = \
    read_data_inspectresu(list_name, list_type)

  bin_size = select_bin_size()

  nfiles = len(file_name)

  calsource = get_calsources_lib()

  fit_table_all = []   # will store gauss coefficients here [SRS_ENE, L#, SD#,
                   # ST#, FLG, peak_centroid, peak_centroid_error,
                   # gauss_sigma, gauss_sigma_error,
                   # norm, norm_error]

  ecal_table = []   # will store ecalib coefficients here [L#, SD#, ST#, FLG,
                    # a1, a2, a1err, a2err]

  emlines = zeros(nfiles)
  for k in range(nfiles):
    # assign energy to emission line used in each file
    emlines[k] = calsource[emsource[k]]

  exec_form = input('\n >>> Do you have list of strips to analyze (yes/no): ')

  # --- LIST INPUT
  if (exec_form.strip() == 'yes') | (exec_form.strip() == '1'):
    list_name = input(' >>> List of strips for analysis: ').strip()
    strip_table, nrow, ncol = read_data(list_name, data_type='float')
  # ***
    for m in range(nrow):
      # -----------------------------------------------------------------------------
      # --- Select strip for analysis
      # -----------------------------------------------------------------------------
      detector_number = int(strip_table[m, 0])
      layer_side = int(strip_table[m, 1])
      strip_number = int(strip_table[m, 2])

      print('\n -----------------------------------------------------------')
      print(' --- Working on strip (layer / side / '
        'strip): ' + str(detector_number) + ' / '+ str(layer_side) +\
        ' / ' + str(strip_number))
      print(' -----------------------------------------------------------')

      work_this_strip = True
      while(work_this_strip == True):
        fit_table_all, ecal_table, work_this_strip = \
          analyze_multi_file_strip_data(detector_number, layer_side, strip_number, \
          data_dir, file_name, emsource, emlines, bin_size, fit_table_all, ecal_table, \
          input_data_type = input_data_type, list_type = list_type)

      exit_flag = input(' >>> Continue work (yes/no): ')
      if (exit_flag.strip() == 'no') | (exit_flag.strip() == '0') \
        | (exit_flag.strip() == 'n'):
        if (list_type == 'var'):
          save2file(fit_table_all, ecal_table, data_dir=data_dir[0])
        else:
          save2file(fit_table_all, ecal_table, data_dir)
        print(' --- Saving data and exiting the program now...')
        sys.exit()

    if (list_type == 'var'):
      save2file(fit_table_all, ecal_table, data_dir=data_dir[0])
    else:
      save2file(fit_table_all, ecal_table, data_dir)

  # --- USER INPUT
  else:
    next_strip_flag = True
    while (next_strip_flag):
      # -----------------------------------------------------------------------------
      # --- Select strip for analysis
      # -----------------------------------------------------------------------------
      user_input = input(
        '\n >>> Define detector'\
        ' number (0-9), layer side (0 / 1), and strip number (0-191) for '\
        'spectral fit: '\
        )
      if user_input:
        detector_number, layer_side, strip_number = user_input.split()
      else:
        print(' *** Ooops! something went wrong, let\'s try again...')
        detector_number, layer_side, strip_number = input(
          '\n >>> Define detector'\
          ' number (0-9), layer side (0 / 1), and strip number (0-191) for '\
          'spectral fit: '\
          ).split()

      detector_number = int(detector_number)
      strip_number = int(strip_number)
      layer_side = int(layer_side)

      print('\n -----------------------------------------------------------')
      print(' --- Working on strip (layer / side / '
        'strip): ' + str(detector_number) + ' / '+ str(layer_side) +\
        ' / ' + str(strip_number))
      print(' -----------------------------------------------------------')

      work_this_strip = True
      while(work_this_strip == True):
        fit_table_all, ecal_table, work_this_strip = \
          analyze_multi_file_strip_data(detector_number, layer_side, strip_number, \
          data_dir, file_name, emsource, emlines, bin_size, fit_table_all, ecal_table, \
          input_data_type = input_data_type, list_type = list_type)

      next_strip_flag = input(' >>> Fit another strip (yes/no): ')
      if (next_strip_flag.strip() == 'yes') | (next_strip_flag.strip() == '1') \
        | (next_strip_flag.strip() == 'y'):
        next_strip_flag = True
      else:
        next_strip_flag = False

    if (list_type == 'var'):
      save2file(fit_table_all, ecal_table, data_dir=data_dir[0])
    else:
      save2file(fit_table_all, ecal_table, data_dir)

  print(' --- Ufff! Finished')

if __name__ == "__main__":
  if (len(sys.argv) == 2):
    list_name = str(sys.argv[1]).rstrip()
    inspectres_user(list_name)
  elif (len(sys.argv) == 3):
    list_name = str(sys.argv[1]).rstrip()
    list_type = str(sys.argv[2]).rstrip()
    inspectres_user(list_name, list_type)
  else:
    print('\n *** Use error: incorrect number of arguments')
    print('\n     Use examples:')
    print('       inspectres_user.py <list_name>')
    print('       inspectres_user.py <list_name> <list_type>')
