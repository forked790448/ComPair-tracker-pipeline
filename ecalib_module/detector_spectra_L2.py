'''
2023-01-26  Anna Zajczyk

  Tool to plot energy-calibrated spectra from L2 data product with ability to
  fit a gaussian. Tool creates and plots spectra broken down by layer side.
  To be able to do it an auxilary file L1p5 is necessary. This auxilary file
  contains energy-calibrated events in a datasets called
    /layerXX/vdata/channel_energy   [event vals in keV; shape (nevent, 2, 192)]
  Dimension of this datasets is same as the dimension of datasets
    /layerXX/vdata/channel_cm_sub   [event vals in ADUs; shape (nevent, 2, 192)]

  The auxilary file can be created with the following tool:

    prep_big_data.py

  To obtain the energy-calibrated events with prep_big_data.py, it should be run

    prep_big_data.py <file_name> <action_flag> <ecalib_name>

  where:

    file_name - is the name of the input h5 file from L1 stage - full access path
    action_flag - should be set to ene
    ecalib_name - is the name of the energy calibration file - full access path


  Updates
  -------
  2023-01-28  No need for L1p5 aux file: one does not need to have L1p5 auxilary
              file to run the tool. When no L1p5 can be found in the directory
              where L2 file resides, the tool plots single spectrum from based
              on the calibrated data in L2 (there is no distinction based on
              the layer side as this information is "lost" when calibrating
              positions&energies from L1 to L2). User can then only inspect and
              fit a gaussian to that single spectrum. The single spectrum can
              be either individual layer or full tracker stack.

'''

import os
import sys
import h5py
import os.path

from datetime import datetime
import matplotlib.pyplot as plt

from scipy.optimize import curve_fit
from numpy import zeros, where, array, histogram, isnan, arange, argmax, \
  sqrt, diag, exp, ones

from ecalib_functions import convert_mask, fgauss


def fgauss_cnst(x, norm, cent, width, cnst):
  """Returns a Gaussian function."""
  return norm*exp(-(x-cent)**2/(2*width**2)) + cnst


def prep_layer_data(layer_side, data_arr, mask):
  layer_data = array(data_arr[:, layer_side, :])

  for i in range(0, 192):
    if (mask[layer_side, i] != 1):
      strip_data = array(data_arr[:, layer_side, i])

      ix1 = where(isnan(strip_data))[0]
      layer_data[ix1, i] = 0.0
      strip_data[ix1] = 0.0

      ix2 = where(strip_data < 0.0)[0]
      strip_data[ix2] = 0.0
      layer_data[ix2, i] = 0.0
    else:
      layer_data[:, i] = 0.0
  return layer_data


def layer_spectra(file_name, h5, detector_number, bin_edges, clean_array=None):
  faux = file_name[:-4]+'1p5.h5'
  h5aux = h5py.File(faux, 'r')

  detector_name = 'layer{:02d}'.format(detector_number)
  mask = convert_mask(h5, detector_name)

  print(' >>> Reading energy values from: ' + faux.split('/')[-1])
  isENE = ('/' + detector_name + '/vdata/channel_energy' in h5aux)
  if (isENE):
    en_arr = h5aux['/' + detector_name + '/vdata/channel_energy']
  else:
    print('\n *** Warning: no /vdata/channel_energy dataset present in HDF5 file!')
    print(' ***    Use: prep_big_data.py to create such dataset.')
    print(' ***    Exiting detector_spectra_layer.py ...')
    sys.exit()


  counts_layer = zeros((len(bin_edges)-1, 2))
  clean_counts_layer = zeros((len(bin_edges)-1, 2))

  nevent = en_arr.shape[0]
  print(' >>> Number of events in L1p5 file = {:d}'.format(nevent))

  slice_size = 100000
  if (nevent > 3*slice_size):
    nslice = nevent // slice_size
    nrest = nevent % slice_size
    print(' >>> Number of {:d} event slices = {:d} (with rest {:d})'.format(slice_size, nslice, nrest))
    print('     --- Slice.No   Evt_start   Evt_end   (exec.time) ---')

    for m in range(nslice):
      tmp_s = datetime.utcnow()
      if (m == 0):
        i_st = 0
        i_ed = (m+1)*slice_size
      elif (m == (nslice-1)):
        i_st = m*slice_size
        i_ed = (m+1)*slice_size+nrest
      else:
        i_st = m*slice_size
        i_ed = (m+1)*slice_size

      layer_side = 0
      layer_ene = prep_layer_data(layer_side, en_arr[i_st:i_ed, :, :], mask)
      counts, bins = histogram(layer_ene, bin_edges)
      counts_layer[:, layer_side] = counts_layer[:, layer_side] + counts

      if (clean_array is not None):
        for i in range(192):
          layer_ene[:, i] = layer_ene[:, i]*clean_array[i_st:i_ed]
        clean_counts, bins = histogram(layer_ene, bin_edges)
        clean_counts_layer[:, layer_side] = clean_counts_layer[:, layer_side] + clean_counts


      layer_side = 1
      layer_ene = prep_layer_data(layer_side, en_arr[i_st:i_ed, :, :], mask)
      counts, bins = histogram(layer_ene, bin_edges)
      counts_layer[:, layer_side] = counts_layer[:, layer_side] + counts

      if (clean_array is not None):
        for i in range(192):
          layer_ene[:, i] = layer_ene[:, i]*clean_array[i_st:i_ed]
        clean_counts, bins = histogram(layer_ene, bin_edges)
        clean_counts_layer[:, layer_side] = clean_counts_layer[:, layer_side] + clean_counts

      tmp_e = datetime.utcnow()
      print('           {:d}   {:d}   {:d}   ({:.2f} s) '.format(m, i_st, i_ed, (tmp_e - tmp_s).total_seconds()))
  else:
    layer_side = 0
    layer_ene = prep_layer_data(layer_side, en_arr, mask)
    counts, bins = histogram(layer_ene, bin_edges)
    counts_layer[:, layer_side] = counts_layer[:, layer_side] + counts

    if (clean_array is not None):
      for i in range(192):
        layer_ene[:, i] = layer_ene[:, i]*clean_array[:]
      clean_counts, bins = histogram(layer_ene, bin_edges)
      clean_counts_layer[:, layer_side] = clean_counts_layer[:, layer_side] + clean_counts

    layer_side = 1
    layer_ene = prep_layer_data(layer_side, en_arr, mask)
    counts, bins = histogram(layer_ene, bin_edges)
    counts_layer[:, layer_side] = counts_layer[:, layer_side] + counts

    if (clean_array is not None):
      for i in range(192):
        layer_ene[:, i] = layer_ene[:, i]*clean_array[:]
      clean_counts, bins = histogram(layer_ene, bin_edges)
      clean_counts_layer[:, layer_side] = clean_counts_layer[:, layer_side] + clean_counts

  return counts_layer, clean_counts_layer


def fit_spectrum(bin_centers, bin_edges, counts, counts_layer=None):
  if (counts_layer is None):
    spec_number = input(' >>> Select spectrum to fit: (1) full layer available only: ')
    spec_number = spec_number.strip()

    if (spec_number == '1'):
      spec_hist = counts
      print('\n -----------------------------------------------------------')
      print(' --- Fitting: L2 both sides spectrum')
    else:
      print(' *** Ooops! Wrong spectrum value... ')
      sys.exit()


    fit_ll, fit_ul = input(\
      ' >>> Define LOWER and UPPER limit for the fit region: ').split()

    fit_ll, fit_ul = [int(fit_ll), int(fit_ul)]

    qf = ((fit_ll < bin_centers) & (bin_centers < fit_ul))
    pinit = [max(spec_hist[qf]), bin_centers[qf][argmax(spec_hist[qf])], 1.0]

    print('\n     Initial parameters for the fit (norm, centroid, sigma): ')
    print('       {:.4f}   {:.4f}   {:.4f}'.format(\
      pinit[0], pinit[1], pinit[2]))
    print('     Total number of counts in the fit region: ')
    print('       {:.0f} \n'.format(sum(spec_hist[qf])))

    fit_params = input(' >>> Execute the fit with these initial'
      ' parameters (yes / no): ')
    if (fit_params.strip() == 'no') | (fit_params.strip() == '0'):
      pi1, pi2, pi3 = input(' >>> Define initial parameters for'
        ' the fit (norm, centroid, sigma): ').split()
      pinit[0] = float(pi1)
      pinit[1] = float(pi2)
      pinit[2] = float(pi3)

    rerror = 1.0 + sqrt(spec_hist + 0.75)

    p1,t1 = curve_fit(
      fgauss, bin_centers[qf], spec_hist[qf], p0=pinit, sigma=rerror[qf], \
        absolute_sigma=True)
    perr1 = sqrt(diag(t1))


    print(' ----')
    print(' RESULTS: Best fit parameters for a Gaussian model')
    print('                Norm = {:11.5f} +/- {:9.5f}'.format(p1[0], \
      perr1[0]))
    print('       Peak centroid = {:11.5f} +/- {:9.5f}'.format(p1[1], \
      perr1[1]))
    print('               Sigma = {:11.5f} +/- {:9.5f}'.format(p1[2], \
      perr1[2]))
    print(' -----------------------------------------------------------\n')

    plt.cla()
    plt.xlabel('Energy [keV]')
    plt.ylabel('Counts')
    plt.ylim(0, 1.1*max(spec_hist[30:]))
    plt.xlim(p1[1]-10*p1[2], p1[1]+10*p1[2])
    plt.stairs(spec_hist, bin_edges)
    plt.plot(bin_centers[qf], fgauss(bin_centers[qf], p1[0], p1[1], p1[2]), '-.', \
      color='red', alpha=0.5)
    plt.show()
    plt.pause(0.0001)

    try_again = input(' >>> Continue fitting for other spectra (yes/no): ')
    if ((try_again.strip() == 'yes') | (try_again.strip() == '1') \
      | (try_again.strip() == 'y')):
      work_this_layer = True
    else:
      work_this_layer = False

  else:
    spec_number = input(' >>> Select spectrum to fit: (1) full layer, (2) side 0, (3) side 1: ')
    spec_number = spec_number.strip()

    if (spec_number == '1'):
      spec_hist = counts
      print('\n -----------------------------------------------------------')
      print(' --- Fitting: L2 both sides spectrum')
    elif (spec_number == '2'):
      spec_hist = counts_layer[:, 0]
      print('\n -----------------------------------------------------------')
      print(' --- Fitting: L1p5 side 0 spectrum')
    elif (spec_number == '3'):
      spec_hist = counts_layer[:, 1]
      print('\n -----------------------------------------------------------')
      print(' --- Fitting: L1p5 side 1 spectrum')
    else:
      print(' *** Ooops! Wrong spectrum value... ')
      sys.exit()


    fit_ll, fit_ul = input(\
      ' >>> Define LOWER and UPPER limit for the fit region: ').split()

    fit_ll, fit_ul = [int(fit_ll), int(fit_ul)]

    qf = ((fit_ll < bin_centers) & (bin_centers < fit_ul))
    pinit = [max(spec_hist[qf]), bin_centers[qf][argmax(spec_hist[qf])], 1.0]

    print('\n     Initial parameters for the fit (norm, centroid, sigma): ')
    print('       {:.4f}   {:.4f}   {:.4f}'.format(\
      pinit[0], pinit[1], pinit[2]))
    print('     Total number of counts in the fit region: ')
    print('       {:.0f} \n'.format(sum(spec_hist[qf])))

    fit_params = input(' >>> Execute the fit with these initial'
      ' parameters (yes / no): ')
    if (fit_params.strip() == 'no') | (fit_params.strip() == '0'):
      pi1, pi2, pi3 = input(' >>> Define initial parameters for'
        ' the fit (norm, centroid, sigma): ').split()
      pinit[0] = float(pi1)
      pinit[1] = float(pi2)
      pinit[2] = float(pi3)

    rerror = 1.0 + sqrt(spec_hist + 0.75)

    p1,t1 = curve_fit(
      fgauss, bin_centers[qf], spec_hist[qf], p0=pinit, sigma=rerror[qf], \
        absolute_sigma=True)
    perr1 = sqrt(diag(t1))


    print(' ----')
    print(' RESULTS: Best fit parameters for a Gaussian model')
    print('                Norm = {:11.5f} +/- {:9.5f}'.format(p1[0], \
      perr1[0]))
    print('       Peak centroid = {:11.5f} +/- {:9.5f}'.format(p1[1], \
      perr1[1]))
    print('               Sigma = {:11.5f} +/- {:9.5f}'.format(p1[2], \
      perr1[2]))
    print(' -----------------------------------------------------------\n')

    if (spec_number == '1'):
      plt.subplot(131)
      plt.cla()
      plt.ylabel('Counts')
    elif (spec_number == '2'):
      plt.subplot(132)
      plt.cla()
    elif (spec_number == '3'):
      plt.subplot(133)
      plt.cla()

    plt.xlabel('Energy [keV]')
    plt.ylim(0, 1.1*max(spec_hist[30:]))
    plt.xlim(p1[1]-10*p1[2], p1[1]+10*p1[2])
    plt.stairs(spec_hist, bin_edges)
    plt.plot(bin_centers[qf], fgauss(bin_centers[qf], p1[0], p1[1], p1[2]), '-.', \
      color='red', alpha=0.5)
    plt.show()
    plt.pause(0.0001)

    try_again = input(' >>> Continue fitting for other spectra (yes/no): ')
    if ((try_again.strip() == 'yes') | (try_again.strip() == '1') \
      | (try_again.strip() == 'y')):
      work_this_layer = True
    else:
      work_this_layer = False

  return work_this_layer


def plot_spectra(file_name, detector_name, counts, bin_edges, \
  counts_layer = None, clean_counts_layer = None):

  FONT_SIZE = 8
  plt.rc('font', size=FONT_SIZE)          # controls default text sizes
  plt.rc('axes', titlesize=FONT_SIZE)     # fontsize of the axes title
  plt.rc('axes', labelsize=FONT_SIZE)     # fontsize of the x and y labels
  plt.rc('xtick', labelsize=FONT_SIZE)    # fontsize of the tick labels
  plt.rc('ytick', labelsize=FONT_SIZE)    # fontsize of the tick labels
  plt.rc('legend', fontsize=FONT_SIZE)    # legend fontsize
  plt.rc('figure', titlesize=FONT_SIZE+2)

  if (counts_layer is None) & (clean_counts_layer is None):
    print(' >>> Skipping to plot and fit spectra section... ')
    plt.close('all')
    plt.ion()
  else:
    print(' >>> Plotting spectra... ')
    yr = [0, 1.1*max([max(counts[30:]), max(counts_layer[30:, 0]), max(counts_layer[30:, 1])])]

    plt.close('all')
    plt.ion()
    plt.figure(1, figsize=(12, 8))
    plt.clf()
    fname = file_name.split('/')[-1]
    plt.suptitle('File: ' + fname + '    /    ' + detector_name)

    plt.subplot(211)
    plt.xlabel('Energy [keV]')
    plt.ylabel('Counts')
    plt.ylim(yr)
    print(' *** Caution: Forcing the first bin to contain ZERO events!!!')
    counts[0] = 0
    plt.stairs(counts, bin_edges, label='L2 both sides')
    plt.stairs(0.5*(counts_layer[:, 0] + counts_layer[:, 1]), bin_edges, ls='--', \
      color='green', label='L1p5 both sides x 0.5')
    plt.stairs(0.5*(clean_counts_layer[:, 0] + clean_counts_layer[:, 1]), bin_edges, \
      color='violet', label='L1p5 both sides x 0.5 cleaned')
    plt.legend()
    plt.show()

    plt.subplot(234)
    plt.xlabel('Energy [keV]')
    plt.ylabel('Counts')
    plt.ylim(yr)
    print(' *** Caution: Forcing the first bin to contain ZERO events!!!')
    counts[0] = 0
    plt.stairs(counts, bin_edges, label='L2 both sides')
    plt.stairs(0.5*(clean_counts_layer[:, 0] + clean_counts_layer[:, 1]), bin_edges, \
      color='violet', label='L1p5 both sides x 0.5 cleaned')
    plt.legend()
    plt.show()

    plt.subplot(235)
    plt.xlabel('Energy [keV]')
    plt.ylim(yr)
    layer_side = 0
    print(' *** Caution: Forcing the first bin to contain ZERO events!!!')
    counts_layer[0, layer_side] = 0
    plt.stairs(counts_layer[:, layer_side], bin_edges, label='side {:d}'.format(layer_side))
    plt.stairs(clean_counts_layer[:, layer_side], bin_edges, color='violet', \
      label='L1p5 side {:d} cleaned'.format(layer_side))
    plt.legend()
    plt.show()

    plt.subplot(236)
    plt.xlabel('Energy [keV]')
    plt.ylim(yr)
    layer_side = 1
    print(' *** Caution: Forcing the first bin to contain ZERO events!!!')
    counts_layer[0, layer_side] = 0
    plt.stairs(counts_layer[:, layer_side], bin_edges, label='side {:d}'.format(layer_side))
    plt.stairs(clean_counts_layer[:, layer_side], bin_edges, color='violet', \
      label='L1p5 side {:d} cleaned'.format(layer_side))
    plt.legend()
    plt.show()
    plt.pause(0.0001)


def plot_and_fit_spectra(file_name, detector_name, counts, bin_edges, \
  counts_layer = None):
  bin_centers = 0.5 * (bin_edges[:-1] + bin_edges[1:])

  if (counts_layer is None):
    yr = [0, 1.1*max(counts[30:])]

    plt.figure(2)
    plt.clf()
    fname = file_name.split('/')[-1]
    plt.title('Fitting screen for file: \n' + fname + '    /    ' + detector_name)

    plt.xlabel('Energy [keV]')
    plt.ylabel('Counts')
    plt.ylim(yr)
    plt.stairs(counts, bin_edges, label='L2 both sides')
    plt.legend()
    plt.show()
    plt.pause(0.0001)

    # -----------------------------------------------------------------------------
    # ---   Choose spectrum for fitting
    # -----------------------------------------------------------------------------
    work_this_layer = True
    while(work_this_layer == True):
      try:
        work_this_layer = fit_spectrum(bin_centers, bin_edges, counts)
      except:
        print(' *** Ooops! Fit failed...')
        user_cont = input(' >>> Do you wish to continue fitting (yes/no)? ')
        user_cont = user_cont.strip()
        if (user_cont == 'yes') | (user_cont == 'y') | (user_cont == '1'):
          work_this_layer = True
        else:
          work_this_layer = False
  else:
    plt.figure(2, figsize=(12, 4))
    plt.clf()
    fname = file_name.split('/')[-1]
    plt.suptitle('Fitting screen for file: ' + fname + '    /    ' + detector_name)

    plt.subplot(131)
    plt.xlabel('Energy [keV]')
    plt.ylabel('Counts')
    yr = [0, 1.1*max(counts[30:])]
    plt.ylim(yr)
    plt.stairs(counts, bin_edges, label='L2 both sides')
    plt.legend()
    plt.show()


    plt.subplot(132)
    plt.xlabel('Energy [keV]')
    layer_side = 0
    yr = [0, 1.1*max(counts_layer[30:, layer_side])]
    plt.ylim(yr)
    plt.stairs(counts_layer[:, layer_side], bin_edges, label='L1p5 side {:d}'.format(layer_side))
    plt.legend()
    plt.show()


    plt.subplot(133)
    plt.xlabel('Energy [keV]')
    layer_side = 1
    yr = [0, 1.1*max(counts_layer[30:, layer_side])]
    plt.ylim(yr)
    plt.stairs(counts_layer[:, layer_side], bin_edges, label='L1p5 side {:d}'.format(layer_side))
    plt.legend()
    plt.show()
    plt.pause(0.0001)

    # -----------------------------------------------------------------------------
    # ---   Choose spectrum for fitting
    # -----------------------------------------------------------------------------
    work_this_layer = True
    while(work_this_layer == True):
      try:
        work_this_layer = fit_spectrum(bin_centers, bin_edges, counts, counts_layer)
      except:
        print(' *** Ooops! Fit failed...')
        user_cont = input(' >>> Do you wish to continue fitting (yes/no)? ')
        user_cont = user_cont.strip()
        if (user_cont == 'yes') | (user_cont == 'y') | (user_cont == '1'):
          work_this_layer = True
        else:
          work_this_layer = False

def detector_spectra_L2(file_name):
  faux = file_name[:-4]+'1p5.h5'
  isFileAux = os.path.isfile(faux)
  if (not isFileAux):
    print('\n >>> File containing auxilary data L1p5 does not exist.')
    print(' >>>    To create this auxilary file run the following tool:')
    print(' >>>          prep_big_data.py <file_name_L1.h5> ene <ecalib_file>')

  h5 = h5py.File(file_name, 'r')
  h5_group_names = array([x for x in h5])
  q = where(h5_group_names != 'vdata')[0]
  h5_group_names = h5_group_names[q]

  print('\n >>> Number of tracker layers: ', len(h5_group_names))
  for p in h5_group_names:
    print(' >>>           ', p)

  user_input = input(
    '\n >>> Define detector number (0-9) or full stack (-1; not implemented yet): ')
  if user_input:
    detector_number = user_input.strip()
  else:
    print(' *** Ooops! something went wrong, let\'s try again...')
    detector_number = input(
      '\n >>> Define detector number (0-9): ').strip()
  detector_number = int(detector_number)

  print('\n >>> Creating energy bins')
  bin_edges = arange(0.0, 1000.0, 1)

  if (detector_number != -1):
    detector_name = 'layer{:02d}'.format(detector_number)
    print(' >>> Getting layer data')
    print(' >>> Reading energy values from: ' + file_name.split('/')[-1])
    en_arr = h5['/' + detector_name + '/vdata/calib_data'][...]
    nevent = en_arr.shape[0]
    print(' >>> Number of events in L2 file = {:d}'.format(nevent))

    counts, bins = histogram(en_arr[:, 4], bin_edges) # --- full layer spectrum
    if (isFileAux):
      qevt = where(en_arr[:, 0] != -1)[0]
      clean_arr = zeros(nevent)
      if (len(qevt) > 0):
        clean_arr[qevt] = 1
      counts_layer, clean_counts_layer = layer_spectra(file_name, h5, detector_number, bin_edges, clean_array=clean_arr) # --- spectrum per side

  else:
    print(' >>> Getting full tracker data')
    print(' >>> Reading energy values from: ' + file_name.split('/')[-1])

    counts = zeros(len(bin_edges)-1)
    counts_layer = zeros((len(bin_edges)-1, 2))
    if (isFileAux):
      clean_counts_layer = zeros((len(bin_edges)-1, 2))

    for detector_name in h5_group_names:
      print(' >>> Working on: ' + detector_name)
      detector_number = int(detector_name[-2:])
      en_arr = h5['/' + detector_name + '/vdata/calib_data'][...]
      nevent = en_arr.shape[0]

      tmp_counts, bins = histogram(en_arr[:, 4], bin_edges) # --- full layer spectrum
      counts[:] = counts[:] + tmp_counts[:]


      if (isFileAux):
        qevt = where(en_arr[:, 0] != -1)[0]
        clean_arr = zeros(nevent)
        if (len(qevt) > 0):
          clean_arr[qevt] = 1
        tmp_counts_layer, tmp_clean_counts_layer = layer_spectra(file_name, h5, detector_number, bin_edges, clean_array=clean_arr) # --- spectrum per side

        counts_layer[:, 0] = counts_layer[:, 0] + tmp_counts_layer[:, 0]
        counts_layer[:, 1] = counts_layer[:, 1] + tmp_counts_layer[:, 1]

        clean_counts_layer[:, 0] = clean_counts_layer[:, 0] + tmp_clean_counts_layer[:, 0]
        clean_counts_layer[:, 1] = clean_counts_layer[:, 1] + tmp_clean_counts_layer[:, 1]

    detector_name = 'full tracker stack'


  # -----------------------------------------------------------------------------
  # ---   Plot spectra for visual inspection
  # -----------------------------------------------------------------------------
  if (isFileAux):
    plot_spectra(file_name, detector_name, counts, bin_edges, counts_layer, \
      clean_counts_layer)
  else:
    plot_spectra(file_name, detector_name, counts, bin_edges)


  # -----------------------------------------------------------------------------
  # ---   Plot spectra and fit emission lines
  # -----------------------------------------------------------------------------
  if (isFileAux):
    plot_and_fit_spectra(file_name, detector_name, counts, bin_edges, \
      counts_layer)
  else:
    plot_and_fit_spectra(file_name, detector_name, counts, bin_edges)


if __name__ == "__main__":
  if (len(sys.argv) == 2):
    file_name = str(sys.argv[1]).rstrip()
    detector_spectra_L2(file_name)
  else:
    print('\n *** Use error: incorrect number of arguments')
    print('\n     Use examples:')
    print('       detector_spectra_L2.py <file_name>')
