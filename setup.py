from pathlib import Path
import re
from setuptools import setup, find_packages


def get_version():
    init_path = Path(__file__).parent / "tracker_pipeline" / "__init__.py"
    with open(init_path) as f:
        for line in f.readlines():
            m = re.search(r"__version__ = [\'\"](?P<version>.*)[\'\"]", line)
            if m:
                return m.groupdict()["version"]
    raise Exception("Could not find __version__ in tracker_pipeline/__init__.py")


setup(
    name="tracker_pipeline",
    version=get_version(),
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        "h5py",
        "numpy",
        "pytest",
        "wheel",
    ],
)
