from glob import iglob
from pathlib import Path
import random

data_dir = Path(__file__).parent / "data"
data_paths = list(iglob(str(data_dir / "*.h5"))) + list(iglob(str(data_dir / "*.hdf5")))

# Not ready for actual data yet...
# assert len(data_paths) > 0, "Please add data to the 'tests/data' directory to run tests"


# def random_path():
#     return random.choice(data_paths)
