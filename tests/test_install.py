from pathlib import Path
import re

import tracker_pipeline


def test_version():
    init_path = Path(__file__).parent.parent / "tracker_pipeline" / "__init__.py"
    version = None
    with open(init_path) as f:
        for line in f.readlines():
            m = re.search(r"__version__ = [\'\"](?P<version>.*)[\'\"]", line)
            if m:
                version = m.groupdict()["version"]
                break
    assert version is not None, "Could not find __version__ in __init__.py"
    assert (
        tracker_pipeline.__version__ == version
    ), "Install version does not match code version"
