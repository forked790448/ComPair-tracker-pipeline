# `tracker_pipeline` tests

We are setting up this directory so that pytest can easily be run to check code.

## Quickstart:
After installing the `tracker_pipeline` code, run all tests with:
```bash
pip3 install pytest --user
./run-tests.sh
```

## pytest
You will need to have pytest installed (use pip, or whatever your system uses). Once
pytest is installed, simply change into this directory, and run the `pytest` command, or
the `./run-tests.sh` script if you forget what a pytest is someday.

Setting up tests using pytest is easy! Add a new module in this directory, named
`test_*.py`. Within your test module, write functions that use assertion statements to
verify code behavior.

## Setting up data for tests
The `tests/data` directory is meant to store the hdf5 files which are used in testing.
Please copy at least one hdf5 file into that directory for the tests to run. For more
information, see `tests/data/README.md`.
