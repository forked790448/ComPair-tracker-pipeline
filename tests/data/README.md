# tests/data

An issue with code development for an analysis pipeline is the need to have actual data
to test code on. The tests/data directory is the place to put data for use in testing.
Please copy an hdf5 data file into this directory, and tests will then run with real data.

The .gitignore is setup to ignore data files in this directory ending in .h5 or .hdf5
