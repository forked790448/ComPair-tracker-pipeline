"""Tests of the tracker_pipeline.layer module.
"""
from pathlib import Path
import tempfile
import h5py
import numpy as np
import pytest

from tracker_pipeline import layer


def test_layer_init():
    """Instantiate a layer.Layer, check initial attributes are ok."""
    x = layer.Layer("test.h5")
    assert x.hdf5 is None, "Layer.hdf5 initialized incorrectly."
    assert not x.is_open, "Layer.is_open initialized incorrectly."
    assert x._h5_path == "test.h5", "Layer._h5_path initialized incorrectly."


def test_layer_bad_path():
    """Check that we recognize an attempt to open a non-existent file."""
    x = layer.Layer("test.h5")
    with pytest.raises(
        FileNotFoundError, match=r"Unable to find layer hdf5 file at test.h5"
    ):
        x.open()


def test_layer_open_close():
    """Test opening an hdf5 file."""
    with tempfile.TemporaryDirectory() as tmpdir:
        h5_path = str(Path(tmpdir) / "test.h5")
        h5 = h5py.File(h5_path, "w")
        h5.close()
        x = layer.Layer(h5_path)
        x.open()
        assert x.is_open, "Layer.is_open was not set after opening hdf5 file."
        assert x.hdf5 is not None, "Layer.hdf5 was not created after opening file."
        x.close()
        assert not x.is_open, "Layer.is_open is still True after closing file."
        assert x.hdf5 is None, "Layer.hdf5 was not closed."


def test_layer_context():
    """Test the use of ``with`` for opening a layer."""
    with tempfile.TemporaryDirectory() as tmpdir:
        h5_path = str(Path(tmpdir) / "test.h5")
        h5 = h5py.File(h5_path, "w")
        h5.close()
        with layer.Layer(h5_path) as x:
            assert (
                x.is_open
            ), "Layer.is_open was not set after opening hdf5 file in with statement."
            assert (
                x.hdf5 is not None
            ), "Layer.hdf5 was not created after opening file in with statement."
        assert (
            not x.is_open
        ), "Layer.is_open is still True after exiting with statement."
        assert x.hdf5 is None, "Layer.hdf5 was not closed upon exiting with statement."


def test_layer_data():
    """Check that the ``Layer.data`` initializes and accesses data correctly.

    Sort of a trivial test.
    """
    data_source = layer.Layer._data_source
    data = np.random.random((100, 2, 192))
    with tempfile.TemporaryDirectory() as tmpdir:
        h5_path = str(Path(tmpdir) / "test.h5")
        with h5py.File(h5_path, "w") as h5:
            h5[data_source] = data
        with layer.Layer(h5_path) as x:
            assert x._data is None, "Layer._data initialized incorrectly."
            assert np.all(x.data == data), "Layer.data did not load correctly."
            assert x._data is not None, "Layer._data did not update after data loading"
