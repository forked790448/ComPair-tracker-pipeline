ecalib\_module package
======================

.. automodule:: ecalib_module
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

ecalib\_module.apply\_calib module
----------------------------------

.. automodule:: ecalib_module.apply_calib
   :members:
   :undoc-members:
   :show-inheritance:

ecalib\_module.ecalib\_functions module
---------------------------------------

.. automodule:: ecalib_module.ecalib_functions
   :members:
   :undoc-members:
   :show-inheritance:

ecalib\_module.fit\_ecalib module
---------------------------------

.. automodule:: ecalib_module.fit_ecalib
   :members:
   :undoc-members:
   :show-inheritance:

ecalib\_module.fit\_emline\_auto module
---------------------------------------

.. automodule:: ecalib_module.fit_emline_auto
   :members:
   :undoc-members:
   :show-inheritance:

ecalib\_module.fit\_emline\_user module
---------------------------------------

.. automodule:: ecalib_module.fit_emline_user
   :members:
   :undoc-members:
   :show-inheritance:

ecalib\_module.plot\_data module
--------------------------------

.. automodule:: ecalib_module.plot_data
   :members:
   :undoc-members:
   :show-inheritance:

ecalib\_module.plot\_heat\_map module
-------------------------------------

.. automodule:: ecalib_module.plot_heat_map
   :members:
   :undoc-members:
   :show-inheritance:

ecalib\_module.subtract\_cmode module
-------------------------------------

.. automodule:: ecalib_module.subtract_cmode
   :members:
   :undoc-members:
   :show-inheritance:
