# Compair Unified Data (CUD) Converters #

### SIM2CUD: (python3 SIM2CUD (path to .sim file)) ###

Generates a .h5 file in the CUD format by parsing through a Cosima .sim file to generate h5 datasets. A text file is also created, starting at a base count of 1 rather than 0 (Event 100 in the .sim file would
correspond to event 99 in the .h5 file but would still correspond to event 100 in the .txt file.

### CUD2RR: (python3 CUD2RR (path to .h5 file)) ###

Generates a revan-readable .evta file by converting CUD-level event data to hit-sorted data specific to MEGALib interpretation.

#### While the converters are functional for the aforementioned purposes, the converters do not yet account for the following features: ####
        
* The current output files are hardcoded as Cosima.txt, Cosima.h5, and Cosima.evta. Manipulation of the parseargs code is needed to allow for the output of specific names files.

* A .sim file converted to a .h5 and back to a .evta file reconstructs more events than the original .sim file. This is likely due to charge sharing (see below).

* Charge sharing in the tracker is as of yet unresolved and discarded. This would appear in the CUD2RR converter, and one possible solution would be to make use of summing energies from adjacent strips.
  A possible method of doing so is included in the comments of the tkr function, and currently is not implemented due to an error of having multiple hits of the same summed energy.

* A similar problem to that of charge sharing arises from hits in adjacent strips in a TKR DSSD layer under the same event


ss









---
> **NOTE** :For any and all questions or concerns please contact Kavic Kumar: kavicrk18@gmail.com


