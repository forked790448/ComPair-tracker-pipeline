#!/usr/bin/env python3
# -------------------------
""" Package with supplementary Scripts for looking at L1 data.

"""
# Created : 	Sambid Wasti
#				04/29/21
# Revision History:
# 05/30/21	:   Sambid Wasti
#			 	Added two functions. conv_ac2sd and conv_sd2ac
# 06/02/21  :	Sambid Wasti
#				Reconfigured the scripts for more modular sections to increase the usage of the script.
#
# Notes:
# 		Future Todos:  Include some config plot to see visually what is on and what is not.
#						Also with channel masks.
#						Additional quicklook plots/data/etc/
# --------------------------
import sys
import math
import h5py
import numpy as np

# import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

def temp_test(fname):
    """ Temporary test procedure.

    A temporary test procedure to check various operations and functions of the scripts.

    """
    plot_cm_hist_all(fname, x_max=200, bsize=5.0, hist_max_x=200)


# plot_cm_hist(FNAME, 3, bsize=1)
# t_array = conv_sd2ac(1,2)
# print(type(t_array))
# print(t_array.shape)
# print(t_array)

def plot_cm_hist(fname, asic, x_max=200.00, y_max=200, bsize=1.0, hist_max_x=100.0):
    """ Plots cm-sub hist for a specific ASIC.

    Generates a pdf of common mode subtracted plots (along with common mode unsubtracted)
    for each each channel of the ASIC as well as summation of all channels.

    Parameters
    ----------
    fname : str
        converted h5 file name.
    asic : int
        Asic Number


    Keyword Args
    ------------
    x_max : float
        max range of the x-axis for all the plots. Default is 200.00 (min is 0)
    y_max : float
        max range of the y-axis for all the plots. Default is 200.00 (min is 0)
    hist_max_x: float
        max range of the dataset value for generating the histogram. Default is 100.0 (min is 0.0)
    bsize: float
        bin size for the histogram. Default is 1. It helps to calculate the nbins for the histogram.
        nbins = math.ceil(hist_max_x/bsize).


    Returns
    -------
    A pdf file name "L0_qlook_asic##.pdf"


    """
    # Input parameters for histogram
    nbins = math.ceil(hist_max_x / bsize)

    # Input parameters for plotting
    x_min = 0.0
    y_min = 0.0

    asic_no = 'asic' + ("{:02d}".format(asic))
    filename = h5py.File(fname, 'r')  # parsing the file in h5 format.
    pdf = PdfPages('L1_qlook_' + asic_no + '.pdf')  # name of the pdf file
    dset = filename['/data/' + asic_no + '/data']  # data set: Only from ASIC 3
    cm_dset = filename['/data/' + asic_no + '/cm_data']  # CMdata form ASIC 3
    # main_dset = np.empty(dset.shape, dtype='float')  # Defining CM subtracted dataset

    dset = np.array(dset)  # redefining an array.
    cm_dset = np.array(cm_dset)  # redefining a numpy array

    main_dset = cm_sub(dset, cm_dset)

    # Plotting
    #temp_hist, bins, patch = plt.hist(dset, bins=nbins, histtype='step')
    temp_hist, bins = plt.hist(dset, bins=nbins, histtype='step')
    plt.xlim([x_min, x_max])
    plt.ylim([y_min, y_max])
    plt.title('ASIC ' + str(asic) + ' : All Channel Data ')
    plt.xlabel(' ADC bin', size=12)
    plt.ylabel(' Counts ', size=12)
    plt.xticks(fontsize=12)
    plt.yticks(fontsize=12)
    plt.text(0.85, -0.12, 'Bsize=' + str(bsize), fontsize=10, transform=plt.gca().transAxes)
    pdf.savefig()
    plt.clf()
    #plt.close

    # Summation
    for i in range(32):
        temp_dset = temp_hist[i, :]
        if i == 0:
            total_hist = temp_dset
        else:
            total_hist = total_hist + temp_dset

    rebins = bins[1:nbins + 1]
    plt.hist(rebins, nbins, weights=total_hist, histtype='step')
    plt.xlim([x_min, x_max])
    plt.ylim([y_min, y_max * 32])
    plt.title('ASIC ' + str(asic) + ' : All Channel Summed ')
    plt.xlabel(' ADC bin', size=12)
    plt.ylabel(' Counts ', size=12)
    plt.xticks(fontsize=10)
    plt.yticks(fontsize=10)
    plt.text(0.85, -0.12, 'Bsize=' + str(bsize), fontsize=10, transform=plt.gca().transAxes)
    pdf.savefig()
    plt.clf()
    #plt.close

    # CM Subtracted plots

    temp_hist, bins = plt.hist(main_dset, bins=nbins, histtype='step')
    plt.xlim([x_min, x_max])
    plt.ylim([y_min, y_max])
    plt.title('ASIC ' + str(asic) + ': All Channel Data-CM ')
    plt.xlabel(' ADC bin', size=12)
    plt.ylabel(' Counts ', size=12)
    plt.xticks(fontsize=12)
    plt.yticks(fontsize=12)
    plt.text(0.85, -0.12, 'Bsize=' + str(bsize), fontsize=10, transform=plt.gca().transAxes)
    pdf.savefig()
    plt.clf()
    #plt.close

    # Summation
    for i in range(32):
        temp_dset = temp_hist[i, :]
        if i == 0:
            total_hist = temp_dset
        else:
            total_hist = total_hist + temp_dset

    rebins = bins[1:nbins + 1]
    plt.hist(rebins, nbins, weights=total_hist, histtype='step')
    plt.xlim([x_min, x_max])
    plt.ylim([y_min, y_max * 32])
    plt.title('ASIC ' + str(asic) + ': All Channel Summed Data-CM ')
    plt.xlabel(' ADC bin', size=12)
    plt.ylabel(' Counts ', size=12)
    plt.xticks(fontsize=12)
    plt.yticks(fontsize=12)
    plt.text(0.85, -0.12, 'Bsize=' + str(bsize), fontsize=10, transform=plt.gca().transAxes)
    pdf.savefig()
    plt.clf()
    #plt.close

    for i in range(dset.shape[1]):
        plt.hist(main_dset[:, i], bins=nbins, histtype='step', color='blue')
        plt.hist(dset[:, i], bins=nbins, histtype='step', color='black', range=(0, 100))
        plt.xlim([x_min, x_max])
        plt.ylim([y_min, y_max])
        plt.title('ASIC ' + str(asic) + ' : Channel ' + str(i))
        plt.xlabel(' ADC bin', size=12)
        plt.ylabel(' Counts ', size=12)
        plt.xticks(fontsize=12)
        plt.yticks(fontsize=12)
        plt.text(0.85, -0.12, 'Bsize=' + str(bsize), fontsize=10, transform=plt.gca().transAxes)
        plt.legend(["Data-CM", "Data"])
        pdf.savefig()
        plt.clf()
        #plt.close

    pdf.close()


def plot_cm_hist_all(fname, x_max=200.00, y_max=200, bsize=1, hist_max_x=100.0):
    r""" Plots Common Mode subtracted plots for all ASICs.

    Generates a pdf containing all the common mode subtraced plots for all ASICs. This just
    calls the plot_cm_hist function for each asics.

    Parameters
    ----------
    fname : str
        converted h5 file name.
    asic : int
        Asic Number


    Keyword Args
    ------------
    x_max : float
        max range of the x-axis for all the plots. Default is 200.00 (min is 0)
    y_max : float
        max range of the y-axis for all the plots. Default is 200.00 (min is 0)
    hist_max_x: float
        max range of the dataset value for generating the histogram. Default is 100.0 (min is 0.0)
    bsize: float
        bin size for the histogram. Default is 1. It helps to calculate the nbins for the histogram.
        nbins = math.ceil(hist_max_x/bsize).

    """
    for asic in range(12):
        plot_cm_hist(fname, asic, x_max=x_max, y_max=y_max, bsize=bsize, hist_max_x=hist_max_x)
    #pass


def cm_sub(asic_dataset, asic_cm):
    r""" CM Subtraction for an ASIC.

    Common Mode subtraction for one ASIC and all channels.


    Parameters
    ----------
    asic_dataset : int
        The numpy.dataset of shape (Event_numbers,32)
    asic_cm  : dataset
        The numpy.datset of shape (Event_numbers)


    Returns
    -------
       main_dataset : dataset
        numpy.dataset of shape (Event_numbers,32)

    Examples
    --------
    Common mode subtraction for ASIC 2

    >>> cm_sub_dataset = cm_sub(asic2_data,asic2_cm_data)


    """
    main_dataset = np.empty(asic_dataset.shape, dtype='float')  # Defining CM subtracted dataset
    # -- Main CM Subtraction. It is done for each event. --
    for i in range(asic_dataset.shape[1]):  # running from 0-31 but using 31 from shape
        for j in range(asic_dataset.shape[0]):  # running for each event
            main_dataset[j, i] = float(asic_dataset[j, i]) - float(asic_cm[j])
    # ----- ----- ----
    return main_dataset


def conv_ac2sd(asic_no, chan_no):
    r""" Converts ASIC/Chan -> Side/Det.

    This is a conversion function to convert the older ASIC/Chan to
    Side/Det.

    Parameters
    ----------
    asic_no : int
        ASIC Number (0-11)
    chan_no  : int
        Channel number 0-31


    Returns
    -------
    r_array : int dataset
        Dataset of shape 2. First element is Side (0-1) and
        second element is detector number 0-191

    Examples
    --------
    For converting ASIC 0, Channel 4:

    >>> conv_array = conv_ac2sd(0,4)
    """
    side = 0
    det_no = 0
    if asic_no < 6:
        side = 0
        det_no = asic_no * 32 + chan_no
    elif 5 < asic_no < 12:
        side = 1
        det_no = (asic_no - 6) * 32 + chan_no
    else:
        print('Invalid Asic Number')

    if chan_no > 31:
        print('Invalid Chan no.')
    r_array = np.array([side, det_no])
    return r_array


def conv_sd2ac(side, det_no):
    r""" Converts Side/Det -> ASIC/Chan.

    This is a conversion function to convert the older Side/Det to
    Asic/Chan.

    Parameters
    ----------
    side : int
        Side of the layer (0-1) (0 top and 1 bottom)
    det_no  : int
        Detector number 0-192


    Returns
    -------
    r_array : int dataset
        Dataset of shape 2. First element is Asic No. (0-11) and
        second element is channel number 0-31

    Examples
    --------
    For converting side1, det 35 :

    >>> conv_array = conv_sd2ac(1,35)
    """

    asic_no = 0

    chan_no = det_no % 32
    t_val = math.floor(det_no / 32)
    if side == 0:
        asic_no = t_val
    elif side == 1:
        asic_no = t_val + 6
    else:
        print('Invalid side number')

    if det_no > 191:
        print('Invalid detector number')
    r_array = np.array([asic_no, chan_no])
    return r_array


if __name__ == "__main__":
    FNAME = "1614115197_50Vbias_Co57.h5"
    if len(sys.argv) == 1:
        FNAME = str(input(" Enter or drag the h5 file :"))  # can drag the file name
        FNAME = FNAME.rstrip()
    elif len(sys.argv) == 2:
        FNAME = str(sys.argv[1])
    else:
        print("Usage Error: Too many arguments")
    # sys.exit()
    # temp_test(FNAME)
    plot_cm_hist_all(FNAME)
