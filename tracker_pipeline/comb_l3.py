""" Combine L3 files. """
import argparse
import os
import glob
import h5py
import numpy as np

def get_files_with_extension(folder_path, extension='_L3.h5'):
    """
    Get a list of files with a specific extension in the given folder path.

    Parameters:
        folder_path (str): The path to the folder.
        extension (str): The file extension to filter the files (default: '.L3').

    Returns:
        List[str]: A list of file paths with the specified extension.
    """
    file_pattern = os.path.join(folder_path, f'*{extension}')
    files_with_extension = glob.glob(file_pattern)
    return files_with_extension



def cli():
    """Command line interface.

    Raises
    ------
    FileNotFoundError
        If no files found
    """

    parser = argparse.ArgumentParser(description="Combine L3 Files")
    parser.add_argument("infolder", help=" Folder with L3 files")
    args = parser.parse_args()

    # Get files with the '.L3' extension in the specified folder
    files = get_files_with_extension(args.infolder)
    if len(files) <= 1:
         raise FileNotFoundError("No L3 Files in directory. Please choose the right directory.")

    tout = "combined.L3.h5"
    outfile = f"{args.infolder}/{tout}"

    if os.path.exists(outfile):
       os.remove(outfile)

    combine_l3_files(sorted(files),outfile = outfile) 



def combine_l3_files(files, outfile= None):
    """combine the L3 files one at a time.

    Parameters
    ----------
    files : list
        list of l3 files
    outfile : output file, optional
        Name of the output file.
    """
    with h5py.File(outfile,'w') as f_out:
        total_samples1 = 0
        total_samples2 = 0
        total_samples3 = 0
        total_samples4 = 0

        first_file_flag = True

        for input_file in files:
            with h5py.File(input_file,'r') as f_in:
                # we have 4 datasets and multiple files.
                print(input_file)
                dset_name1 = 'tracker/energy'
                dset1 = f_in[dset_name1][...]
                n_samples1 = dset1.shape[0]
                xshape1 = dset1.shape[1]  # this is 6

                dset_name2 = 'tracker/pulse_height'
                dset2 = f_in[dset_name2][...]
                n_samples2 = dset2.shape[0]

                dset_name3 = 'tracker/event_id'
                dset3 = f_in[dset_name3][...]
                n_samples3 = dset3.shape[0]

                dset_name4 = 'tracker/event_time'
                dset4 = f_in[dset_name4][...]
                n_samples4 = dset4.shape[0]

                if first_file_flag:
                    # create dataset for first file.
                    first_file_flag = False
                    f_out.create_dataset(dset_name1, shape=(total_samples1 + n_samples1, xshape1), maxshape=(None,xshape1), dtype=dset1.dtype)
                    f_out.create_dataset(dset_name2, shape=(total_samples2 + n_samples2, 10,2,192), maxshape=(None, 10,2,192), dtype=dset2.dtype)
                    f_out.create_dataset(dset_name3, shape=(total_samples3 + n_samples3,), maxshape=(None, ), dtype=dset3.dtype)
                    f_out.create_dataset(dset_name4, shape=(total_samples4 + n_samples4,), maxshape=(None, ), dtype=dset4.dtype)


                f_out[dset_name1].resize((total_samples1 + n_samples1, xshape1))
                f_out[dset_name1][total_samples1:] = dset1[:]
                total_samples1 += n_samples1

                f_out[dset_name2].resize((total_samples2 + n_samples2, 10,2,192))
                f_out[dset_name2][total_samples2:] = dset2[:]
                total_samples2 += n_samples2

                f_out[dset_name3].resize((total_samples3 + n_samples3, ))
                f_out[dset_name3][total_samples3:] = dset3[:]
                total_samples3 += n_samples3
                
                f_out[dset_name4].resize((total_samples4 + n_samples4, ))
                f_out[dset_name4][total_samples4:] = dset4[:]
                total_samples4 += n_samples4


if __name__ == "__main__":
    cli()