#!/usr/bin/env python3
"""
Select the relevant datasets and convert the Tracker L2 to L3
Currently, the L3 (CUD format for tracker has 4 datasets.

Event_id (dim = n_event)
Event_time (dim = n_event)
Pulse_heiht (dim = n_event,10, 2, 192)
Energy (dim = n_event, 10, 2, 192)

usage can be retrieved

>>> conv_l2tol3 filepath

for additional options

>>> conv_l2tol3 --help

"""

# version s1.0c = Fixed a small bug where the energies were not properly being placed.
# version s1.0d = Now the attributs from previous levels are preserved.

import os
import math
import h5py
import numpy as np
import ecalib_module.ecalib_functions as ecal
import argparse
import sys

import time

VERSION = "s1.0d"

def create_l3(fname, outfile, verbose=False, comp = True):
    """Main function to read L2, select the fields and create L3 data.

    Parameters
    ----------
    fname : String
        L2 File
    outfile : String
        L3 File
    verbose : Boolean
        Flag to trigger the verbose on to print out information while the script is running. Default is False.

    Returns
    -------
    l3file : File
        L3 h5 file, ready to be appended to CUD with other subsystem.
    """

    #layer_list = ['layer00','layer01','layer02','layer03','layer04','layer05','layer06','layer07','layer08','layer09']
    layer_list = [f"layer{n:02d}" for n in range(10)]

    # Sanity Check
    nsync = [file[f"{layer}/data/sync_index"].size for layer in layer_list if layer in file]
    max_size = nsync[0]
    # Sanity check...
    assert all(np.array(nsync) == max_size),"Insanity Error: The Sync_Indices are not the Same"

    max_size = max_size-1 #We are skipping the EVENTID=0 or sync_index=0


    # Event Id and Event Time (shape = eventid)
    # This is same for all layers due to sync_index
    # We only need 1 layer but we are looping in case layer 0 is not plugged in the layer stack.
    if verbose == True:
        print("<<<< Event IDs and Event Time started .....>>>>>")

    for i in range(10):
        if layer_list[i] in file.keys():
            sync_index = file[f"{layer_list[i]}/data/sync_index"][...]
            evtdata = file[f"{layer_list[i]}/data/event_id"][...]
            event_id = evtdata[sync_index[1:]]

            evttime = file[f"{layer_list[i]}/data/system_time"][...]
            event_time = evttime[sync_index[1:]]

            if verbose == True:
                print(f" Layer :{i} done")
            break

    if verbose == True:
        print(f"Event ID and Event Time completed ......")
        print("<<<< Pulse Height.. Started >>>>>")

    # Pulse Height (dim = nev, 10, 2, 192)

    pulse_height = np.zeros((max_size,10,2,192))
    for i in range(10):
        if layer_list[i] in file.keys():
            data = file[f"{layer_list[i]}/vdata/channel_cm_sub"][...]
            sync_index = file[f"{layer_list[i]}/data/sync_index"][...]
            sync_index = np.array(sync_index)

            if verbose == True:
                print(f"Layer:{i} , Id Len:{len(sync_index)}")

            pulse_height[:, i, :, :] = data[sync_index[1:], :, :]

    if verbose == True:
        print("<<<< Pulse Height... Ended >>>>>")
        print("<<<< Energy... Started >>>>>")

    # Energy (dim = n_ev, 10, 2, 192)
    # Slightly tricky because, sync_index not applied previously and not all event ids are valid ecalib which
    # are skipped.
    energy = np.zeros((max_size, 10, 2, 192))

    # extract the table data.
    xtable, ytable, ztable = ecal.get_xyz_pos()
    xtab = np.array(xtable, dtype='f2')
    ytab = np.array(ytable, dtype='f2')
    ztab = np.array(ztable, dtype='f2') # not used here.
    for i in range(10):
        if layer_list[i] in file.keys():
            layer = i

            cdata = file[f"{layer_list[i]}/vdata/calib_data"][...] # Grab data
            evtdata = file[f"{layer_list[i]}/data/event_id"][...]  # Grab event id
            ecalib_sync_index = file[f"{layer_list[i]}/data/sync_index"][...] # grab sync_index

            evtid = evtdata[ecalib_sync_index[1:]]                            #get sync indexed event ids.
            calid = cdata[:,0]

            # get common array indices
            temp_arr, cal_in, evt_in = np.intersect1d(calid, evtid, return_indices=True)
            cdata_fil = cdata[cal_in, ...]
            ar_len = cdata_fil.shape[0]

            if verbose == True:
                print(f"Layer:{i} , Ids:{ar_len}")

            for j in range(ar_len):
                xpos = cdata_fil[j][1]  # 192 array. side 1
                ypos = cdata_fil[j][2]  # 192 array. side 2
                zpos = cdata_fil[j][3]  # 10 array ..
                ener = cdata_fil[j][4]  # energy

                a = np.where(xtab == xpos)
                b = np.where(ytab == ypos)
                #c = np.where(ztab == zpos)  # z is taken from the layer no.?

                k = evt_in[j]
                # energy =(max_size,10,2,192)
                energy[k, layer, 0, a] = ener
                energy[k, layer, 1, b] = ener

    if verbose == True:
        print("<<< Energy... Ended>>>>")
        print("<<< Generating *_L3.h5 file .... >>>")

    # Check data types first
    for i in range(10):
        if layer_list[i] in file.keys():
            id_type = file[f"{layer_list[i]}/data/event_id"].dtype
            time_type = file[f"{layer_list[i]}/data/system_time"].dtype
            pulse_type = file[f"{layer_list[i]}/vdata/channel_cm_sub"].dtype
            energy_type= file[f"{layer_list[i]}/vdata/calib_data"].dtype
            break

    with h5py.File(outfile, "w") as l3file:
        h5group = l3file.create_group('tracker')
        h5group.create_dataset(name='event_id', data=event_id, shape=(max_size,),
                               dtype=id_type, compression = comp)
        h5group.create_dataset(name='event_time', data=event_time, shape=(max_size,),
                               dtype=time_type , compression = comp)
        h5group.create_dataset(name='pulse_height', data=pulse_height, shape=(max_size, 10, 2, 192),
                               dtype=pulse_type, compression = comp)
        h5group.create_dataset(name='energy', data=energy, shape=(max_size, 10, 2, 192),
                               dtype=energy_type, compression = comp)

        #Attributes:
        # New Attributes
        l3file.attrs['trk_conv_l2tol3_ver'] = VERSION
        # Old Attributes
        temp_att_dict = dict(fname.attrs.items()) # save all the previous attributes.
        for keys in temp_att_dict:
            l3file.attrs[f'trk_{keys}'] = temp_att_dict[f'{keys}']


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Convert L2 to L3")
    parser.add_argument("-v", action='store_true', help=" Verbose flag to monitor progress or to debug")
    parser.add_argument("-u", action='store_true', help=" UnCompression. Default is Compression.")
    parser.add_argument("file", help="L2 File path")

    args = parser.parse_args()

    if args.v:
        verbose_flag = True
    else:
        verbose_flag = False

    if args.u:
        comp = False
    else:
        comp = True

    outfile = f"{args.file.split('_L2.h5')[0]}_L3.h5"
    if os.path.exists(outfile):
        os.remove(outfile)

    with h5py.File(args.file, "r") as file:
        create_l3(file, outfile, verbose=verbose_flag, comp = comp)  #The verbose_flag remains the variable.
