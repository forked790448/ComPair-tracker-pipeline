"""Object-based data access for a single layer's HDF5 file.


Notes
-----
The following example section can help guide development, by demonstrating the type of
usage that should be supported.


Examples
--------
Initialize the layer.

>>> layer = Layer("path/to/layer/data.h5")

Open the underlying hdf5 file.

>>> layer.open()

Use of the ``data`` attribute: collect all of side-A, channel-0 data.

>>> data_a0 = layer.data[:, 0, 0]

... After playing with data, close the layer's hdf5 file.

>>> layer.close()

Use of the "with" statement, so hdf5 opening closing can be ignored.

>>> with Layer("path/to/layer/data.h5") as layer:
...     data_b7 = layer.data[:, 1, 7]
...     my_channel_analysis(data_by)  # Run your analysis.

"""
from pathlib import Path

import h5py
import numpy as np


class Layer:
    """Access data stored in a layer's hdf5 file via this class."""

    _data_source = "/vdata/channel_data"  # HDF5 source for Layer.data

    def __init__(self, h5_path):
        """Create silicon layer data object from an hdf5 file.

        Parameters
        ----------
        h5_path: str
            Path to a layer's hdf5 file.

        Attributes
        ----------
        data
        n_events
        asic_trigger_bit (not implemented yet)
        trigger_status (not implemented yet)
        hdf5: h5py.File
            An hdf5 file to be opened using h5py via the ``open`` method.
        is_open: bool
            Whether the layer's hdf5 file is opened.
        """
        self.hdf5 = None
        self.is_open = False
        self._h5_path = h5_path  # private, to discourage modifying path mid usage.
        self._data = None
        self._n_events = None

    ##########################
    # HDF5 file manipulation #
    ##########################

    def open(self):
        """Open the layer's hdf5 file.

        If called while the file is already open, take no action.

        Raises
        ------
        FileNotFoundError
            If the object's hdf5 path does not exist.
        """
        if self.is_open:
            # Immediately return with no side effects.
            return
        if not Path(self._h5_path).is_file():
            raise FileNotFoundError(
                f"Unable to find layer hdf5 file at {self._h5_path}"
            )
        self.hdf5 = h5py.File(self._h5_path, "r")
        self.is_open = True

    def close(self):
        """Close the layer's hdf5 file.

        If called while the file is not open, take no action.
        """
        if not self.is_open:
            return
        self.hdf5.close()
        self.hdf5 = None
        self.is_open = False

    #########################
    # Data access functions #
    #########################

    @property
    def data(self):
        """Return channel data. On first access, read data from the hdf5 file.

        Returns
        -------
        np.array
            A numpy array of raw data, organized by event, side, channel. Explicitly,
            returned data array will be of shape (# events, 2, 192) as each layer has 2
            sides, 192 channels.
        """
        if self._data is None:
            self.open()
            data_source = self.hdf5[self._data_source]
            self._data = np.zeros(data_source.shape, dtype=data_source.dtype, order="C")
            data_source.read_direct(self._data)
        return self._data

    @data.setter
    def data(self, new_data):
        """Setter for channel data. This might be a bad idea.
        """
        self._data = new_data

    @property
    def n_events(self):
        """Number of events in the data file.

        Returns
        -------
        n_events: int
            Number of events.
        """
        if self._n_events is None:
            self.open()
            self._n_events = self.hdf5.attrs["n_packet"]
        return self._n_events

    @property
    def asic_trigger_bit(self):
        """Array of the ASIC trigger bit values for each channel.

        Returns
        -------
        np.array
            The trigger bit value when the asic was readout. Note that all channels with
            the same asic will share the same value. Data type is ``bool``.
        """
        raise Exception("Not implemented yet")

    @property
    def trigger_mask(self):
        """Trigger mask, indicating cause of data readout for each channel.

        Returns
        -------
        np.array
            Trigger mask. Shape TBD.
        """
        raise Exception("Not implemented yet")

    ######################
    # Context management #
    ######################

    def __enter__(self):
        self.open()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()
