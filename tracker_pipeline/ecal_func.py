"""This script was created to separate the dependecy on on ecalib functions for 
for applying calibration. This will only have functions to introduce energy calibration
and not the other supportive functions.

Sambid Wasti 2022/12/05 --initial commit--
"""

import os
import math
import numpy as np
import h5py

VERSION="1.1"

def conv_ac2sd(asic_no, chan_no):
    r""" Converts ASIC/Chan -> Side/Det.

    This is a conversion function to convert the older ASIC/Chan to
    Side/Det.

    Parameters
    ----------
    asic_no : int
        ASIC Number (0-11)
    chan_no  : int
        Channel number 0-31


    Returns
    -------
    r_array : int dataset
        Dataset of shape 2. First element is Side (0-1) and
        second element is detector number 0-191

    Examples
    --------
    For converting ASIC 0, Channel 4:

    >>> conv_array = conv_ac2sd(0,4)
    """
    side = 0
    det_no = 0
    if asic_no < 6:
        side = 0
        det_no = asic_no * 32 + chan_no
    elif 5 < asic_no < 12:
        side = 1
        det_no = (asic_no - 6) * 32 + chan_no
    else:
        print('Invalid Asic Number')

    if chan_no > 31:
        print('Invalid Chan no.')
    r_array = np.array([side, det_no])
    return r_array\

def conv_sd2ac(side, det_no):
    r""" Converts Side/Det -> ASIC/Chan.

    This is a conversion function to convert the older Side/Det to
    Asic/Chan.

    Parameters
    ----------
    side : int
        Side of the layer (0-1) (0 top and 1 bottom)
    det_no  : int
        Detector number 0-192


    Returns
    -------
    r_array : int dataset
        Dataset of shape 2. First element is Asic No. (0-11) and
        second element is channel number 0-31

    Examples
    --------
    For converting side1, det 35 :

    >>> conv_array = conv_sd2ac(1,35)
    """

    asic_no = 0

    chan_no = det_no % 32
    t_val = math.floor(det_no / 32)
    if side == 0:
        asic_no = t_val
    elif side == 1:
        asic_no = t_val + 6
    else:
        print('Invalid side number')

    if det_no > 191:
        print('Invalid detector number')
    r_array = np.array([asic_no, chan_no])
    return r_array

def conv_mask(outfile,layer_name):
    """mask is in asic, channel so we need to remap it to side, det.
    """
    mask_ac = np.zeros((12,32),dtype=int)
    mask_sd = np.zeros((2,192),dtype=int)

    for asic in range(12):
        mask_ac[asic,:] = outfile[f'{layer_name}/config/asic{asic:02d}/chan_disable']
    
    for asic in range(12):
        for chan in range(32):
            delme_val = conv_ac2sd(asic,chan)
            mask_sd[delme_val[0],delme_val[1]] = mask_ac[asic,chan]
    
    return mask_sd



def read_ecal_file(ecal_file):
    """Read the energy calibration file.

    Parameters
    ----------
    ecal_file : file(txt)
        energy calibration file.

    Returns
    -------
    ecal_table (np.array, float)
        returns the table of energy calibration function.
    """
    
    temp_table = np.genfromtxt(ecal_file, dtype ='float', comments='#') # shape = (3840,8) -> 10x2x192 = 3480
    # format: x8 = det, side, strip, fitflag, a1coef, a1coef_err a2coef, a2coef_err
    # a1 = m, a2 = c

    #-- Divide the table into 10x2x192x8
    ecal_table = np.zeros((10,2,192,5),dtype=float)
    nrows = temp_table.shape[0] # should be 3840 but just in case.


    for i in range(nrows):
        temp_data = temp_table[i][...]

        tlayer      = int(temp_data[0])
        tside       = int(temp_data[1])
        tdet        = int(temp_data[2])
        tflag       = temp_data[3]
        
        tslope      = temp_data[4]
        tslope_err  = temp_data[5]
        tinter      = temp_data[6]
        tinter_err  = temp_data[7]

        # REVIEW: Probably can parse the array at once. Need to verify
        ecal_table[tlayer,tside,tdet, 0] = tflag
        ecal_table[tlayer,tside,tdet, 1] = tslope
        ecal_table[tlayer,tside,tdet, 2] = tslope_err
        ecal_table[tlayer,tside,tdet, 3] = tinter
        ecal_table[tlayer,tside,tdet, 4] = tinter_err

    return ecal_table

def apply_ecal(outfile, ecalib_fname, sig_level = 3.0, verbose_flag =False, comp=True):
    """Applies energy calibration.

    Parameters
    ----------
    outfile : hdf5 object
        Output file.
        L2 output file, file name passed as hdf5 object.
    ecalib_fname : str
        Energy Calibration file path. 
    sig_level : float
        Significance level. default 3.0
    verbose_flag : bool, optional
        Verbose Flag, by default False
    comp : bool, optional
        Compression, by default True
    """

    print("    >>> --- ecal_func: apply_ecal: ") if verbose_flag else None
    # STEP: -- Parsing Energy Calibration File --
    print(f"        --- Parsing ecalibration file ") if verbose_flag else None
    
    ecal_table = read_ecal_file(ecalib_fname)
    cflag_array = ecal_table[:,:,:,0]
    print(f"        --- Done ") if verbose_flag else None
    
    # -- Defining Variables --
    layer_list = [f"layer{n:02d}" for n in range(10)]
    
    print(f"        --- Per Layer --- ") if verbose_flag else None
    # -- Per Layer --
    for layer in range(10):
        if layer_list[layer] in outfile.keys():
            print( f" -- Layer : {layer} --")
            print(f"        --- Read Data ") if verbose_flag else None
            
            
            cmdata = outfile[f"{layer_list[layer]}/vdata/channel_cm_sub"][...] # data to apply energy calibration.
            nevent = cmdata.shape[0]
            
            # Get Mask : Could be faster if just sending in config..?
            mask_sd = conv_mask(outfile,layer_list[layer])
            # print(outfile[f'{layer_list[layer]}/config'].keys())

            # STEP: Signifiance calculations.
            print(f"         -- Significance Calculations ") if verbose_flag else None
            sig_array= np.zeros((nevent,2,192), dtype=float)

            # NOTE: Changing order of the loops and calculation of meand and stdev, to execute the script faster.
            for j in range(2):
                for k in range(192):
                    if (mask_sd[j,k] == 0):
                        t_stdev= np.std(cmdata[:,j,k])
                        if t_stdev != 0.0:   # For simulated, there are cases of t_stdev as 0.
                            t_mean = np.mean(cmdata[:,j,k])
                            sig_array[:,j,k] = (cmdata[:,j,k] - t_mean) / t_stdev
                        else:
                            sig_array[:,j,k]=0.0

                                
            
            # Select only significance higher than the level.
            sig_array[(sig_array < sig_level)] = 0.0

            # STEP: Apply Energy Calibration
            print(f"         -- Applying Energy Calibration ") if verbose_flag else None
    
            counter_sig=0
            counter_cflag =0
            counter_nrg =0

            energy_arr = np.zeros((nevent, 2, 192), dtype=float)

            # --- Will need to explore this ---
            # for j in range(2):
            #     for k in range(192):
            #         if (cflag_array[layer,j,k] > 0.0): 
            #             counter_cflag+=1

            #             adc_val = cmdata[i,j,k] # adcvalue
                        
            #             # Check Sig, CFlag and Energy before converting.
            #             if (sig_array[i,j,k] > 0.0):
            #                 counter_sig+= 1
                            
            #                     temp_nrg = ecal_table[layer,j,k,1]* adc_val + ecal_table[layer,j,k,3] # 1 is slope and 3 is intercept.
            #                     if (temp_nrg > 1.0):
            #                         counter_nrg+=1
            #                         energy_arr[i,j,k] = temp_nrg

            for i in range(nevent):
                for j in range(2):
                    for k in range(192):
                        adc_val = cmdata[i,j,k] # adcvalue
                        
                        # Check Sig, CFlag and Energy before converting.
                        if (sig_array[i,j,k] > 0.0):
                            counter_sig+= 1
                            if (cflag_array[layer,j,k] > 0.0): 
                                counter_cflag+=1
                                temp_nrg = ecal_table[layer,j,k,1]* adc_val + ecal_table[layer,j,k,3] # 1 is slope and 3 is intercept.
                                if (temp_nrg > 1.0):
                                    counter_nrg+=1
                                    energy_arr[i,j,k] = temp_nrg

            print(f'Sig : {counter_sig} | Cflag : {counter_cflag} | Nrg: {counter_nrg} ') if verbose_flag else None

            # STEP: Write the dataset.
            print(f"         -- Writing Datasets: nrg and sig_arr ") if verbose_flag else None
            # -- Significance --
            outfile.create_dataset(name=f'/{layer_list[layer]}/vdata/sig_arr', data=sig_array, shape=sig_array.shape, dtype='float64', compression = comp)
            # -- nrg --
            outfile.create_dataset(name=f'/{layer_list[layer]}/vdata/nrg', data=energy_arr, shape=energy_arr.shape,dtype='float64', compression = comp)
        
    #cflag_array
    print(f"        --- Writing Datasets: cflag ") if verbose_flag else None
    outfile.create_dataset(name=f'/Calibration/cflag', data=cflag_array, shape=cflag_array.shape,dtype='float64', compression = comp)
    outfile.attrs['l1tol2_ecal_func_version'] = VERSION