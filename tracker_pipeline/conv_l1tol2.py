""" Script to initiate conver L1 to L2 by applying energy calibration.
    Initial commit : 12/5/22 Sambid Wasti

    USAGE:
    -----

    >>> python conv_l1tol2 l1file* ecalibfile*
    
    >>> python conv_l1tol2 -h (for additional keywords)
"""
#TODO: REPLACE verbose with logging console.
import os
import math
import numpy as np
import tracker_pipeline.ecal_func as ecal
import h5py
from datetime import datetime
import shutil as sh # sh script util.

def create_l2(infile, ecalib_file, verbose_flag=False, comp=True, sig_level=3.0):
    """Script to initiate L2 file.

    Parameters
    ----------
    infile : str
       Input l1 file.
    ecalib_file : str
        Energy Calibraiton file.
    verbose : bool, optional
        verbose flag, by default False
    comp : bool, optional
        compresssion flag, by default True
    sig_level : float
        significance to apply. Default = 3.0
    """
    dstart = datetime.utcnow() # Start time to calculate execution of script.
    print(">>>>>>> --- Starting Script : Convert L1 to L2 --- <<<<<<<")
    print("    >>> --- conv_l1tol2: ") if verbose_flag else None
    print(f"        --- Significance: {sig} ") if verbose_flag else None
    outfile = f"{args.infile.split('L1.h5')[0]}L2.h5"
    if os.path.exists(outfile):
       os.remove(outfile)

    print(f"        --- Outfile {outfile} ") if verbose_flag else None

    sh.copy(infile, outfile)   # --- duplicate file content

    with h5py.File(outfile, "a") as file:
        file.attrs['ecalib_file'] = ecalib_file.split('/')[-1]  # extract only the file name and add to attribute.
        print(f"        --- Added attribute ") if verbose_flag else None
        ecal.apply_ecal(file, ecalib_file, verbose_flag=verbose_flag, comp=comp, sig_level=sig_level)

    dend = datetime.utcnow()
    print(f">>>>>>> --- Ending Script: Execution Time [s] : {(dend-dstart).total_seconds()} --- <<<<<<<")



if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description="Convert L1 to L2")
    parser.add_argument("-v", action='store_true', help=" Verbose flag to monitor progress or to debug")
    parser.add_argument("-u", action='store_true', help=" UnCompression. Default is Compression.")
    
    parser.add_argument("--sig", type=float, default=None, help="Significance")
    parser.add_argument('infile', help='File path')
    parser.add_argument("ecalib_file", help='Energy Calibration File')

    args = parser.parse_args()
 
    if args.v:
        verbose_flag = True
    else:
        verbose_flag = False

    if args.u:
        comp = False
    else:
        comp = True


    if args.sig:
        sig = float(args.sig)
    else:
        sig = 3.0

    create_l2(args.infile, args.ecalib_file,verbose_flag=verbose_flag, comp=comp, sig_level=sig)