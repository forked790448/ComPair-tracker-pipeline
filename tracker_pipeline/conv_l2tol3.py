#!/usr/bin/env python3
"""
Select the relevant datasets and convert the Tracker L2 to L3
Currently, the L3 (CUD format for tracker has 4 datasets.

Event_id (dim = n_event)
Event_time (dim = n_event)
Pulse_heiht (dim = n_event,10, 2, 192)
Energy (dim = n_event, 10, 2, 192)

usage can be retrieved

>>> conv_l2tol3 filepath

for additional options

>>> conv_l2tol3 --help

"""

# version s1.0c = Fixed a small bug where the energies were not properly being placed.
# version s1.0d = Now the attributs from previous levels are preserved.
# version s2.0a = Includes multihit and takes in new L1toL2 file.
# version s2.0b = Fixing errors.
# version s2.0c = Adding 3 hit.
#               - Adding auto nlayer determination
# version s2.1  = fixing issues with auto nlayers and option to set Nlayers
# version s2.2  = fixing coordinate system. Refer to Issue # 13 for further information.

#TODO: 2. Make neighboring for 4 or more.
#TODO: 3. Make generalized neighboring.

import os
import math
import h5py
import numpy as np
import argparse
import sys

import time

VERSION = "s2.2"

#==== Defining some global variables ====
NLAYERS = 10
NLAYERS_FLAG = False            # flag to define nlayers from commanline, default calculates Nlayers.
NRG_PER = 60 # percentage to check energy
NEIGHBOR_LENGTH = 1

XPOS_TBL = np.zeros((192))
YPOS_TBL = np.zeros((192))
ZPOS_TBL = np.zeros((10))

def define_XYZ_table():
    """Defining XYZ table via function. Need to define here and use global for the ability of the script to modify with different NLAYERS value.
    """
        
    global NLAYERS
    global XPOS_TBL
    global YPOS_TBL
    global ZPOS_TBL

    TKR_STRIP_PITCH     = 0.051                             # [cm] 510 microns
    TKR_WIDTH           = 192 * TKR_STRIP_PITCH             # [cm] 192 strips of constant pitch
    TKR_EDGE            = TKR_WIDTH/2.0                     # DSSD is centered at 0, thus the edges are equivalent to half of the width
    TKR_STRIP_CENTROID  = TKR_EDGE - (TKR_STRIP_PITCH)/2.0  # Gives the center of strip 191
    TRK_LAYER_THICKNESS = 0.05
    TKR_LAYER_SPACING   = 1.9                               # [cm] z spacing between layers
    TKR_TOP_LAYER       = (TRK_LAYER_THICKNESS/2.0) + (NLAYERS - 1) * TKR_LAYER_SPACING

    for detector_number in range(NLAYERS):
        ZPOS_TBL[detector_number] = float(-detector_number * TKR_LAYER_SPACING + TKR_TOP_LAYER)

    for strip_number in range(192):
        XPOS_TBL[strip_number] = float(-strip_number * TKR_STRIP_PITCH + TKR_STRIP_CENTROID)
        YPOS_TBL[strip_number] = float(strip_number * TKR_STRIP_PITCH - TKR_STRIP_CENTROID)

# ===== Functions =====
def chk_neighbor(loc1, loc2):
    """
    Checks if the two location are neighbors.
    Parameters
    ----------
    loc1 : int
        location 1. could be x or y
    loc2 : int
        location 2. could be x or y

    Returns
    -------
    Flag : Boolean
        True if neighbor, False if not neighbor
    """
    neigh_flag = False
    if abs(loc1-loc2) == NEIGHBOR_LENGTH:
        neigh_flag = True
    return neigh_flag

def avg_ypos(y1, y2):
    """Average the Y positions

    Parameters
    ----------
    y1 : int 
        y1 location number.
    y2 : int
        y2 location number.

    Returns
    -------
    float
        Average of y1 and y2 position.
    """
    ypos1 = YPOS_TBL[y1]
    ypos2 = YPOS_TBL[y2]
    return ((ypos1+ypos2)/2.0)

def avg_xpos(x1,x2):
    """Average x position.

    Parameters
    ----------
    x1 : int
        x1 position location
    x2 : int
        x2 position location

    Returns
    -------
    float
        Average position of x1 and x2.
    """
    xpos1 = XPOS_TBL[x1]
    xpos2 = XPOS_TBL[x2]
    return ((xpos1+xpos2)/2.0)

def chk_common_strip(sin_hit, dbl_hit_1, dbl_hit_2 ):
    """
    Check the energy of single hit with sum of double hit for common strip.

    Parameters
    ----------
    sin_hit : float
        Single hit energy X or Y strips.
    dbl_hit_1 : float
        one of the double hit energy. X or Y stirps.
    dbl_hit_2 :
        one of the double hit energy. X or Y strips.
    Returns
    -------
    True if common strip and false if not common.
    """
    if (chk_nrg_onehit(sin_hit,dbl_hit_1+dbl_hit_2)) is not None:
        return True
    else:
        return False

# ==== One Hit Functions ====
def chk_nrg_onehit(xnrg, ynrg):
    """
    This is to check energies between x and y for 1 hit and return average if true.

    Parameters
    ----------
    xnrg : float
        Energy of X hit.
    ynrg : float
        Energy of Y hit.

    Returns
    -------
    Average Energy : Float
        Average energy if hit energy within defined percentage value. Else, it returns NONE.
    """
    avg_flag = False

    xnrg = float(xnrg)
    ynrg = float(ynrg)

    nrg_diff = abs(ynrg-xnrg)
    nrg_add = ynrg+xnrg

    if (nrg_diff/nrg_add) < (NRG_PER/100.0):
        avg_flag = True

    if (avg_flag):
        return ((xnrg + ynrg) / 2.0)
    else:
        return None

def one_hit_fnc(evt,layer):
    """
    One Hit Function . 1 X and 1 Y hit.
    Parameters
    ----------
    evt : numpy.array
        Event Energy array
    layer : int
        Layer array.

    Returns
    -------
    np.array
        Numpy of length 5; x,y,z postion, energy and quality flag if valid.
    None :
        If invalid.
    """
    #global cnt_1a
    xloc = np.where(evt[0,:] > 0)[0]
    yloc = np.where(evt[1,:] > 0)[0]

    t_xloc1 = xloc[0]
    t_yloc1 = yloc[0]

    t_xnrg1 = evt[0, xloc[0]]
    t_ynrg1 = evt[1, yloc[0]]

    avg_nrg = (chk_nrg_onehit(t_xnrg1, t_ynrg1))


    if (avg_nrg) is not None:
        #cnt_1a += 1
        xpos = XPOS_TBL[t_xloc1]
        ypos = YPOS_TBL[t_yloc1]
        zpos = ZPOS_TBL[layer]
        qflag =1 
        return [[xpos, ypos, zpos, avg_nrg, qflag]]
    else:
        return None

# ==== Two Hit Functions ====
def two_hit_A1B2(xloc, yloc, xpos, ypos, xnrg, ynrg, layer,skip_y=False):
    zpos    = ZPOS_TBL[layer]
    qflag = 1
    
    if skip_y is False:
        if (chk_neighbor(yloc[0],yloc[1])): 
            # cnt_2a_1 += 1

            new_nrg = ynrg[0] + ynrg[1]
            ypos    = (ypos[0]+ypos[1])/2.0 

            new_avg_nrg= chk_nrg_onehit(xnrg[0], new_nrg)

            if (new_avg_nrg) is not None:

                return [[xpos[0], ypos, zpos, new_avg_nrg, qflag]]
            else:
                return None

    elif chk_common_strip(xnrg[0],ynrg[0],ynrg[1]):
        # cnt_2a_2+= 1
            
        xnrg = xnrg[0]
        ynrg_0 = ynrg[0]
        ynrg_1 = ynrg[1]
        
        new_avg_nrg= chk_nrg_onehit(xnrg, ynrg_0+ynrg_1)
        if new_avg_nrg is not None:

            xnrg_0 = xnrg * (ynrg_0/ (ynrg_0+ynrg_1))
            xnrg_1 = xnrg * (ynrg_1 / (ynrg_0 + ynrg_1))

            nrg0 = (xnrg_0+ynrg_0)/2.0
            nrg1 = (xnrg_1+ynrg_1)/2.0

            ypos0 = ypos[0]
            ypos1 = ypos[1]
 
            return [[xpos[0],ypos0,zpos,nrg0,qflag],[xpos[0],ypos1,zpos,nrg1,qflag]]

    else:
        xnrg1 = xnrg[0]
        ynrg1 = ynrg[0]
        ynrg2 = ynrg[1]

        diff1 = abs(xnrg-ynrg1)
        diff2 = abs(xnrg-ynrg2)

        if (diff1 < diff2):
            avg_nrg = (chk_nrg_onehit(xnrg1,ynrg1))
            if (avg_nrg) is not None:
                # cnt_2a_3a += 1
                ypos = ypos[0]
                return [[xpos[0], ypos, zpos, avg_nrg, qflag]]
            else:
                return None
        else:
            avg_nrg = (chk_nrg_onehit(xnrg1,ynrg2))
            if avg_nrg is not None:
                # cnt_2a_3a += 1
                ypos = ypos[1]
                return [[xpos[0], ypos, zpos, avg_nrg, qflag]]
            else:
                return None 

def greedy_qij(x,y,sigx,sigy):
    """ Calculates the q_ij for a pair of hits.

    Parameters
    ----------
    x : float
        Energy of the x hit.
    y : float
        Energy of the y hit.
    sigx : float
        Resolution of the x strip.
    sigy : float
        Resolution of the y strip.

    Returns
    -------
    Float
        Returns the quality value for the single pair.
    """

    t_nume = (x-y)*(x-y)
    t_deno  = (sigx*sigx) + (sigy*sigy)
    return (t_nume/t_deno)

def greedy_algo_A2B2(xpos_ar, ypos_ar, xnrg_ar, ynrg_ar, zpos):
    """Greedy algorithm for 2 X and 2 Y hits. 
    This might be divided into different sub-functions later.

    Parameters
    ----------
    xpos1 : float
        x1 location
    xpos2 : float
        x2 location
    ypos1 : float
        y1 location     
    ypos2 : float
        y2 location
    t_xnrg1 : float
        x1 energy
    t_xnrg2 : float
        x2 energy
    t_ynrg1 : float
        y1 energy
    t_ynrg2 : float
        y2 energy
    zpos : float
        z position

    Returns
    -------
    list
        list of hits [x, y, z, nrg, qflag]
    """
    # Take the values and calculate quality. 
    # low q means better. So x and y. 
    # Might need a defined metric for this Q factor. 
    
    xpos1 = xpos_ar[0]
    xpos2 = xpos_ar[1]
    ypos1 = ypos_ar[0]
    ypos2 = ypos_ar[1]
    
    t_xnrg1 = xnrg_ar[0]
    t_xnrg2 = xnrg_ar[1]
    t_ynrg1 = ynrg_ar[0]
    t_ynrg2 = ynrg_ar[1]

    # sigma would be from resolution but atm using a possion statistics. sqrt.
    sig_x1 = math.sqrt(t_xnrg1)
    sig_x2 = math.sqrt(t_xnrg2)
    sig_y1 = math.sqrt(t_ynrg1)
    sig_y2 = math.sqrt(t_ynrg2)
    
    # all qs.
    q11 = greedy_qij(t_xnrg1, t_ynrg1, sig_x1, sig_y1)
    q12 = greedy_qij(t_xnrg1, t_ynrg2, sig_x1, sig_y2)
    q21 = greedy_qij(t_xnrg2, t_ynrg1, sig_x2, sig_y1)
    q22 = greedy_qij(t_xnrg2, t_ynrg2, sig_x2, sig_y2)
    
    #greedy algo works with combinations of possible pairs.
    #Here there are 2 combinations so we average them. 
    
    # 1st Combination.
    # 11 and 22
    q1 = (q11+q22)/2.0
    q2 = (q12+q21)/2.0

    if (q1 < q2):
        qflag = 2
        qflag2 = 3

        nrg1 = (t_xnrg1+t_ynrg1)/2.0
        nrg2 = (t_xnrg2+t_ynrg2)/2.0
        
        return [[xpos1,ypos1,zpos,nrg1, qflag],[xpos2,ypos2,zpos,nrg2,qflag],[xpos1,ypos2,zpos,nrg1, qflag2],[xpos2,ypos1,zpos,nrg2, qflag2] ]
    else:
        qflag = 2
        qflag2 = 3

        nrg1 = (t_xnrg1+t_ynrg2)/2.0
        nrg2 = (t_xnrg2+t_ynrg1)/2.0
        
        return [[xpos1,ypos2,zpos,nrg1,qflag],[xpos2,ypos1,zpos,nrg2, qflag],[xpos1,ypos1,zpos,nrg1, qflag2],[xpos2,ypos2,zpos,nrg2,qflag2]]
        
def two_hit_A2B1(xloc, yloc, xpos, ypos, xnrg, ynrg, layer,skip_x=False):
    zpos    = ZPOS_TBL[layer]
    ypos = ypos[0]
    qflag = 1
    
    if skip_x is False:
        if (chk_neighbor(xloc[0],xloc[1])): 
        
            new_nrg = xnrg[0] + xnrg[1]
            xpos    = (xpos[0]+xpos[1])/2.0 

            new_avg_nrg= chk_nrg_onehit( new_nrg,ynrg[0])

            if (new_avg_nrg) is not None:
                return [[xpos, ypos, zpos, new_avg_nrg, qflag]]
            else:
                return None
    elif chk_common_strip(ynrg[0],xnrg[0],xnrg[1]):
            
        xnrg_0 = xnrg[0]
        xnrg_1 = xnrg[1]
        ynrg = ynrg[0]
        
        new_avg_nrg= chk_nrg_onehit(xnrg_0+xnrg_1, ynrg)
        if new_avg_nrg is not None:

            ynrg_0 = ynrg * (xnrg_0/ (xnrg_0+xnrg_1))
            ynrg_1 = ynrg * (xnrg_1 / (xnrg_0 + xnrg_1))

            nrg0 = (xnrg_0+ynrg_0)/2.0
            nrg1 = (xnrg_1+ynrg_1)/2.0

            xpos0 = xpos[0]
            xpos1 = xpos[1]

            return [[xpos0,ypos,zpos,nrg0,qflag],[xpos1,ypos,zpos,nrg1,qflag]]
    else:
        xnrg1 = xnrg[0]
        xnrg2 = xnrg[1]
        ynrg = ynrg[0]

        diff1 = abs(xnrg1-ynrg)
        diff2 = abs(xnrg2-ynrg)

        if (diff1 < diff2):
            avg_nrg = (chk_nrg_onehit(xnrg1,ynrg))
            if (avg_nrg) is not None:
                xpos = xpos[0]
                return [[xpos, ypos, zpos, avg_nrg, qflag]]
            else:
                return None
        else:
            avg_nrg = (chk_nrg_onehit(xnrg2,ynrg))
            if avg_nrg is not None:
                xpos = xpos[1]
                return [[xpos, ypos, zpos, avg_nrg, qflag]]
            else:
                return None 

def two_hit_A2B2(xloc, yloc, xpos, ypos, xnrg, ynrg, layer, skip_x = False, skip_y = False):
    """ This function handles A2B2.

    Parameters
    ----------
    t_xloc1 : int
        1st X location. 2 locations.
    t_xloc2 : int
        2nd X location. 2 locations.
    t_yloc1 : int
        1st Y location, 2 locations.
    t_yloc2 : int
        2nd Y location, 2 locations.
    t_xnrg1 : float
        1st X nrg. 2 hits.
    t_xnrg2 : float
        2nd X nrg. 2 hits.
    t_ynrg1 : float
        1st Y nrg. 2 Hits.
    t_ynrg2 : float
        2nd Y nrg. 2 Hits.
    layer :   int
        Layer no. for z position.

    Returns
    -------
    Hit info : np.array
        Array with x, y, z postion with energy if valid.
    None :
        If invalid.

    """
    #global cnt_2c_1
    #global cnt_2c_1x
    #global cnt_2c_1y
    #global cnt_2c_1a
    #global cnt_2c_1b
    #global cnt_2c_1c
    #global cnt_2c_1d

    xpos_ar = xpos
    xnrg_ar = xnrg
    ypos_ar = ypos
    ynrg_ar = ynrg
    zpos    = ZPOS_TBL[layer]

    # -- Check Neighbor --
    # Starts as A2B2
    if skip_x is False:
        if (chk_neighbor(xloc[0],xloc[1])):
            # if x neighbor.. replace the pos and nrg x arr by vlaues.
            #cnt_2c_1x += 1

            new_nrg = xnrg_ar[0]+xnrg_ar[1]
            xnrg_ar = [new_nrg]
            xpos_ar = [(xpos_ar[0]+xpos_ar[1])/2.0]
        # if this is true then it becomes A1B2

    if skip_y is False: 
        if (chk_neighbor(yloc[0],yloc[1])):
            # if y neighbor.. replace the pos and nrg y arr by vlaues.
            #cnt_2c_1y += 1

            new_nrg = ynrg_ar[0]+ynrg_ar[1]
            ynrg_ar = [new_nrg]
            ypos_ar = [(ypos_ar[0]+ypos_ar[1])/2.0]

    # Recheck no. of hits on each side and outsource the job.
    sidea_cnt = len(xnrg_ar)
    sideb_cnt = len(ynrg_ar)

    if sidea_cnt == 1 and sideb_cnt == 1:
        #cnt_2c_1a += 1
        avg_nrg = (chk_nrg_onehit(xnrg_ar[0], ynrg_ar[0]))
        qflag = 1
        if (avg_nrg) is not None:
            xpos = xpos_ar[0]
            ypos = ypos_ar[0]
            zpos = zpos
            return [[xpos, ypos, zpos, avg_nrg, qflag]]
        else:
            return None
    elif sidea_cnt == 1 and sideb_cnt == 2:
        #cnt_2c_1b += 1
        return (two_hit_A1B2(xloc, yloc,xpos_ar, ypos_ar, xnrg_ar, ynrg_ar,layer, skip_y=True))

    elif sidea_cnt == 2 and sideb_cnt == 1:
        #cnt_2c_1c += 1
        return (two_hit_A2B1(xloc, yloc,xpos_ar, ypos_ar, xnrg_ar, ynrg_ar,layer, skip_x=True))

    else:  # These are 2A 2B
        # FIXME: Need to pass in the averaged positoin rather than xloc.. Need to translate that...
        # Currently working on non averaged
        #--- Position ---
        xpos_ar=xpos
        xnrg_ar=xnrg
        ypos_ar=ypos
        ynrg_ar=ynrg

        return(greedy_algo_A2B2(xpos_ar, ypos_ar, xnrg_ar, ynrg_ar, zpos))

def two_hit_fnc(evt, layer):
    """2 Hit function to handle primarily A1B2, A2B1 and A2B2 events.

    Parameters
    ----------
    evt : dataset
        Event information
    layer : int
        Layer number, mostly for z info.

    Returns
    -------
    Hit info : np.array
        Array with x, y, z postion with energy if valid.
    None :
        If invalid.
    """
    
    # This should return
    x_cnt = np.count_nonzero(evt[0, :] > 0.0)
    y_cnt = np.count_nonzero(evt[1, :] > 0.0)

    xloc = np.where(evt[0, :] > 0)[0]
    yloc = np.where(evt[1, :] > 0)[0]

    xnrg = [evt[0,:][xloc]][0]
    ynrg = [evt[1,:][yloc]][0]

    # this has output as 1,1 or 2,2
    if (x_cnt == 1) and (y_cnt == 2):
        #cnt_2a += 1
        
        xpos = [XPOS_TBL[xloc[0]]]
        ypos = [YPOS_TBL[yloc[0]], YPOS_TBL[yloc[1]]]

        return(two_hit_A1B2(xloc, yloc, xpos, ypos, xnrg, ynrg, layer))

    elif (x_cnt == 2) and (y_cnt == 1):
        #cnt_2b += 1

        xpos = [XPOS_TBL[xloc[0]],XPOS_TBL[xloc[1]] ]
        ypos = [YPOS_TBL[yloc[0]]]

        return(two_hit_A2B1(xloc, yloc, xpos, ypos, xnrg, ynrg, layer))

    elif (x_cnt == 2) and (y_cnt == 2):
        #cnt_2c += 1

        xpos = [XPOS_TBL[xloc[0]],XPOS_TBL[xloc[1]] ]
        ypos = [YPOS_TBL[yloc[0]],YPOS_TBL[yloc[1]] ]

        return(two_hit_A2B2(xloc, yloc, xpos, ypos, xnrg, ynrg, layer))

    else:
        print('What')

# ==== Three Hit Functions ====

def check_neighbor_y_3(yloc,ynrg):
    """Check neighbor for 3 Y hits and return nrg and combined

    Parameters
    ----------
    yloc : int arr
        y location hit
    ynrg : float arr
        y nrg hit

    Returns
    -------
    2D-list
        Returns 2D list of position and energy if they are neighbors.
    """

    y1 = yloc[0]
    y2 = yloc[1]
    y3 = yloc[2]

    y1_nrg = ynrg[0]
    y2_nrg = ynrg[1]
    y3_nrg = ynrg[2]
    
    cmn1_flag = False
    cmn2_flag = False
    if abs(y1-y2) == 1:
        cmn1_flag=True
    
    if abs(y2-y3) ==1:
        cmn2_flag = True
        
    if cmn1_flag and cmn2_flag:    
        tot_ypos = (YPOS_TBL[y1]+YPOS_TBL[y2]+YPOS_TBL[y3])/3.0
        tot_ynrg = (y1_nrg +y2_nrg+y3_nrg)
        return([tot_ypos],[tot_ynrg])
    elif cmn1_flag:
        tot_ypos = (YPOS_TBL[y1]+YPOS_TBL[y2])/2.0
        tot_ynrg = (y1_nrg +y2_nrg)
        return([tot_ypos,YPOS_TBL[y3]],[tot_ynrg,y3_nrg])
    elif cmn2_flag:
        tot_ypos = (YPOS_TBL[y2]+YPOS_TBL[y3])/2.0
        tot_ynrg = (y2_nrg +y3_nrg)
        return([YPOS_TBL[y1],tot_ypos],[y1_nrg,tot_ynrg]) 
    else:
        return None

def three_hit_A1B3(xloc, yloc,xpos,ypos,xnrg, ynrg, layer,skip_neighbor = False):
    """_summary_

    Parameters
    ----------
    xloc : _type_
        _description_
    yloc : _type_
        _description_
    xpos : _type_
        _description_
    ypos : _type_
        _description_
    xnrg : _type_
        _description_
    ynrg : _type_
        _description_
    layer : _type_
        _description_

    Returns
    -------
    _type_
        _description_
    """

    if skip_neighbor is False:
        t_val = check_neighbor_y_3(yloc,ynrg)
        if t_val is not None:
            ypos, ynrg = t_val
    
    xcnt = 1
    ycnt = len(ypos)
    zpos = ZPOS_TBL[layer]
    if ycnt ==1:
        avg_nrg = chk_nrg_onehit(xnrg[0], ynrg[0])
            
        if (avg_nrg) is not None:
            xpos = xpos[0]
            ypos = ypos[0]
            zpos = ZPOS_TBL[layer]
        
            qflag = 1
            return [[xpos, ypos, zpos, avg_nrg, qflag]]
        else:
            return None
    elif ycnt ==2:
        return( two_hit_A1B2(xloc,yloc,xpos,ypos,xnrg,ynrg,layer,skip_y=True) )
        
    if ycnt ==1:
        #its a A1B1 hit
        
        avg_nrg = chk_nrg_onehit(xnrg[0], ynrg[0])
            
        if (avg_nrg) is not None:
            xpos = xpos[0]
            ypos = ypos[0]
            
            return [[xpos, ypos, zpos, avg_nrg, qflag]]
        else:
            return None
    elif ycnt ==2:
        return( two_hit_A1B2(xloc,yloc,xpos,ypos,xnrg,ynrg,layer,skip_y=True) )
        
    elif ycnt == 3:
        
        xnrg0 = xnrg[0]
        ynrg1 = ynrg[0]
        ynrg2 = ynrg[1]
        ynrg3 = ynrg[2]
        
        xpos0 = xpos[0]
        qflag =1
        if (chk_nrg_onehit(xnrg0,ynrg1+ynrg2+ynrg3)) is not None: 
            
            xnrg1 = xnrg0 * (ynrg1/ (ynrg1+ynrg2+ynrg3))
            xnrg2 = xnrg0 * (ynrg2/ (ynrg1+ynrg2+ynrg3))
            xnrg3 = xnrg0 * (ynrg3/ (ynrg1+ynrg2+ynrg3))
            
            nrg1 = (xnrg1+ynrg1)/2.0
            nrg2 = (xnrg2+ynrg2)/2.0
            nrg3 = (xnrg3+ynrg3)/2.0
            
            ypos1 = ypos[0]
            ypos2 = ypos[1]
            ypos3 = ypos[2]
            
            return [[xpos0,ypos1,zpos,nrg1,qflag],[xpos0,ypos2,zpos,nrg2,qflag],[xpos0,ypos3,zpos,nrg3,qflag]]
        else:
            #REVIEW:STEP:TODO: Change this to highest significance?
            #find the closest Energy
            diff1 = abs(xnrg0-ynrg1)
            diff2 = abs(xnrg0-ynrg2)
            diff3 = abs(xnrg0-ynrg3)
            
            if min(diff1,diff2,diff3) == diff1:
                    avg_nrg = (chk_nrg_onehit(xnrg0,ynrg1))
                    if (avg_nrg) is not None:
                        return [[xpos0, ypos[0], zpos, avg_nrg, qflag]]
                    else:
                        return None
            
            elif min(diff1,diff2,diff3) == diff2:
                    avg_nrg = (chk_nrg_onehit(xnrg0,ynrg2))
                    if (avg_nrg) is not None:
                        return [[xpos0, ypos[1], zpos, avg_nrg, qflag]]
                    else:
                        return None
            
            elif min(diff1,diff2,diff3) == diff3:
                    avg_nrg = (chk_nrg_onehit(xnrg0,ynrg3))
                    if (avg_nrg) is not None:
                        return [[xpos0, ypos[2], zpos, avg_nrg, qflag]]
                    else:
                        return None
            else:
                return None

def check_neighbor_x_3(xloc,xnrg):
    """_summary_

    Parameters
    ----------
    xloc : _type_
        _description_
    xnrg : _type_
        _description_
    """
    x1 = xloc[0]
    x2 = xloc[1]
    x3 = xloc[2]

    x1_nrg = xnrg[0]
    x2_nrg = xnrg[1]
    x3_nrg = xnrg[2]
    
    cmn1_flag = False
    cmn2_flag = False
    if abs(x1-x2) == 1:
        cmn1_flag=True
    
    if abs(x2-x3) ==1:
        cmn2_flag = True
        
    if cmn1_flag and cmn2_flag:    
        tot_xpos = (XPOS_TBL[x1]+XPOS_TBL[x2]+XPOS_TBL[x3])/3.0
        tot_xnrg = (x1_nrg +x2_nrg+x3_nrg)
        return([tot_xpos],[tot_xnrg])
    elif cmn1_flag:
        tot_xpos = (XPOS_TBL[x1]+XPOS_TBL[x2])/2.0
        tot_xnrg = (x1_nrg +x2_nrg)
        return([tot_xpos,XPOS_TBL[x3]],[tot_xnrg,x3_nrg])
    elif cmn2_flag:
        tot_xpos = (XPOS_TBL[x2]+XPOS_TBL[x3])/2.0
        tot_xnrg = (x2_nrg +x3_nrg)
        return([XPOS_TBL[x1],tot_xpos],[x1_nrg,tot_xnrg])   
    else:
        return None
    
def three_hit_A3B1(xloc, yloc,xpos,ypos,xnrg, ynrg, layer,skip_neighbor = False):
    """_summary_

    Parameters
    ----------
    xloc : _type_
        _description_
    yloc : _type_
        _description_
    xpos : _type_
        _description_
    ypos : _type_
        _description_
    xnrg : _type_
        _description_
    ynrg : _type_
        _description_
    layer : _type_
        _description_
    """
    if skip_neighbor is False:
        t_val = check_neighbor_x_3(xloc,xnrg)
        if t_val is not None:
            xpos, xnrg = t_val   
    
    ycnt = 1
    xcnt = len(xpos)
    zpos = ZPOS_TBL[layer]
    qflag = 1
    
    if xcnt ==1:
        
        avg_nrg = chk_nrg_onehit(xnrg[0], ynrg[0])
            
        if (avg_nrg) is not None:
            xpos = xpos[0]
            ypos = ypos[0]
            
            return [[xpos, ypos, zpos, avg_nrg, qflag]]
        else:
            return None
    elif xcnt ==2:
        return( two_hit_A2B1(xloc,yloc,xpos,ypos,xnrg,ynrg,layer,skip_x=True) )
        
    elif xcnt == 3:
        
        ynrg0 = ynrg[0]
        xnrg1 = xnrg[0]
        xnrg2 = xnrg[1]
        xnrg3 = xnrg[2]
        
        ypos0 = ypos[0]
        
        if (chk_nrg_onehit(xnrg1+xnrg2+xnrg3,ynrg0)) is not None: 
            ynrg1 = ynrg0 * (xnrg1/ (xnrg1+xnrg2+xnrg3))
            ynrg2 = ynrg0 * (xnrg2/ (xnrg1+xnrg2+xnrg3))
            ynrg3 = ynrg0 * (xnrg3/ (xnrg1+xnrg2+xnrg3))
            
            nrg1 = (xnrg1+ynrg1)/2.0
            nrg2 = (xnrg2+ynrg2)/2.0
            nrg3 = (xnrg3+ynrg3)/2.0
            
            xpos1 = xpos[0]
            xpos2 = xpos[1]
            xpos3 = xpos[2]
            
            return [[xpos1,ypos0,zpos,nrg1,qflag],[xpos2,ypos0,zpos,nrg2,qflag],[xpos3,ypos0,zpos,nrg3,qflag]]
            """
            #NOTE: Since we cant account for the Xpos 3.. we dont do the below.
            elif(chk_nrg_onehit(xnrg1+xnrg2,ynrg0)) is not None:
                
                ynrg1 = ynrg0 * (xnrg1/ (xnrg1+xnrg2))
                ynrg2 = ynrg0 * (xnrg2/ (xnrg1+xnrg2))

                nrg1 = (xnrg1+ynrg1)/2.0
                nrg2 = (xnrg2+ynrg2)/2.0
                
                xpos1 = xpos[0]
                xpos2 = xpos[1]
                
                return [[xpos1,ypos0,zpos,nrg1,qflag],[xpos2,ypos0,zpos,nrg2,qflag]]
                
                
            elif(chk_nrg_onehit(xnrg2+xnrg3,ynrg0)) is not None:

                ynrg2 = ynrg0 * (xnrg2/ (xnrg2+xnrg3))
                ynrg3 = ynrg0 * (xnrg3/ (xnrg2+xnrg3))
                
                
                nrg2 = (xnrg2+ynrg2)/2.0
                nrg3 = (xnrg3+ynrg3)/2.0
                
                
                xpos2 = xpos[1]
                xpos3 = xpos[2]
                
                return [[xpos2,ypos0,zpos,nrg2,qflag],[xpos3,ypos0,zpos,nrg3,qflag]]

                
            elif(chk_nrg_onehit(xnrg0,ynrg1+ynrg3)) is not None:
                
                ynrg1 = ynrg0 * (xnrg1/ (xnrg1+xnrg2+xnrg3))
                ynrg3 = ynrg0 * (xnrg3/ (xnrg1+xnrg2+xnrg3))
                
                nrg1 = (xnrg1+ynrg1)/2.0
                nrg3 = (xnrg3+ynrg3)/2.0
                
                xpos1 = xpos[0]
                xpos2 = xpos[1]
                
                return [[xpos1,ypos0,zpos,nrg1,qflag],[xpos3,ypos0,zpos,nrg3,qflag]]
            """
        else:
            #NOTE:Could just avoid this.
            diff1 = abs(xnrg1-ynrg0)
            diff2 = abs(xnrg2-ynrg0)
            diff3 = abs(xnrg3-ynrg0)
            
            if min(diff1,diff2,diff3) == diff1:
                    avg_nrg = (chk_nrg_onehit(xnrg1,ynrg0))
                    if (avg_nrg) is not None:
                        return [[xpos[0], ypos0, zpos, avg_nrg, qflag]]
                    else:
                        return None
            
            elif min(diff1,diff2,diff3) == diff2:
                    avg_nrg = (chk_nrg_onehit(xnrg2,ynrg0))
                    if (avg_nrg) is not None:
                        return [[xpos[1], ypos0, zpos, avg_nrg, qflag]]
                    else:
                        return None
            
            elif min(diff1,diff2,diff3) == diff3:
                    avg_nrg = (chk_nrg_onehit(xnrg3,ynrg0))
                    if (avg_nrg) is not None:
                        return [[xpos[2], ypos0, zpos, avg_nrg, qflag]]
                    else:
                        return None
            else:
                return None

def three_hit_A2B3(xloc, yloc,xpos,ypos,xnrg, ynrg, layer, skip_neighbor=True):
    if skip_neighbor is False:
        t_val = check_neighbor_y_3(yloc,ynrg)
        if t_val is not None:
            ypos, ynrg = t_val

        t_val = check_neighbor_y_3(yloc,ynrg)
        if t_val is not None:
            ypos, ynrg = t_val
    
    xcnt = len(xpos)
    ycnt = len(ypos)
    zpos = ZPOS_TBL[layer]
    qflag = 1
    
    
    if xcnt == 1 and ycnt == 1:
        avg_nrg = chk_nrg_onehit(xnrg[0], ynrg[0])
            
        if (avg_nrg) is not None:
            xpos = xpos[0]
            ypos = ypos[0]
            
            return [[xpos, ypos, zpos, avg_nrg, qflag]]
        else:
            return None
    elif xcnt == 1 and ycnt == 2:
        return( two_hit_A1B2(xloc,yloc,xpos,ypos,xnrg,ynrg,layer,skip_y=True) )
    elif xcnt == 2 and ycnt == 1:
        return( two_hit_A2B1(xloc,yloc,xpos,ypos,xnrg,ynrg,layer,skip_x=True) )
    elif xcnt == 2 and ycnt == 2:
        return( two_hit_A2B2(xloc, yloc, xpos, ypos, xnrg, ynrg, layer, skip_x = True, skip_y = True) )
    elif xcnt == 1 and ycnt == 3:
        return(three_hit_A3B1(xloc, yloc,xpos,ypos,xnrg, ynrg, layer, skip_neighbor = True))
    elif xcnt == 2 and ycnt == 3:
        return None
    else:
        return None

def three_hit_A3B2(xloc, yloc,xpos,ypos,xnrg, ynrg, layer, skip_neighbor=True):
    if skip_neighbor is False:
        t_val = check_neighbor_y_3(yloc,ynrg)
        if t_val is not None:
            ypos, ynrg = t_val

        t_val = check_neighbor_y_3(yloc,ynrg)
        if t_val is not None:
            ypos, ynrg = t_val
    
    xcnt = len(xpos)
    ycnt = len(ypos)
    zpos = ZPOS_TBL[layer]
    qflag = 1
    
    
    if xcnt == 1 and ycnt == 1:
        avg_nrg = chk_nrg_onehit(xnrg[0], ynrg[0])
            
        if (avg_nrg) is not None:
            xpos = xpos[0]
            ypos = ypos[0]
            
            return [[xpos, ypos, zpos, avg_nrg, qflag]]
        else:
            return None
    elif xcnt == 1 and ycnt == 2:
        return( two_hit_A1B2(xloc,yloc,xpos,ypos,xnrg,ynrg,layer,skip_y=True) )
    elif xcnt == 2 and ycnt == 1:
        return( two_hit_A2B1(xloc,yloc,xpos,ypos,xnrg,ynrg,layer,skip_x=True) )
    elif xcnt == 2 and ycnt == 2:
        return( two_hit_A2B2(xloc, yloc, xpos, ypos, xnrg, ynrg, layer, skip_x = True, skip_y = True) )
    elif xcnt == 3 and ycnt == 1:
        return(three_hit_A3B1(xloc, yloc,xpos,ypos,xnrg, ynrg, layer, skip_neighbor = True))
    elif xcnt == 3 and ycnt == 2:
        return None
        # Since Neighbors checked and this makes A3B3 and we are doing nothing with A3B3 atm.
    else:
        return None

def three_hit_A3B3(xloc, yloc,xpos,ypos,xnrg, ynrg, layer):
    #print("A3B3")
    #-- Check Y Neighbors --
    t_val = check_neighbor_y_3(yloc,ynrg)
    if t_val is not None:
        ypos, ynrg = t_val
        
    #-- Check X Neighbors --
    t_val = check_neighbor_x_3(xloc,xnrg)
    if t_val is not None:
        xpos, xnrg = t_val
    
    xcnt = len(xpos)
    ycnt = len(ypos)
    zpos = ZPOS_TBL[layer]

    if xcnt == 1 and ycnt == 1:
        avg_nrg = chk_nrg_onehit(xnrg[0], ynrg[0])
        qflag = 1
        if (avg_nrg) is not None:
            xpos = xpos[0]
            ypos = ypos[0]
            
            return [[xpos, ypos, zpos, avg_nrg, qflag]]
        else:
            return None
    elif xcnt == 1 and ycnt == 2:
        return( two_hit_A1B2(xloc,yloc,xpos,ypos,xnrg,ynrg,layer,skip_y=True) )
    elif xcnt == 2 and ycnt == 1:
        return( two_hit_A2B1(xloc,yloc,xpos,ypos,xnrg,ynrg,layer,skip_x=True) )
    elif xcnt == 2 and ycnt == 2:
        return( two_hit_A2B2(xloc, yloc, xpos, ypos, xnrg, ynrg, layer, skip_x = True, skip_y = True) )
    elif xcnt == 3 and ycnt == 1:
        return(three_hit_A1B3(xloc, yloc,xpos,ypos,xnrg, ynrg, layer, skip_neighbor = True))
    elif xcnt == 3 and ycnt == 2:
        return(three_hit_A3B2(xloc, yloc,xpos,ypos,xnrg, ynrg, layer, skip_neighbor=True))
    elif xcnt == 1 and ycnt == 3:
        return(three_hit_A3B1(xloc, yloc,xpos,ypos,xnrg, ynrg, layer, skip_neighbor = True))
    elif xcnt == 2 and ycnt == 3:
        return(three_hit_A2B3(xloc, yloc,xpos,ypos,xnrg, ynrg, layer, skip_neighbor=True))
    elif xcnt == 3 and ycnt == 3:
        #Ideally Greedy 3 but doesnt make sense.
        return None
    else:
        return None
    
def three_hit_fnc(evt,layer):
    """_summary_

    Parameters
    ----------
    evt : _type_
        _description_
    layer : _type_
        _description_
    """
    x_cnt = np.count_nonzero(evt[0,:]>0.0)
    y_cnt = np.count_nonzero(evt[1,:]>0.0)
    
    xloc = np.where(evt[0,:] > 0)[0]
    yloc = np.where(evt[1,:] > 0)[0]
    
    xnrg = [evt[0,:][xloc]][0]
    ynrg = [evt[1,:][yloc]][0]
    
    if x_cnt ==1 and y_cnt==3:
        xpos = [XPOS_TBL[xloc[0]]]
        ypos = [YPOS_TBL[yloc][0], YPOS_TBL[yloc][1], YPOS_TBL[yloc][2]]
        return(three_hit_A1B3(xloc, yloc,xpos,ypos,xnrg, ynrg, layer))
    elif x_cnt ==3 and y_cnt==1:
        xpos = [XPOS_TBL[xloc[0]], XPOS_TBL[xloc[1]],XPOS_TBL[xloc[2]]]
        ypos = [YPOS_TBL[yloc][0]]
        return(three_hit_A3B1(xloc, yloc,xpos,ypos,xnrg, ynrg, layer))
    elif x_cnt ==2 and y_cnt==3:
        xpos = [XPOS_TBL[xloc[0]], XPOS_TBL[xloc[1]] ]
        ypos = [YPOS_TBL[yloc[0]], YPOS_TBL[yloc[1]], YPOS_TBL[yloc][2]]
        return(three_hit_A2B3(xloc, yloc,xpos,ypos,xnrg, ynrg, layer))
    elif x_cnt ==3 and y_cnt==2:
        xpos = [XPOS_TBL[xloc[0]], XPOS_TBL[xloc[1]],XPOS_TBL[xloc[2]]]
        ypos = [YPOS_TBL[yloc][0], YPOS_TBL[yloc][1]]
        return(three_hit_A3B2(xloc, yloc,xpos,ypos,xnrg, ynrg, layer))
    elif x_cnt ==3 and y_cnt==3:
        xpos = [XPOS_TBL[xloc[0]], XPOS_TBL[xloc[1]],XPOS_TBL[xloc[2]]]
        ypos = [YPOS_TBL[yloc[0]], YPOS_TBL[yloc[1]], [YPOS_TBL[yloc[2]]]]
        return(three_hit_A3B3(xloc, yloc,xpos,ypos,xnrg, ynrg, layer))
    else:
        None

def change_Nlayers(nlayer):
    """Change NLAYERS by defining it from available keys.

    Parameters
    ----------
    nlayer :int
        No. of layers to be set. 
    """
    global NLAYERS
    print(f"--- No. of Layers detected and reset to : {nlayer} ---")
    NLAYERS = nlayer

def change_Nlayers_flag(flag_val = True):
    """Define a NLAYERS flag and change it, if the option is triggered from Commandline output.

    Parameters
    ----------
    flag_val : bool, optional
        default is true., by default True
    """
    global NLAYERS_FLAG
    NLAYERS_FLAG= flag_val

def create_l3(fname, outfile, verbose=False, comp = True):
    # layer_list = ['layer00','layer01','layer02','layer03','layer04','layer05','layer06','layer07','layer08','layer09']
    layer_list = [f"layer{n:02d}" for n in range(10)]

    # Sanity Check
    
    nsync = [file[f"{layer}/data/sync_index"].size for layer in layer_list if layer in file]
    max_size = nsync[0]
    # Sanity check...
    assert all(np.array(nsync) == max_size), "Insanity Error: The Sync_Indices are not the Same"

    max_size = max_size - 1  # We are skipping the EVENTID=0 or sync_index=0

    # Event Id and Event Time (shape = eventid)
    # This is same for all layers due to sync_index
    # We only need 1 layer but we are looping in case layer 0 is not plugged in the layer stack.
    if verbose == True:
        print("<<<< Event IDs and Event Time started .....>>>>>")
    
    # --- determine N Layers --- 
    if (NLAYERS_FLAG):
        change_Nlayers(NLAYERS)
    else:
        nolayer=0
        for i in range(10):
            if layer_list[i] in file.keys():
                nolayer+=1
        change_Nlayers(nolayer)
    
    define_XYZ_table() # Define XYZ after nlayers defined. Doesnt affect X and Y.

    for i in range(10):
        if layer_list[i] in file.keys():
            sync_index = file[f"{layer_list[i]}/data/sync_index"][...]
            evtdata = file[f"{layer_list[i]}/data/event_id"][...]
            event_id = evtdata[sync_index[1:]]

            evttime = file[f"{layer_list[i]}/data/system_time"][...]
            event_time = evttime[sync_index[1:]]

            if verbose == True:
                print(f" Layer :{i} done")
            # break

    if verbose == True:
        print(f"Event ID and Event Time completed ......")
        print("<<<< Pulse Height.. Started >>>>>")

    # Pulse Height (dim = nev, 10, 2, 192)

    pulse_height = np.zeros((max_size, 10, 2, 192))
    for i in range(10):
        if layer_list[i] in file.keys():

            data = file[f"{layer_list[i]}/vdata/channel_cm_sub"][...]
            sync_index = file[f"{layer_list[i]}/data/sync_index"][...]
            sync_index = np.array(sync_index)

            
            print(f"Layer:{i} , Id Len:{len(sync_index)}") if verbose == True else None

            pulse_height[:, i, :, :] = data[sync_index[1:], :, :]

    if verbose == True:
        print("<<<< Pulse Height... Ended >>>>>")
        print("<<<< Energy... Started >>>>>")

    energy_array_list = [] # some weird float to object when appending on numpy.

    for i in range(10):

        if layer_list[i] in file.keys():
            layer = i
            
            nrg_data = file[f"{layer_list[i]}/vdata/nrg"][...]  # Grab nrg data
            evtdata = file[f"{layer_list[i]}/data/event_id"][...]  # Grab event id
            nrg_sync_index = file[f"{layer_list[i]}/data/sync_index"][...]  # grab sync_index

            nrg_arr = nrg_data[nrg_sync_index[1:], :, :]
            id_arr = evtdata[nrg_sync_index[1:]]

            counter0 = 0
            counter1 = 0
            counter1a= 0
            counter2 = 0
            counter2a = 0
            counter3=0
            counter4=0
            counter5=0
            
            #NOTE: The x and y position (side a and b are switched)
            # The best way to deal with it seems to be to switch it here.

            for j in range(len(id_arr)):
                cur_evt = nrg_arr[j, :, :]
                a_cnt = np.count_nonzero(cur_evt[0, :] > 0.0)  # side a count
                b_cnt = np.count_nonzero(cur_evt[1, :] > 0.0)  # side b count

                counter4 += (a_cnt+b_cnt)
                if a_cnt < 1 or b_cnt < 1:
                    counter0+=(a_cnt+b_cnt)
                    None
                elif a_cnt == 1 and b_cnt == 1:
                    counter1+=(a_cnt+b_cnt)
                    ret_val = one_hit_fnc(cur_evt, layer)
                    
                    if ret_val is not None:
                        [xpos_val, ypos_val,   zpos_val, nrg_val, qflag] = ret_val[0]
                        temp_arr = [id_arr[j],  ypos_val, xpos_val, zpos_val, nrg_val, qflag]
                        #energy_array = np.append(energy_array, [temp_arr], axis=0)
                        energy_array_list.append(temp_arr)
                        counter1a+=2

                elif (1 <= a_cnt <= 2) and (1 <= b_cnt <= 2):
                    counter2+=(a_cnt+b_cnt)
                    ret_val = two_hit_fnc(cur_evt, layer)
                    if ret_val is not None:
                        counter2a+= (len(ret_val)*2)
                        for k in range(len(ret_val)):
                            [xpos_val, ypos_val,   zpos_val, nrg_val,qflag] = ret_val[k]
                            temp_arr = [id_arr[j],ypos_val, xpos_val,  zpos_val, nrg_val, qflag]
                            energy_array_list.append(temp_arr)

                elif (1 <= a_cnt <= 3) and (1 <= b_cnt <= 3):
                    counter3+= a_cnt+b_cnt
                    ret_val = three_hit_fnc(cur_evt,layer)
                    if ret_val is not None:
                        for k in range(len(ret_val)):
                            [xpos_val, ypos_val,   zpos_val, nrg_val, qflag] = ret_val[k]
                            temp_arr = [id_arr[j],ypos_val, xpos_val,  zpos_val, nrg_val, qflag]
                            energy_array_list.append(temp_arr)
                else:
                    counter4+=(a_cnt+b_cnt)

            print(f"Layer:{i} | Total Hits:{counter4} | None: {counter0} |1Hit(in):{counter1} | 1Hit(out):{counter1} | 2Hit(in):{counter2} | 2Hit(out):{counter2a} | 3Hit(in): {counter3} " ) if verbose == True else None

    energy_array = np.array(energy_array_list, dtype = float)
    energy = energy_array[energy_array[:,0].argsort()]
    # Sort the array by Event ID.

    print(len(energy_array_list))
    if verbose == True:
        print("<<< Energy... Ended>>>>")
        print("<<< Generating *_L3.h5 file .... >>>")

    # Check data types first
    for i in range(10):
        if layer_list[i] in file.keys():
            id_type = file[f"{layer_list[i]}/data/event_id"].dtype
            time_type = file[f"{layer_list[i]}/data/system_time"].dtype
            pulse_type = file[f"{layer_list[i]}/vdata/channel_cm_sub"].dtype
            energy_type =   energy_array.dtype# file[f"{layer_list[i]}/vdata/calib_data"].dtype
            break

    with h5py.File(outfile, "w") as l3file:
        h5group = l3file.create_group('tracker')
        h5group.create_dataset(name='event_id', data=event_id, shape=(max_size,),
                               dtype=id_type, compression=comp)
        h5group.create_dataset(name='event_time', data=event_time, shape=(max_size,),
                               dtype=time_type, compression=comp)
        h5group.create_dataset(name='pulse_height', data=pulse_height, shape=(max_size, 10, 2, 192),
                               dtype=pulse_type, compression=comp)
        h5group.create_dataset(name='energy', data=energy, shape=energy.shape,
                               dtype=energy_type, compression=comp)

        # Attributes:
        # New Attributes
        l3file.attrs['trk_conv_l2tol3_ver'] = VERSION
        # Old Attributes
        temp_att_dict = dict(fname.attrs.items())  # save all the previous attributes.
        for keys in temp_att_dict:
            l3file.attrs[f'trk_{keys}'] = temp_att_dict[f'{keys}']

# def create_l3_old(fname, outfile, verbose=False, comp = True):
#     """Main function to read L2, select the fields and create L3 data.

#     Parameters
#     ----------
#     fname : String
#         L2 File
#     outfile : String
#         L3 File
#     verbose : Boolean
#         Flag to trigger the verbose on to print out information while the script is running. Default is False.

#     Returns
#     -------
#     l3file : File
#         L3 h5 file, ready to be appended to CUD with other subsystem.
#     """

#     #layer_list = ['layer00','layer01','layer02','layer03','layer04','layer05','layer06','layer07','layer08','layer09']
#     layer_list = [f"layer{n:02d}" for n in range(10)]

#     # Sanity Check
#     nsync = [file[f"{layer}/data/sync_index"].size for layer in layer_list if layer in file]
#     max_size = nsync[0]
#     # Sanity check...
#     assert all(np.array(nsync) == max_size),"Insanity Error: The Sync_Indices are not the Same"

#     max_size = max_size-1 #We are skipping the EVENTID=0 or sync_index=0


#     # Event Id and Event Time (shape = eventid)
#     # This is same for all layers due to sync_index
#     # We only need 1 layer but we are looping in case layer 0 is not plugged in the layer stack.
#     if verbose == True:
#         print("<<<< Event IDs and Event Time started .....>>>>>")

#     for i in range(10):
#         if layer_list[i] in file.keys():
#             sync_index = file[f"{layer_list[i]}/data/sync_index"][...]
#             evtdata = file[f"{layer_list[i]}/data/event_id"][...]
#             event_id = evtdata[sync_index[1:]]

#             evttime = file[f"{layer_list[i]}/data/system_time"][...]
#             event_time = evttime[sync_index[1:]]

#             if verbose == True:
#                 print(f" Layer :{i} done")
#             break

#     if verbose == True:
#         print(f"Event ID and Event Time completed ......")
#         print("<<<< Pulse Height.. Started >>>>>")

#     # Pulse Height (dim = nev, 10, 2, 192)

#     pulse_height = np.zeros((max_size,10,2,192))
#     for i in range(10):
#         if layer_list[i] in file.keys():

#             data = file[f"{layer_list[i]}/vdata/channel_cm_sub"][...]
#             sync_index = file[f"{layer_list[i]}/data/sync_index"][...]
#             sync_index = np.array(sync_index)

#             if verbose == True:
#                 print(f"Layer:{i} , Id Len:{len(sync_index)}")

#             pulse_height[:, i, :, :] = data[sync_index[1:], :, :]

#     if verbose == True:
#         print("<<<< Pulse Height... Ended >>>>>")
#         print("<<<< Energy... Started >>>>>")

#     # Energy (dim = n_ev, 10, 2, 192)
#     # Slightly tricky because, sync_index not applied previously and not all event ids are valid ecalib which
#     # are skipped.

#     a = 1
#     a_1 = 2

#     #*energy = np.zeros((max_size, 10, 2, 192))
#     energy_array = np.zeros((1, 5), dtype=float)

#     # extract the table data.
#     xtable, ytable, ztable = ecal.get_xyz_pos()
#     xtab = np.array(xtable, dtype='f2')
#     ytab = np.array(ytable, dtype='f2')
#     ztab = np.array(ztable, dtype='f2') # not used here.
#     for i in range(10):
#         if layer_list[i] in file.keys():
#             layer = i

#             nrg_data = file[f"{layer_list[i]}/vdata/nrg"][...]              # Grab nrg data
#             evtdata = file[f"{layer_list[i]}/data/event_id"][...]           # Grab event id
#             nrg_sync_index = file[f"{layer_list[i]}/data/sync_index"][...]  # grab sync_index

#             nrg_arr = nrg_data[nrg_sync_index[1:], :, :]
#             id_arr = evtdata[nrg_sync_index[1:]]

#             for j in range(len(id_arr)):
#                 cur_evt = nrg_arr[j, :, :]
#                 a_cnt = np.count_nonzero(cur_evt[0, :] > 0.0)  # side a count
#                 b_cnt = np.count_nonzero(cur_evt[1, :] > 0.0)  # side b count

#                 if a_cnt < 1 or b_cnt < 1:
#                     None
#                 elif a_cnt == 1 and b_cnt == 1:
#                     #cnt_1 += 1
#                     ret_val = one_hit_fnc(cur_evt,layer)
#                     if ret_val is not None:
#                         [xpos_val, ypos_val, zpos_val, nrg_val] = ret_val[0]
#                         temp_arr = [id_arr[j], xpos_val, ypos_val, zpos_val, nrg_val]
#                         energy_array = np.append(energy_array, [temp_arr], axis=0)

#                 elif (1 <= a_cnt <= 2) and (1 <= b_cnt <= 2):
#                     ret_val = two_hit_fnc(cur_evt, layer)
#                     if ret_val is not None:
#                         for k in range(len(ret_val)):
#                             [xpos_val, ypos_val, zpos_val, nrg_val] = ret_val[k]
#                             temp_arr = [id_arr[j], xpos_val, ypos_val, zpos_val, nrg_val]
#                             energy_array = np.append(energy_array, [temp_arr], axis=0)
#                 else:
#                     a_1+= 1

#         print(i,len(energy_array))
#                     # print(a_cnt, b_cnt)
#     energy = energy_array[1:]   # first is a zero array.

#     if verbose == True:
#         print("<<< Energy... Ended>>>>")
#         print("<<< Generating *_L3.h5 file .... >>>")

#     # Check data types first
#     for i in range(10):
#         if layer_list[i] in file.keys():
#             id_type = file[f"{layer_list[i]}/data/event_id"].dtype
#             time_type = file[f"{layer_list[i]}/data/system_time"].dtype
#             pulse_type = file[f"{layer_list[i]}/vdata/channel_cm_sub"].dtype
#             energy_type= "<f8" #file[f"{layer_list[i]}/vdata/calib_data"].dtype
#             break

#     with h5py.File(outfile, "w") as l3file:
#         h5group = l3file.create_group('tracker')
#         h5group.create_dataset(name='event_id', data=event_id, shape=(max_size,),
#                                dtype=id_type, compression = comp)
#         h5group.create_dataset(name='event_time', data=event_time, shape=(max_size,),
#                                dtype=time_type , compression = comp)
#         h5group.create_dataset(name='pulse_height', data=pulse_height, shape=(max_size, 10, 2, 192),
#                                dtype=pulse_type, compression = comp)
#         h5group.create_dataset(name='energy', data=energy, shape=energy.shape,
#                                dtype=energy_type, compression = comp)

#         #Attributes:
#         # New Attributes
#         l3file.attrs['trk_conv_l2tol3_ver'] = VERSION
#         # Old Attributes
#         temp_att_dict = dict(fname.attrs.items()) # save all the previous attributes.
#         for keys in temp_att_dict:
#             l3file.attrs[f'trk_{keys}'] = temp_att_dict[f'{keys}']

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Convert L2 to L3")
    parser.add_argument("-v", action='store_true', help=" Verbose flag to monitor progress or to debug")
    parser.add_argument("-u", action='store_true', help=" UnCompression. Default is Compression.")
    parser.add_argument("-N", type=int, help=" Define custom no. of Layers")
    parser.add_argument("file", help="L2 File path")

    args = parser.parse_args()
    
    if args.N:
        change_Nlayers_flag(True)
        print(f"No. of layers forcing to be: {args.N}")


    if args.v:
        verbose_flag = True
    else:
        verbose_flag = False

    if args.u:
        comp = False
    else:
        comp = True

    outfile = f"{args.file.split('L2.h5')[0]}L3.h5"
    if os.path.exists(outfile):
        os.remove(outfile)

    with h5py.File(args.file, "r") as file:
        create_l3(file, outfile, verbose=verbose_flag, comp = comp)  #The verbose_flag remains the variable.
