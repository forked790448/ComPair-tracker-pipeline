"""Generate a dummy ecal for 1to1 conversion.
"""

import os

def create_dummyfile():
    """Generate the dummy file.
    """
    
    txt_file = "tkr_dummy_ecal.dat"

    with open(txt_file, 'w') as file1: 

        # Replicating header
        file1.write("# Date created [UTC]: \n")
        file1.write("# Concatenated calibration files:\n")
        file1.write("# Files \n")
        file1.write("# Files \n")
        file1.write("# Energy calibration form: E = a1coef * PH + a2coef \n")
        file1.write("# FitFlag values: \n")
        file1.write("#        -3 - masked channel \n")
        file1.write("#        -2 - no em lines / no data \n")
        file1.write("#        -1 - fit failed \n")
        file1.write("#         1 - > 2em lines \n")
        file1.write("#         2 - = 2 em lines \n")
        file1.write("#         3 - only 1 em line \n")
        file1.write("#   \n")
        file1.write("#  Detector   LayerSide   Strip   FitFlag   a1coef   a1coeff_err   a2coef   a2coef_err \n")

        #for each datapoint
        for s in range(2):
            for l in range(10):
                for d in range(192):
                    tstr = f"    {l}    {s}    {d}    {1}    {1.0}    {0.0}    {0.0}    {0.0} \n"
                    file1.write(tstr)

if __name__ == "__main__":
    create_dummyfile()