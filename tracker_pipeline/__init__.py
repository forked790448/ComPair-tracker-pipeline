"""tracker_pipeline package for ComPair tracker pipeline

Package dedicated to reading, processing and analyzing ComPair tracker data.
Currently, the development has been done for raw (L0) data and working on L1.

"""
__version__ = "1.2"
