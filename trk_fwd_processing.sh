#!/bin/bash

# Check if directory path is provided as a command-line argument
if [ $# -eq 0 ]; then
  echo "Error: Please provide a directory path of the .tdata file or the folder containing it as the first command-line argument!"
  exit 1
fi

if [ $# -eq 1 ]; then
  echo "Error: Please provide a path to ecalib file as teh secondary argument!"
  exit 1
fi

dir_path=$1
ecal_file=$2
dir_ext=".tdata"

# Check if the path is a directory
if [ -d "$dir_path" ]; then
    # Check if the path ends with ".tdata"
    if [[ "$dir_path" == *".tdata" ]]; then
        echo "--- Single file processing ---"
        
        # need to check if the L1 already exists because the fast_raw2hdf doesnt check and hdf5 file has issues with it.
        l1_file="${dir_path%.tdata}_L1.h5"
        # echo "$l1_file"
        # echo "$dir_path"
        #remove the l1 file if it already exists.
        if [ -e "$l1_file" ]; then
            rm $l1_file
        fi
        echo "-- Running raw2hdf --"
        raw2hdf $dir_path
        echo "-- raw2hdf completed --"

        echo "-- Running L1 to L2 --"    
        python tracker_pipeline/conv_l1tol2.py $l1_file $ecal_file -v
        
        echo "-- Running L2 to L3 next --"
        l2_file="${l1_file%_L1.h5}_L2.h5"
        python tracker_pipeline/conv_l2tol3.py $l2_file -v
        echo "-- L3 file created. --"
    else
        
        subfolders=$(find "$dir_path" -type d -name "*.tdata" -print)
        if [ -z "$subfolders" ]; then
            echo "The path '$dir_path' is a bigger folder, but no subfolders with the '.tdata' extension were found."
        else
            echo "--- Processing multiple files ---"
            for folder in $subfolders; do
                # check the L1 
                echo "----- ** -New File- ** ----- "
                echo "-- Running raw2hdf... " 
                l1_file="${folder%.tdata}_L1.h5"
                #remove the l1 file if it already exists.
                if [ -e "$l1_file" ]; then
                    rm $l1_file
                fi
                raw2hdf $folder
                echo "-- Running L1 to L2 --"    
                python tracker_pipeline/conv_l1tol2.py $l1_file $ecal_file
                echo "-- Running L2 to L3 --"
                l2_file="${l1_file%_L1.h5}_L2.h5"
                python tracker_pipeline/conv_l2tol3.py $l2_file 
                echo "-- L3 file created moving to next--"
                echo "-- * ---- ** ---- * --"
                echo " "
                echo " "
            done
        fi
    fi
fi
