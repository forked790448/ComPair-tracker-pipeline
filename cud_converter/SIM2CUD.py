########################################################################################################
# |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
# |||                                                                                                  #
# ||| |||||||##############|||||||                                                                     #
# ||| |||                      |||#####||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
# ||| ||| I.) BACKGROUND & USE |||#####||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
# ||| |||                      |||#####||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
# ||| |||||||##############|||||||                                                                     #
# |||                                                                                                  #
# |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
########################################################################################################
#                                                                                                     
########################################################################################################
#  |||*| |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| |*|||  #
#  |||*| |||********************************************************************************||| |*|||  #
#  |||*| |||*||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||*||| |*|||  #
#  |||*| |||*|||                                                                        |||*||| |*|||  #
#  |||*| |||*||| To run this file, please use the following format in the command line: |||*||| |*|||  #
#  |||*| |||*|||                                                                        |||*||| |*|||  #
#  |||*| |||*|||                                                                        |||*||| |*|||  #
#                                                                                                      #
#                              python3 SIM2CUD.py (Path to .sim file)                                  #<-------------------------------------------------------
#                                                                                                      #
#  |||*| |||*|||                                                                        |||*||| |*|||  #
#  |||*| |||*|||                                                                        |||*||| |*|||  #
#  |||*| |||*||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||*||| |*|||  #
#  |||*| |||********************************************************************************||| |*|||  #
#  |||*| |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| |*|||  #
########################################################################################################

########################################################################################################
#                                             ##
# I.1) REVERSE PIPELINE (Cosima -> Subsytem)  ##
#                                             ##
################################################

# This parser disects a Cosima .sim file to pull the following event properties:
#	Event ID			| Total simulated event number 
#	Hit ID				| Relative hit number per event
#	Photon ID			| Total number of photons emitted before the hit detection
#	Time (s) of Hit			| Time since the start of the simulation
#	Energy (keV) of Hit		| Energy deposited by the hit in keV
#	Position (X, Y, Z) of Hit	| Detected position of hit (units relative to geomega model) 

# The data is then written to a text file and an HDF5 file (default = Cosima.txt/h5)
# You may also include "-t (Name of text file)" to specify the text file output
# The following properties are saved as datasets to the HDF5 file under the group (Hit Data):
#	Event ID	Time (s)	Energy (keV)	Position (X, Y, Z)

# A histogram of the hit energies is generated in a new window through running this script.

# The established HDF5 output is categorized into groups via subsystem (Event Data):
#       TKR       CsI       CZT       ACD (Not Yet Implemented) <-------------------------------------------------------------------------

# Each subsytem group will have corresponding subgroups corresponding to energy, position, etc. of hits
# in the respective subsytem in the CUD format.

########################################################################################################



#------------------------------------------------------------------------------------------------------#
#||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||#
#||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||#
#||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||#
#------------------------------------------------------------------------------------------------------#




########################################################################################################
# |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
# |||                                                                                                  #
# ||| |||||||##############|||||||                                                                     #
# ||| |||                      |||#####||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
# ||| ||| II.) IMPORT PACKAGES |||#####||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
# ||| |||                      |||#####||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
# ||| |||||||##############|||||||                                                                     #
# |||                                                                                                  #
# |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
########################################################################################################

import argparse
import numpy as np
import matplotlib.pyplot as plt
import h5py
import time
#import datetime
#import tzinfo

# Other Possibly Useful Packages

#import pickle
#from array import array
#import seaborn as sns
#import sys # system-specific parameters/functions
#import csv # comma-separated values
#import os # operating-system-dependant utilities




#------------------------------------------------------------------------------------------------------#
#||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||#
#||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||#
#||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||#
#------------------------------------------------------------------------------------------------------#




########################################################################################################
# |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
# |||                                                                                                  #
# ||| |||||||###############|||||||                                                                    #
# ||| |||                       |||#####|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
# ||| ||| III.) FILES & OPTIONS |||#####|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
# ||| |||                       |||#####|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
# ||| |||||||###############|||||||                                                                    #
# |||                                                                                                  #
# |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
########################################################################################################

# Use argparse to call a .sim file as an input (see "||| I.) BACKGROUND & USE |||")
# Define optional command line inputs for specification of text file
# Assign default text output to Cosima.txt <-------------------------------------------------------------------------------------------------

def parseargs():

    parser = argparse.ArgumentParser(description = "Parse the .sim file")
    parser.add_argument("infile", help="Path to input .sim file", type=argparse.FileType('r'))
    parser.add_argument("-t", "--text", help="Path to text file output", default = 'Cosima.txt')
    args = parser.parse_args()
    return args




#------------------------------------------------------------------------------------------------------#
#||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||#
#||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||#
#||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||#
#------------------------------------------------------------------------------------------------------#




########################################################################################################
# |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
# |||                                                                                                  #
# ||| |||||||##################|||||||                                                                 #
# ||| |||                          |||#####||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
# ||| ||| IV.) GEOMETRIC VARIABLES |||#####||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
# ||| |||                          |||#####||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
# ||| |||||||##################|||||||                                                                 #
# |||                                                                                                  #
# |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
########################################################################################################

# IV.1) TKR

TKR_strip_pitch = 0.051 # 510 microns
TKR_width = 192 * TKR_strip_pitch # 192 strips of constant pitch
TKR_edge = TKR_width/2 # DSSD is centered at 0, thus the edges are equivalent to half of the width
TKR_strip_centroid = TKR_edge - (TKR_strip_pitch)/2 # Gives the center of strip 191
TKR_layer_spacing = 1.9 # z spacing between layers
TKR_top_layer = 8.55 # Global z position of layer 0 of the TKR (centroid)

# IV.2) CsI

CsI_top_centroid = -20.641 # Global z position of the top layer of the CsI (centroid)
CsI_layer_spacing = 1.7 # z spacing between layers
CsI_x_centroid = 4.25
CsI_y_centroid = 4.25

# IV.3 CZT

CZT_edge = 5
CZT_edge_midpoint = 3.75
CZT_block_spacing = 2.5
CZT_bar_edge = 1.25
CZT_bar_spacing = 0.625




#------------------------------------------------------------------------------------------------------#
#||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||#
#||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||#
#||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||#
#------------------------------------------------------------------------------------------------------#




########################################################################################################
# |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
# |||                                                                                                  #
# ||| |||||||#######|||||||                                                                            #
# ||| |||               |||#####|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
# ||| ||| V.) FUNCTIONS |||#####|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
# ||| |||               |||#####|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
# ||| |||||||#######|||||||                                                                            #
# |||                                                                                                  #
# |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
########################################################################################################

########################################################################################################
#                                             ##
# V.1) PINPOINT FUNCTIONS                     ##
#                                             ##
################################################

def TKR_pinpoint(xpos, ypos, zpos):

    '''Converts the (x,y,z) position of hits in silicon to corresponding
    strip numbers and layer number.

    Keyword Arguments:
    xpos = X position of hit in DSSD tracker
    ypos = Y position of hit in DSSD tracker
    zpos = Z layer of hit in DSSD tracker

    Returns:
    xstrip = x-oriented DSSD strip ranging from 0:191
    ystrip = y-oriented DSSD strip ranging from 0:191 
    zstrip = DSSD layer ranging from 0:9

    Converts the x and y positions of the hits in the TKR (HTsim 1) to a
    corresponding x-strip # and y-strip #, both ranging from 0:191. The
    z-position is then converted to a layer ranging from 0:9, with the top
    layer being layer 0. The conversion equations are obtained via the
    TKR geometry as follows:

    The TKR consists of 10 layers of 192 total strips with pitch 
    p = 510 microns, thus it has an effective width of W = 192*p. This 
    geometry is centered at zero, and thus ranges in x and y from 
    -W/2:+W/2, with strip "edges" defined in steps of p microns. The strip 
    centers, which correspond to the Cosima centroid output, are thus 
    defined by (-W+p)/2:(W-p)/2 in steps of p microns. Every position is 
    thus by nature divisible by p, and thus adding an initial offset of the 
    furthest-from-origin centeryields a 0-count of corresponding strip number 
    ranging from 0:191. Please note that the ystrip equation is modified 
    slightly to account for the orientation of the y-strips (strip 0 
    corresponds to 4.8705 as opposed to strip 191).

    The corresponding layers range from -4.5:4.5 in steps of 1, and thus
    the ten total layers are defined with the top layer corresponding to
    the zeroeth layer by subtracting the 4.5 offset.

    '''

    xstrip = int((float(xpos)+TKR_strip_centroid)/TKR_strip_pitch)
    ystrip = int(-(float(ypos)-TKR_strip_centroid)/TKR_strip_pitch)
    zlayer = int(abs((float(zpos)-TKR_top_layer)/TKR_layer_spacing))
    #print('X-strip # ' + str(xstrip) + '; Y-strip # ' + str(ystrip) +
    #      '; Z-layer # ' + str(zlayer))
    return xstrip, ystrip, zlayer
###############################################################################
    
def CsI_pinpoint(xpos, ypos, zpos):

    '''Converts the (x,y,z) position of hits in CsI to corresponding
    log number and layer number.

    Keyword Arguments:
    xpos = X position of hit in CsI
    ypos = Y position of hit in CsI
    zpos = Z layer of hit in CsI

    Returns:
    CsIlog = log number ranging from 0:5
    CsIlayer = layer number ranging from 0:4

    Converts the x and y positions of the hits in the CsI (HTsim 2) to a
    corresponding x or y log #, both ranging from 0:5. The z-position is
    then converted to a layer ranging from 0:5, with the top layer being 
    layer 0. The conversion equations are obtained via the CsI geometry 
    as follows:

    The corresponding layers exist in hodoscopic fasion, with the top layer
    defined as layer 0, and oriented in x. The five layers (centroid) range 
    from -20.641:-27.441 in steps of 1.7 with layer 1 oriented in y, layer 2
    oriented in x, and so on.

    Each layer contains 6 logs, which store pos_downlog info based on the
    orientation of the log (logs oriented in x store x info). The negative-
    most log in each orientation corresponds to log 0, and the position of
    each subsequent log is given by the addition of the width of a CsI log
    to the known location of the centroid of log 0.

    '''

    CsIlayer = int(abs(float(zpos)-CsI_top_centroid)/CsI_layer_spacing)
    if CsIlayer % 2 ==0:
        CsIlog = int((float(xpos)+CsI_x_centroid)/CsI_layer_spacing)
    else:
        CsIlog = int((float(ypos)+CsI_y_centroid)/CsI_layer_spacing)   
    return CsIlayer, CsIlog
###############################################################################

def CZT_pinpoint(xpos, ypos, zpos):

    '''Converts the (x,y,z) position of hits in CZT to corresponding
    block number and bar number.

    Keyword Arguments:
    xpos = X position of hit in CZT
    ypos = Y position of hit in CZT
    zpos = Z layer of hit in CZT

    Returns:
    CZT_Block = Corresponding block (0:15) of detected hit
    CZT_Bar = Returns bar (0:15) of detected hit inside of CZT_Block
    CZT_Xrel = The x-position of the hit relative to the center of CZT_Bar
    CZT_Yrel = The y-position of the hit relative to the center of CZT_Bar

    Converts the x and y positions of the hits in the CZT (HTsim 7) to a
    corresponding x or y block #, both ranging from 0:3 (there are 16 total
    blocks ranging from 0:15). Block 0 is defined as the negative-most block in 
    x, and the most positive in y. Block 1 is defined by increasing x for a set y,
    likewise up to block 3. Block 4 is thus defined as the negative-most in
    x and the second-most positive in y. This continues up to block 16, which
    is the most positive in x and most negative in y.

    The x and y position relative to the center of the correpsonding block is
    then converted to a bar ranging from 0:15, defined similarly (256 total bars).
    The (x,y) position of the center of this bar is then calculated, and the position
    relative to this center is stored as CZT_(X,Y)rel.

    '''

    xpos = float(xpos)
    ypos = float(ypos)
    zpos = float(zpos)

    CZT_XBlock=int((xpos+CZT_edge)/CZT_block_spacing) # Defines a count for X-blocks (left=0, right=3)
    CZT_YBlock=int((CZT_edge-ypos)/CZT_block_spacing) # Defines a count for Y-blocks (top=0, bottom=3)
    CZT_Block=int(float(CZT_XBlock)+(4*float(CZT_YBlock))) # Defines block number in 4x4 grid (0:15, row 1 is block 0:3, row 2 is 4:7 etc)
    
    CZT_XBlock_mid = CZT_block_spacing*CZT_XBlock - CZT_edge_midpoint # Defines the x position of the middle of the detected block
    CZT_YBlock_mid = CZT_edge_midpoint-CZT_block_spacing*CZT_YBlock # Defines the y position of the middle of the detected block

    CZT_XBar = int((xpos-(CZT_XBlock_mid-CZT_bar_edge))/CZT_bar_spacing) # Defines a count for x bars within the block (left=0, right=3)
    CZT_YBar = int(((CZT_YBlock_mid+CZT_bar_edge)-ypos)/CZT_bar_spacing) # Defines a count for y bars within the block (top=0, bottom=3)
    CZT_Bar = CZT_XBar + 4*CZT_YBar

#    CZT_Xrel = xpos - CZT_XBlock_mid # Returns x position relative to the x-center of the block
#    CZT_Yrel = ypos - CZT_YBlock_mid # Returns y position relative to the y-center of the block

    CZT_XBar_mid = CZT_XBlock_mid - CZT_bar_edge + CZT_bar_spacing/2 + CZT_bar_spacing*(CZT_XBar)
    CZT_YBar_mid = CZT_YBlock_mid + CZT_bar_edge - CZT_bar_spacing/2 - CZT_bar_spacing*(CZT_YBar)

    CZT_Xrel = xpos - CZT_XBar_mid # Returns x position relative to the x-center of the bar
    CZT_Yrel = ypos - CZT_YBar_mid # Returns y position relative to the y-center of the bar   
    return CZT_Block, CZT_Bar, CZT_Xrel, CZT_Yrel

###############################################################################

###############################################################################
###############################################################################

def cli():




    #--------------------------------------------------------------------------------------------------#
    #||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||#
    #||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||#
    #||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||#
    #--------------------------------------------------------------------------------------------------#




    ####################################################################################################
    # |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
    # |||                                                                                              #
    # ||| |||||||##########|||||||                                                                     #
    # ||| |||                  |||#####||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
    # ||| ||| VI.) DEFINITIONS |||#####||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
    # ||| |||                  |||#####||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
    # ||| |||||||##########|||||||                                                                     #
    # |||                                                                                              #
    # |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
    ####################################################################################################
 
    # Define output files:

    args = parseargs()
    filename = args.infile

    outf1 = open(args.text, 'w') # default text file output is "Cosima.txt"
    outf2 = h5py.File('Cosima.h5', 'w') # h5 output is hardcoded as "Cosima.h5" <---------------------------------------------------

    # Initialize counts:

    count = 0        #The corresponding hit number for a specified event
    SEcount = 0      #The corresponding event number (used for by-line analysis)
    SEtotal = 0      #The total number of events in the sim file
    HTcount = 0      #The total number of hits for all events
    TKR_count = 0   #The total number of hits in Silicon DSSD
    CsI_count = 0
    CZT_count = 0

    # Define arrays:

    ID = []          #Array of event IDs
    time = []        #Time of the event (roughly the time of the corresponding hits)
    energy = []      #Energy of each hit
    positionx = []   #Corresponding (x,y,z) position of a hit
    positiony = []
    positionz = []

    #Define h5 groups:

    Hits = outf2.create_group('Hits')
    Events = outf2.create_group('Events')




    #--------------------------------------------------------------------------------------------------#
    #||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||#
    #||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||#
    #||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||#
    #--------------------------------------------------------------------------------------------------#




    ####################################################################################################
    # |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
    # |||                                                                                              #
    # ||| |||||||##############|||||||                                                                 #
    # ||| |||                      |||#####||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
    # ||| ||| VII.) WRITE TO FILES |||#####||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
    # ||| |||                      |||#####||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
    # ||| |||||||##############|||||||                                                                 #
    # |||                                                                                              #
    # |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
    ####################################################################################################

########################################################################################################
#                                               ##
# VII.1) ESTABLISH HDF5 DATASET ARRAYS          ##
#                                               ##
##################################################


    # Open sim file and count the total number of events
    try:
        with open(filename.name) as f:
            lines=f.readlines() # Pull all lines from the simulation file
            for line in lines: # Parse through each line
                if 'SE' in line: # Search for the start of an event
                    SEtotal = SEtotal + 1
    except: print('Could not count events')

    # Define H5 Dataset Arrays:
    TKR_Energy = np.zeros([SEtotal, 10, 2, 192],dtype=float) # 10 layers, 2 orientations, 192 strips
    CsI_Energy = np.zeros([SEtotal,5,6],dtype=float) # 5 layers, 6 logs
    CsI_pos_down_log = np.zeros([SEtotal,5,6],dtype=float) # stores x/y info dependent on orientation
    CZT_Energy = np.zeros([SEtotal,16,16],dtype=float) # 16 bars, 16 blocks
    CZT_Position = np.zeros([SEtotal,16,16,3],dtype=float) # stores position relative to center of bar
 
    UTC=np.zeros([SEtotal], dtype=int) # One time per event <----------------------------------------------------------------

########################################################################################################
#                                             ##
# VII.2) PARSING THROUGH SIM FILE             ##
#                                             ##
################################################


   
    try:
        print(filename.name)
        with open(filename.name) as f:
            lines = f.readlines()
            for line in lines:
                if 'SE' in line: # Search for the start of an event
                    outf1.write('\n')
                    count = 0
                    SEcount = SEcount + 1
                    outf1.write('SE ' + str(SEcount) + '\n')

                if 'ID' in line:
                    idsplit = line.split()
                    hitid = idsplit[1]
                    photonid = idsplit[2]
                    # ID.append(float(hitid))
                    # outf1.write(str(idsplit[0])  + '=' + str(hitid) + '\n')
                    # outf1.write('PHOTON ID = ' + str(photonid) + '\n'

                if 'TI' in line: # Search for the time associated with the start of an event
                    ti = line.split()
                    # time.append(float(ti[1]))
                    outf1.write('HT Time (s) = ' + str(ti[1]) + '\n')
                    UTC[(int(SEcount)-1)] = int(float(ti[1])*1e9)

                if 'HT' in line: # Search for hits inside of an event
                    data = line.split(';')
                    energy.append(float(data[4]))
                    count = count + 1
                    HTcount = HTcount + 1 #Internal hit counter per event
                    # Write to text file
                    outf1.write('HT# = ' + str(count) + '\n')
                    outf1.write('HT DETECTOR ID = ' + str(data[0]) + '\n')
                    outf1.write('HT POSITION (X, Y, Z) = (' + str(data[1])
                                + ', ' + str(data[2]) + ', ' + str(data[3])
                                + ')' + '\n')
                    outf1.write('HT Energy (keV) = ' + str(data[4]) + '\n')
                    # Write to datasets
                    positionx.append(float(data[1]))
                    positiony.append(float(data[2]))
                    positionz.append(float(data[3]))
                    ID.append(int(hitid))
                    time.append(float(ti[1]))

                if 'HTsim 1' in line: #Search for hits in TKR
                    TKR_count = TKR_count + 1
                    (xstrip,ystrip,zlayer)= TKR_pinpoint(data[1], data[2], data[3])
                    TKR_Energy[(int(SEcount)-1),int(zlayer),0,int(xstrip)]+=float(data[4])
                    TKR_Energy[(int(SEcount)-1),int(zlayer),1,int(ystrip)]+=float(data[4])
                    outf1.write('TKR' + '\n')
                    outf1.write('(X Strip = ' + str(xstrip) + '; Y Strip = ' + str(ystrip) 
                    + '; Layer = ' + str(zlayer) + ')' + '\n')

                if 'HTsim 2' in line: # Search for hits in CsI
                    CsI_count = CsI_count + 1
                    (CsIlayer,CsIlog)= CsI_pinpoint(data[1],data[2],data[3])
                    if CsIlayer % 2 ==0: #Store y data if layer is even
                        CsI_Energy[(int(SEcount)-1),int(CsIlayer),int(CsIlog)]+=float(data[4])
                        CsI_pos_down_log[(int(SEcount)-1),int(CsIlayer),int(CsIlog)]+=float(data[2])
                        outf1.write('CsI' + '\n')
                        outf1.write('Layer = ' + str(CsIlayer) + '; Log = ' + str(CsIlog) 
                        + '; Y Down Log = ' + str(data[2]) + '\n')
                    else: # Store x data for odd layer
                        CsI_Energy[(int(SEcount)-1),int(CsIlayer),int(CsIlog)]+=float(data[4])
                        CsI_pos_down_log[(int(SEcount)-1),int(CsIlayer),int(CsIlog)]+=float(data[1])
                        outf1.write('CsI' + '\n')
                        outf1.write('Layer = ' + str(CsIlayer) + '; Log = ' + str(CsIlog) 
                        + '; X Down Log' + str(data[1]) + '\n')

                if 'HTsim 7' in line: # Search for hits in CZT
                    CZT_count=CZT_count + 1
                    (CZT_Block,CZT_Bar,CZT_Xrel,CZT_Yrel)=CZT_pinpoint(data[1],data[2],data[3])
                    CZT_Energy[(int(SEcount)-1),int(CZT_Block),int(CZT_Bar)]+=float(data[4])
                    CZT_Position[(int(SEcount)-1),int(CZT_Block),int(CZT_Bar),0]=float(CZT_Xrel)
                    CZT_Position[(int(SEcount)-1),int(CZT_Block),int(CZT_Bar),1]=float(CZT_Yrel)
                    CZT_Position[(int(SEcount)-1),int(CZT_Block),int(CZT_Bar),2]=float(data[3])
                    outf1.write('CZT' +'\n')
                    outf1.write('(Block = ' + str(CZT_Block) + '; Bar = ' + str(CZT_Bar) 
                    + '; Xrel = ' + str(CZT_Xrel) + '; Yrel = ' + str(CZT_Yrel) + ')' +  '\n')

    except FileNotFoundError:
        print('The file could not be opened.')

    outf1.write('Total HT count = ' + str(HTcount))
    position = np.dstack((positionx, positiony, positionz))

########################################################################################################
#                                             ##
# VII.3) ASSIGN DATASETS TO HDF5 GROUPS       ##
#                                             ##
################################################



    Hits.create_dataset('EventID', data=ID)
    Hits.create_dataset('Time(s)', data=time)
    Hits.create_dataset('Energy(keV)', data=energy)
    Hits.create_dataset('Position(X,Y,Z)', data=position)

    TKR=Events.create_group('TKR')
    TKR.create_dataset('Energy', data=TKR_Energy)

    CsI=Events.create_group('CsI')
    CsI.create_dataset('Energy',data=CsI_Energy)
    CsI.create_dataset('PosDownLog',data=CsI_pos_down_log)

    CZT=Events.create_group('CZT')
    CZT.create_dataset('Energy', data=CZT_Energy)
    CZT.create_dataset('Position',data=CZT_Position)

    Summary = Events.create_group('Summary')
    UTC=Summary.create_dataset('UTC', data=UTC)

    #--------------------------------------------------------------------------------------------------#
    #||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||#
    #||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||#
    #||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||#
    #--------------------------------------------------------------------------------------------------#

  
    outf2.close()

if __name__ == '__main__': cli()
