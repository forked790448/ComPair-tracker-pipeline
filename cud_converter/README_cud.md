# Compair Unified Data (CUD) Converters #

### SIM2CUD: (python3 SIM2CUD (path to .sim file)) ###

Generates a .h5 file in the CUD format by parsing through a Cosima .sim file to generate h5 datasets. A text file is also created, starting at a base count of 1 rather than 0 (Event 100 in the .sim file would
correspond to event 99 in the .h5 file but would still correspond to event 100 in the .txt file.

### CUD2RR: (python3 CUD2RR (path to .h5 file)) ###

Generates a revan-readable .evta file by converting CUD-level event data to hit-sorted data specific to MEGALib interpretation.

**While the converters are functional for the aforementioned purposes, the converters do not yet account for the following features:**
        
* An error was detected in the geometric functions for the CZT. This error was resolved and tested as of 07/13/21, however not more testing is required to be sure that the CZT code functions properly. Any unforseen errors in the converter may be due to this, so please take note of this.

* The CZT position array in both the CUD2RR and SIM2CUD currenlt stores and assumes from the CUD the global z-position rather than the z position relative to the center of the CZT. The x and y positions are both properly stored as relative positions to the center of the respective bar. The CZT is centered at -15.06, so to convert from global to local add 15.06 to data[3] (z-position) in the SIM2CUD converter, and subtract -15.06 from the z-position read out from the CZT_pos_arr in the CUD2RR converter. CHECK THAT THIS VALUE AND DATA ELEMENT ARE CORRECT BEFORE DOING SO (SEE ComPairBase.geo.setup file for reference in ComPairBeamTest geometry git location)

* Please be certain that all ComPair axes/geometry orientations match with the CUD descriptions as listed in the converters (ex: CsI log 1 layer 1 is consistent with the code)

* The current output files are hardcoded as Cosima.txt, Cosima.h5, and Cosima.evta. Manipulation of the parseargs code is needed to allow for the output of specific names files.

* A .sim file converted to a .h5 and back to a .evta file reconstructs more events than the original .sim file. This is likely due to charge sharing (see below).

* Charge sharing in the tracker is as of yet unresolved and discarded. This would appear in the CUD2RR converter, and one possible solution would be to make use of summing energies from adjacent strips.
  A possible method of doing so is included in the comments of the tkr function, and currently is not implemented due to an error of having multiple hits of the same summed energy.

* A similar problem to that of charge sharing arises from hits in adjacent strips in a TKR DSSD layer under the same event





---
> **For any and all questions or concerns please contact Kavic Kumar:** kavicrk18@gmail.com 


