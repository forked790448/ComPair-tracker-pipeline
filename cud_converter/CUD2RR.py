########################################################################################################
# |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
# |||                                                                                                  #
# ||| |||||||##############|||||||                                                                     #
# ||| |||                      |||#####||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
# ||| ||| I.) BACKGROUND & USE |||#####||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
# ||| |||                      |||#####||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
# ||| |||||||##############|||||||                                                                     #
# |||                                                                                                  #
# |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
########################################################################################################
#                                                                                                     
########################################################################################################
#  |||*| |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| |*|||  #
#  |||*| |||********************************************************************************||| |*|||  #
#  |||*| |||*||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||*||| |*|||  #
#  |||*| |||*|||                                                                        |||*||| |*|||  #
#  |||*| |||*||| To run this file, please use the following format in the command line: |||*||| |*|||  #
#  |||*| |||*|||                                                                        |||*||| |*|||  #
#  |||*| |||*|||                                                                        |||*||| |*|||  #
#                                                                                                      #
#                              python3 CUD2RR.py (Path to .h5 file)                                    #<-------------------------------------------------------
#                                                                                                      #
#  |||*| |||*|||                                                                        |||*||| |*|||  #
#  |||*| |||*|||                                                                        |||*||| |*|||  #
#  |||*| |||*||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||*||| |*|||  #
#  |||*| |||********************************************************************************||| |*|||  #
#  |||*| |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| |*|||  #
########################################################################################################

########################################################################################################
#                                              ##
# I.1) FORWARD PIPELINE (CUD -> Revan Readable ##
#                                              ##
#################################################

# This parser disects a CUD .h5 file to pull the following properties:
#       Event ID                        | Total simulated event number 
#       Hit ID                          | Relative hit number per event
#       Time (s) of Hit                 | Time since the start of the simulation
#       Energy (keV) of Hit             | Energy deposited by the hit in keV
#       Position (X, Y, Z) of Hit       | Detected position of hit (units relative to geomega model) 

# The data is then written to a revan-readable .evta file  (default = Cosima.evta)

# The established HDF5 output is categorized into groups via subsystem (Event Data):
#       TKR       CsI       CZT       ACD (Not Yet Implemented) <-------------------------------------------------------------------------

# Each subsytem group will have corresponding datasets corresponding to energy, position, etc. of hits
# in the respective subsytem in the CUD format. Each data array will contain inherent geometric
# information in the dimensionality that may be used to derive the position of hits.

########################################################################################################



#------------------------------------------------------------------------------------------------------#
#||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||#
#||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||#
#||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||#
#------------------------------------------------------------------------------------------------------#

# Import Packages
#########################################################################################################

import numpy as np
import h5py
import time
#import tzinfo
import argparse
import matplotlib.pyplot as plt

###########################################################################################################
# Arguments

def parseargs():
    # Default output set to Cosima.evta
    parser = argparse.ArgumentParser(description = "Parse the .sim file")
    parser.add_argument("infile", help="Path to input .sim file", type=argparse.FileType('r'))
    parser.add_argument("-e", "--evta", help="Path to text file output", default = 'Cosima.evta')
    #parser.add_argument("-h", "--hdf5", help="Path to hdf5 file output")
    args = parser.parse_args()
    return args

############################################################################################################
# Geometric Variables

# IV.1) TKR

TKR_strip_pitch = 0.051 # 510 microns
TKR_width = 192 * TKR_strip_pitch # 192 strips of constant pitch
TKR_edge = TKR_width/2 # DSSD is centered at 0, thus the edges are equivalent to half of the width
TKR_strip_centroid = TKR_edge - (TKR_strip_pitch)/2 # Gives the center of strip 191
TKR_layer_spacing = 1.9 # z spacing between layers
TKR_top_layer = 8.55 # Global z position of layer 0 of the TKR (centroid)

# IV.2) CsI

CsI_top_centroid = -20.641 # Global z position of the top layer of the CsI (centroid)
CsI_layer_spacing = 1.7 # z spacing between layers
CsI_x_centroid = 4.25
CsI_y_centroid = 4.25

# IV.3 CZT

CZT_edge = 5 # CZT is 10x10, edge is at 5 (center of CZT = 0)
CZT_edge_midpoint = 3.75 # The center of the blocks nearest the edge
CZT_block_spacing = 2.5 # Each block is 2.5 wide
CZT_bar_edge = 1.25 # Each block has a width of 2.5; center at 0 -> ranges from -1.25:1.25
CZT_bar_spacing = 0.625 # Each bar is 0.625 wide
CZT_bar_edge_mid = 0.625 # The center of the bars nearest the edge of the block




###############################################################################
# Functions
###############################################################################




# Define functions for ach subsystem that use the datasets to return:
# array of energies, array of xpos, array of ypos, and array of zpos

#get_tracker_hits(tkr_ene_arr[i_evt, :, :, :])

#get_csi_hits(csi_ene_arr[i_evt, ...], positions)

#get_czt_hits(czt_ene_arr[i_evt, ...], positions)

#each returns
#    (array of energies, array of pos_x, array of pos_y, array of pos_z)

def get_tracker_hits(tkr_ene_arr):

    '''For a single event, this function eparates the TKR energy array into
    energy and position arrays for encompassing all hits in the TKR for the
    event.

    Keyword Arguments:
    tkr_ene_arr: Event-sorted energy array in h5 format ([10, 2, 192])
   
    Returns:    
    hit_tkr_(ene, xpos, ypos, zpos) = array of hit info for the event

    Steps through a TKR layer, then selects either x or y strips and
    looks at the information from each strip for the selected event.
    If the element is above the energy threshold the energy is appended
    to the hit_tkr_ene array. The (x,y,z) position is determined
    geometrically and stored in their respective arrays. The x info is
    read for orientation 0 and y information is read for orientation 1.     

    '''

    hit_tkr_ene = []
    hit_tkr_xpos = []
    hit_tkr_ypos = []
    hit_tkr_zpos = []
    [a,b,c] = tkr_ene_arr.shape
    for layer in np.arange(a):
        temp_ene = []
        temp_x = []    
        temp_y = []
        temp_z = []
        for orientation in np.arange(b):
            for strip in np.arange(c):
                if tkr_ene_arr[layer,orientation,strip] > 0:
                    ene = tkr_ene_arr[layer,orientation,strip]
                    if orientation == 0:
                        xpos = float(strip*TKR_strip_pitch - TKR_strip_centroid)
                        ypos = 0
                    if orientation == 1:
                        xpos = 0
                        ypos = float(-strip*TKR_strip_pitch + TKR_strip_centroid)
                    zpos = float(-layer*TKR_layer_spacing + TKR_top_layer)
                    temp_ene.append(float(ene))
                    temp_x.append(float(xpos))
                    temp_y.append(float(ypos))
                    temp_z.append(float(zpos))
        if len(temp_ene) == 0:
            continue
        if len(temp_ene)%2 == 1:
            print('Skipped layered event data due to odd number of strip hits')
            continue
        idx_sort = np.argsort(temp_ene)
        temp_ene = np.array(temp_ene)[idx_sort]
        
        temp_x = np.array(temp_x)[idx_sort]
        temp_y = np.array(temp_y)[idx_sort]
        temp_z = np.array(temp_z)[idx_sort]

        temp_x = temp_x[temp_x!=0]
        temp_y = temp_y[temp_y!=0]    
        temp_z = temp_z[::2]
        temp_ene = temp_ene[::2]
        hit_tkr_ene.extend(temp_ene.tolist())
        hit_tkr_xpos.extend(temp_x.tolist())
        hit_tkr_ypos.extend(temp_y.tolist())
        hit_tkr_zpos.extend(temp_z.tolist())

    return np.array(hit_tkr_ene), np.array(hit_tkr_xpos), np.array(hit_tkr_ypos), np.array(hit_tkr_zpos)

def get_csi_hits(csi_ene_arr, csi_pos_arr):

    '''For a single event, this function separates the CsI energy array into
    energy and position arrays for encompassing all hits in the CsI for the
    event. Information is then read from the position array to overwrite the
    previous corresponding geometric position information.

    Keyword Arguments:
    csi_ene_arr: Event-sorted energy array in h5 format ([5, 6])
    csi_pos_arr: Event-sorted energy array in h5 format ([5, 6])
   
    Returns:    
    hit_csi_(ene, xpos, ypos, zpos) = array of hit info for the event

    Steps through a CsI layer, then looks at the information from each log for 
    the selected event. If the element is above the energy threshold the energy 
    is appended to the hit_csi_ene array. The (x,y,z) position is determined
    geometrically and stored in their respective arrays. The CsI holds 1-D
    position information in a separate array corresponding to the lengthwise
    position with respect to the center of the log. Since the logs are oriented
    hodoscopically with the top layer in x, y information is stored for even (x)
    layers, and y infomation is stored for odd (y) layers. This information
    overwrites previously obtained geometric information.     

    '''

    hit_csi_ene = []
    hit_csi_xpos = []
    hit_csi_ypos = []
    hit_csi_zpos = []
    [a,b] = csi_ene_arr.shape
    for CsIlayer in np.arange(a):
        for CsIlog in np.arange(b):
            if csi_ene_arr[CsIlayer, CsIlog] > 0:
                ene = csi_ene_arr[CsIlayer, CsIlog]
                xpos = CsIlog*CsI_layer_spacing - CsI_x_centroid
                ypos = CsIlog*CsI_layer_spacing - CsI_y_centroid
                zpos = -CsIlayer*CsI_layer_spacing + CsI_top_centroid
                if CsIlayer % 2 ==0:
                    ypos = csi_pos_arr[CsIlayer, CsIlog]
                else:
                    xpos=csi_pos_arr[CsIlayer, CsIlog]
                hit_csi_ene.append(float(ene))
                hit_csi_xpos.append(float(xpos))
                hit_csi_ypos.append(float(ypos))
                hit_csi_zpos.append(float(zpos))
    return np.array(hit_csi_ene), np.array(hit_csi_xpos), np.array(hit_csi_ypos), np.array(hit_csi_zpos)

def get_czt_hits(czt_ene_arr, czt_pos_arr):

    '''For a single event, this function filters the CZT energy array into
    an energy array for encompassing all hits in the CZT for the event. 
    Information is then read from the position array and manipulated through
    geometric functions to obtain the coordinate position.

    Keyword Arguments:
    czt_ene_arr: Event-sorted energy array in h5 format ([16, 16])
    czt_pos_arr: Event-sorted energy array in h5 format ([16, 16, 3])
   
    Returns:    
    hit_czt_(ene, xpos, ypos, zpos) = array of hit info for the event

    Steps through a CZT block, then looks at the information from each bar for 
    the selected event. If the element is above the energy threshold the energy 
    is appended to the hit_czt_ene array. The (x,y,z) position is read from the
    position array, which holds position information relative to the center of
    the bar. This position information is used to obtain the global coordinate
    position geometrically.

    '''


    hit_czt_ene = []
    hit_czt_xpos = []
    hit_czt_ypos = []
    hit_czt_zpos = []
    [a,b] = czt_ene_arr.shape
    for block in np.arange(a):
        for bar in np.arange(b):
            if czt_ene_arr[block, bar] > 0:
                ene = czt_ene_arr[block, bar]

                CZT_YBar = int(bar/4)
                CZT_XBar = bar - 4*CZT_YBar
           
                CZT_YBlock = int(block/4)
                CZT_XBlock = block - 4*CZT_YBlock
                CZT_XBlock_mid = CZT_block_spacing*CZT_XBlock - CZT_edge_midpoint
                CZT_YBlock_mid = CZT_edge_midpoint-CZT_block_spacing*CZT_YBlock
           
                zpos = czt_pos_arr[block, bar, 2]

                CZT_Xrel = czt_pos_arr[block, bar, 0]
                xpos = CZT_Xrel + CZT_XBlock_mid - CZT_bar_edge + CZT_bar_spacing/2 + CZT_bar_spacing*(CZT_XBar)
               
                CZT_Yrel = czt_pos_arr[block, bar, 1]
                ypos = CZT_Yrel + CZT_YBlock_mid + CZT_bar_edge - CZT_bar_spacing/2 - CZT_bar_spacing*(CZT_YBar)
                
                hit_czt_ene.append(float(ene))
                hit_czt_xpos.append(float(xpos))
                hit_czt_ypos.append(float(ypos))
                hit_czt_zpos.append(float(zpos))
    return np.array(hit_czt_ene),np.array(hit_czt_xpos), np.array(hit_czt_ypos), np.array(hit_czt_zpos)
###############################################################################

args = parseargs()
filename = args.infile

outf1 = open(args.evta, 'w')

data=h5py.File('Cosima.h5','r')
# Events Hits

Events =data.get('Events')
# TKR CsI CZT

TKR = Events.get('TKR')
TKR_Energy = TKR.get('Energy')

CsI = Events.get('CsI')
CsI_Energy = CsI.get('Energy')
CsI_PosDownLog = CsI.get('PosDownLog')

n_evts=CsI_Energy.shape[0]

CZT = Events.get('CZT')
CZT_Energy = CZT.get('Energy')
CZT_Position = CZT.get('Position')

Time = np.array(Events.get('Summary').get('UTC'))/1e9


#n_evts = np.arange(UTC) # UTC has the same dimensionality as the number of events
hit_enes = [] # Define a list of hit energies, each input is an array
hit_xpos = []
hit_ypos = []
hit_zpos = []

for i_evt in np.arange(n_evts): # for each event in the range of the total number of events
    this_eid = i_evt # defines the event ID for the event
#    this_utc = evt_utc[i_evt] # defines the UTC time for the event
    
    # Use the functions to pull the data
    hit_tkr_ene, hit_tkr_x, hit_tkr_y, hit_tkr_z = get_tracker_hits(TKR_Energy[i_evt,...] )
    hit_csi_ene, hit_csi_x, hit_csi_y, hit_csi_z = get_csi_hits(CsI_Energy[i_evt,...],CsI_PosDownLog[i_evt,...] )
    hit_czt_ene, hit_czt_x, hit_czt_y, hit_czt_z = get_czt_hits(CZT_Energy[i_evt,...],CZT_Position[i_evt,...])

    # Define the number of hits in each subsystem for the event
    n_hit_tkr = hit_tkr_ene.size
    n_hit_csi = hit_csi_ene.size
    n_hit_czt = hit_czt_ene.size

    this_n_hits = n_hit_tkr + n_hit_csi + n_hit_czt # total number of hits for the event

    this_hit_ene = np.zeros(this_n_hits) # define an array to store all hit energies
    this_hit_xpos = np.zeros(this_n_hits) # define an array to store all x positions
    this_hit_ypos = np.zeros(this_n_hits) # define an array to store all y positions
    this_hit_zpos = np.zeros(this_n_hits) # define an array to store all z positions

    # Populate the arrays with each subsytem data

    idx_0 = 0 # Define an arbitrary index
    idx_f = n_hit_tkr # Limit the index to the number of events in the tkr
    this_hit_ene[idx_0:idx_f] = hit_tkr_ene
    this_hit_xpos[idx_0:idx_f] = hit_tkr_x
    this_hit_ypos[idx_0:idx_f] = hit_tkr_y
    this_hit_zpos[idx_0:idx_f] = hit_tkr_z

    idx_0 = idx_f
    idx_f = idx_0 + n_hit_csi
    this_hit_ene[idx_0:idx_f] = hit_csi_ene
    this_hit_xpos[idx_0:idx_f] = hit_csi_x
    this_hit_ypos[idx_0:idx_f] = hit_csi_y
    this_hit_zpos[idx_0:idx_f] = hit_csi_z

    idx_0 = idx_f
    idx_f = idx_0 + n_hit_czt
    this_hit_ene[idx_0:idx_f] = hit_czt_ene
    this_hit_xpos[idx_0:idx_f] = hit_czt_x
    this_hit_ypos[idx_0:idx_f] = hit_czt_y
    this_hit_zpos[idx_0:idx_f] = hit_czt_z

    # Import the arrays into the lists
    hit_enes += [ this_hit_ene ]
    hit_xpos += [ this_hit_xpos ]
    hit_ypos += [ this_hit_ypos ]
    hit_zpos += [ this_hit_zpos ]

# Define an index for the total number of hits
n_hits_tot = 0

# Iterate over each array in the list of hit energies
for hit_arr in hit_enes:
    n_hits_tot += hit_arr.size
# Define an array of all hit energies
all_hit_enes = np.zeros(n_hits_tot)

idx_0 = 0
for this_evt_hit_enes in hit_enes:
    idx_f = idx_0 + this_evt_hit_enes.size
    all_hit_enes[idx_0:idx_f] = this_evt_hit_enes
    idx_0 = idx_f

# Define an array of all x positions
all_hit_xpos = np.zeros(n_hits_tot)

idx_0 = 0
for this_evt_hit_xpos in hit_xpos:
    idx_f = idx_0 + this_evt_hit_xpos.size
    all_hit_xpos[idx_0:idx_f] = this_evt_hit_xpos
    idx_0 = idx_f
# Define an array of all y positions
all_hit_ypos = np.zeros(n_hits_tot)

idx_0 = 0
for this_evt_hit_ypos in hit_ypos:
    idx_f = idx_0 + this_evt_hit_ypos.size
    all_hit_ypos[idx_0:idx_f] = this_evt_hit_ypos
    idx_0 = idx_f
# Define an array of all z positions
all_hit_zpos = np.zeros(n_hits_tot)

idx_0 = 0
for this_evt_hit_zpos in hit_zpos:
    idx_f = idx_0 + this_evt_hit_zpos.size
    all_hit_zpos[idx_0:idx_f] = this_evt_hit_zpos
    idx_0 = idx_f

idx_0 = 0
# Define an array of all hit IDs
all_hit_eids = np.zeros(n_hits_tot)
for i_evt, these_enes in enumerate(hit_enes): # enumerate also returns a dummy index
    idx_f = idx_0 + these_enes.size
    all_hit_eids[idx_0:idx_f] = i_evt
    idx_0 = idx_f

Hits = data.get('Hits')
det = 0

HitID = all_hit_eids
Position = [all_hit_xpos, all_hit_ypos, all_hit_zpos]
Energy = all_hit_enes

outf1.write('Version 21' + '\n' + 'Type EVTA' + '\n' + '\n')

for i in range(0, n_hits_tot):
    if -8.55 <= all_hit_zpos[i] <= 8.55:
        det=1
    if -16.061<= all_hit_zpos[i] <= -14.061:
        det=7
    if -28.291 <= all_hit_zpos[i] <= -19.791:
        det=2
    if int(i) == 0:
        outf1.write('SE' + '\n')
        outf1.write('ID ' + str(int(HitID[i]+1)) + '\n')
        outf1.write('TI ' + str(Time[int(HitID[i])]) + '\n')
        outf1.write('HT ' + str(det) + '; ' + str(all_hit_xpos[i]) + '; ' + str(all_hit_ypos[i]) + '; ' + str(all_hit_zpos[i]) + '; ' + str(Energy[i])+ '\n')
    else:
        if int(HitID[i])!=int(HitID[i-1]):
            outf1.write('SE' + '\n')
            outf1.write('ID ' + str(int(HitID[i]+1)) + '\n')
            outf1.write('TI ' + str(Time[int(HitID[i])]) + '\n')
            outf1.write('HT ' + str(det) + '; ' + str(all_hit_xpos[i]) + '; ' + str(all_hit_ypos[i]) + '; ' + str(all_hit_zpos[i]) + '; ' + str(Energy[i])+ '\n')
        else:
            outf1.write('HT ' + str(det) + '; ' + str(all_hit_xpos[i]) + '; ' + str(all_hit_ypos[i]) + '; ' + str(all_hit_zpos[i]) + '; ' + str(Energy[i])+ '\n')
outf1.write('\n' + 'EN')
outf1.close()
