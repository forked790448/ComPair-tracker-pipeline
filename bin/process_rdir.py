#!/usr/bin/env python3

from silayer import raw2hdf

import argparse

import os


def main(rdir, odir):

    """
    Convert data to hdf5. 
    """

    basename = os.path.basename(os.path.normpath(rdir))

    raw2hdf.DataRun.open(rdir).write_hdf5(f"{odir}/{basename.split('.')[0]}.h5") 


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Run L1 conversion on raw data. ")
    parser.add_argument("--rdir", "-r", type=str, default=None)
    parser.add_argument("--odir", "-o", type=str, default="./")
    
    args = parser.parse_args()

    #This could be made prettier but I'm on the clock.
    if args.rdir is None:
        raise OSError(f"Run must be specified!")
    elif not os.path.isdir(args.rdir):
        raise OSError(f"Run {args.rdir} not found!")
    else:

        main(args.rdir, args.odir)